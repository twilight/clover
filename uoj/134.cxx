#include <cstdio>
#include <unordered_set>
#include <utility>

typedef std::pair<int, int> Edge;

const int N = 5000 + 10;

int n, m, x[N], y[N], z[N], ans[N];
std::unordered_multiset<int> adj[N];

bool flag[N];

bool dfs(int a, const int t) {
  if (a == t) return true;
  if (flag[a]) return false;
  flag[a] = true;
  for (auto b : adj[a]) if (dfs(b, t)) return true;
  return false;
}

int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= m; ++i) {
    scanf("%d%d%d", x + i, y + i, z + i);
    adj[x[i]].insert(y[i]);
    if (!z[i]) adj[y[i]].insert(x[i]);
  }
  for (int i = 1; i <= m; ++i) {
    if (z[i]) continue;
    adj[x[i]].erase(adj[x[i]].find(y[i]));
    adj[y[i]].erase(adj[y[i]].find(x[i]));
    std::fill(flag + 1, flag + n + 1, false);
    if (dfs(x[i], y[i])) {
      ans[i] = 1;
      adj[y[i]].insert(x[i]);
    } else {
      adj[x[i]].insert(y[i]);
    }
  }
  for (int i = 1; i <= m; ++i) printf("%d\n", ans[i]);
  return 0;
}
