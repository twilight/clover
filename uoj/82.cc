#include <bits/stdc++.h>

typedef unsigned long long u64;

int n;
u64 m;

int main() {
  scanf("%d%llu", &n, &m);
  u64 temp = 1;
  for (int i = 1; i <= n; ++i) temp *= i;
  if (m > temp * n) {
    puts(">w<");
  } else {
    for (int i = 1; m; ++i) {
      temp /= i;
      if (m >= temp) printf("%llu\n", m / temp * temp);
      m %= temp;
    }
  }
  return 0;
}
