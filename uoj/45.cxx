#include <cstdio>
#include <climits>
#include <random>
#include <vector>
#include <utility>
#include <algorithm>

typedef std::pair<unsigned, unsigned> Hash;

const int N = 16900 + 10, L = 130 + 10, SZ = 10;

int n, m, l;
unsigned mem[2 * N * L], *gene[2 * N];

int Intersection(int lhs, int rhs) {
  int res = 0;
  for (int i = 0, j = 0; i < l && j < l;) {
    if (gene[lhs][i] == gene[rhs][j]) {
      ++res;
      ++i, ++j;
    } else if (gene[lhs][i] < gene[rhs][j]) {
      ++i;
    } else {
      ++j;
    }
  }
  return res;
}

inline void Check(unsigned &lhs, unsigned rhs) { if (rhs < lhs) lhs = rhs; }

int main() {
  scanf("%d%d%d %s", &n, &m, &l, (char*)mem);
  for (int i = 0; i < 2 * n; ++i) {
    gene[i] = mem + i * l;
    std::sort(gene[i], gene[i] + l);
  }
  std::mt19937 gen;
  static int ans[2 * N];
  static std::vector<int> pool;
  static Hash hash[2 * N];
  for (int i = 0; i < 2 * n; ++i) pool.push_back(i);
  while (!pool.empty()) {
    Hash a(gen(), gen()), b(gen(), gen());
    for (int i = 0; i < pool.size(); ++i) {
      hash[pool[i]].first = hash[pool[i]].second = UINT_MAX;
      for (int j = 0; j < l; ++j) {
        Check(hash[pool[i]].first, a.first * gene[pool[i]][j] + b.first);
        Check(hash[pool[i]].second, a.second * gene[pool[i]][j] + b.second);
      }
    }
    std::sort(pool.begin(), pool.end(), [&] (int u, int v) { return hash[u] < hash[v]; });
    static std::vector<int> temp;
    temp.clear();
    for (int it = 0; it < pool.size(); ++it) {
      if (it + 1 < pool.size()) {
        int i = pool[it], j = pool[it + 1];
        if (hash[i] == hash[j] && Intersection(i, j) == l / 2) {
          ans[i] = j;
          ans[j] = i;
          ++it;
          continue;
        }
      }
      temp.push_back(pool[it]);
    }
    pool = temp;
  }
  for (int i = 0; i < 2 * n; ++i) printf("%d\n", ans[i] + 1);
  return 0;
}
