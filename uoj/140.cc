#include <cstdio>

typedef long long i64;

const int N = 20, M = 1000, K = 400 + 10, G = 200;

i64 r;
int k;

int main() {
  scanf("%lld%d", &r, &k);
  static int bit[N];
  int n = 0;
  for (i64 t = r; t; t /= 10) bit[n++] = t % 10;
  static i64 f[N][2][M][K];
  f[0][0][0][G] = 1;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < 2; ++j) {
      for (int p = 0; p < M; ++p) {
        for (int q = 0; q < K; ++q) {
          i64 t = f[i][j][p][q];
          if (!t) continue;
          for (int c = 0; c < 10; ++c) {
            int x = (c > bit[i] || (c == bit[i] && j));
            int temp = c * k + p;
            int y = temp / 10;
            int z = q + temp % 10 - c;
            f[i + 1][x][y][z] += t;
          }
        }
      }
    }
  }
  i64 ans = 0;
  for (int i = 0; i < M; ++i) {
    int j = 0;
    for (int p = i; p; p /= 10) j += p % 10;
    ans += f[n][0][i][G - j];
  }
  printf("%lld\n", ans - 1);
  return 0;
}
