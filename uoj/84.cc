#include <bits/stdc++.h>

typedef long long i64;

const int N = 5000000 + 10;
const i64 INF = 1LL << 60;

int n;
int fa[N], dep[N], size[N], up[N], succ[N], mx[N];
i64 sum[N];

i64 f[N];

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  static char s[N * 2];
  scanf("%d%s", &n, s);
  int v = 0, tot = 0;
  for (char *it = s; *it != '\0'; ++it) {
    if (*it == '(') {
      fa[++tot] = v;
      v = tot;
    } else {
      v = fa[v];
    }
  }
  static int deg[N];
  for (int i = 2; i <= n; ++i) dep[i] = dep[fa[i]] + 1, ++deg[fa[i]];
  for (int a = n; a > 1; --a) {
    if (!deg[a]) {
      size[a] = 1;
      mx[a] = sum[a] = dep[a];
    }
    int p = fa[a], t;
    for (int &x = succ[p]; x && dep[x] <= mx[a]; x = succ[x]) up[x] = fa[a];
    for (t = a; t && dep[t] <= mx[p]; t = succ[t]) up[t] = p;
    if (t) succ[p] = t;
    sum[p] += sum[a];
    size[p] += size[a];
    mx[p] = std::max(mx[p], mx[a]);
  }
  i64 ans = INF;
  for (int a = 2; a <= n; ++a) {
    f[a] = INF;
    if (up[a]) {
      int p = up[a];
      f[a] = f[p] + sum[p] - sum[a] - (size[p] - size[a]) * (i64)dep[p];
    }
    if (deg[fa[a]] == 1) f[a] = std::min(f[a], f[fa[a]] + 1);
    if (!deg[a]) ans = std::min(ans, f[a]);
  }
  printf("%lld\n", n == 1 ? 0 : ans);
  return 0;
}
