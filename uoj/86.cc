#include <cstdio>
#include <cstring>
#include <numeric>
#include <algorithm>

typedef long long i64;

const int N = 1 << 17;
const int MOD = 7 * 17 * (1 << 23) + 1;

typedef int BigInteger[N];

int p, len, proot;
BigInteger n, l, r;

int Pow(int base, int exp, int m = MOD) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) res = (i64)res * base % m;
    base = (i64)base * base % m;
  }
  return res;
}

int w[N + 1];

void NTT(BigInteger a, int sw) {
  static BigInteger temp;
  for (int i = 0, lim = 31 - __builtin_clz(len); i < len; ++i) {
    int k = 0;
    for (int j = 0; j < lim; ++j) (k <<= 1) |= (i >> j & 1);
    temp[k] = a[i];
  }
  for (int m = 2; m <= len; m *= 2) {
    int gap = m / 2, layer = len / m;
    for (int i = 0; i < gap; ++i) {
      int t = (sw > 0 ? w[i * layer] : w[len - i * layer]);
      for (int j = i; j < len; j += m) {
        int u = temp[j], v = temp[j + gap];
        temp[j] = (u + (i64)t * v % MOD) % MOD;
        temp[j + gap] = (u - (i64)t * v % MOD + MOD) % MOD;
      }
    }
  }
  std::copy(temp, temp + len, a);
  if (sw == -1) {
    int t = Pow(len, MOD - 2);
    for (int i = 0; i < len; ++i) a[i] = (i64)a[i] * t % MOD;
  }
}

int Read(BigInteger a) {
  static char s[N];
  static BigInteger temp;
  scanf(" %s", s);
  int t = strlen(s);
  std::copy(s, s + t, temp);
  for (int i = 0; i < t; ++i) temp[i] -= '0';
  int res = 0;
  for (int i = 0; i < t; ++i) res = ((i64)res * 10 + temp[i]) % MOD;
  std::reverse(temp, temp + t);
  for (int i = 0;; ++i) {
    int rem = 0;
    for (int j = t - 1; j >= 0; --j) {
      temp[j] += rem * 10;
      rem = temp[j] % p;
      temp[j] /= p;
    }
    a[i] = rem;
    if (!std::accumulate(temp, temp + t, 0)) break;
  }
  return res;
}

inline void Dec(BigInteger a) {
  --a[0];
  for (int i = 0; i + 1 < N && a[i] < 0; ++i) {
    a[i] += p;
    --a[i + 1];
  }
}

int log[N];

int PrimitiveRoot(int p) {
  for (int i = 1; i < p; ++i) {
    int t = i;
    for (int j = p - 2; j--;) {
      if (t == 1) {
        t = 0;
        break;
      }
      t = (i64)t * i % p;
    }
    if (!t) continue;
    for (int j = 0, t = 1; j < p - 1; ++j) {
      log[t] = j;
      t = (i64)t * i % p;
    }
    return i;
  }
}

BigInteger ans;

void Solve(BigInteger x, int sw) {
  int t = N - 1;
  while (~t && !x[t] && !n[t]) --t;
  static BigInteger f, g;
  std::fill(f, f + p, 0);
  std::fill(g, g + p, 0);
  f[0] = g[0] = 1;
  for (int i = 0; i <= t; ++i) {
    static BigInteger a, b;
    std::fill(a, a + len, 0);
    std::fill(b, b + len, 0);
    int w = -1;
    for (int j = n[i], c = 1; j < p; ++j) {
      ++b[log[c]];
      if (j < x[i]) ++a[log[c]];
      if (j == x[i]) w = log[c];
      c = (i64)c * (j + 1) % p * Pow(j + 1 - n[i], p - 2, p) % p;
    }
    if (~w) {
      static BigInteger pool;
      std::fill(pool, pool + p, 0);
      for (int j = 0; j < p; ++j)
        (pool[(j + w) % (p - 1)] += f[j]) %= MOD;
      std::copy(pool, pool + p, f);
    } else {
      std::fill(f, f + p, 0);
    }
    NTT(a, 1);
    NTT(b, 1);
    NTT(g, 1);
    for (int i = 0; i < len; ++i) a[i] = (i64)a[i] * g[i] % MOD;
    for (int i = 0; i < len; ++i) b[i] = (i64)b[i] * g[i] % MOD;
    NTT(a, -1);
    NTT(b, -1);
    for (int i = 0; i < len; ++i) (f[i % (p - 1)] += a[i]) %= MOD;
    std::fill(g, g + len, 0);
    for (int i = 0; i < len; ++i) (g[i % (p - 1)] += b[i]) %= MOD;
  }
  for (int i = 0, j = 1; i < p - 1; ++i, j = (i64)j * proot % p)
    (ans[j] += sw * f[i]) %= MOD;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d", &p);
  Read(n);
  int x = Read(l), y = Read(r);
  proot = PrimitiveRoot(p);
  for (len = 1; len <= p;) len *= 2;
  len *= 2;
  w[0] = 1, w[1] = Pow(3, (MOD - 1) / len);
  for (int i = 2; i <= len; ++i) w[i] = (i64)w[i - 1] * w[1] % MOD;
  Solve(r, 1);
  if (std::accumulate(l, l + len, 0)) {
    Dec(l);
    Solve(l, -1);
  }
  ans[0] = (y - x + 1) % MOD;
  for (int i = 1; i < p; ++i) (ans[0] -= ans[i]) %= MOD;
  for (int i = 0; i < p; ++i) printf("%d\n", (ans[i] + MOD) % MOD);
  return 0;
}
