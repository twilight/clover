#include <cstdio>
#include <queue>
#include <stack>
#include <vector>
#include <algorithm>

typedef long long i64;

const int N = 300000 + 10, V = N * 2;

int n;

namespace Tree {

const int LOG = 21;

int cnt;

int fa[LOG][V], dep[V], left[V], right[V];
i64 dist[V], sum[V], dir[V];

struct edge_t {
  int adj;
  i64 w;
  edge_t *next;
  edge_t(int _a, i64 _w, edge_t *_n): adj(_a), w(_w), next(_n) {}
} *e[V];

inline void Link(int a, int b, i64 c) {
  e[a] = new edge_t(b, c, e[a]);
}

void DFS(int a) {
  static int tot;
  left[a] = ++tot;
  for (edge_t *it = e[a]; it; it = it->next) {
    int b = it->adj;
    fa[0][b] = a;
    dep[b] = dep[a] + 1;
    dist[b] = dist[a] + it->w;
    DFS(b);
  }
  right[a] = ++tot;
}

inline void BuildTable() {
  for (int i = 1; i < LOG; ++i)
    for (int j = 1; j <= cnt; ++j)
      fa[i][j] = fa[i - 1][fa[i - 1][j]];
}

int Jump(int a, int h) {
  for (int i = LOG - 1; i >= 0; --i)
    if (h >> i & 1) a = fa[i][a];
  return a;
}

int LCA(int a, int b) {
  if (dep[a] > dep[b]) std::swap(a, b);
  b = Jump(b, dep[b] - dep[a]);
  if (a == b) return a;
  for (int i = LOG - 1; i >= 0; --i)
    if (fa[i][a] != fa[i][b])
      a = fa[i][a], b = fa[i][b];
  return fa[0][a];
}

}

namespace Cactus {

const int E = N * 10;

int adj[N];
int to[E], next[E], w[E];

void Link(int a, int b, int c) {
  static int cnt = 2;
  to[cnt] = b;
  next[cnt] = adj[a];
  w[cnt] = c;
  adj[a] = cnt++;
}

using Tree::sum;
using Tree::dir;

void Tarjan(int a, int pre = -1) {
  static std::vector<int> stk;
  static int low[N], dfn[N], tot;
  static i64 dist[N];
  dfn[a] = low[a] = ++tot;
  stk.push_back(a);
  for (int it = adj[a]; it; it = next[it]) {
    int b = to[it];
    if ((it ^ 1) == pre) continue;
    if (!dfn[b]) {
      dist[b] = dist[a] + w[it];
      Tarjan(b, it);
      if (dfn[b] == low[b]) Tree::Link(a, b, w[it]);
      low[a] = std::min(low[a], low[b]);
    } else if (dfn[b] < dfn[a]) {
      size_t j = stk.size() - 1;
      while (stk[j] != b) --j;
      sum[++Tree::cnt] = dist[a] - dist[b] + w[it];
      Tree::Link(b, Tree::cnt, 0);
      for (size_t i = j + 1; i < stk.size(); ++i) {
        dir[stk[i]] = dist[stk[i]] - dist[b];
        Tree::Link(Tree::cnt, stk[i], std::min(dir[stk[i]], sum[Tree::cnt] - dir[stk[i]]));
      }
      low[a] = std::min(low[a], dfn[b]);
    }
  }
  stk.pop_back();
}

}

namespace AuxiliaryCactus {

std::vector<int> son[V];

using Tree::left;
using Tree::right;
using Tree::LCA;

inline bool cmp(int a, int b) { return left[a] < left[b]; }

i64 mx[V];

void Build(std::vector<int> &node) {
  std::sort(node.begin(), node.end(), cmp);
  for (size_t i = 1, lim = node.size(); i < lim; ++i) node.push_back(LCA(node[i - 1], node[i]));
  std::sort(node.begin(), node.end());
  node.erase(std::unique(node.begin(), node.end()), node.end());
  std::sort(node.begin(), node.end(), cmp);
  for (std::vector<int>::iterator it = node.begin(); it != node.end(); ++it) son[*it].clear();
  static std::vector<int> stk;
  stk.clear();
  for (std::vector<int>::iterator it = node.begin(); it != node.end(); ++it) {
    while (!stk.empty() && right[stk.back()] < left[*it]) stk.pop_back();
    if (!stk.empty()) son[stk.back()].push_back(*it);
    stk.push_back(*it);
  }
}

inline void check(i64 &mx, i64 &sub, i64 val) {
  if (val > mx) {
    sub = mx;
    mx = val;
  } else if (val > sub) {
    sub = val;
  }
}

using Tree::dep;
using Tree::dist;
using Tree::dir;
using Tree::sum;
using Tree::Jump;

i64 Solve(int a) {
  i64 res = 0, sub = mx[a] = 0;
  for (std::vector<int>::iterator it = son[a].begin(); it != son[a].end(); ++it) {
    res = std::max(res, Solve(*it));
    check(mx[a], sub, mx[*it] + dist[*it] - dist[a]);
  }
  if (a <= n) {
    res = std::max(res, mx[a] + sub);
  } else {
    typedef std::pair<i64, i64> info_t;
    static std::vector<info_t> pool;
    pool.clear();
    for (std::vector<int>::iterator it = son[a].begin(); it != son[a].end(); ++it) {
      int b = Jump(*it, dep[*it] - dep[a] - 1);
      pool.push_back(info_t(dir[b], mx[*it] + dist[*it] - dist[b]));
      pool.push_back(info_t(sum[a] + dir[b], mx[*it] + dist[*it] - dist[b]));
    }
    std::sort(pool.begin(), pool.end());
    static std::deque<int> q;
    q.clear();
    q.push_back(0);
    for (size_t i = 1; i < pool.size(); ++i) {
      while (!q.empty() && pool[i].first - pool[q[0]].first > sum[a] / 2) q.pop_front();
      if (!q.empty()) res = std::max(res, pool[i].first - pool[q[0]].first + pool[i].second + pool[q[0]].second);
      while (!q.empty() && pool[q.back()].second - pool[q.back()].first <= pool[i].second - pool[i].first) q.pop_back();
      q.push_back(i);
    }
  }
  return res;
}

}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int m;
  for (scanf("%d%d", &n, &m); m--;) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    Cactus::Link(u, v, w);
    Cactus::Link(v, u, w);
  }
  Tree::cnt = n;
  Cactus::Tarjan(1);
  Tree::DFS(1);
  Tree::BuildTable();
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    int cnt;
    static std::vector<int> node;
    node.clear();
    for (scanf("%d", &cnt); cnt--;) {
      int a;
      scanf("%d", &a);
      node.push_back(a);
    }
    AuxiliaryCactus::Build(node);
    printf("%lld\n", AuxiliaryCactus::Solve(node[0]));
  }
  return 0;
}
