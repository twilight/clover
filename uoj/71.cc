#include <cstdio>
#include <cstring>
#include <climits>
#include <queue>
#include <bitset>
#include <vector>
#include <algorithm>

const int N = 150000 + 10, M = 2 * N, INF = 0x23333333;

int n, m, s, t, k;
int x[M], y[M], z[M];

namespace kpath {

const int V = 150000 + 10;

typedef long long int64;

struct node_t {
  node_t *lch, *rch, *succ;
  int val;
  node_t(): lch(NULL), rch(NULL), succ(NULL), val(0) {}
  node_t(node_t *s, int v): lch(NULL), rch(NULL), succ(s), val(v) {}
};

node_t *merge(node_t *u, node_t *v) {
  if (!u) return v;
  if (!v) return u;
  if (u->val > v->val) std::swap(u, v);
  node_t *res = new node_t;
  *res = *u;
  res->rch = merge(res->rch, v);
  std::swap(res->lch, res->rch);
  return res;
}

struct info_t {
  int64 det;
  node_t *heap;
  info_t() {}
  info_t(int64 d, node_t *h): det(d), heap(h) {}
  bool operator< (const info_t &rhs) const { return det > rhs.det; }
};

void solve() {
  static int dis[2][V];
  for (int i = 1; i <= m; ++i)
    if (x[i] == s) dis[0][y[i]] = z[i]; else dis[1][x[i]] = z[i];
  for (int i = 1; i <= n; ++i)
    if (dis[0][i] > dis[1][i]) std::swap(dis[0][i], dis[1][i]);
  int64 ans = 0;
  node_t *last = NULL;
  for (int i = 1; i <= n; ++i) {
    if (i != s && i != t) {
      ans += dis[0][i];
      node_t *cur = new node_t;
      cur->succ = last;
      cur = merge(last,
                  merge(new node_t(last, dis[1][i] - dis[0][i]),
                        new node_t(last, dis[1][i])));
      last = cur;
    }
  }
  std::priority_queue<info_t> q;
  k--, printf("%lld\n", ans);
  q.push(info_t(last->val, last));
  for (; k-- && !q.empty();) {
    info_t cur = q.top();
    q.pop();
    printf("%lld\n", ans + cur.det);
    if (cur.heap) {
      if (cur.heap->succ)
        q.push(info_t(cur.det + cur.heap->succ->val, cur.heap->succ));
      if (cur.heap->lch)
        q.push(info_t(cur.det - cur.heap->val + cur.heap->lch->val, cur.heap->lch));
      if (cur.heap->rch)
        q.push(info_t(cur.det - cur.heap->val + cur.heap->rch->val, cur.heap->rch));
    }
  }
  if (~k) puts("-1");
}

}

namespace kcut {

const int N = 50 + 1, M = 1500 + 1;
const int V = 50 + 1, E = M * 2 + 10;

int start, terminal;

int adj[V];
int to[E], next[E], cap[E];

inline void link(int a, int b, int c) {
  static int cnt = 2;
  to[cnt] = b, next[cnt] = adj[a], cap[cnt] = c, adj[a] = cnt++;
  to[cnt] = a, next[cnt] = adj[b], cap[cnt] = 0, adj[b] = cnt++;
}

struct state_t {
  int cut, edge;
  std::bitset<M> must, dont;
  inline bool operator< (const state_t &rhs) const { return cut > rhs.cut; }
};

int h[V], gap[V];

inline int DFS(int a, int df) {
  if (a == terminal) return df;
  int tot = 0;
  for (int it = adj[a]; it; it = next[it]) {
    int b = to[it];
    if (cap[it] && h[a] == h[b] + 1) {
      int f = DFS(b, std::min(df - tot, cap[it]));
      if (f) {
        cap[it] -= f;
        cap[it ^ 1] += f;
        tot += f;
      }
      if (tot == df) return tot;
    }
  }
  if (--gap[h[a]] == 0) h[start] = n;
  ++gap[++h[a]];
  return tot;
}

int flow(int s, int t) {
  if (s == t) return INF;
  int res = 0;
  memset(h, 0, sizeof h);
  memset(gap, 0, sizeof gap);
  start = s, terminal = t;
  while (h[s] < n && res < INF) res += DFS(s, INT_MAX);
  return std::min(res, INF);
}

bool flag[V];

void detect(int s) {
  static std::queue<int> q;
  memset(flag, false, sizeof flag);
  flag[s] = true;
  for (q.push(s); !q.empty(); q.pop()) {
    int a = q.front();
    for (int it = adj[a]; it; it = next[it]) {
      int b = to[it];
      if ((it & 1) == 0 && cap[it] && !flag[b]) q.push(b), flag[b] = true;
    }
  }
}

int label[V];

void suboptimize(state_t &state) {
  int tmp = 0;
  for (int i = 1; i <= m; ++i) {
    cap[i * 2 + 1] = 0;
    if (state.must[i]) {
      tmp += z[i];
      cap[i * 2] = 0;
    } else if (state.dont[i]) {
      cap[i * 2] = INF;
    } else {
      cap[i * 2] = z[i];
    }
  }
  state.cut = INF;
  state.edge = -1;
  if ((tmp += flow(s, t)) >= INF) return;
  detect(s);
  static int _cap[E];
  memcpy(_cap, cap, sizeof cap);
  memset(label, -1, sizeof label);
  for (int i = 1; i <= m; ++i) {
    if (!state.must[i] && !state.dont[i]) {
      int f = tmp;
      if (flag[x[i]] && !flag[y[i]]) {
        if (!~label[x[i]]) {
          memcpy(cap, _cap, sizeof cap);
          label[x[i]] = flow(s, x[i]);
        }
        if (!~label[y[i]]) {
          memcpy(cap, _cap, sizeof cap);
          label[y[i]] = flow(y[i], t);
        }
        f += std::min(label[x[i]], label[y[i]]);
      } else {
        f += z[i];
      }
      if (f < state.cut) {
        state.cut = f;
        state.edge = i;
      }
    }
  }
}

void solve() {
  for (int i = 1; i <= m; ++i) link(x[i], y[i], z[i]);
  state_t init;
  init.must.reset();
  init.dont.reset();
  int ans = flow(s, t);
  k--, printf("%d\n", ans);
  suboptimize(init);
  static std::priority_queue<state_t> q;
  if (~init.edge) q.push(init);
  while (k-- && !q.empty()) {
    state_t cur = q.top(), next;
    q.pop();
    printf("%d\n", cur.cut);
    int tmp = cur.edge;
    cur.must[tmp] = 1;
    suboptimize(cur);
    if (~cur.edge) q.push(cur);
    cur.must[tmp] = 0, cur.dont[tmp] = 1;
    suboptimize(cur);
    if (~cur.edge) q.push(cur);
  }
  if (~k) puts("-1");
}

}

namespace brute {

const int N = 20 + 5;

std::vector<int> adj[N];

int BFS(int s, int t) {
  static std::queue<int> q;
  static bool flag[N];
  std::fill(flag + 1, flag + n + 1, false);
  for (q.push(s), flag[s] = true; !q.empty(); q.pop()) {
    int a = q.front();
    for (std::vector<int>::iterator it = adj[a].begin(); it != adj[a].end(); ++it) {
      int b = *it;
      if (!flag[b]) q.push(b), flag[b] = true;
    }
  }
  return flag[t];
}

void solve() {
  static std::vector<int> ans;
  for (int i = 0; i < (1 << m); ++i) {
    for (int j = 1; j <= n; ++j) adj[j].clear();
    for (int j = 1; j <= m; ++j)
      if (i & (1 << (j - 1))) adj[x[j]].push_back(y[j]);
    if (!BFS(s, t)) {
      int sum = 0;
      for (int j = 1; j <= m; ++j) if (!(i & (1 << (j - 1)))) sum += z[j];
      ans.push_back(sum);
    }
  }
  std::sort(ans.begin(), ans.end());
  for (int i = 0, lim = std::min<int>(ans.size(), k); i < lim; ++i) printf("%d\n", ans[i]);
  if (ans.size() < k) puts("-1");
}

}

bool judge() {
  static int cnt[N];
  for (int i = 1; i <= m; ++i) {
    if (!((x[i] == s) ^ (y[i] == t))) return false;
    if (x[i] == s) {
      if (cnt[y[i]] & 2) return false;
      cnt[y[i]] |= 2;
    } else {
      if (cnt[x[i]] & 1) return false;
      cnt[x[i]] |= 1;
    }
  }
  for (int i = 1; i <= n; ++i) if (i != s && i != t && cnt[i] < 3) return false;
  return true;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  scanf("%d%d%d%d%d", &n, &m, &s, &t, &k);
  for (int i = 1; i <= m; ++i) scanf("%d%d%d", x + i, y + i, z + i);
  if (judge()) {
    kpath::solve();
  } else if (n <= 50 && m <= 1500 && k <= 100) {
    kcut::solve();
  } else {
    brute::solve();
  }
  return 0;
}

