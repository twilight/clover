#include <cstdio>
#include <vector>

const int N = 100000 + 10;

int n, deg[N], dep[N], fa[N];
std::vector<int> adj[N];

int main() {
  scanf("%d", &n);
  for (int i = n - 1; i--;) {
    int a, b;
    scanf("%d%d", &a, &b);
    ++deg[a], ++deg[b];
    adj[a].push_back(b);
    adj[b].push_back(a);
  }
  std::vector<int> q;
  q.push_back(1);
  dep[1] = 1;
  for (int i = 0; i < q.size(); ++i) {
    int a = q[i];
    for (int b : adj[a]) {
      if (b != fa[a]) {
        q.push_back(b);
        dep[b] = dep[fa[b] = a] + 1;
      }
    }
  }
  static int min[N];
  for (int i = q.size() - 1; i >= 0; --i) {
    int a = q[i];
    min[a] = (a > 1 && deg[a] == 1 ? dep[a] : n);
    for (int b : adj[a]) if (b != fa[a]) min[a] = std::min(min[a], min[b]);
  }
  static int rem[N];
  rem[1] = n;
  for (int i = 2; i <= n; ++i) if (deg[i] == 1) rem[1] = std::min(rem[1], dep[i]);
  int ans = 0;
  for (int i = 0; i < q.size(); ++i) {
    int a = q[i];
    int t = rem[a];
    if (min[a] - dep[a] + 1 <= rem[a]) {
      --t;
      ++ans;
    }
    for (int b : adj[a]) rem[b] = t;
  }
  printf("%d\n", ans);
  return 0;
}
