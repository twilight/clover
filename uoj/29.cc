#include "holiday.h"
#include <cstdio>
#include <climits>
#include <vector>
#include <algorithm>

typedef long long i64;

const int N = 100000 + 10, D = 2 * N + N / 2, SZ = 30 * N;

int n, d, start, attraction[N];

std::vector<int> sorted;

i64 f[N], g[N];

int root[N];
int lch[SZ], rch[SZ], size[SZ], tot;
i64 sum[SZ];

int modify(int id, int l, int r, int pos) {
  int res = ++tot;
  lch[res] = lch[id], rch[res] = rch[id];
  size[res] = size[id] + 1, sum[res] = sum[id] + sorted[pos];
  if (l == r) return res;
  int mid = (l + r) / 2;
  if (pos <= mid) lch[res] = modify(lch[id], l, mid, pos); else rch[res] = modify(rch[id], mid + 1, r, pos);
  return res;
}

i64 query(int id, int l, int r, int sz) {
  if (sz <= 0) return 0;
  if (sz >= size[id]) return sum[id];
  if (l == r) return std::min<i64>(sz, size[id]) * l;
  int mid = (l + r) / 2;
  return query(rch[id], mid + 1, r, sz) + query(lch[id], l, mid, sz - size[rch[id]]);
}

void go_left(int l, int r, int p, int q, int w, i64 res[]) {
  if (l > r) return;
  int mid = (l + r) / 2;
  res[mid] = q;
  i64 mx = query(root[q], 0, sorted.size(), mid - (start - q) * w);
  for (int i = q - 1; i >= p; --i) {
    i64 temp = query(root[i], 0, sorted.size(), mid - (start - i) * w);
    if (temp > mx) {
      mx = temp;
      res[mid] = i;
    }
  }
  go_left(l, mid - 1, res[mid], q, w, res);
  go_left(mid + 1, r, p, res[mid], w, res);
}

void go_right(int l, int r, int p, int q, int w, i64 res[]) {
  if (l > r) return;
  int mid = (l + r) / 2;
  res[mid] = p;
  i64 mx = query(root[p], 0, sorted.size(), mid - (p - start) * w);
  for (int i = p + 1; i <= q; ++i) {
    i64 temp = query(root[i], 0, sorted.size(), mid - (i - start) * w);
    if (temp > mx) {
      mx = temp;
      res[mid] = i;
    }
  }
  go_right(l, mid - 1, p, res[mid], w, res);
  go_right(mid + 1, r, res[mid], q, w, res);
}

i64 f_left[D], g_left[D], f_right[D], g_right[D];

i64 findMaxAttraction(int n, int start, int d, int attraction[]) {
  ::n = n;
  ::start = start;
  ::d = d;
  std::copy(attraction, attraction + n, ::attraction);
  sorted.assign(attraction, attraction + n);
  std::sort(sorted.begin(), sorted.end());
  sorted.erase(std::unique(sorted.begin(), sorted.end()), sorted.end());
  for (int i = 0; i < n; ++i) attraction[i] = std::lower_bound(sorted.begin(), sorted.end(), attraction[i]) - sorted.begin();
  for (int i = start + 1; i < n; ++i) root[i] = modify(root[i - 1], 0, sorted.size(), attraction[i]);
  for (int i = start - 1; i >= 0; --i) root[i] = modify(root[i + 1], 0, sorted.size(), attraction[i]);
  go_left(0, d, 0, start, 1, f_left);
  go_left(0, d, 0, start, 2, g_left);
  go_right(0, d, start, n - 1, 1, f_right);
  go_right(0, d, start, n - 1, 2, g_right);
  i64 res = 0;
  for (int i = 0; i <= d; ++i) {
    i64 temp = query(root[f_left[i]], 0, sorted.size(), i - (start - f_left[i]));
    temp += query(root[g_right[d - i]], 0, sorted.size(), d - i - 2 * (g_right[d - i] - start));
    res = std::max(res, temp);
    temp = query(root[f_right[i]], 0, sorted.size(), i - (f_right[i] - start));
    temp += query(root[g_left[d - i]], 0, sorted.size(), d - i - 2 * (start - g_left[d - i]));
    res = std::max(res, temp);
  }
  d -= 1;
  for (int i = 0; i <= d; ++i) {
    i64 temp = query(root[f_left[i]], 0, sorted.size(), i - (start - f_left[i]));
    temp += query(root[g_right[d - i]], 0, sorted.size(), d - i - 2 * (g_right[d - i] - start));
    res = std::max(res, temp + sorted[attraction[start]]);
    temp = query(root[f_right[i]], 0, sorted.size(), i - (f_right[i] - start));
    temp += query(root[g_left[d - i]], 0, sorted.size(), d - i - 2 * (start - g_left[d - i]));
    res = std::max(res, temp + sorted[attraction[start]]);
  }
  return res;
}
