#include "wall.h"
#include <cassert>
#include <climits>
#include <algorithm>

const int N = 2000000 * 2 + 10;

int min[N], max[N];
bool flag[N];

inline int pos(int l, int r) { return (l + r) | (l != r); }

inline void cmin(int &lhs, int rhs) { if (rhs < lhs) lhs = rhs; }
inline void cmax(int &lhs, int rhs) { if (rhs > lhs) lhs = rhs; }

inline void apply(int id, int a, int b) {
  if (a >= max[id]) {
    min[id] = max[id] = a;
  } else if (b <= min[id]) {
    min[id] = max[id] = b;
  } else {
    cmax(min[id], a);
    cmin(max[id], b);
  }
  flag[id] = true;
}

inline void release(int l, int r) {
  int id = pos(l, r), mid = (l + r) / 2;
  if (l == r || !flag[id]) return;
  int lch = pos(l, mid), rch = pos(mid + 1, r);
  apply(lch, min[id], max[id]);
  apply(rch, min[id], max[id]);
  flag[id] = false;
}

inline void update(int l, int r) {
  if (l == r) return;
  int id = pos(l, r), mid = (l + r) / 2;
  int lch = pos(l, mid), rch = pos(mid + 1, r);
  min[id] = std::min(min[lch], min[rch]);
  max[id] = std::max(max[lch], max[rch]);
}

inline void modify(int l, int r, int p, int q, int a, int b) {
  release(l, r);
  int id = pos(l, r), mid = (l + r) / 2;
  if (p <= l && r <= q) {
    apply(id, a, b);
    return;
  }
  if (p <= mid) modify(l, mid, p, q, a, b);
  if (q > mid) modify(mid + 1, r, p, q, a, b);
  update(l, r);
}

inline int query(int l, int r, int p) {
  release(l, r);
  int id = pos(l, r), mid = (l + r) / 2;
  if (l == r) return min[id];
  return (p <= mid) ? query(l, mid, p) : query(mid + 1, r, p);
}

void buildWall(int n, int k, int op[], int left[], int right[], int height[], int res[]) {
  for (int i = 0; i < k; ++i) {
    int a, b;
    if (op[i] == 1) a = height[i], b = INT_MAX; else a = 0, b = height[i]; 
    modify(1, n, left[i] + 1, right[i] + 1, a, b);
  }
  for (int i = 1; i <= n; ++i) res[i - 1] = query(1, n, i);
}
