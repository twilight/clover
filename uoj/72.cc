#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>

typedef unsigned int u32;
typedef unsigned long long u64;

const int N = 64, M = 4, L = 256, Q = 1024, ROUND = 10;

int n, m, l, q;
int u[Q], v[Q], s[Q], d[Q], e[Q];

inline u64 get(u64 x, int pos) { return x >> pos & 1; }
inline void set(u64 &x, int pos, u64 val) { if (get(x, pos) != val) x ^= 1ULL << pos; }

inline u64 randll() { 
  u64 res = (u64)rand() << 32 | rand();
  if (n < 64) res &= (1ULL << n) - 1;
  return res;
}

inline void parallel_calc(const std::vector<u64> &x, std::vector<int> &res) {
  res.resize(x.size());
  static u32 y[L];
  for (int i = 0; i < x.size(); i += 32) {
    memset(y, 0, sizeof y);
    int r = std::min<int>(x.size() - i, 32);
    for (int j = 0; j < r; ++j)
      for (int k = 0; k < n; ++k)
        y[k] |= (u32)get(x[i + j], k) << j;
    for (int j = 0; j < q; ++j) y[u[j]] = ~(y[v[j]] & y[s[j]]) ^ y[d[j]] ^ y[e[j]];
    for (int j = 0; j < r; ++j) res[i + j] = get(y[0], j);
  }
}

inline void gen_basis(std::vector<u64> &res) {
  static std::vector<u64> temp;
  temp.resize(res.size());
  while (1) {
    for (int i = 0; i < res.size(); ++i) res[i] = temp[i] = randll();
    bool flag = true;
    for (int i = 0; flag && i < res.size(); ++i) {
      for (int j = 0; j < i; ++j)
        if (temp[i] & (temp[j] & -temp[j]))
          temp[i] ^= temp[j];
      if (!temp[i]) {
        flag = false;
        break;
      }
    }
    if (flag) return;
  }
}

std::vector<u64> x;
std::vector<int> x_res;

void gen_frequencies(std::vector<u64> &pool) {
  static std::vector<u64> v;
  v.resize(std::min(n, M));
  gen_basis(v);
  static std::vector<u64> tx;
  static std::vector<int> tx_res;
  tx.clear();
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < ROUND; ++j) {
      u64 temp = randll();
      for (int z = 0; z < 2; ++z) {
        set(temp, i, z);
        for (int a = 0; a < (1 << v.size()); ++a) {
          u64 cur = temp;
          for (int k = 0; k < v.size(); ++k) if (get(a, k)) cur ^= v[k];
          tx.push_back(cur);
        }
      }
    }
  }
  parallel_calc(tx, tx_res);
  for (int a = 0; a < (1 << v.size()); ++a) {
    std::vector<int>::iterator it = tx_res.begin();
    u64 cur = 0;
    for (int i = 0; i < n; ++i) {
      int tot = 0;
      for (int j = 0; j < ROUND; ++j) {
        int cnt[2] = {0, 0};
          for (int z = 0; z < 2; ++z)
            for (int b = 0; b < (1 << v.size()); ++b)
              cnt[z] += ((*it++ == __builtin_parity(a & b)) ? 1 : -1);
        tot += (cnt[0] > 0) != (cnt[1] > 0);
      }
      if (tot > ROUND / 2) cur |= 1ULL << i;
    }
    int tot = 0;
    for (int i = 0; i < x.size(); ++i) tot += ((x_res[i] == __builtin_parityll(cur & x[i])) ? 1 : -1);
    if (std::abs(tot) >= x.size() * 0.04) {
      for (std::vector<u64>::iterator it = pool.begin(); it != pool.end(); ++it) if (cur & (*it & -*it)) cur ^= *it;
      if (cur) pool.push_back(cur);
    }
  }
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  srand(0x49640877);
  scanf("%d%d%d%d", &n, &m, &l, &q);
  for (int i = 0; i < q; ++i) scanf("%d%d%d%d%d", u + i, v + i, s + i, d + i, e + i);
  x.resize(100000);
  for (int i = 0; i < x.size(); ++i) x[i] = randll();
  parallel_calc(x, x_res);
  static std::vector<u64> pool;
  while (pool.size() < m) gen_frequencies(pool);
  for (int i = 0; i < m; ++i, putchar('\n'))
    for (int j = 0; j < n; ++j)
      putchar('0' + get(pool[i], j));
  static int h[1 << M][2];
  for (int i = 0; i < x.size(); ++i) {
    u64 cur = 0;
    for (int j = 0; j < m; ++j) cur = (cur << 1) | __builtin_parityll(x[i] & pool[j]);
    ++h[cur][x_res[i]];
  }
  for (int i = 0; i < (1 << m); ++i) putchar('0' + (h[i][0] < h[i][1]));
  putchar('\n');
  return 0;
}
