#include <cstdio>
#include <map>

typedef long long i64;

const int N = 2000 + 10;

int n, a[N];
i64 sum[N];

i64 gcd(i64 x, i64 y) { return y ? gcd(y, x % y) : x; }

i64 ans[N];

void check(i64 &lhs, i64 rhs) {
  if (rhs > lhs) lhs = rhs;
}

void check(i64 d) {
  static i64 temp[N];
  for (int i = 1; i <= n; ++i) temp[i] = sum[i] % d;
  static std::map<i64, int> cnt;
  cnt.clear();
  for (int i = 1; i <= n; ++i) ++cnt[temp[i]];
  for (std::map<i64, int>::iterator it = cnt.begin(); it != cnt.end(); ++it) check(ans[it->second], d);
}

int main() {
  scanf("%d", &n);
  for (int i = 1; i <= n; ++i) scanf("%d", a + i);
  for (int i = 1; i <= n; ++i) sum[i] = sum[i - 1] + a[i];
  i64 tot = sum[n];
  for (i64 i = 1; i * i <= tot; ++i) {
    if (tot % i == 0) {
      check(i);
      check(tot / i);
    }
  }
  for (int i = n - 1; i > 0; --i) check(ans[i], ans[i + 1]);
  for (int i = 1; i <= n; ++i) printf("%lld\n", ans[i]);
  return 0;
}
