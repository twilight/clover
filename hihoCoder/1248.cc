#include <cstdio>
#include <cstring>

typedef long long i64;

const int N = 100000 + 10, MOD = 1000000007;

int n, k;

int fact[N], inv[N];

int power(int base, int exp) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) res = (i64)res * base % MOD;
    base = (i64)base * base % MOD;
  }
  return res;
}

int binom(int n, int m) {
  if (m > n) return 0;
  return (i64)fact[n] * inv[m] % MOD * inv[n - m] % MOD;
}

int f[N];

int solve(int m) {
  memset(f, 0, (n + 1) * sizeof(int));
  f[0] = 1;
  for (int i = 1; i <= n; ++i)
    for (int j = 1; j <= m && j <= i; ++j)
      if (m % j == 0) f[i] = (f[i] + (i64)f[i - j] * binom(i - 1, j - 1) % MOD * fact[j - 1] % MOD) % MOD;
  return f[n] - 1;
}

int vierergruppe() {
  int s = solve(2);
  int res = 0;
  static int g[N], h[N];
  g[0] = h[0] = 1;
  for (int i = 1; i <= n; ++i) {
    if (i >= 2) {
      g[i] = 2LL * g[i - 2] % MOD * (i - 1) % MOD;
      h[i] = h[i - 2] * (i - 1LL) % MOD;
    }
    g[i] = (g[i] + 2LL * g[i - 1]) % MOD;
  }
  for (int i = 1; 2 * i <= n; ++i)
    res = (res + (i64)g[i] * h[2 * i] % MOD * f[n - 2 * i] % MOD * binom(n, 2 * i) % MOD) % MOD;
  res = ((res - 2LL * s) % MOD + MOD) % MOD;
  return (i64)res * power(6, MOD - 2) % MOD;
}

int main() {
  scanf("%d%d", &n, &k);
  fact[0] = 1;
  for (int i = 1; i < N; ++i) fact[i] = (i64)fact[i - 1] * i % MOD;
  inv[N - 1] = power(fact[N - 1], MOD - 2);
  for (int i = N - 2; i >= 0; --i) inv[i] = inv[i + 1] * (i + 1LL) % MOD;
  switch (k) {
    case 1:
      puts("1");
      break;
    case 2:
    case 3:
    case 5:
      printf("%lld\n", (i64)solve(k) * power(k - 1, MOD - 2) % MOD);
      break;
    case 4:
      printf("%lld\n", ((i64)(solve(4) - solve(2) + MOD) % MOD * power(2, MOD - 2) % MOD + vierergruppe()) % MOD);
      break;
  }
  return 0;
}
