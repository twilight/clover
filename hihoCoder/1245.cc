#include <cmath>
#include <cstdio>
#include <algorithm>

typedef long double f128;

int a, b, c, l;

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d%d%d", &a, &b, &c, &l);
    f128 p = (a + b + c + l) / 2.0;
    f128 x = p - a, y = p - b, z = p - c;
    if (y < x) std::swap(x, y);
    if (z < x) std::swap(x, z);
    if (z < y) std::swap(y, z);
    f128 rem = l;
    if (z > y) {
      f128 temp = z - y;
      f128 cur = std::min(rem, temp);
      z -= cur;
      rem -= cur;
    }
    if (y > x) {
      f128 temp = y - x;
      f128 cur = std::min(rem / 2.0, temp);
      y -= cur, z -= cur, rem -= 2.0 * cur;
    }
    f128 cur = rem / 3.0;
    x -= cur, y -= cur, z -= cur;
    printf("%.10Lf\n", sqrtl(p * x * y * z));
  }
  return 0;
}
