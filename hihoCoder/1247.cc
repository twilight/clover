#include <cstdio>
#include <algorithm>

#define fst first
#define snd second

typedef long long i64;
typedef std::pair<i64, i64> Info;

const int N = 100000 + 10, E = 2 * N;
const i64 INF = 1LL << 57;

int n;

int adj[N];
int to[E], next[E], cnt;

void link(int a, int b) {
  to[cnt] = b;
  next[cnt] = adj[a];
  adj[a] = cnt++;
}

int size[N];
i64 f[N], g[N], ans[N], val[N];

void check(Info &lhs, i64 rhs) {
  if (rhs > lhs.fst) {
    lhs.snd = lhs.fst;
    lhs.fst = rhs;
  } else {
    lhs.snd = std::max(lhs.snd, rhs);
  }
}

int main() {
  scanf("%d", &n);
  cnt = 2;
  for (int i = n - 1; i--;) {
    int u, v;
    scanf("%d%d", &u, &v);
    link(u, v);
    link(v, u);
  }
  static int order[N], fa[N];
  for (int it = 1, r = order[1] = 1; it <= r; ++it) {
    int a = order[it];
    for (int i = adj[a]; i; i = next[i]) {
      int b = to[i];
      if (b != fa[a]) {
        fa[b] = a;
        order[++r] = b;
      }
    }
  }
  cnt = 2;
  for (int i = 1; i <= n; ++i) adj[i] = 0;
  for (int i = 2; i <= n; ++i) link(fa[i], i);
  for (int i = 1; i <= n; ++i) size[i] = 1;
  for (int i = n; i > 0; --i) size[fa[order[i]]] += size[order[i]];
  for (int i = 1; i <= n; ++i) val[i] = (i64)size[i] * (n - size[i]);
  for (int it = n; it > 0; --it) {
    int a = order[it];
    if (!adj[a]) {
      f[a] = 0;
      continue;
    }
    i64 cur = 0, mx = 0;
    for (int i = adj[a]; i; i = next[i]) {
      int b = to[i];
      cur += f[b];
      cur += val[b];
      mx = std::max(mx, val[b]);
    }
    f[a] = cur - mx;
  }
  val[1] = 0;
  for (int it = 1; it <= n; ++it) {
    int a = order[it];
    ans[a] = g[a] + val[a];
    i64 mx = val[a];
    for (int i = adj[a]; i; i = next[i]) {
      int b = to[i];
      ans[a] += f[b] + val[b];
      mx = std::max(mx, val[b]);
    }
    ans[a] -= mx;
    i64 sum = g[a] + val[a];
    Info cur(val[a], 0);
    for (int i = adj[a]; i; i = next[i]) {
      int b = to[i];
      check(cur, val[b]);
      sum += f[b] + val[b];
    }
    for (int i = adj[a]; i; i = next[i]) {
      int b = to[i];
      g[b] = sum - f[b] - val[b];
      if (cur.fst == val[b]) g[b] -= cur.snd; else g[b] -= cur.fst;
    }
  }
  for (int i = 1; i <= n; ++i) printf("%lld\n", ans[i]);
  return 0;
}
