#include <bits/stdc++.h>

typedef long long int64;

const int N = 1000000 + 10;

int n, m, l, type;

char s[100010];

int len;

struct node {
  char c;
  int priority;
  double tag;
  node *ch[2], *next;
} pool[N], *top = pool + 1, *root, *suffix[N], *shadow[N];

inline bool cmp(const node *a, const node *b) {
  return a->c < b->c || (a->c == b->c && a->next->tag < b->next->tag);
}

void rebuild(node *cur, double l, double r) {
  if (!cur) return;
  double mid = (l + r) / 2.0;
  cur->tag = mid;
  rebuild(cur->ch[0], l, mid);
  rebuild(cur->ch[1], mid, r);
}

inline void lrot(node *&p, double l, double r) {
  node *u = p->ch[1];
  p->ch[1] = u->ch[0];
  u->ch[0] = p;
  rebuild(p = u, l, r);
}

inline void rrot(node *&p, double l, double r) {
  node *u = p->ch[0];
  p->ch[0] = u->ch[1];
  u->ch[1] = p;
  rebuild(p = u, l, r);
}

void insert(node *&cur, double l, double r) {
  if (!cur) {
    (cur = top++)->tag = (l + r) / 2.0;
    return;
  }
  double mid = (l + r) / 2.0;
  if (cmp(top, cur)) {
    insert(cur->ch[0], l, mid);
    if (cur->ch[0]->priority > cur->priority) rrot(cur, l, r);
  } else {
    insert(cur->ch[1], mid, r);
    if (cur->ch[1]->priority > cur->priority) lrot(cur, l, r);
  }
}

void insert(char ch) {
  top->c = ch;
  top->priority = rand();
  top->next = top > pool ? top - 1 : NULL;
  suffix[++len] = top;
  insert(root, 0, 1);
}

int min[N * 4];

inline int sufmin(int a, int b) {
  if (!~a) return b;
  return (shadow[a]->tag < shadow[b]->tag || (shadow[a]->tag == shadow[b]->tag && a < b)) ? a : b;
}

void build(int id, int l, int r) {
  if (l == r) {
    min[id] = l;
    return;
  }
  int mid = (l + r) / 2;
  build(id << 1, l, mid);
  build(id << 1 | 1, mid + 1, r);
  min[id] = sufmin(min[id << 1], min[id << 1 | 1]);
}

void modify(int id, int l, int r, int pos) {
  if (l == r) return;
  int mid = (l + r) / 2;
  if (pos <= mid)
    modify(id << 1, l, mid, pos);
  else
    modify(id << 1 | 1, mid + 1, r, pos);
  min[id] = sufmin(min[id << 1], min[id << 1 | 1]);
}

int query(int id, int l, int r, int p, int q) {
  if (p <= l && r <= q) return min[id];
  int mid = (l + r) / 2, res = -1;
  if (p <= mid) res = sufmin(res, query(id << 1, l, mid, p, q));
  if (q > mid) res = sufmin(res, query(id << 1 | 1, mid + 1, r, p, q));
  return res;
}

inline int RD() {
  static char ch;
  while (!isdigit(ch = getchar())) {}
  int res = ch - '0';
  while (isdigit(ch = getchar())) res = res * 10 + ch - '0';
  return res;
}

inline char RC() {
  char ch = getchar();
  while (!isupper(ch)) ch = getchar();
  return ch;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  srand(time(NULL));
  scanf("%d%d%d%d %s", &n, &m, &l, &type, s);
  for (int i = l - 1; i >= 0; --i) insert(s[i]);
  for (int i = 1; i <= n; ++i) shadow[i] = suffix[RD()];
  build(1, 1, n);
  int lastans = 0;
  while (m--) {
    char op = RC();
    int a = RD();
    switch (op) {
      case 'I':
        if (type) a ^= lastans;
        insert(a + 'a');
        break;
      case 'C':
        shadow[a] = suffix[RD()];
        modify(1, 1, n, a);
        break;
      case 'Q':
        printf("%d\n", lastans = query(1, 1, n, a, RD()));
        break;
    }
  }
  return 0;
}
