#include <bits/stdc++.h>

const int N = 10000 + 10, V = N * 30, E = V * 10, INF = 0x3f3f3f3f;

int n, m, s, t;
std::vector<int> e[N];

int lch[V], rch[V], map[V];

int tot = 0;
int music[N], root[N];

int adj[V], aug[V];
int to[E], next[E], cap[E];

void link(int a, int b, int c = INF) {
  static int cnt = 0;
  to[cnt] = b, next[cnt] = adj[a], cap[cnt] = c, adj[a] = cnt++;
  to[cnt] = a, next[cnt] = adj[b], cap[cnt] = 0, adj[b] = cnt++;
}

int init(int l, int r, int pos) {
  int res = ++tot;
  if (l < r) {
    int mid = (l + r) / 2;
    if (pos <= mid)
      link(res, lch[res] = init(l, mid, pos));
    else
      link(res, rch[res] = init(mid + 1, r, pos));
  } else {
    link(res, t, 1);
  }
  return res;
}

int merge(int a, int b, int l, int r) {
  if (!a || !b) return a + b;
  int res = ++tot;
  if (l == r) {
    link(res, a);
    link(res, b);
  } else {
    int mid = (l + r) / 2;
    link(res, lch[res] = merge(lch[a], lch[b], l, mid));
    link(res, rch[res] = merge(rch[a], rch[b], mid + 1, r));
  }
  return res;
}

void dfs(int a) {
  for (std::vector<int>::iterator it = e[a].begin(); it != e[a].end(); ++it) {
    int b = *it;
    dfs(b);
    root[a] = merge(root[a], root[b], 1, n);
  }
}

void query(int id, int l, int r, int p, int q, int cur) {
  if (p <= l && r <= q) {
    link(cur, id);
    return;
  }
  int mid = (l + r) / 2;
  if (p <= mid) query(lch[id], l, mid, p, q, cur);
  if (q > mid) query(rch[id], mid + 1, r, p, q, cur);
}

int h[V], gap[V];

int sap(int a, int df, const int s, const int t) {
  if (a == t) return df;
  int f = 0;
  for (int &it = aug[a]; it != -1; it = next[it]) {
    int b = to[it], c = cap[it];
    if (c && h[a] == h[b] + 1) {
      int tmp = sap(b, std::min(c, df - f), s, t);
      f += tmp;
      cap[it] -= tmp;
      cap[it ^ 1] += tmp;
    }
    if (f == df) return f;
  }
  if (--gap[h[a]] == 0) h[s] = tot;
  ++gap[++h[a]];
  aug[a] = adj[a];
  return f;
}

inline int flow(const int s, const int t) {
  memset(h, 0, sizeof h);
  memset(gap, 0, sizeof gap);
  memcpy(aug, adj, sizeof aug);
  int res = 0;
  while (h[s] < tot) res += sap(s, INF, s, t);
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d%d", &n, &m);
  memset(adj, -1, sizeof adj);
  for (int i = 2; i <= n; ++i) {
    int a;
    scanf("%d", &a);
    e[a].push_back(i);
  }
  s = ++tot, t = ++tot;
  for (int i = 1; i <= n; ++i) {
    int h;
    scanf("%d", &h);
    root[i] = init(1, n, h);
  }
  dfs(1);
  while (m--) {
    int l, r, d, t;
    scanf("%d%d%d%d", &l, &r, &d, &t);
    int cur = ++tot;
    link(s, cur, t);
    query(root[d], 1, n, l, r, cur);
  }
  printf("%d\n", flow(s, t));
  return 0;
}
