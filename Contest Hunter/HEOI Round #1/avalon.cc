#include <cmath>
#include <cstdio>
#include <vector>
#include <algorithm>

const int N = 500000 + 10;

typedef std::pair<double, int> Event;

struct Point {
  double x, y;
  Point(double _x = 0.0, double _y = 0.0): x(_x), y(_y) {}
  inline Point& operator+= (const Point &rhs) {
    x += rhs.x;
    y += rhs.y;
    return *this;
  }
  inline Point& operator-= (const Point &rhs) {
    x -= rhs.x;
    y -= rhs.y;
    return *this;
  }
};

int n, k, l, p[N], t[N], u[N];
Point point[N], orig[N];

std::vector<int> son[N];

int dep[N];

inline bool Compare(int a, int b) { return dep[a] < dep[b]; }

inline std::vector<int>* Melt(std::vector<int> *lhs, std::vector<int> *rhs) {
  if (!lhs) return rhs;
  if (!rhs) return lhs;
  if (lhs->size() < rhs->size()) std::swap(lhs, rhs);
  for (int i = 0; i < rhs->size(); ++i) {
    int val = rhs->at(i);
    lhs->push_back(val);
    std::push_heap(lhs->begin(), lhs->end(), Compare);
  }
  delete rhs;
  return lhs;
}

void BFS(int s) {
  static std::vector<int> *heap[N], q;
  q.clear();
  q.push_back(s);
  for (int i = 0; i < q.size(); ++i) {
    int a = q[i];
    for (int j = 0; j < son[q[i]].size(); ++j) {
      int b = son[a][j];
      q.push_back(b);
      dep[b] = dep[a] + 1;
    }
  }
  for (int i = q.size() - 1; i >= 0; --i) {
    int a = q[i];
    point[a] = orig[a];
    if (son[a].empty()) {
      heap[a] = new std::vector<int>;
      heap[a]->push_back(a);
    } else {
      for (int j = 0; j < son[a].size(); ++j) {
        int b = son[a][j];
        point[a] += point[b];
        heap[a] = Melt(heap[a], heap[b]);
      }
      while (!heap[a]->empty() && dep[heap[a]->front()] - dep[a] >= k) {
        point[a] -= orig[heap[a]->front()];
        std::pop_heap(heap[a]->begin(), heap[a]->end(), Compare);
        heap[a]->pop_back();
      }
    }
  }
}

inline double sqr(double x) { return x * x; }

inline double Dist(const Point &a, const Point &b) {
  return sqrt(sqr(b.x - a.x) + sqr(b.y - a.y));
}

int main() {
  scanf("%d%d%d", &n, &k, &l);
  static std::vector<int> pool;
  for (int i = 1; i <= n; ++i) {
    scanf("%lf%lf%d%d%d", &orig[i].x, &orig[i].y, p + i, t + i, u + i);
    if (~t[i]) son[t[i]].push_back(i);
    if (u[i]) pool.push_back(i);
  }
  BFS(1);
  int ans = 0;
  for (int i = 0; i < pool.size(); ++i) {
    int a = pool[i];
    static std::vector<Event> event;
    event.clear();
    for (int j = 0; j < pool.size(); ++j) {
      int b = pool[j];
      double d = Dist(point[a], point[b]);
      if (d >= 2.0 * l) continue;
      double alpha = atan2(point[b].y - point[a].y, point[b].x - point[a].x);
      double beta = acos(d / 2.0 / l);
      event.push_back(Event(fmodl(alpha - beta, 2.0 * M_PI), p[b]));
      event.push_back(Event(fmodl(alpha + beta, 2.0 * M_PI), -p[b]));
    }
    std::sort(event.begin(), event.end());
    for (int j = 0, cnt = 0; j < event.size(); ++j)
      ans = std::max(ans, cnt += event[j].second);
  }
  printf("%d\n", ans);
  return 0;
}
