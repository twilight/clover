#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <map>
#include <vector>

typedef long long i64;

struct Complex {
  double real, imag;
  Complex() { real = imag = 0.0; }
  Complex(double _re, double _im = 0.0): real(_re), imag(_im) {}

  inline Complex& operator/= (double rhs) {
    real /= rhs;
    imag /= rhs;
    return *this;
  }

  inline Complex& operator*= (const Complex &rhs) {
    double u = real * rhs.real - imag * rhs.imag;
    double v = imag * rhs.real + real * rhs.imag;
    real = u, imag = v;
    return *this;
  }
};

inline Complex operator* (const Complex &lhs, double rhs) {
  return Complex(lhs.real * rhs, lhs.imag * rhs);
}

inline Complex operator* (const Complex &lhs, const Complex &rhs) {
  return Complex(lhs.real * rhs.real - lhs.imag * rhs.imag, lhs.imag * rhs.real + lhs.real * rhs.imag);
}

inline Complex operator+ (const Complex &lhs, const Complex &rhs) {
  return Complex(lhs.real + rhs.real, lhs.imag + rhs.imag);
}

inline Complex operator- (const Complex &lhs, const Complex &rhs) {
  return Complex(lhs.real - rhs.real, lhs.imag - rhs.imag);
}

const int LOG = 18, N = 1 << LOG, MOD = 479 * (1 << 21) + 1, g = 3;
const Complex I(0.0, 1.0);

inline int Pow(int base, int exp) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) res = (i64)res * base % MOD;
    base = (i64)base * base % MOD;
  }
  return res;
}

int n, a[N], s[N];
int phi[N];

std::map<int, int> Log;

int w[N + 1];

void Preprocessing() {
  static std::vector<int> prime;
  static bool sieve[N];
  phi[1] = 1;
  for (int i = 2; i < N; ++i) {
    if (!sieve[i]) {
      prime.push_back(i);
      phi[i] = i - 1;
    }
    for (int j = 0; j < prime.size() && i * prime[j] < N; ++j) {
      sieve[i * prime[j]] = true;
      if (i % prime[j] == 0) {
        phi[i * prime[j]] = phi[i] * prime[j];
        break;
      } else {
        phi[i * prime[j]] = phi[i] * phi[prime[j]];
      }
    }
  }
  for (int i = 0; i < N; ++i) (phi[i] %= 23);
  for (int i = 0, j = 1; i < N; ++i, j = (i64)j * g % MOD) Log[j] = i;
  w[0] = 1, w[1] = Pow(g, (MOD - 1) / N);
  for (int i = 2; i <= N; ++i) w[i] = (i64)w[i - 1] * w[1] % MOD;
}

void DFT(Complex a[], int sw) {
  static Complex temp[N];
  for (int i = 0; i < N; ++i) {
    int k = 0;
    for (int j = i, cnt = LOG; cnt--; j >>= 1) (k <<= 1) |= (j & 1);
    temp[k] = a[i];
  }
  for (int m = 2; m <= N; m *= 2) {
    int gap = m / 2;
    for (int i = 0; i < gap; ++i) {
      double t = sw * 2.0 * M_PI * i / m;
      Complex w(cos(t), sin(t));
      for (int j = i; j < N; j += m) {
        Complex u(temp[j]), v(temp[j + gap]);
        temp[j] = u + w * v;
        temp[j + gap] = u - w * v;
      }
    }
  }
  for (int i = 0; i < N; ++i) a[i] = temp[i];
  if (sw == -1) for (int i = 0; i < N; ++i) a[i] /= N;
}

void NTT(int a[], int sw) {
  static int temp[N];
  for (int i = 0; i < N; ++i) {
    int k = 0;
    for (int j = i, cnt = LOG; cnt--; j >>= 1) (k <<= 1) |= (j & 1);
    temp[k] = a[i];
  }
  for (int m = 2; m <= N; m *= 2) {
    int gap = m / 2, layer = N / m;
    for (int i = 0; i < gap; ++i) {
      int t = w[sw > 0 ? i * layer : N - i * layer];
      for (int j = i; j < N; j += m) {
        int u = temp[j], v = temp[j + gap];
        temp[j] = (u + (i64)t * v % MOD) % MOD;
        temp[j + gap] = (u - (i64)t * v % MOD + MOD) % MOD;
      }
    }
  }
  for (int i = 0; i < N; ++i) a[i] = temp[i];
  if (sw == -1) {
    int inv = Pow(N, MOD - 2);
    for (int i = 0; i < N; ++i) a[i] = (i64)a[i] * inv % MOD;
  }
}

void Convolution(int lhs[], const int rhs[], int mod) {
  static Complex u[N], v[N];
  for (int i = 0; i < N; ++i) {
    u[i] = lhs[i];
    v[i] = rhs[i];
  }
  DFT(u, 1);
  DFT(v, 1);
  for (int i = 0; i < N; ++i) u[i] *= v[i];
  DFT(u, -1);
  for (int i = 0; i < N; ++i) lhs[i] = (i64)(u[i].real + 0.5) % mod;
}

void Convolution(int lhs[], const int rhs[]) {
  static int temp[N];
  for (int i = 0; i < N; ++i) temp[i] = rhs[i];
  NTT(lhs, 1);
  NTT(temp, 1);
  for (int i = 0; i < N; ++i) lhs[i] = (i64)lhs[i] * temp[i] % MOD;
  NTT(lhs, -1);
}

int main() {
  scanf("%d", &n);
  Preprocessing();
  for (int i = 1; i <= n; ++i) {
    scanf("%d", a + i); 
    s[i] = (s[i - 1] + a[i]) % MOD;
  }
  static int f[N];
  int p = n + 1;
  for (int i = 1; i <= n; ++i) {
    if (s[i]) {
      f[i] = Log[s[i]]; 
    } else {
      p = i;
      break;
    }
  }
  for (int i = n + 1; i < N; ++i) phi[i] = 0;
  Convolution(f, phi, MOD - 1);
  for (int i = 1; i < p; ++i) f[i] = Pow(g, f[i]);
  for (int i = p; i < N; ++i) f[i] = 0;
  Convolution(f, f);
  int ans = 0;
  for (int i = 1; i < p; ++i) (ans += f[i]) %= MOD;
  printf("%d\n", ans);
  return 0;
}
