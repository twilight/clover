#include <cstdio>
#include <cmath>
#include <complex>
#include <algorithm>

//typedef std::complex<double> point;
typedef std::pair<double, double> point;
typedef std::pair<point, point> line;

const double eps = 1e-8;

#define fst first
#define snd second
#define x first
#define y second

point operator+ (const point &a, const point &b) {
  return point(a.x + b.x, a.y + b.y);
}

point operator- (const point &a, const point &b) {
  return point(a.x - b.x, a.y - b.y);
}

point operator* (const point &a, const double b) {
  return point(a.x * b, a.y * b);
}

double det(const point &a, const point &b) {
  return a.x * b.y - a.y * b.x;
}

double det(const point &o, const point &a, const point &b) {
  return det(a - o, b - o);
}

double det(const line &a, const line &b) {
  return det(a.snd - a.fst, b.snd - b.fst);
}

int dot(const point &a, const point &b) {
  return a.x * b.x + a.y * b.y;
}

inline int sgn(double x) {
  if (fabs(x) < eps) return 0;
  return x > eps ? 1 : -1;
}

bool coline(const line &a, const line &b) {
  return
    sgn(det(a.fst, a.snd, b.fst)) == 0 &&
    sgn(det(a.fst, a.snd, b.snd)) == 0;
}

bool parallel(const line &a, const line &b) {
  return det(a, b) == 0;
}

point intersection(const line &a, const line &b) {
  return a.fst + (a.snd - a.fst) * (det(b.fst, a.fst, b.snd) / det(b, a));
}

int main() {
  int tcase;
  puts("INTERSECTING LINES OUTPUT");
  for (scanf("%d", &tcase); tcase--;) {
    static line seg[2];
    scanf("%lf%lf%lf%lf", &seg[0].fst.x, &seg[0].fst.y, &seg[0].snd.x, &seg[0].snd.y);
    scanf("%lf%lf%lf%lf", &seg[1].fst.x, &seg[1].fst.y, &seg[1].snd.x, &seg[1].snd.y);
    if (coline(seg[0], seg[1])) {
      puts("LINE");
    } else {
      if (parallel(seg[0], seg[1])) {
        puts("NONE");
      } else {
        point tmp = intersection(seg[0], seg[1]);
        printf("POINT %.2lf %.2lf\n", tmp.x, tmp.y);
      }
    }
  }
  puts("END OF OUTPUT");
  return 0;
}
