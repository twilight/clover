#include <cstdio>
#include <cmath>
#include <vector>
#include <complex>
#include <algorithm>

typedef std::complex<double> point;
typedef std::pair<point, point> line;

const int N = 100000 + 10;
const double eps = 1e-8;

#define fst first
#define snd second
#define x real()
#define y imag()

int cur;
line p[N];
std::vector<int> pool;

double det(const point &o, const point &a, const point &b) {
  return (a.x - o.x) * (b.y - o.y) - (a.y - o.y) * (b.x - o.x);
}

inline int sgn(double num) {
  if (fabs(num) < eps) return 0;
  return num > eps ? 1 : -1;
}

bool isIntersect(const line &a, const line &b) {
  if (a.fst == b.fst || a.fst == b.snd || a.snd == b.fst || a.snd == b.snd) return true;
  return
    /*
    std::min(a.fst.x, a.snd.x) <= std::max(b.fst.x, b.snd.x) &&
    std::min(a.fst.y, a.snd.y) <= std::max(b.fst.y, b.snd.y) &&
    std::min(b.fst.x, b.snd.x) <= std::max(a.fst.x, a.snd.x) &&
    std::min(b.fst.y, b.snd.y) <= std::max(a.fst.y, a.snd.y) &&
    */
    sgn(det(a.fst, b.fst, a.snd)) * sgn(det(a.fst, b.snd, a.snd)) < 0 &&
    sgn(det(b.fst, a.fst, b.snd)) * sgn(det(b.fst, a.snd, b.snd)) < 0;
}

bool filter(int i) { return isIntersect(p[i], p[cur]); }

int main() {
  for (int n; scanf("%d", &n), n;) {
    pool.clear();
    for (int i = 1; i <= n; ++i) {
      scanf("%lf%lf%lf%lf", &p[i].fst.x, &p[i].fst.y, &p[i].snd.x, &p[i].snd.y);
      cur = i;
      pool.erase(std::remove_if(pool.begin(), pool.end(), filter), pool.end());
      pool.push_back(i);
    }
    printf("Top sticks:");
    for (std::vector<int>::iterator it = pool.begin(); it != pool.end(); ++it) {
      printf(" %d", *it);
      putchar((it + 1) != pool.end() ? ',' : '.');
    }
    putchar('\n');
  }
  return 0;
}
