#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <climits>
#include <memory>

const int N = 50000 + 10;

int m, det;

struct node {
  char c;
  int size;
  std::shared_ptr<node> ch[2];
  node(char _c): c(_c), size(1) { ch[0] = ch[1] = nullptr; }
  node(char _c, int _sz, std::shared_ptr<node> _lch, std::shared_ptr<node> _rch): c(_c), size(_sz) {
    ch[0] = _lch;
    ch[1] = _rch;
  }
  node(std::shared_ptr<node> a) { *this = *a; }
  #define size(x) ((x) ? ((x)->size) : 0)
  void update() { size = size(ch[0]) + size(ch[1]) + 1; }
};

std::shared_ptr<node> root[N];

inline bool randchoice(double probability) {
  return (double)rand() / RAND_MAX <= probability;
}

std::shared_ptr<node> merge(std::shared_ptr<node> a, std::shared_ptr<node> b) {
  if (!a) return b;
  if (!b) return a;
  std::shared_ptr<node> res;
  if (randchoice((double)size(a) / (size(a) + size(b)))) {
    res = std::shared_ptr<node>(new node(a->c));
    res->ch[0] = a->ch[0];
    res->ch[1] = merge(a->ch[1], b);
  } else {
    res = std::shared_ptr<node>(new node(b->c));
    res->ch[0] = merge(a, b->ch[0]);
    res->ch[1] = b->ch[1];
  }
  res->update();
  return res;
}

void split(std::shared_ptr<node> cur, int pos, std::shared_ptr<node> &a, std::shared_ptr<node> &b) {
  if (!cur) {
    a = b = NULL;
    return;
  }
  std::shared_ptr<node> p(new node(cur));
  if (size(cur->ch[0]) < pos) {
    split(cur->ch[1], pos - size(cur->ch[0]) - 1, a, b);
    p->ch[1] = a;
    p->update();
    a = p;
  } else {
    split(cur->ch[0], pos, a, b);
    p->ch[0] = b;
    p->update();
    b = p;
  }
}

std::shared_ptr<node> build(const char *s, int l, int r) {
  if (l > r) return NULL;
  int mid = (l + r) >> 1;
  std::shared_ptr<node> cur(new node(s[mid]));
  cur->ch[0] = build(s, l, mid - 1);
  cur->ch[1] = build(s, mid + 1, r);
  cur->update();
  return cur;
}

void inorder(std::shared_ptr<node> cur) {
  if (!cur) return;
  inorder(cur->ch[0]);
  putchar(cur->c);
  det += (cur->c == 'c');
  inorder(cur->ch[1]);
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d", &m);
  int op, v, p, cnt, vcnt = 0;
  char s[110];
  std::shared_ptr<node> a, b, c, tmp;
  for (int i = 1; i <= m; ++i) {
    scanf("%d", &op);
    switch (op) {
      case 1:
        scanf("%d %s", &p, s);
        p -= det;
        tmp = build(s, 0, strlen(s) - 1);
        split(root[vcnt], p, a, b);
        root[++vcnt] = merge(merge(a, tmp), b);
        break;
      case 2:
        scanf("%d%d", &p, &cnt);
        p -= det, cnt -= det;
        split(root[vcnt], p - 1, a, tmp);
        split(tmp, cnt, b, c);
        root[++vcnt] = merge(a, c);
        break;
      case 3:
        scanf("%d%d%d", &v, &p, &cnt);
        v -= det, p -= det, cnt -= det;
        split(root[v], p - 1, a, tmp);
        split(tmp, cnt, b, c);
        inorder(b);
        putchar('\n');
        break;
    }
  }
  return 0;
}
