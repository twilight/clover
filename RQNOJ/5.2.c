/*
Way: DP
Accept 485ms
*/

#include <stdio.h>

#define maxn 220
#define max(p,q) ((p)>(q)?(p):(q))

int main() {
	int n;
	scanf("%d",&n);
	int i,a[maxn];
	for (i=1; i<=n; i++) {
		scanf("%d",&(a[i]));
		a[n+i]=a[i];
	}
	int f[maxn][maxn],k,len;
	for (len=2; len<=n; len++) 
		for (i=1; i<=n*2-len+1; i++)
			for (k=i+1; k<=i+len-1; k++) 
				f[i][i+len]=max(f[i][i+len],f[i][k]+f[k][i+len]+a[i]*a[k]*a[i+len]);
	int ans=0;
	for (i=1; i<=n; i++) ans=max(ans,f[i][i+n]);
	printf("%d\n",ans);
	return 0;
}