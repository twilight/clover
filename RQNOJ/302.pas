const
  maxn=210;

var
  q,s:longint;
  patten:array [0..maxn] of string;
  text:string;

procedure init;
var
  i,p:longint;
  tmp:string;

begin
  readln(p,q);
  for i:=1 to p do
    begin
      readln(tmp);
      text:=text+tmp;
    end;
  readln(s);
  for i:=1 to s do readln(patten[i]);
end;

function max(p,q:longint):longint;
begin
  if p>q then exit(p) else exit(q);
end;

function match(st:string):boolean;
var
  i:longint;

begin
  match:=false; 
  for i:=1 to s do if pos(patten[i],st)=1 then exit(true);
end;

procedure main;
var
  i,j,k,len:longint;
  sum,opt:array [0..maxn,0..maxn] of longint;

begin
  fillchar(sum,sizeof(sum),0);
  fillchar(opt,sizeof(opt),0);
  len:=length(text);
  for i:=len downto 1 do 
    for j:=i to len do 
      if match(copy(text,i,j-i+1)) then sum[i,j]:=sum[i+1,j]+1 else sum[i,j]:=sum[i+1,j];
  opt[1]:=sum[1];
  for i:=2 to q do 
    for j:=i+1 to len do 
      for k:=i+1 to j-1 do 
        opt[i,j]:=max(opt[i,j],opt[i-1,k]+sum[k+1,j]);
  writeln(opt[q,len]);
end;

begin
  init;
  main;
end.
