/*
Way: memorized-DFS
Accept  858ms
*/

#include <stdio.h>

#define maxn 220

int a[maxn],f[maxn][maxn];

int max(int p, int q) {
	return p>q?p:q;
}

int query(int l, int r) {
	if (f[l][r]) return f[l][r];
	if (l>=r) return 0;
	int k;
	for (k=l+1; k<=r-1; k++) {
		f[l][k]=query(l,k);
		f[k][r]=query(k,r);
		f[l][r]=max(f[l][r],f[l][k]+f[k][r]+a[l]*a[k]*a[r]);
	}
	return f[l][r];
}

int main() {
	int n;
	int i;
	scanf("%d",&n);
	for (i=1; i<=n; i++) {
		scanf("%d",&(a[i]));
		a[n+i]=a[i];
	}
	int ans=0;
	for (i=1; i<=n; i++) ans=max(ans,query(i,i+n));
	printf("%d\n",ans);
	return 0;
}