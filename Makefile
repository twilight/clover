CFLAGS = -Wall -lm -O2 -DONLINE_JUDGE
C_SRC = $(wildcard ./*/*.c)
C_SRC += $(wildcard ./*/*/*/*.c)
C_OBJ = $(patsubst %.c, %.o, $(C_SRC))
CXX_SRC = $(wildcard ./*/*.cc)
CXX_SRC += $(wildcard ./*/*/*.cc)
CXX_OBJ = $(patsubst %.cc, %.o, $(CXX_SRC))

all: $(C_OBJ) $(CXX_OBJ)

$(C_OBJ): $(C_SRC)
	$(CC) $< -o $@ $(CFLAGS)

$(CXX_OBJ): $(CXX_SRC)
	$(CXX) $< -o $@ $(CFLAGS)

clean:
	rm $(C_OBJ) $(CXX_OBJ)
