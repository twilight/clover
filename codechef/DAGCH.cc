#include <bits/stdc++.h>

const int N = 100000 + 10, INF = 0x3f3f3f3f;

inline int cmin(int &a, int b) { if (b < a) a = b; }
inline int cmax(int &a, int b) { if (b > a) a = b; }

int n, m;
std::vector<int> e[N];

int fa[N], semi[N];
int anc[N], best[N];

int query(int a) {
  if (anc[a] == a) return best[a];
  best[a] = std::min(best[a], query(anc[a]));
  anc[a] = anc[anc[a]];
  return best[a];
}

inline void tarjan() {
  memset(semi, 0x3f, sizeof semi);
  memset(best, 0x3f, sizeof best);
  for (int i = 1; i <= n; ++i) anc[i] = i;
  for (int a = n; a > 0; --a) {
    for (auto b : e[a]) cmin(semi[a], b < a ? b : query(b));
    anc[a] = fa[a];
    best[a] = semi[a];
  }
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    int cnt;
    scanf("%d%d%d", &n, &m, &cnt);
    for (int i = 1; i <= n; ++i) e[i].clear();
    memset(fa, 0, sizeof fa);
    for (int i = m; i--;) {
      int u, v;
      scanf("%d%d", &u, &v);
      e[v].push_back(u);
    }
    for (int a = 1; a <= n; ++a)
      for (auto b : e[a]) if (b < a) cmax(fa[a], b);
    tarjan();
    static int ans[N];
    memset(ans, 0, sizeof ans);
    semi[1] = 0;
    for (int i = 1; i <= n; ++i) if (semi[i] != INF) ans[semi[i]]++;
    for (int i = 1; i <= cnt; ++i) {
      int a;
      scanf("%d", &a);
      printf("%d", ans[a]);
      putchar(i == cnt ? '\n' : ' ');
    }
  }
  return 0;
}
