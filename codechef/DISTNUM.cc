#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <set>
#include <vector>
#include <algorithm>

typedef long long i64;

const int N = 200000 + 10, SZ = 200 * N, MOD = 1000000007;

int nextInt() {
  static char ch;
  while (!isdigit(ch = getchar_unlocked())) {}
  int res = ch - '0';
  while (isdigit(ch = getchar_unlocked())) res = 10 * res + ch - '0';
  return res;
}

int n, q, a[N];

int power(int base, int exp) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) res = (i64)res * base % MOD;
    base = (i64)base * base % MOD;
  }
  return res;
}

int idx[N];

namespace Splay {

struct node {
  node *p, *ch[2];
  int size, act, sum;
  void update();
  inline int dir() { return this == p->ch[1]; }
  void rotate();
  void splay(node*);
} pool[N], *tot = pool, *root;

void node::update() {
  size = 1, sum = act;
  if (ch[0]) size += ch[0]->size, sum += ch[0]->sum;
  if (ch[1]) size += ch[1]->size, sum += ch[1]->sum;
}

void node::rotate() {
  node *v = p;
  int d = dir();
  if (v->p) v->p->ch[v->dir()] = this;
  p = v->p;
  if (ch[!d]) ch[!d]->p = v;
  v->ch[d] = ch[!d];
  ch[!d] = v;
  v->p = this;
  v->update(), update();
}

void node::splay(node *target = NULL) {
  while (p != target) {
    if (p->p != target) (dir() == p->dir() ? p : this)->rotate();
    rotate();
  }
  update();
  if (!target) root = this;
}

int select(int k) {
  node *p = root;
  while (1) {
    int t = (p->ch[0] ? p->ch[0]->sum : 0) + p->act;
    if (k == t && p->act) {
      p->splay();
      return p - pool;
    }
    if (k <= t) p = p->ch[0]; else p = p->ch[1], k -= t;
  }
}

int insert(int p) {
  if (!p) {
    node *t = pool + 1;
    t->splay();
    while (t->ch[0]) t = t->ch[0];
    t->splay();
    t->ch[0] = ++tot;
    tot->p = t;
    tot->act = 1;
    tot->update();
    t->update();
    tot->splay();
    return tot - pool;
  }
  node *t = pool + select(p);
  t->splay();
  ++tot;
  tot->act = 1;
  tot->update();
  tot->ch[1] = t->ch[1];
  if (t->ch[1]) t->ch[1]->p = tot;
  t->ch[1] = tot;
  tot->p = t;
  tot->update();
  t->update();
  tot->splay();
  return tot - pool;
}

void preprocessing() {
  for (int i = 1; i < n; ++i) {
    pool[i].ch[1] = pool + i + 1;
    pool[i + 1].p = pool + i;
  }
  for (int i = 1; i <= n; ++i) pool[i].act = 1;
  (tot = pool + n)->splay();
}

void dfs(node *cur, int det = 0) {
  if (cur->ch[0]) dfs(cur->ch[0], det);
  idx[cur - pool] = (det += (cur->ch[0] ? cur->ch[0]->size : 0) + 1);
  if (cur->ch[1]) dfs(cur->ch[1], det);
}

}

int op[N], x[N], y[N];

void parseInput() {
  Splay::preprocessing();
  for (int i = 1; i <= q; ++i) {
    switch (op[i]) {
      case 1:
      case 5:
        x[i] = Splay::select(x[i]);
        y[i] = Splay::select(y[i]);
        break;
      case 2:
        x[i] = Splay::select(x[i]);
        break;
      case 3: {
        Splay::node *p = Splay::pool + (x[i] = Splay::select(x[i]));
        p->splay();
        p->act = 0;
        p->update();
        break;
      }
      case 4:
        x[i] = Splay::insert(x[i]);
        break;
    }
  }
  Splay::dfs(Splay::root);
  for (int i = 1; i <= q; ++i) {
    x[i] = idx[x[i]];
    if (op[i] == 1 || op[i] == 5) y[i] = idx[y[i]];
  }
}

inline int sqr(int x) { return (i64)x * x % MOD; }
inline int cube(int x) { return (i64)sqr(x) * x % MOD; }

#define CHECK(x) {         \
    if (x >= MOD) x -= MOD; \
}

struct Tuple {
  int val[4];
  inline Tuple() { memset(val, 0, sizeof val); }
  explicit Tuple(int x) {
    val[0] = 1;
    val[1] = x;
    val[2] = (i64)x * x % MOD;
    val[3] = (i64)val[2] * x % MOD;
  }
  inline int& operator[] (const size_t p) { return val[p]; }
  inline int operator[] (const size_t p) const { return val[p]; }
  Tuple& operator+= (const Tuple &rhs) {
    val[0] += rhs[0];
    val[1] += rhs[1];
    val[2] += rhs[2];
    val[3] += rhs[3];
    CHECK(val[0]);
    CHECK(val[1]);
    CHECK(val[2]);
    CHECK(val[3]);
    return *this;
  }
  Tuple& operator-= (const Tuple &rhs) {
    val[0] += MOD - rhs[0];
    val[1] += MOD - rhs[1];
    val[2] += MOD - rhs[2];
    val[3] += MOD - rhs[3];
    CHECK(val[0]);
    CHECK(val[1]);
    CHECK(val[2]);
    CHECK(val[3]);
    return *this;
  }
  Tuple operator-() const {
    Tuple res;
    res[0] = MOD - val[0];
    res[1] = MOD - val[1];
    res[2] = MOD - val[2];
    res[3] = MOD - val[3];
    return res;
  }
  inline int calc() {
    if (val[0] < 3) return 0;
    static const int inv = 166666668;
    int res = ((cube(val[1]) - 3LL * val[1] % MOD * val[2] % MOD + 2LL * val[3]) % MOD) * inv % MOD;
    if (res < 0) res += MOD;
    return res;
  }
};

inline Tuple operator- (Tuple lhs, const Tuple &rhs) { return lhs -= rhs; }

namespace BIT {

int tot;

namespace Seg {

int cnt, lch[SZ], rch[SZ];
Tuple sum[SZ];

int cur[N];
Tuple tuple;

void add(int l, int r, register int p) {
  int temp = cur[0];
  register int a = tuple.val[0], b = tuple.val[1], c = tuple.val[2], d = tuple.val[3];
  register int i, j, mid, *it;
  for (;;) {
    for (i = 1; i <= temp; ++i) {
      it = sum[cur[i]].val;
      it[0] += a;
      CHECK(it[0]);
      it[1] += b;
      CHECK(it[1]);
      it[2] += c;
      CHECK(it[2]);
      it[3] += d;
      CHECK(it[3]);
    }
    if (l == r) return;
    mid = (l + r) >> 1;
    if (p <= mid) {
      for (i = 1; i <= temp; ++i) {
        j = cur[i];
        if (!lch[j]) lch[j] = ++cnt;
        cur[i] = lch[j];
      }
      r = mid;
    } else {
      for (i = 1; i <= temp; ++i) {
        j = cur[i];
        if (!rch[j]) rch[j] = ++cnt;
        cur[i] = rch[j];
      }
      l = mid + 1;
    }
  }
}

Tuple query(int id, int l, int r, int p, int q) {
  if (p <= l && r <= q) return sum[id];
  Tuple res;
  int mid = (l + r) >> 1;
  if (p <= mid && lch[id]) res = query(lch[id], l, mid, p, q);
  if (q > mid && rch[id]) res += query(rch[id], mid + 1, r, p, q);
  return res;
}

}

void add(int p, int q, const Tuple &v) {
  Seg::cur[0] = 0;
  for (; p <= tot; p += p & -p) Seg::cur[++Seg::cur[0]] = p;
  Seg::tuple = v;
  Seg::add(0, tot, q);
}

Tuple query(int p, int q) {
  Tuple res;
  for (int t = q; t; t ^= t & -t) res += Seg::query(t, 0, tot, 0, p - 1);
  for (int t = p - 1; t; t ^= t & -t) res -= Seg::query(t, 0, tot, 0, p - 1);
  return res;
}

}

std::set<int> pos[N];
std::vector<int> sorted;

int val[N];

void assign(int p, int v) {
  val[p] = v;
  std::set<int>::iterator it = pos[v].insert(p).first, a = it, b = it;
  int pred = (a == pos[v].begin() ? 0 : *--a);
  int succ = (++b == pos[v].end() ? N : *b);
  BIT::add(p, pred, Tuple(sorted[v]));
  if (succ != N) {
    BIT::add(succ, pred, -Tuple(sorted[val[succ]]));
    BIT::add(succ, p, Tuple(sorted[val[succ]]));
  }
}

void clear(int p) {
  int v = val[p];
  std::set<int>::iterator it = pos[v].find(p), a = it, b = it;
  int pred = (a == pos[v].begin() ? 0 : *--a);
  int succ = (++b == pos[v].end() ? N : *b);
  BIT::add(p, pred, -Tuple(sorted[v]));
  if (succ != N) {
    BIT::add(succ, p, -Tuple(sorted[val[succ]]));
    BIT::add(succ, pred, Tuple(sorted[val[succ]]));
  }
  pos[v].erase(it);
}

int main() {
  n = nextInt();
  q = nextInt();
  for (int i = 1; i <= n; ++i) sorted.push_back(a[i] = nextInt());
  for (int i = 1; i <= q; ++i) {
    op[i] = nextInt(), x[i] = nextInt();
    if (op[i] != 3) y[i] = nextInt();
    if (op[i] == 2 || op[i] == 4) sorted.push_back(y[i]);
  }
  std::sort(sorted.begin(), sorted.end());
  sorted.erase(std::unique(sorted.begin(), sorted.end()), sorted.end());
  for (int i = 1; i <= n; ++i) a[i] = std::lower_bound(sorted.begin(), sorted.end(), a[i]) - sorted.begin();
  for (int i = 1; i <= q; ++i) if (op[i] == 2 || op[i] == 4) y[i] = std::lower_bound(sorted.begin(), sorted.end(), y[i]) - sorted.begin();
  parseInput();
  BIT::Seg::cnt = BIT::tot = Splay::tot - Splay::pool;
  for (int i = 1; i <= n; ++i) assign(idx[i], a[i]);
  for (int i = 1; i <= q; ++i) {
    switch (op[i]) {
      case 1:
        printf("%d\n", BIT::query(x[i], y[i]).calc());
        break;
      case 2:
        clear(x[i]);
        assign(x[i], y[i]);
        break;
      case 3:
        clear(x[i]);
        break;
      case 4:
        assign(x[i], y[i]);
        break;
      case 5:
        printf("%d\n", BIT::query(x[i], y[i])[0]);
        break;
    }
  }
  return 0;
}
