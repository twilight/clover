#include <cctype>
#include <cstdio>

const int N = 500 + 10, M = 5000 + 10, L = 50000 + 10, S = 63;

int n;
int next[S][N * M], fail[N * M], cnt[N * M], tot = 1;

int trans(char c) {
  if (isdigit(c)) return c - '0';
  if (isupper(c)) return c - 'A' + 10;
  if (islower(c)) return c - 'a' + 36;
  return 62;
}

void init(int x) {
  for (int i = 0; i < S; ++i) next[i][x] = 0;
  cnt[x] = fail[x] = 0;
}

int append(int i, char s[]) {
  for (char *it = s; *it; ++it) {
    int c = trans(*it);
    if (!next[c][i]) init(next[c][i] = ++tot);
    i = next[c][i];
  }
  return i;
}

int order[N * M];

void build() {
  int r = order[1] = 1;
  for (int i = 1; i <= r; ++i) {
    int a = order[i];
    for (int c = 0; c < S; ++c) {
      int b = next[c][a];
      if (!b) continue;
      int k = fail[a];
      while (k && !next[c][k]) k = fail[k];
      fail[order[++r] = b] = (k ? next[c][k] : 1);
    }
  }
}

void match(char t[]) {
  int i = 1;
  for (char *it = t; *it; ++it) {
    int c = trans(*it);
    while (i && !next[c][i]) i = fail[i];
    if (i) i = next[c][i]; else i = 1;
    ++cnt[i];
  }
}

int main() {
  scanf("%d", &n);
  static int ans[N];
  init(tot = 1);
  for (int i = 1; i <= n; ++i) {
    static char s[M];
    scanf(" %s", s);
    ans[i] = append(1, s);
  }
  build();
  int m;
  for (scanf("%d", &m); m--;) {
    static char t[L];
    scanf(" %s", t);
    match(t);
  }
  for (int i = tot; i > 0; --i) cnt[fail[order[i]]] += cnt[order[i]];
  for (int i = 1; i <= n; ++i) printf("%d\n", cnt[ans[i]]);
  return 0;
}
