#include <cctype>
#include <cstdio>
#include <stack>

typedef long long i64;

const int Z = 8, P = 4, D = 2, A = 1, N = 1 << 4, S = (1 << N) - 1, L = 100 + 10, MOD = 1000003;

int n, z, p, d, a;
int f[N], g[N];

int power(int base, int exp, int mod) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) res = (i64)res * base % mod;
    base = (i64)base * base % mod;
  }
  return res;
}

void preprocessing() {
  for (int i = 0; i < N; ++i) {
    if (i & 8) z |= 1 << i;
    if (i & 4) p |= 1 << i;
    if (i & 2) d |= 1 << i;
    if (i & 1) a |= 1 << i;
  }
  int div = power(2, MOD - 2, MOD);
  g[0] = power(2, power(2, n, MOD - 1), MOD);
  g[Z] = g[P] = (i64)g[0] * div % MOD;
  g[D] = power(2, power(2, n - 1, MOD - 1), MOD);
  g[A] = power(2, n + 1, MOD);
  g[Z | P] = (i64)g[Z] * div % MOD;
  g[Z | P | D] = g[Z | D] = g[P | D] = (i64)g[D] * div % MOD;
  g[Z | A] = g[P | A] = (i64)g[A] * div % MOD;
  g[D | A] = power(2, n, MOD);
  g[Z | P | A] = (i64)g[Z | A] * div % MOD;
  g[Z | P | D | A] = g[P | D | A] = g[Z | D | A] = (i64)g[D | A] * div % MOD;
  for (int i = N - 1; i >= 0; --i) {
    f[i] = g[i];
    for (int j = i + 1; j < N; ++j) if ((i & j) == i) (f[i] -= f[j]) %= MOD;
    (f[i] += MOD) %= MOD;
  }
}

inline int dispatch(char c) {
  switch (c) {
    case 'Z': return z;
    case 'P': return p;
    case 'D': return d;
    case 'A': return a;
  }
}

int calc(int lhs, int rhs, char op) {
  switch (op) {
    case 'v': return lhs | rhs;
    case '^': return lhs & rhs;
    case '\\': return lhs - (lhs & rhs);
  }
}

std::stack<int> num;
std::stack<char> op;

void maintain() {
  for (; !op.empty() && op.top() != '('; op.pop()) {
    if (op.top() == '!') {
      num.top() ^= S;
    } else {
      int t = num.top();
      num.pop();
      num.top() = calc(num.top(), t, op.top());
    }
  }
}

int main() {
  scanf("%d", &n);
  preprocessing();
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    static char s[L];
    scanf(" %s", s);
    while (!num.empty()) num.pop();
    while (!op.empty()) op.pop();
    for (char *it = s; *it; ++it) {
      if (isupper(*it)) {
        num.push(dispatch(*it));
        maintain();
      } else {
        if (*it == ')') {
          op.pop();
          maintain();
        } else {
          op.push(*it);
        }
      }
    }
    int ans = 0;
    for (int i = 0; i < N; ++i) if (num.top() >> i & 1) (ans += f[i]) %= MOD;
    printf("%d\n", ans);
  }
  return 0;
}
