#include <cmath>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <algorithm>

typedef long long i64;

const int N = 100000 + 10, SZ = 325;

int n, size;
char s[N];

int nextInt() {
  static char ch;
  while (!isdigit(ch = getchar_unlocked())) {}
  int res = ch - '0';
  while (isdigit(ch = getchar_unlocked())) res = 10 * res + ch - '0';
  return res;
}

void nextString(char *s, int &len) {
  char ch = len = 0;
  while (islower(ch = getchar_unlocked())) s[++len] = ch;
}

#define sqr(x) ((i64)(x) * (x))

int mask[N];
int s0[SZ][N];
i64 s1[SZ][N], s2[SZ][N];

int c0[N];
i64 c1[N], c2[N];

int cur0;
i64 cur1, cur2;

void preprocessing() {
  mask[0] = 0;
  for (int i = 1; i <= n; ++i) mask[i] = mask[i - 1] ^ (1 << (s[i] - 'a'));
  static int sorted[N];
  memcpy(sorted, mask, (n + 1) * sizeof(int));
  std::sort(sorted, sorted + n + 1);
  int tot = std::unique(sorted, sorted + n + 1) - sorted;
  for (int i = 0; i <= n; ++i) mask[i] = std::lower_bound(sorted, sorted + tot, mask[i]) - sorted;
  for (int i = 0; i <= n; i += size) {
    int k = i / size;
    cur0 = cur1 = cur2 = 0;
    for (int j = i; j >= 0; --j) c0[mask[j]] = c1[mask[j]] = c2[mask[j]] = 0;
    for (int j = i; j >= 0; --j) {
      s2[k][j] = (cur2 += c2[mask[j]] - 2 * c1[mask[j]] * j + c0[mask[j]] * sqr(j));
      s1[k][j] = (cur1 += llabs(c1[mask[j]] - (i64)c0[mask[j]] * j));
      s0[k][j] = (cur0 += c0[mask[j]]);
      ++c0[mask[j]];
      c1[mask[j]] += j;
      c2[mask[j]] += sqr(j);
    }
    cur0 = cur1 = cur2 = 0;
    for (int j = i; j <= n; ++j) c0[mask[j]] = c1[mask[j]] = c2[mask[j]] = 0;
    for (int j = i; j <= n; ++j) {
      s2[k][j] = (cur2 += c2[mask[j]] - 2 * c1[mask[j]] * j + c0[mask[j]] * sqr(j));
      s1[k][j] = (cur1 += llabs(c1[mask[j]] - (i64)c0[mask[j]] * j));
      s0[k][j] = (cur0 += c0[mask[j]]);
      ++c0[mask[j]];
      c1[mask[j]] += j;
      c2[mask[j]] += sqr(j);
    }
  }
}

i64 solve(int l, int r, int t) {
  if (r - l <= size) {
    i64 res = 0;
    for (int i = l; i <= r; ++i) c0[mask[i]] = 0;
    if (t) for (int i = l; i <= r; ++i) c1[mask[i]] = 0;
    if (t > 1) for (int i = l; i <= r; ++i) c2[mask[i]] = 0;
    for (int i = l; i <= r; ++i) {
      switch (t) {
        case 0:
          res += c0[mask[i]];
          break;
        case 1:
          res += llabs(c1[mask[i]] - (i64)c0[mask[i]] * i);
          break;
        case 2:
          res += c2[mask[i]] - 2 * c1[mask[i]] * i + c0[mask[i]] * sqr(i);
          break;
      }
      ++c0[mask[i]];
      if (t) c1[mask[i]] += i;
      if (t > 1) c2[mask[i]] += sqr(i);
    }
    return res;
  }
  int x = (l + size - 1) / size, y = r / size;
  int p = x * size, q = y * size;
  i64 res = 0;
  switch (t) {
    case 0:
      res = s0[x][r] + s0[y][l] - s0[x][q];
      break;
    case 1:
      res = s1[x][r] + s1[y][l] - s1[x][q];
      break;
    case 2:
      res = s2[x][r] + s2[y][l] - s2[x][q];
      break;
  }
  for (int i = l; i < p; ++i) c0[mask[i]] = 0;
  for (int i = r; i > q; --i) c0[mask[i]] = 0;
  if (t) {
    for (int i = l; i < p; ++i) c1[mask[i]] = 0;
    for (int i = r; i > q; --i) c1[mask[i]] = 0;
  }
  if (t > 1) {
    for (int i = l; i < p; ++i) c2[mask[i]] = 0;
    for (int i = r; i > q; --i) c2[mask[i]] = 0;
  }
  for (int i = l; i < p; ++i) {
    ++c0[mask[i]];
    if (t) c1[mask[i]] += i;
    if (t > 1) c2[mask[i]] += sqr(i);
  }
  for (int i = r; i > q; --i) {
    switch (t) {
      case 0:
        res += c0[mask[i]];
        break;
      case 1:
        res += llabs(c1[mask[i]] - (i64)c0[mask[i]] * i);
        break;
      case 2:
        res += c2[mask[i]] - 2 * c1[mask[i]] * i + c0[mask[i]] * sqr(i);
        break;
    }
  }
  return res;
}

int main() {
  for (int tcase = nextInt(); tcase--;) {
    nextString(s, n);
    size = sqrt(n) + 0.5;
    preprocessing();
    i64 a = 0, b = 0;
    for (int q = nextInt(); q--;) {
      int x = nextInt(), y = nextInt();
      int l = (x + a) % n + 1;
      int r = (y + b) % n + 1;
      if (l > r) std::swap(l, r);
      a = b, b = solve(l - 1, r, nextInt());
      printf("%lld\n", b);
    }
  }
  return 0;
}
