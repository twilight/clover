#include <cstdio>
#include <cstring>
#include <vector>

typedef long long i64;

const int N = 10000000 + 10, MOD = 1000000007;

int tcase, type;
int n, m, l, r;

inline int Power(int base, int exp) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) res = (i64)res * base % MOD;
    base = (i64)base * base % MOD;
  }
  return res;
}

int moe[N], sum[N];
std::vector<int> prime;

inline void Preprocessing() {
  static bool flag[N];
  moe[1] = 1;
  for (int i = 2; i < N; ++i) {
    if (!flag[i]) {
      moe[i] = -1;
      prime.push_back(i);
    }
    for (int j = 0; j < prime.size() && i * prime[j] < N; ++j) {
      flag[i * prime[j]] = true;
      if (i % prime[j] == 0) {
        moe[i * prime[j]] = 0;
        break;
      } else {
        moe[i * prime[j]] = -moe[i];
      }
    }
  }
  for (int i = 1; i < N; ++i) sum[i] = sum[i - 1] + moe[i];
}

int power[N], tag[N];

inline int f(int x) {
  int res = 0;
  for (int i = 1; i <= x;) {
    int a = x / i;
    if (tag[a] != n) power[a] = Power(a, tag[a] = n);
    int j = x / a;
    (res += (i64)(sum[j] - sum[i - 1]) * power[a] % MOD) %= MOD;
    i = j + 1;
  }
  return (res + MOD) % MOD;
}

inline int Calc(int l, int r) {
  int res = 0;
  for (int i = l; i <= r;) {
    int j = std::min(r, m / (m / i));
    (res += (i64)(j - i + 1) * f(m / i) % MOD) %= MOD;
    i = j + 1;
  }
  return res;
}

int main() {
  Preprocessing();
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d%d%d", &n, &m, &l, &r);
    printf("%d\n", Calc(l, r));
  }
  return 0;
}
