#include <cstdio>
#include <cstring>

const int N = 500000 + 10, LOG = 20, S = N * LOG;

int n, root[N];

int size[S], next[S][2], tot = 0;

int add(int orig, int cur, int dep = LOG - 1) {
  int res = ++tot;
  memcpy(next[res], next[orig], 2 * sizeof(int));
  size[res] = size[orig] + 1;
  if (dep < 0) return res;
  int c = cur >> dep & 1;
  next[res][c] = add(next[orig][c], cur, dep - 1);
  return res;
}

int query(int lhs, int rhs, int x) {
  int res = 0;
  for (int i = LOG - 1; i >= 0; --i) {
    int c = !(x >> i & 1);
    int s = size[next[rhs][c]] - size[next[lhs][c]];
    if (!s) c ^= 1;
    res |= c << i;
    lhs = next[lhs][c];
    rhs = next[rhs][c];
  }
  return res;
}

int count(int lhs, int rhs, int x) {
  int res = 0;
  for (int i = LOG - 1; i >= 0; --i) {
    int c = x >> i & 1;
    if (c) res += size[next[rhs][0]] - size[next[lhs][0]];
    lhs = next[lhs][c];
    rhs = next[rhs][c];
  }
  return res + size[rhs] - size[lhs];
}

int select(int lhs, int rhs, int k) {
  int res = 0;
  for (int i = LOG - 1; i >= 0; --i) {
    int s = size[next[rhs][0]] - size[next[lhs][0]];
    int c = k > s;
    if (c) k -= s;
    res |= c << i;
    lhs = next[lhs][c];
    rhs = next[rhs][c];
  }
  return res;
}

int main() {
  int m;
  for (scanf("%d", &m); m--;) {
    int op, l, r, x, k;
    scanf("%d", &op);
    switch (op) {
      case 0:
        scanf("%d", &x);
        ++n;
        root[n] = add(root[n - 1], x);
        break;
      case 1:
        scanf("%d%d%d", &l, &r, &x);
        printf("%d\n", query(root[l - 1], root[r], x));
        break;
      case 2:
        scanf("%d", &k);
        n -= k;
        break;
      case 3:
        scanf("%d%d%d", &l, &r, &x);
        printf("%d\n", count(root[l - 1], root[r], x));
        break;
      case 4:
        scanf("%d%d%d", &l, &r, &k);
        printf("%d\n", select(root[l - 1], root[r], k));
        break;
    }
  }
  return 0;
}
