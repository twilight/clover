#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>
#include <functional>

typedef long long i64;

const int MOD = 1000000007, SZ = 26, N = 10000 + 10;

int n, m;
int fact[N], inv[N];

struct Node {
  Node *next[SZ], *pre;
  int count, len;
} pool[N], *root, *last, *top;

inline Node *Makenode() {
  memset(top, 0, sizeof(*top));
  return top++;
}

inline void Append(int ch) {
  ch -= 'a';
  Node *np = Makenode(), *p = last;
  np->len = p->len + 1;
  np->count = 1;
  for (; p && !p->next[ch]; p = p->pre) p->next[ch] = np;
  if (!p) {
    np->pre = root;
  } else {
    Node *q = p->next[ch];
    if (q->len == p->len + 1) {
      np->pre = q;
    } else {
      Node *nq = Makenode();
      *nq = *q;
      nq->count = 0;
      nq->len = p->len + 1;
      np->pre = q->pre = nq;
      for (; p && p->next[ch] == q; p = p->pre) p->next[ch] = nq;
    }
  }
  last = np;
}

inline int C(int n, int m) {
  if (m > n) return 0;
  return (i64)fact[n] * inv[m] % MOD * inv[n - m] % MOD;
}

inline int Pow(int base, int exp) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) res = (i64)res * base % MOD;
    base = (i64)base * base % MOD;
  }
  return res;
}

void Preprocessing() {
  static int cnt[N];
  memset(cnt, 0, sizeof cnt);
  static Node *order[N];
  for (Node *it = pool; it < top; ++it) ++cnt[it->len];
  for (int i = 1; i <= n; ++i) cnt[i] += cnt[i - 1];
  for (Node *it = pool; it < top; ++it) order[--cnt[it->len]] = it;
  for (int it = top - pool - 1; it > 0; --it) order[it]->pre->count += order[it]->count;
}

int main() {
  fact[0] = 1;
  for (int i = 1; i < N; ++i) fact[i] = (i64)fact[i - 1] * i % MOD;
  inv[N - 1] = Pow(fact[N - 1], MOD - 2);
  for (int i = N - 2; i >= 0; --i) inv[i] = (i64)inv[i + 1] * (i + 1) % MOD;
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    static char s[N];
    scanf("%d%d %s", &n, &m, s);
    top = pool;
    last = root = Makenode();
    for (int i = 0; i < n; ++i) Append(s[i]);
    Preprocessing();
    static int tag[N];
    memset(tag, 0, sizeof tag);
    for (Node *it = pool + 1; it < top; ++it) tag[it->count] += it->len - it->pre->len;
    static int ans[N];
    memset(ans, 0, sizeof ans);
    for (int i = 1; i <= n; ++i) 
      for (int j = n; j >= i; --j)
        (ans[i] += (i64)tag[j] * C(j, i) % MOD) %= MOD;
    while (m--) {
      int k;
      scanf("%d", &k);
      printf("%d\n", k <= n ? ans[k] : 0);
    }
  }
  return 0;
}
