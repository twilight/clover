#include <bits/stdc++.h>

#define x first
#define y second

typedef long long int64;
typedef std::pair<int64, int64> point;

const int N = 30000 + 10;

int n, m, c[N];

inline int64 cross(const point &o, const point &a, const point &b) {
  return (a.x - o.x) * (b.y - o.y) - (a.y - o.y) * (b.x - o.x);
}

inline double sqr(double x) { return x * x; }

inline double cost(const point &a, const point &b) {
  return sqr((b.y - a.y) / (double)(b.x - a.x)) * (b.x - a.x);
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= n; ++i) scanf("%d", c + i), c[i] += c[i - 1];
    static std::deque<point> sup, inf;
    sup.clear(), inf.clear();
    sup.push_back(point(0, 0)), inf.push_back(point(0, 0));
    long double ans = 0;
    for (int i = 1; i <= n; ++i) {
      point up(i, c[i] + m), down(i, c[i]);
      while (sup.size() > 1 && cross(sup[sup.size() - 2], sup.back(), up) <= 0) sup.pop_back();
      while (inf.size() > 1 && cross(inf[inf.size() - 2], inf.back(), down) >= 0) inf.pop_back();
      if (sup.size() == 1) {
        for (; inf.size() > 1 && cross(inf[0], inf[1], up) <= 0; inf.pop_front(), sup[0] = inf[0])
          ans += cost(inf[0], inf[1]);
      }
      if (inf.size() == 1) {
        for (; sup.size() > 1 && cross(sup[0], sup[1], down) >= 0; sup.pop_front(), inf[0] = sup[0])
          ans += cost(sup[0], sup[1]);
      }
      sup.push_back(up);
      inf.push_back(down);
    }
    for (; inf.size() > 1; inf.pop_front()) ans += cost(inf[0], inf[1]);
    std::cout << std::fixed << std::setprecision(5) << ans / c[n] << std::endl;
  }
  return 0;
}
