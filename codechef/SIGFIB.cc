#include <cctype>
#include <cstdio>
#include <cstring>

typedef long long i64;

const int K = 13, S = 200;

//const int numer[K] = {0, 0, 0, 1, 0, 3, 0, 3, 0, 1, 0, 0, 0};
const int numer[K] = {0, 0, 0, 6, 0, 18, 0, 18, 0, 6, 0, 0, 0};
const int denom[K] = {1, -6, 9, 10, -30, -6, 41, 6, -30, -10, 9, 6, 1};

i64 n;
int mod;

int f[K];

void multiply(int lhs[], const int rhs[]) {
  static int temp[2 * K];
  memset(temp, 0, sizeof temp);
  for (int i = 0; i < K; ++i)
    for (int j = 0; j < K; ++j)
      temp[i + j] = (temp[i + j] + (i64)lhs[i] * rhs[j]) % mod;
  for (int i = 2 * K - 1; i >= K; --i)
    for (int j = 1; j < K; ++j)
      temp[i - j] = (temp[i - j] + (i64)temp[i] * -denom[j]) % mod;
  memcpy(lhs, temp, K * sizeof(int));
}

void power(int base[], i64 exp) {
  static int res[K];
  memset(res, 0, sizeof res);
  res[0] = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) multiply(res, base);
    multiply(base, base);
  }
  memcpy(base, res, sizeof res);
}


i64 nextInt() {
  char ch;
  while (!isdigit(ch = getchar())) {}
  i64 res = ch - '0';
  while (isdigit(ch = getchar())) res = 10 * res + ch - '0';
  return res;
}

int mem[S][100000], len[S];

void preprocess() {
  memcpy(f, numer, sizeof f);
  for (int i = 1; i < K; ++i)
    for (int j = 0; j < i; ++j)
      f[i] += f[j] * -denom[i - j];
  for (int mod = 2; mod < S; ++mod) {
    for (int i = 0; i < K; ++i) mem[mod][i] = (f[i] % mod + mod) % mod;
    for (int i = K;; ++i) {
      int &cur = mem[mod][i] = 0;
      for (int j = 1; j < K; ++j) cur = (cur + (i64)mem[mod][i - j] * -denom[j] % mod) % mod;
      cur = (cur + mod) % mod;
      len[mod] = i - K + 1;
      for (int j = 0; j < K; ++j) if (mem[mod][j] != mem[mod][len[mod] + j]) goto fail;
      break;
   fail:
      continue;
    }
  }
}

int main() {
  preprocess();
  for (int tcase = nextInt(); tcase--;) {
    mod = nextInt();
    n = nextInt();
    if (mod == 1) {
      puts("0");
    } else if (mod < S) {
      printf("%d\n", mem[mod][n % len[mod]]);
    } else {
      static int poly[K];
      memset(poly, 0, sizeof poly);
      poly[1] = 1;
      power(poly, n);
      int ans = 0;
      for (int i = 0; i < K; ++i) ans = (ans + (i64)f[i] * poly[i] % mod) % mod;
      printf("%d\n", (ans + mod) % mod);
    }
  }
  return 0;
}
