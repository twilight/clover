#include <cstdio>
#include <vector>
#include <algorithm>

const int N = 200000 + 10;

int n, m;

inline void cmin(int &a, int b) { if (b < a) a = b; }

struct node_t {
  node_t *p, *ch[2];
  bool flag;
  int min_timestamp;

  inline void clear();

  inline void flip() {
    flag ^= 1;
    std::swap(ch[0], ch[1]);
  }

  inline void release() {
    if (flag) {
      if (ch[0]) ch[0]->flip();
      if (ch[1]) ch[1]->flip();
      flag = false;
    }
  }

  inline void update();

  inline int sgn() {
    if (!this || !p || !(this == p->ch[0] || this == p->ch[1])) return -1;
    return this == p->ch[1];
  }

  inline void fix() {
    if (~sgn()) p->fix();
    release();
  }

  void rotate();
  void splay();
  node_t *expose();
  inline void evert();
  void link(node_t*);
  void cut(node_t*);
} pool[N * 2];

inline void node_t::clear() {
  p = ch[0] = ch[1] = NULL;
  flag = false;
  min_timestamp = this - pool;
}

inline void node_t::update() {
  min_timestamp = this - pool;
  if (ch[0]) cmin(min_timestamp, ch[0]->min_timestamp);
  if (ch[1]) cmin(min_timestamp, ch[1]->min_timestamp);
}

void node_t::rotate() {
  node_t *v = p;
  v->release(), release();
  int d = sgn();
  if (~v->sgn()) v->p->ch[v->sgn()] = this;
  p = v->p;
  v->ch[d] = ch[!d];
  if (ch[!d]) ch[!d]->p = v;
  ch[!d] = v;
  v->p = this;
  v->update(), update();
}

void node_t::splay() {
  fix();
  while (~sgn()) {
    if (~p->sgn())
      (sgn() == p->sgn()) ? (p->rotate(), rotate()) : (rotate(), rotate());
    else
      rotate();
  }
}

node_t* node_t::expose() {
  node_t *u = this, *v;
  for (v = NULL; u; u = u->p) {
    u->splay();
    u->ch[1] = v;
    (v = u)->update();
  }
  return v;
}

inline void node_t::evert() {
  expose()->flip();
  splay();
}

void node_t::link(node_t *u) {
  evert();
  p = u;
  expose();
}

void node_t::cut(node_t *u) {
  evert();
  u->expose();
  u->splay();
  u->ch[0]->p = NULL;
  u->ch[0] = NULL;
  u->update();
}

int tree[N];

inline void add(int pos, int val) {
  for (; pos <= m; pos += pos & -pos) tree[pos] += val;
}

inline int query(int pos) {
  int res = 0;
  for (; pos; pos ^= pos & -pos) res += tree[pos];
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    static std::vector<int> event[N];
    static int q, l[N], r[N], u[N], v[N];
    scanf("%d%d%d", &n, &m, &q);
    for (int i = 1; i <= m; ++i) event[i].clear();
    for (int i = 1; i <= m; ++i) scanf("%d%d", u + i, v + i);
    for (int i = 1; i <= q; ++i) {
      scanf("%d%d", l + i, r + i);
      event[r[i]].push_back(i);
    }
    for (int i = 1; i <= n + m; ++i) pool[i].clear();
    std::fill(tree + 1, tree + m + 1, 0);
    static int ans[N];
    for (int i = 1; i <= m; ++i) {
      if (u[i] != v[i]) {
        node_t *a = pool + m + u[i], *b = pool + m + v[i], *c = pool + i;
        a->evert();
        b->expose();
        b->splay();
        if (~a->sgn()) {
          int id = b->min_timestamp;
          node_t *e = pool + id;
          node_t *x = pool + m + u[id], *y = pool + m + v[id];
          add(id, -1);
          x->cut(e);
          y->cut(e);
        }
        a->link(c);
        b->link(c);
        add(i, 1);
      }
      for (std::vector<int>::iterator it = event[i].begin(); it != event[i].end(); ++it) ans[*it] = n - (query(i) - query(l[*it] - 1));
    }
    for (int i = 1; i <= q; ++i) printf("%d\n", ans[i]);
  }
  return 0;
}
