#include <cstdio>
#include <cstring>
#include <vector>

typedef long long i64;

const int N = 1000 + 10, NOD = 1000000007;

int tcase, type;
int n, m, l, r;

inline int Power(int base, int exp) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) res = (i64)res * base % NOD;
    base = (i64)base * base % NOD;
  }
  return res;
}

std::vector<int> prime, factor[N];

inline void Preprocessing() {
  static bool flag[N];
  for (int i = 2; i < N; ++i) {
    if (!flag[i]) prime.push_back(i);
    for (int j = 0; j < prime.size() && i * prime[j] < N; ++j) {
      flag[i * prime[j]] = true;
      if (i % prime[j] == 0) break;
    }
  }
  factor[1].push_back(1);
  for (int t = 2; t < N; ++t) {
    int x = t;
    for (int i = 0; i < prime.size() && prime[i] <= x; ++i) {
      if (x % prime[i] == 0) {
        factor[t].push_back(1);
        for (; x % prime[i] == 0; x /= prime[i]) factor[t].back() *= prime[i];
      }
    }
  }
}

int Calc(int x) {
  int res = 0;
  for (int s = 0; s < (1 << factor[x].size()); ++s) {
    int cur = m;
    for (int t = s; t; t = (t - 1) & s) {
      int temp = 1;
      for (int i = 0; i < factor[x].size(); ++i) if (t >> i & 1) temp *= factor[x][i];
      cur += (__builtin_parity(t) ? -1 : 1) * (m / temp);
    }
    (res += (__builtin_parity(s) ? -1 : 1) * Power(cur, n)) %= NOD;
  }
  return (res + NOD) % NOD;
}

int Calc(int l, int r) {
  int res = 0;
  for (int i = l; i <= r; ++i) (res += Calc(i)) %= NOD;
  return res;
}

int main() {
  Preprocessing();
  scanf("%d", &tcase);
  while (tcase--) {
    scanf("%d%d%d%d", &n, &m, &l, &r);
    printf("%d\n", Calc(l, r));
  }
  return 0;
}
