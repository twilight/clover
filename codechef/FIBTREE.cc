#include <cstdio>
#include <vector>

typedef long long i64;

const int N = 100000 + 10, MOD = 1000000009;
const int S = 616991993;

struct Info  {
  int fst, snd;
  Info() { fst = snd = 0; }
  Info(int x) { fst = snd = x; }
  Info(int f, int s): fst(f), snd(s) {}
  inline Info& operator+= (const Info &rhs) {
    (fst += rhs.fst) %= MOD;
    (snd += rhs.snd) %= MOD;
    return *this;
  }
  inline Info& operator-= (const Info &rhs) {
    (fst += MOD - rhs.fst) %= MOD;
    (snd += MOD - rhs.snd) %= MOD;
    return *this;
  }
  inline Info& operator*= (const Info &rhs) {
    fst = (i64)fst * rhs.fst % MOD;
    snd = (i64)snd * rhs.snd % MOD;
    return *this;
  }
} zero, pdt[N], psum[N];

inline Info operator+ (Info lhs, const Info &rhs) {
  return lhs += rhs;
}

inline Info operator- (Info lhs, const Info &rhs) {
  return lhs -= rhs;
}

inline Info operator* (Info lhs, const Info &rhs) {
  return lhs *= rhs;
}

int power(int base, int exp) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) res = (i64)res * base % MOD;
    base = (i64)base * base % MOD;
  }
  return res;
}

inline Info calc(const Info &a, int cnt) {
  return a * psum[cnt - 1];
}

int n;
std::vector<int> adj[N];

int fa[N], dep[N], size[N], son[N], top[N], left[N], right[N];

void dfs(int a) {
  size[a] = 1;
  for (int i = 0; i < adj[a].size(); ++i) {
    int b = adj[a][i];
    if (b != fa[a]) {
      fa[b] = a;
      dep[b] = dep[a] + 1;
      dfs(b);
      size[a] += size[b];
      if (size[b] > size[son[a]]) son[a] = b;
    }
  }
}

void hld(int a, int t) {
  static int tot = 0;
  left[a] = ++tot;
  top[a] = t;
  if (son[a]) hld(son[a], t);
  for (int i = 0; i < adj[a].size(); ++i) {
    int b = adj[a][i];
    if (b != fa[a] && b != son[a]) hld(b, b);
  }
  right[a] = tot;
}

namespace pst {

const int SZ = 300 * N;

int tot, lch[SZ], rch[SZ];
Info tag[SZ], rtag[SZ], sum[SZ];

Info calc(Info t, int l, int r, int p, int q) {
  if (p > l) t *= pdt[p - l];
  return ::calc(t, std::min(r, q) - std::max(l, p) + 1);
}

Info rcalc(Info t, int l, int r, int p, int q) {
  if (q < r) t *= pdt[r - q];
  return ::calc(t, std::min(r, q) - std::max(l, p) + 1);
}

int add(int id, int l, int r, int p, int q, const Info &t) {
  int res = ++tot;
  tag[res] = tag[id], rtag[res] = rtag[id], sum[res] = sum[id];
  lch[res] = lch[id], rch[res] = rch[id];
  if (p <= l && r <= q) {
    tag[res] += t * pdt[l - p];
    return res;
  }
  sum[res] += calc(t, p, q, l, r);
  int mid = (l + r) / 2;
  if (p <= mid) lch[res] = add(lch[id], l, mid, p, q, t);
  if (q > mid) rch[res] = add(rch[id], mid + 1, r, p, q, t);
  return res;
}

int radd(int id, int l, int r, int p, int q, const Info &t) {
  int res = ++tot;
  tag[res] = tag[id], rtag[res] = rtag[id], sum[res] = sum[id];
  lch[res] = lch[id], rch[res] = rch[id];
  if (p <= l && r <= q) {
    rtag[res] += t * pdt[q - r];
    return res;
  }
  sum[res] += rcalc(t, p, q, l, r);
  int mid = (l + r) / 2;
  if (q > mid) rch[res] = radd(rch[id], mid + 1, r, p, q, t);
  if (p <= mid) lch[res] = radd(lch[id], l, mid, p, q, t);
  return res;
}

Info query(int id, int l, int r, int p, int q) {
  if (!id) return zero;
  Info res = calc(tag[id], l, r, p, q) + rcalc(rtag[id], l, r, p, q);
  if (p <= l && r <= q) return res + sum[id];
  int mid = (l + r) / 2;
  if (p <= mid) res += query(lch[id], l, mid, p, q);
  if (q > mid) res += query(rch[id], mid + 1, r, p, q);
  return res;
}

}

void select(int x, int y, std::vector<Info> &a, std::vector<Info> &b) {
  a.clear(), b.clear();
  while (top[x] != top[y]) {
    int p = top[x], q = top[y];
    if (dep[p] > dep[q]) {
      a.push_back(Info(x, p));
      x = fa[p];
    } else {
      b.push_back(Info(q, y));
      y = fa[q];
    }
  }
  (dep[x] > dep[y] ? a : b).push_back(Info(x, y));
}

int go(int x, int y) {
  while (top[x] != top[y]) {
    x = top[x];
    if (fa[x] == y) return x; else x = fa[x];
  }
  return son[y];
}

std::vector<Info> a, b;

int add(int id, int x, int y) {
  select(x, y, a, b);
  Info t = pdt[1];
  int res = id;
  for (int i = 0; i < a.size(); ++i) {
    int u = a[i].fst, v = a[i].snd;
    res = pst::radd(res, 1, n, left[v], left[u], t);
    t *= pdt[left[u] - left[v] + 1];
  }
  for (int i = b.size() - 1; i >= 0; --i) {
    int u = b[i].fst, v = b[i].snd;
    res = pst::add(res, 1, n, left[u], left[v], t);
    t *= pdt[left[v] - left[u] + 1];
  }
  return res;
}

Info query(int id, int x, int y) {
  select(x, y, a, b);
  Info res(0, 0);
  for (int i = 0; i < a.size(); ++i) res += pst::query(id, 1, n, left[a[i].snd], left[a[i].fst]);
  for (int i = 0; i < b.size(); ++i) res += pst::query(id, 1, n, left[b[i].fst], left[b[i].snd]);
  return res;
}

int main() {
  pdt[0].fst = pdt[0].snd = 1;
  pdt[1].fst = (1 + S) / 2, pdt[1].snd = (1 - S) / 2 + MOD;
  for (int i = 1; i < N; ++i) pdt[i] = pdt[i - 1] * pdt[1];
  psum[0] = pdt[0];
  for (int i = 1; i < N; ++i) psum[i] = psum[i - 1] + pdt[i];
  int q;
  scanf("%d%d", &n, &q);
  for (int i = n - 1; i--;) {
    int x, y;
    scanf("%d%d", &x, &y);
    adj[x].push_back(y);
    adj[y].push_back(x);
  }
  dep[1] = 1;
  dfs(1);
  hld(1, 1);
  static int root[N];
  for (int i = 1, ans = 0; i <= q; ++i) {
    static char op[10];
    int x, y;
    scanf(" %s%d", op, &x);
    x ^= ans;
    if (op[0] == 'R') {
      root[i] = root[x];
    } else {
      scanf("%d", &y);
      root[i] = root[i - 1];
      if (op[0] == 'A') {
        root[i] = add(root[i], x, y);
      } else {
        Info temp;
        if (op[1] == 'S') {
          if (x == y) {
            temp = pst::query(root[i], 1, n, 1, n);
          } else if (left[y] <= left[x] && left[x] <= right[y]) {
            int p = go(x, y);
            temp = pst::query(root[i], 1, n, 1, n) - pst::query(root[i], 1, n, left[p], right[p]);
          } else {
            temp = pst::query(root[i], 1, n, left[y], right[y]);
          }
        } else {
          temp = query(root[i], x, y);
        }
        printf("%d\n", ans = ((i64)temp.fst - temp.snd + MOD) * power(S, MOD - 2) % MOD);
      }
    }
  }
  return 0;
}
