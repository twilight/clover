#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>

typedef long long i64;

const int N = 100000 + 10;

int n, m;

std::vector<int> adj[N], pre[N];

int id[N], dfn[N], fa[N], idom[N], sdom[N], rdom[N], fdom[N], tot;

void dfs(int a) {
  id[dfn[a] = ++tot] = a;
  for (int i = 0; i < adj[a].size(); ++i) {
    int b = adj[a][i];
    if (!fa[b]) {
      fa[b] = a;
      dfs(b);
    }
  }
}

int anc[N], min[N];

inline void check(int &lhs, int rhs) {
  if (!rhs) return;
  if (dfn[sdom[rhs]] < dfn[sdom[lhs]]) lhs = rhs;
}

int find(int x) {
  if (anc[x] == x) {
    return x;
  } else {
    int y = find(anc[x]);
    check(min[x], min[anc[x]]);
    return anc[x] = y;
  }
}

void tarjan() {
  for (int i = 1; i <= n; ++i) anc[i] = i;
  static std::vector<int> query[N];
  for (int i = tot; i > 0; --i) {
    int a = id[i], temp = i;
    for (int j = 0; j < pre[a].size(); ++j) {
      int b = pre[a][j];
      if (!dfn[b]) continue;
      if (dfn[b] < dfn[a]) {
        temp = std::min(temp, dfn[b]);
      } else {
        find(b);
        temp = std::min(temp, dfn[sdom[min[b]]]);
      }
    }
    for (int j = 0; j < query[a].size(); ++j) {
      int b = query[a][j];
      find(b);
      rdom[b] = min[b];
    }
    if (a > 1) anc[a] = fa[a];
    min[a] = a;
    query[sdom[a] = id[temp]].push_back(a);
  }
  for (int i = 1; i <= tot; ++i) {
    int a = id[i];
    idom[a] = (sdom[a] != sdom[rdom[a]] ? idom[rdom[a]] : sdom[a]);
  }
  for (int i = 2; i <= tot; ++i) {
    int a = id[i];
    fdom[a] = (idom[a] > 1 ? fdom[idom[a]] : a);
  }
}

int main() {
  scanf("%d%d", &n, &m);
  while (m--) {
    int x, y;
    scanf("%d%d", &x, &y);
    adj[x].push_back(y);
    pre[y].push_back(x);
  }
  fa[1] = -1;
  dfs(1);
  tarjan();
  static int cnt[N];
  for (int i = 2; i <= n; ++i) if (dfn[i]) ++cnt[fdom[i]];
  i64 ans = 0;
  for (int i = 1; i <= n; ++i) if (cnt[i]) ans += (i64)cnt[i] * (tot - 1 - cnt[i]);
  printf("%lld\n", ans / 2 + tot - 1);
  return 0;
}
