#include <cstdio>
#include <algorithm>

const int N = 100000 + 10;

int n, l[N], r[N];

int row[N], pos[N];

struct Heap {
  Heap *lch, *rch;
  int val;
} *heap[N], pool[N];

Heap* merge(Heap *lhs, Heap *rhs) {
  if (!lhs) return rhs;
  if (!rhs) return lhs;
  if (r[lhs->val] > r[rhs->val]) std::swap(lhs, rhs);
  lhs->rch = merge(lhs->rch, rhs);
  std::swap(lhs->lch, lhs->rch);
  return lhs;
}

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d", &n);
    for (int i = 1; i <= n; ++i) {
      scanf("%d%d", l + i, r + i);
      pool[i].lch = pool[i].rch = NULL;
      pool[i].val = row[i] = pos[i] = i;
    }
    for (int i = 1; i <= n; ++i) heap[i] = NULL;
    for (int i = 1; i <= n; ++i) heap[l[i]] = merge(heap[l[i]], pool + i);
    bool ans = true;
    for (int i = 1; i <= n; ++i) {
      if (!heap[i]) goto draw;
      int e = heap[i]->val;
      if (r[e] < i) goto draw;
      if (row[i] != e) {
        int j = pos[e], t = row[i];
        row[i] = e, pos[e] = i;
        row[j] = t, pos[t] = j;
        ans ^= 1;
      }
      if (r[e] < n) heap[r[e] + 1] = merge(heap[r[e] + 1], merge(heap[i]->lch, heap[i]->rch));
    }
    puts(ans ? "Alex" : "Fedor");
    continue;
 draw:
    puts("Draw");
  }
  return 0;
}
