#include <cmath>
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>

const int M = 77 + 1, LIM = 2 * M, LOG = 22, N = 1 << LOG;
const double PI = acos(-1.0);

int n, q;
double a[N], b[N];

struct Complex {
  double real, imag;
  explicit Complex(double re = 0.0, double im = 0.0): real(re), imag(im) {}
  inline Complex& operator+= (const Complex &rhs) {
    real += rhs.real;
    imag += rhs.imag;
    return *this;
  }
  inline Complex& operator-= (const Complex &rhs) {
    real -= rhs.real;
    imag -= rhs.imag;
    return *this;
  }
  inline Complex& operator*= (const Complex &rhs) {
    double temp = real * rhs.real - imag * rhs.imag;
    imag = real * rhs.imag + imag * rhs.real;
    real = temp;
    return *this;
  }
  inline Complex conj() const { return Complex(real, -imag); }
};

inline Complex operator+ (const Complex &lhs, const Complex &rhs) {
  return Complex(lhs.real + rhs.real, lhs.imag + rhs.imag);
}

inline Complex operator- (const Complex &lhs, const Complex &rhs) {
  return Complex(lhs.real - rhs.real, lhs.imag - rhs.imag);
}

inline Complex operator* (const Complex &lhs, const Complex &rhs) {
  return Complex(lhs.real * rhs.real - lhs.imag * rhs.imag, lhs.real * rhs.imag + lhs.imag * rhs.real);
}

inline Complex sqr(const Complex &val) { return val * val; }

int pos(int a, int b, int c) {
  if (a < 0) a += M;
  if (b < 0) b += M;
  if (c < 0) c += M;
  return a * LIM * LIM + b * LIM + c;
}

void fft(Complex a[], int sw) {
  for (register int i = 0, j = 0; i < N; ++i) {
    if (i > j) std::swap(a[i], a[j]);
    for (register int k = N >> 1; (j ^= k) < k; k >>= 1) {}
  }
  for (register int m = 2; m <= N; m *= 2) {
    register int gap = m / 2;
    double arg = sw * 2.0 * PI / m;
    Complex w(cos(arg), sin(arg));
    for (register int i = 0; i < N; i += m) {
      Complex o(1.0, 0.0);
      for (register int j = i; j < i + gap; ++j, o *= w) {
        Complex v = o * a[j + gap];
        a[j + gap] = a[j] - v;
        a[j] += v;
      }
    }
  }
  if (sw < 0) for (register int i = 0; i < N; ++i) a[i].real /= N, a[i].imag /= N;
}

void convolution(double lhs[], const double rhs[]) {
  static Complex res[N], temp[N + 1];
  for (int i = 0; i < N; ++i) temp[i] = Complex(lhs[i], rhs[i]);
  fft(temp, 1);
  Complex k(0.0, 0.25);
  temp[N] = temp[0];
  for (int i = 0; i < N; ++i) res[i] = (sqr(temp[N - i].conj()) - sqr(temp[i])) * k;
  fft(res, -1);
  for (int i = 0; i < N; ++i) lhs[i] = res[i].real;
}

inline int quad(int x) { return x * x * x * x; }

int nextInt() {
  static char ch;
  while (!isdigit(ch = getchar_unlocked())) {}
  int res = ch - '0';
  while (isdigit(ch = getchar_unlocked())) res = res * 10 + ch - '0';
  return res;
}

int main() {
  n = nextInt(), q = nextInt();
  for (register int i = n; i--;) {
    register int x = nextInt(), y = nextInt(), z = nextInt();
    a[pos(x, y, z)] += 1.0;
    b[pos(-x, -y, -z)] += 1.0;
  }
  convolution(a, b);
  static double denom[N];
  double *it = denom;
  for (register int i = -M; i < M; ++i)
    for (register int j = -M; j < M; ++j)
      for (register int k = -M; k < M; ++k)
        *it++ = 1.0 / sqrt(quad(i) + quad(j) + quad(k));
  denom[pos(M, M, M)] = 0.0;
  while (q--) {
    register int a = nextInt(), b = nextInt(), c = nextInt(), d = nextInt();
    register double ans = 0, *it = denom;
    register double *p = ::a;
    for (register int i = -M; i < M; ++i)
      for (register int j = -M; j < M; ++j)
        for (register int k = -M; k < M; ++k)
          ans += *p++ * abs(a * i + b * j + c * k + d) * *it++;
    ans /= n * (n - 1.0);
    printf("%.10f\n", ans);
  }
  return 0;
}
