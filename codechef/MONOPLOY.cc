#include <cstdio>
#include <cstring>
#include <vector>

typedef long long i64;

const int N = 100000 + 10;

int n;
std::vector<int> adj[N];

struct Node {
  Node *p, *ch[2];
  inline int dir();
  void Rotate();
  void Splay();
} pool[N];

int left[N], right[N], tot = 0;

inline int pos(int l, int r) { return (l + r) | (l != r); }

i64 tag[2 * N], sum[2 * N];

void Add(int l, int r, int p, int q, int val) {
  if (l > q || r < p) return;
  int id = pos(l, r);
  if (p <= l && r <= q) {
    tag[id] += val;
    sum[id] += (i64)val * (r - l + 1);
    return;
  }
  int mid = (l + r) / 2;
  int lch = pos(l, mid), rch = pos(mid + 1, r);
  if (tag[id]) {
    tag[lch] += tag[id];
    tag[rch] += tag[id];
    sum[lch] += (i64)tag[id] * (mid - l + 1);
    sum[rch] += (i64)tag[id] * (r - mid);
    tag[id] = 0;
  }
  Add(l, mid, p, q, val);
  Add(mid + 1, r, p, q, val);
  sum[id] = sum[lch] + sum[rch];
}

i64 Query(int l, int r, int p, int q) {
  if (l > q || r < p) return 0;
  int id = pos(l, r);
  if (p <= l && r <= q) return sum[id];
  int mid = (l + r) / 2;
  int lch = pos(l, mid), rch = pos(mid + 1, r);
  if (tag[id]) {
    tag[lch] += tag[id];
    tag[rch] += tag[id];
    sum[lch] += (i64)tag[id] * (mid - l + 1);
    sum[rch] += (i64)tag[id] * (r - mid);
    tag[id] = 0;
  }
  i64 res = Query(l, mid, p, q) + Query(mid + 1, r, p, q);
  sum[id] = sum[lch] + sum[rch];
  return res;
}

inline int Node::dir() {
  if (!p || (this != p->ch[0] && this != p->ch[1])) return -1;
  return this == p->ch[1];
}

void Node::Rotate() {
  Node *u = p;
  int d = dir();
  if (~u->dir()) u->p->ch[u->dir()] = this;
  p = u->p;
  u->ch[d] = ch[!d];
  if (ch[!d]) ch[!d]->p = u;
  ch[!d] = u;
  u->p = this;
}

void Node::Splay() {
  while (~dir()) {
    if (~p->dir()) (dir() == p->dir() ? p : this)->Rotate();
    Rotate();
  }
}

Node *Expose(Node *u) {
  Node *v;
  for (v = NULL; u; v = u, u = u->p) {
    u->Splay();
    Node *w = u->ch[1];
    if (w) {
      while (w->ch[0]) w = w->ch[0];
      Add(1, n, left[w - pool], right[w - pool], 1);
    }
    u->ch[1] = v;
    if (v) {
      while (v->ch[0]) v = v->ch[0];
      Add(1, n, left[v - pool], right[v - pool], -1);
    }
  }
  return v;
}

void DFS(int a, int fa = -1, int dep = 0) {
  left[a] = ++tot;
  for (std::vector<int>::iterator it = adj[a].begin(); it != adj[a].end(); ++it) {
    int b = *it;
    if (b != fa) {
      pool[b].p = pool + a;
      DFS(b, a, dep + 1);
    }
  }
  right[a] = tot;
  Add(1, n, left[a], left[a], dep);
}

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d", &n);
    for (int i = 0; i < n; ++i) adj[i].clear();
    memset(sum, 0, sizeof sum);
    memset(tag, 0, sizeof tag);
    memset(pool, 0, sizeof pool);
    for (int i = n - 1; i--;) {
      int u, v;
      scanf("%d%d", &u, &v);
      adj[u].push_back(v);
      adj[v].push_back(u);
    }
    tot = 0;
    DFS(0);
    int q;
    for (scanf("%d", &q); q--;) {
      char op;
      int u;
      scanf(" %c%d", &op, &u);
      switch (op) {
        case 'O':
          Expose(pool + u);
          break;
        case 'q':
          printf("%.10f\n", (double)Query(1, n, left[u], right[u]) / (right[u] - left[u] + 1));
          break;
      }
    }
  }
  return 0;
}
