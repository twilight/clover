#include <cmath>
#include <cstdio>

typedef long long i64;

const int LOG = 18, N = 1 << LOG, MOD = 99991;
const int s = 89887, alpha = (1 + s) / 2, beta = ((1 - s) / 2 + MOD) % MOD;

inline int lg2(int x) { return 31 - __builtin_clz(x); }

int n, k, a[N];

struct Complex {
  double x, y;
  Complex(double _x = 0.0, double _y = 0.0): x(_x), y(_y) {}
  inline Complex& operator+= (const Complex &rhs) {
    x += rhs.x;
    y += rhs.y;
    return *this;
  }
  inline Complex& operator-= (const Complex &rhs) {
    x -= rhs.x;
    y -= rhs.y;
    return *this;
  }
  inline Complex& operator*= (const Complex &rhs) {
    double u = x * rhs.x - y * rhs.y, v = x * rhs.y + y * rhs.x;
    x = u, y = v;
    return *this;
  }
  inline Complex& operator/= (double rhs) {
    x /= rhs;
    y /= rhs;
    return *this;
  }
};

inline Complex operator+ (Complex lhs, const Complex &rhs) { return lhs += rhs; }
inline Complex operator- (Complex lhs, const Complex &rhs) { return lhs -= rhs; }
inline Complex operator* (Complex lhs, const Complex &rhs) { return lhs *= rhs; }

void FFT(Complex a[], int len, int sw) {
  static Complex temp[N];
  for (int i = 0; i < len; ++i) {
    int k = 0;
    for (int cnt = lg2(len), j = i; cnt--; j >>= 1) (k <<= 1) |= (j & 1);
    temp[k] = a[i];
  }
  for (int m = 2; m <= len; m *= 2) {
    int gap = m / 2;
    for (int i = 0; i < gap; ++i) {
      double arg = sw * i * 2.0 * M_PI / m;
      Complex w(cos(arg), sin(arg));
      for (int j = i; j < len; j += m) {
        Complex u(temp[j]), v(temp[j + gap]);
        temp[j] = u + w * v;
        temp[j + gap] = u - w * v;
      }
    }
  }
  for (int i = 0; i < len; ++i) a[i] = temp[i];
  if (sw == -1) for (int i = 0; i < len; ++i) a[i] /= len;
}

void Convolution(int res[], const int lhs[], const int rhs[], int len) {
  static Complex u[N], v[N];
  for (int i = 0; i < len; ++i) u[i] = v[i] = 0.0;
  for (int i = 0; i < len / 2; ++i) u[i] = lhs[i], v[i] = rhs[i];
  FFT(u, len, 1);
  FFT(v, len, 1);
  for (int i = 0; i < len; ++i) u[i] *= v[i];
  FFT(u, len, -1);
  for (int i = 0; i < len; ++i) res[i] = (i64)(u[i].x + 0.5) % MOD;
}

inline int Pow(int base, int exp) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) res = (i64)res * base % MOD;
    base = (i64)base * base % MOD;
  }
  return res;
}

void Solve(int res[], int len, int l, int r, const int v) {
  if (l == r) {
    res[0] = 1;
    res[1] = Pow(v, a[l]);
    return;
  }
  int mid = (l + r) / 2;
  Solve(res, len / 2, l, mid, v);
  Solve(res + len / 2, len / 2, mid + 1, r, v);
  static int temp[N];
  Convolution(temp, res, res + len / 2, len);
  for (int i = 0; i < len; ++i) res[i] = temp[i];
}

int main() {
  scanf("%d%d", &n, &k);
  for (int i = 1; i <= n; ++i) scanf("%d", a + i);
  static int fib[N], temp[2][N];
  fib[1] = fib[2] = 1;
  for (int i = 3; i < MOD; ++i) fib[i] = (fib[i - 1] + fib[i - 2]) % MOD;
  Solve(temp[0], N, 1, n, alpha);
  Solve(temp[1], N, 1, n, beta);
  printf("%lld\n", (((i64)(temp[0][k] - temp[1][k]) * Pow(s, MOD - 2) % MOD) + MOD) % MOD);
  return 0;
}
