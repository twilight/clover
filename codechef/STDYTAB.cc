#include <cstdio>

typedef long long i64;

const int N = 4000 + 10, MOD = 1000000000;

int n, m, C[N][N];

int main() {
  for (int i = 0; i < N; ++i) {
    C[i][0] = 1;
    for (int j = 1; j <= i; ++j) C[i][j] = (C[i - 1][j] + C[i - 1][j - 1]) % MOD;
  }
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d", &n, &m);
    static int f[N][N];
    f[0][0] = 1;
    for (int i = 1; i <= n; ++i) {
      static int sum[N];
      for (int j = 0; j <= m; ++j) {
        sum[j] = f[i - 1][j];
        if (j) (sum[j] += sum[j - 1]) %= MOD;
        f[i][j] = (i64)C[j + m - 1][j] * sum[j] % MOD;
      }
    }
    int ans = 0;
    for (int i = 0; i <= m; ++i) (ans += f[n][i]) %= MOD;
    printf("%d\n", ans);
  }
  return 0;
}
