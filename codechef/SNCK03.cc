#include <bits/stdc++.h>

typedef long long int64;

const int N = 20000 + 10, K = 25, MOD = 1000000007;

inline void INC(int &a, int b) { a = (a + b) % MOD; }
inline void MUL(int &a, int b) { a = (int64)a * b % MOD; }
inline int pdt(int a, int b) { return (int64)a * b % MOD; }

inline int RD() {
  static char ch;
  while (!isdigit(ch = getchar())) {}
  int res = ch - '0';
  while (isdigit(ch = getchar())) res = res * 10 + ch - '0';
  return res;
}

int n, m;
int a[N];

int dp[K][K][N];

inline int pow(int base, int exp) {
  int res = 1;
  while (exp) {
    if (exp & 1) MUL(res, base);
    MUL(base, base);
    exp >>= 1;
  }
  return res;
}

int fact[N], inv[N];

inline int cal(int n, int m) { return pdt(fact[n], pdt(inv[m], inv[n - m])); }

int solve(int l, int r, int k) {
  if (l + 1 == r) return 1;
  int &res = dp[l][r][k];
  if (~res) return res;
  res = 0;
  int cur = a[r] - a[l] - k - (l > 1) - (r < m);
  for (int mid = l + 1 + ((l ^ r) & 1); mid < r; mid += 2) {
    int tmp = cal(cur, a[mid] - a[l] - k - (l > 1));
    MUL(tmp, solve(l, mid, k));
    MUL(tmp, solve(mid, r, 0));
    INC(res, tmp);
  }
  if (((l ^ r) & 1) && (a[l] + k + (l > 1) < a[l + 1])) INC(res, solve(l, r, k + 1));
  return res;
}

int main() {
  fact[0] = inv[0] = 1;
  for (int i = 1; i < N; ++i) {
    fact[i] = pdt(fact[i - 1], i);
    inv[i] = pow(fact[i], MOD - 2);
  }
  for (int tcase = RD(); tcase--;) {
    n = RD(), m = RD();
    for (int i = 1; i <= m; ++i) a[i] = RD();
    for (int i = 1; i <= m; ++i)
      for (int j = 1; j <= m; ++j)
        for (int k = 0; k <= n; ++k)
          dp[i][j][k] = -1;
    printf("%d\n", solve(1, m, 0));
  }
  return 0;
}
