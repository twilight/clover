#include <cmath>
#include <cstdio>
#include <algorithm>

typedef unsigned long long u64;

const int N = 100000 + 10, S = 400;

int n, a[N], s;

int l[N], r[N];

namespace BIT {

u64 sum[N];

inline void add(int p, int v) {
  for (; p <= n; p += p & -p) sum[p] += v;
}

inline u64 query(int p) {
  u64 res = 0;
  for (; p; p ^= p & -p) res += sum[p];
  return res;
}

}

u64 sum[S];
int coef[S][N];

void preprocess() {
  for (int i = 1; i <= n; ++i) BIT::add(i, a[i]);
  static u64 temp[N];
  temp[0] = 0;
  for (int i = 1; i <= n; ++i) temp[i] = temp[i - 1] + a[i];
  for (int i = 0; i < n; ++i) {
    int p = i / s;
    sum[p] += temp[r[i]] - temp[l[i] - 1];
    ++coef[p][r[i]], --coef[p][l[i] - 1];
  }
  for (int i = 0; i * s < n; ++i)
    for (int j = n - 1; j >= 0; --j)
      coef[i][j] += coef[i][j + 1];
}

u64 query(int i, int x, int y) {
  int p = i * s, q = std::min(p + s, n);
  if (x <= p && q <= y) return sum[i];
  u64 res = 0;
  for (int j = std::max(p, x), k = std::min(q, y); j < k; ++j) res += BIT::query(r[j]) - BIT::query(l[j] - 1);
  return res;
}

int main() {
  scanf("%d", &n);
  s = std::min<int>(sqrt(12.0 * n / log2(n)), n);
  for (int i = 1; i <= n; ++i) scanf("%d", a + i);
  for (int i = 0; i < n; ++i) scanf("%d%d", l + i, r + i);
  preprocess();
  int q;
  for (scanf("%d", &q); q--;) {
    int op, x, y;
    scanf("%d%d%d", &op, &x, &y);
    if (op == 1) {
      BIT::add(x, y - a[x]);
      for (int i = 0; i * s < n; ++i) sum[i] += (u64)coef[i][x] * (y - a[x]);
      a[x] = y;
    } else {
      --x;
      u64 ans = 0;
      for (int i = x / s, r = y / s; i <= r; ++i) ans += query(i, x, y);
      printf("%llu\n", ans);
    }
  }
  return 0;
}
