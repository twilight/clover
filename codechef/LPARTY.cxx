#include <cctype>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <unordered_map>

#define fst first
#define snd second

typedef std::pair<unsigned, int> Info;

const int N = 5, M = 100, INF = 1 << 27;

int n, m, tot, ext;

unsigned target;

bool flag[1 << N][1 << N];

std::vector<Info> info;
unsigned suf[M];

void implicant() {
  info.clear();
  static int use[M], cap[M];
  for (int s = tot; s >= 0; --s) {
    int c = __builtin_popcount(s);
    int t = s;
    do {
      if (!flag[s][t]) continue;
      bool cur = true;
      for (int i = 0; i < n; ++i) {
        if (!(s >> i & 1)) continue;
        if (flag[s][t ^ (1 << i)]) {
          cur = false;
          flag[s ^ (1 << i)][t & ~(1 << i)] = true;
        }
      }
      if (cur) {
        if (c == n) {
          ext += c;
          target ^= 1U << t;
        } else {
          use[info.size()] = s, cap[info.size()] = t;
          info.push_back(Info(0, c));
        }
      }
    } while ((t = (t - 1) & s) != s);
  }
  for (int s = 0; s <= tot; ++s)
    if (target >> s & 1)
      for (int i = 0; i < info.size(); ++i)
        if ((s & use[i]) == cap[i]) info[i].fst |= (1U << s);
  std::sort(info.begin(), info.end());
}

int best;

std::unordered_map<unsigned, int> mem[M];

int dfs(int dep, unsigned rem, int cur) {
  if (cur >= best) return INF;
  if (!rem) {
    best = cur;
    return 0;
  }
  if ((suf[dep] & rem) != rem) return INF;
  if (mem[dep].find(rem) != mem[dep].end()) return mem[dep][rem];
  int res = dfs(dep + 1, rem, cur);
  if (info[dep].fst & rem) res = std::min(res, dfs(dep + 1, rem & ~info[dep].fst, cur + info[dep].snd) + info[dep].snd);
  if (res < INF) mem[dep][rem] = res;
  return res;
}

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d", &n, &m);
    tot = (1 << n) - 1;
    target = 0;
    for (int i = 0; i <= tot; ++i)
      for (int j = 0; j <= tot; ++j)
        flag[i][j] = false;
    for (int i = 0; i < m; ++i) {
      static char s[10];
      scanf(" %s", s);
      int cur = 0;
      for (int j = 0; j < n; ++j) if (isupper(s[j])) cur |= 1 << (s[j] - 'A');
      target |= 1U << cur;
      flag[tot][cur] = true;
    }
    ext = 0;
    implicant();
    suf[info.size()] = 0;
    for (int i = 0; i < info.size(); ++i) suf[i] = info[i].fst;
    for (int i = info.size() - 1; i >= 0; --i) suf[i] |= suf[i + 1];
    for (int i = 0; i < info.size(); ++i) mem[i].clear();
    best = INF;
    printf("%d\n", dfs(0, target, 0) + ext);
  }
  return 0;
}
