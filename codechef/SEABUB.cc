#include <cstdio>
#include <algorithm>

typedef long long i64;
typedef long double f128;

const int N = 100 + 10;

int n, a[N];
i64 m;

int tree[N];

inline void Add(int p) {
  for (; p <= n; p += p & -p) ++tree[p];
}

inline int Query(int p) {
  int res = 0;
  for (; p; p ^= p & -p) res += tree[p];
  return res;
}

int Count() {
  int res = 0;
  std::fill(tree + 1, tree + n + 1, 0);
  for (int i = n; i > 0; --i) {
    res += Query(a[i]);
    Add(a[i]);
  }
  return res;
}

f128 fact[N];
f128 cnt[N][N * N], f[N][N * N];

void Preprocessing() {
  fact[0] = 1.0;
  for (int i = 1; i < N; ++i) fact[i] = fact[i - 1] * i;
  cnt[1][0] = 1.0;
  for (int i = 2; i < N; ++i) {
    int tot = i * (i - 1) / 2;
    int l = -i, r = -1;
    f128 sum = 0.0;
    for (int j = 0; j <= tot; ++j) {
      if (l >= 0) sum -= cnt[i - 1][l];
      ++l;
      if (++r <= (i - 1) * (i - 2) / 2) sum += cnt[i - 1][r];
      cnt[i][j] = sum;
    }
  }
  for (int i = 1; i < N; ++i) {
    int tot = i * (i - 1) / 2;
    static f128 sum[2][N * N];
    for (int j = 0; j <= tot; ++j) {
      sum[0][j] = cnt[i][j];
      sum[1][j] = j * cnt[i][j];
      if (j) {
        sum[0][j] += sum[0][j - 1];
        sum[1][j] += sum[1][j - 1];
      }
    }
    f[i][0] = sum[1][tot] / fact[i];
    for (int j = 1; j <= tot; ++j) {
      int a = j, b = std::min<int>(f[i][j - 1] + j, tot);
      int p = std::max(j, (int)std::min<f128>(f[i][j - 1] + j, tot));
      if (a < b) f[i][j] += (sum[1][b] - sum[1][a]) - a * (sum[0][b] - sum[0][a]);
      if (b < tot) f[i][j] += (sum[0][tot] - sum[0][b]) * f[i][j - 1];
      f[i][j] /= fact[i];
    }
  }
}

int main() {
  Preprocessing();
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%lld", &n, &m);
    for (int i = 1; i <= n; ++i) scanf("%d", a + i);
    int c = Count();
    f128 ans = std::max<f128>(c - m, 0);
    ans = std::min(ans, (m ? f[n][std::min<i64>(m - 1, n * (n - 1) / 2)] : 1e100));
    printf("%.10Lf\n", ans);
  }
  return 0;
}
