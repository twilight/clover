#include <cstdio>
#include <cstring>
#include <algorithm>

typedef long long i64;

const int F = 1000 + 10, MOD = 2003;

int n, k, l, f;

int Power(int base, int exp) {
  base %= MOD;
  exp %= (MOD - 1);
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) (res *= base) %= MOD;
    (base *= base) %= MOD;
  }
  return res;
}

int inv[MOD + 1];
int val[MOD + 1];

int mem[F][F];

void StirlingS2() {
  for (int i = 1; i < F; ++i) {
    mem[i][1] = 1;
    for (int j = 2; j <= i; ++j)
      mem[i][j] = (mem[i - 1][j] * j + mem[i - 1][j - 1]) % MOD;
  }
}

void Convolution(int lhs[], const int rhs[]) {
  static int res[MOD + 1];
  memset(res, 0, sizeof res);
  for (int i = 0; i <= MOD; ++i)
    for (int j = 0; i + j <= MOD; ++j)
      (res[i + j] += lhs[i] * rhs[j]) %= MOD;
  memcpy(lhs, res, sizeof res);
}

void Preprocessing() {
  inv[0] = 1, inv[1] = Power(k, MOD - 2);
  for (int i = 2; i <= MOD; ++i) inv[i] = inv[i - 1] * inv[1] % MOD;
  static int base[MOD + 1];
  memset(val, 0, sizeof val);
  val[0] = 1;
  memset(base, 0, sizeof base);
  for (int i = 0; i <= f; ++i) base[i] = mem[f][i];
  for (int exp = l; exp; exp >>= 1) {
    if (exp & 1) Convolution(val, base);
    Convolution(base, base);
  }
}

int main() {
  int tcase;
  StirlingS2();
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d%d%d", &n, &k, &l, &f);
    Preprocessing();
    int ans = 0, pal = 1;
    for (int i = 0; i < l; ++i) (pal *= (n - i) % MOD) %= MOD;
    for (int p = l; pal && p <= l * f; ++p) {
      (ans += pal * val[p] % MOD * inv[p] % MOD) %= MOD;
      (pal *= (n - p) % MOD) %= MOD;
    }
    printf("%d\n", ans);
  }
  return 0;
}
