#include <cstdio>
#include <cstring>
#include <algorithm>

const int N = 100000 + 10;

typedef long long i64;

struct Point {
  int x, y;
  Point(int _x = 0, int _y = 0): x(_x), y(_y) {}
};

inline i64 Cross(const Point &a, const Point &b) {
  return (i64)a.x * b.y - (i64)a.y * b.x;
}

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    int n;
    scanf("%d", &n);
    static i64 area[N];
    memset(area, 0, sizeof area);
    for (int i = 1; i <= n; ++i) {
      int m;
      static Point point[N];
      scanf("%d", &m);
      for (int j = 1; j <= m; ++j) scanf("%d%d", &point[j].x, &point[j].y);
      for (int j = 1; j <= m; ++j) area[i] += Cross(point[j], point[j % m + 1]);
      if (area[i] < 0) area[i] = -area[i];
    }
    static int order[N];
    for (int i = 1; i <= n; ++i) order[i] = i;
    std::sort(order + 1, order + n + 1, [&] (int a, int b) { return area[a] < area[b]; });
    static int rank[N];
    for (int i = 1; i <= n; ++i) rank[order[i]] = i;
    for (int i = 1; i <= n; ++i) printf("%d ", rank[i] - 1);
    putchar('\n');
  }
  return 0;
}
