#include <cmath>
#include <cctype>
#include <cstdio>
#include <cstring>
#include <algorithm>

typedef long long i64;

const int LOG = 16, SZ = 1 << LOG, N = 100000 + 10;

int read() {
  static char ch;
  while (!isdigit(ch = getchar())) {}
  int res = ch - '0';
  while (isdigit(ch = getchar())) res = res * 10 + ch - '0';
  return res;
}

struct Complex {
  double real, imag;
  Complex(double re = 0.0, double im = 0.0): real(re), imag(im) {}
};

inline Complex operator+ (const Complex &lhs, const Complex &rhs) {
  return Complex(lhs.real + rhs.real, lhs.imag + rhs.imag);
}

inline Complex operator- (const Complex &lhs, const Complex &rhs) {
  return Complex(lhs.real - rhs.real, lhs.imag - rhs.imag);
}

inline Complex operator* (const Complex &lhs, const Complex &rhs) {
  return Complex(lhs.real * rhs.real - lhs.imag * rhs.imag, lhs.real * rhs.imag + lhs.imag * rhs.real);
}

int n, a[N];

int rev[SZ];
Complex w[SZ + 1];

void fft(Complex a[], int sw = 1) {
  for (register int i = 0; i < SZ; ++i) if (i < rev[i]) std::swap(a[i], a[rev[i]]);
  for (register int m = 2; m <= SZ; m *= 2) {
    register int gap = m / 2, layer = SZ / m;
    for (register int i = 0; i < gap; ++i) {
      Complex o(w[sw > 0 ? i * layer : SZ - i * layer]);
      for (register int j = i; j < SZ; j += m) {
        Complex u(a[j]), v(o * a[j + gap]);
        a[j] = u + v;
        a[j + gap] = u - v;
      }
    }
  }
  if (sw == -1) for (register int i = 0; i < SZ; ++i) a[i].real /= SZ;
}

void convolution(const int a[], const int b[], i64 c[]) {
  static Complex u[SZ], v[SZ];
  for (register int i = 0; i < SZ; ++i) u[i].real = a[i], u[i].imag = 0.0;
  for (register int i = 0; i < SZ; ++i) v[i].real = b[i], v[i].imag = 0.0;
  fft(u);
  fft(v);
  for (register int i = 0; i < SZ; ++i) u[i] = u[i] * v[i];
  fft(u, -1);
  for (register int i = 0; i < SZ; ++i) c[i] = u[i].real + 0.5;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  for (int i = 0; i <= SZ; ++i) w[i] = Complex(cos(2.0 * M_PI * i / SZ), sin(2.0 * M_PI * i / SZ));
  for (int i = 0; i < SZ; ++i) {
    rev[i] = 0;
    for (int j = LOG, k = i; j--; k >>= 1) (rev[i] <<= 1) |= k & 1;
  }
  n = read();
  for (int i = 0; i < n; ++i) a[i] = read();
  i64 ans = 0;
  static int l[SZ], r[SZ];
  for (int i = 0; i < n; ++i) ++r[a[i]];
  for (int i = 0, size = 3000; i < n; i += size) {
    int j = std::min(i + size, n);
    for (int k = i; k < j; ++k) --r[a[k]];
    static i64 res[SZ];
    convolution(l, r, res);
    for (register int k = i; k < j; ++k) ans += res[2 * a[k]];
    for (register int x = i; x < j; ++x) {
      for (register int y = x + 1; y < j; ++y) {
        if (2 * a[x] - a[y] >= 0) ans += l[2 * a[x] - a[y]];
        if (2 * a[y] - a[x] >= 0) ans += r[2 * a[y] - a[x]];
      }
      ++l[a[x]];
    }
  }
  printf("%lld\n", ans);
  return 0;
}
