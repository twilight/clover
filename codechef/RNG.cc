#include <cstdio>
#include <cstring>
#include <algorithm>

typedef long long i64;

const int LOG = 16, N = 1 << LOG;
const int MOD = 104857601, G = 3;

int k;

int power(int base, int exp) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) res = (i64)res * base % MOD;
    base = (i64)base * base % MOD;
  }
  return res;
}

namespace Poly {

int w[N + 1], rev[N];

void preprocess() {
  for (int i = 0; i < N; ++i) {
    int &cur = rev[i] = 0;
    for (int j = i, k = LOG; k--; j >>= 1) (cur <<= 1) |= (j & 1);
  }
  w[0] = 1;
  w[1] = power(G, (MOD - 1) / N);
  for (int i = 2; i <= N; ++i) w[i] = (i64)w[i - 1] * w[1] % MOD;
}

void ntt(int a[], int len, int sig) {
  for (int i = 0, t = 31 - __builtin_clz(len); i < len; ++i) {
    int j = rev[i << (LOG - t)];
    if (j < i) std::swap(a[i], a[j]);
  }
  for (int m = 2; m <= len; m *= 2) {
    int gap = m / 2, layer = N / m;
    for (int i = 0; i < gap; ++i) {
      int o = w[sig > 0 ? i * layer : (N - i * layer)];
      for (int j = i; j < len; j += m) {
        int u = a[j], v = (i64)o * a[j + gap] % MOD;
        a[j] = (u + v) % MOD;
        a[j + gap] = (u - v + MOD) % MOD;
      }
    }
  }
  if (sig < 0) {
    int inv = power(len, MOD - 2);
    for (int i = 0; i < len; ++i) a[i] = (i64)a[i] * inv % MOD;
  }
}

void inv(int a[], int len) {
  static int res[N];
  memset(res, 0, sizeof res);
  res[0] = power(a[0], MOD - 2);
  for (int l = 2;; l *= 2) {
    static int temp[N];
    int t = 2 * l;
    memcpy(temp, a, l * sizeof(int));
    memset(temp + l, 0, l * sizeof(int));
    ntt(temp, t, 1);
    ntt(res, t, 1);
    for (int i = 0; i < t; ++i) res[i] = (i64)res[i] * (2LL - (i64)temp[i] * res[i] % MOD + MOD) % MOD;
    ntt(res, t, -1);
    memset(res + l, 0, l * sizeof(int));
    if (l >= len) break;
  }
  memcpy(a, res, len * sizeof(int));
}

int mod[N], nirmod[N], nmod[N];

void modulo(int val[]) {
  static int b[N], d[N];
  memset(b, 0, sizeof b);
  std::reverse_copy(val, val + 2 * k + 1, b);
  for (int i = k + 1; i < N; ++i) b[i] = 0;
  ntt(b, N, 1);
  for (int i = 0; i < N; ++i) d[i] = (i64)b[i] * nirmod[i] % MOD;
  ntt(d, N, -1);
  for (int i = k + 1; i < N; ++i) d[i] = 0;
  std::reverse(d, d + k + 1);
  ntt(d, N, 1);
  for (int i = 0; i < N; ++i) d[i] = (i64)d[i] * nmod[i] % MOD;
  ntt(d, N, -1);
  for (int i = 0; i <= 2 * k; ++i) val[i] = (val[i] - d[i] + MOD) % MOD;
}

void power(int base[], i64 exp) {
  static int res[N];
  memset(res, 0, sizeof res);
  res[0] = 1;
  memset(nirmod, 0, sizeof nirmod);
  std::reverse_copy(mod, mod + k + 1, nirmod);
  inv(nirmod, k + 1);
  for (int i = k + 1; i < N; ++i) nirmod[i] = 0;
  ntt(nirmod, N, 1);
  memcpy(nmod, mod, sizeof mod);
  ntt(nmod, N, 1);
  for (; exp; exp >>= 1) {
    ntt(base, N, 1);
    if (exp & 1) {
      ntt(res, N, 1);
      for (int i = 0; i < N; ++i) res[i] = (i64)res[i] * base[i] % MOD;
      ntt(res, N, -1);
      modulo(res);
    }
    for (int i = 0; i < N; ++i) base[i] = (i64)base[i] * base[i] % MOD;
    ntt(base, N, -1);
    modulo(base);
  }
  memcpy(base, res, sizeof res);
}

}

i64 n;
int a[N], c[N];

int main() {
  Poly::preprocess();
  scanf("%d%lld", &k, &n);
  for (int i = 0; i < k; ++i) scanf("%d", a + i);
  for (int i = 0; i < k; ++i) scanf("%d", c + i);
  for (int i = 0; i < k; ++i) Poly::mod[k - 1 - i] = MOD - c[i];
  Poly::mod[k] = 1;
  static int poly[N];
  poly[1] = 1;
  Poly::power(poly, --n);
  int ans = 0;
  for (int i = 0; i < k; ++i) ans = (ans + (i64)poly[i] * a[i] % MOD) % MOD;
  printf("%d\n", ans);
  return 0;
}
