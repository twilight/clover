#include <cstdio>
#include <algorithm>

typedef long long i64;

const int N = 1100000, MOD = 998244353;
const int P = 50 + 10, M = 35000 + 10;

int n, p, m;

int power(int base, int exp) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) res = (i64)res * base % MOD;
    base = (i64)base * base % MOD;
  }
  return res;
}

#define check(x) {        \
  if (x >= MOD) x -= MOD; \
}

namespace NTT {

int n;
int w[N + 1], rev[N];

int inv;

void preprocessing(int len) {
  for (n = 1; n <= 2 * len;) n *= 2;
  w[0] = 1;
  w[1] = power(3, (MOD - 1) / n);
  for (int i = 2; i <= n; ++i) w[i] = (i64)w[i - 1] * w[1] % MOD;
  for (int i = 0, c = 31 - __builtin_clz(n); i < n; ++i) {
    rev[i] = 0;
    for (int j = i, k = c; k--; j >>= 1) (rev[i] <<= 1) |= (j & 1);
  }
  inv = power(n, MOD - 2);
}

void ntt(int a[], bool sig = false) {
  static int temp[N];
  for (int i = 0; i < n; ++i) temp[rev[i]] = a[i];
  for (int m = 2; m <= n; m *= 2) {
    int gap = m / 2, layer = n / m;
    for (int i = 0; i < gap; ++i) {
      int o = w[sig ? i * layer : (n - i * layer)];
      for (int j = i; j < n; j += m) {
        int u = temp[j], v = (i64)o * temp[j + gap] % MOD;
        temp[j] = (u + v) % MOD;
        temp[j + gap] = (u - v + MOD) % MOD;
      }
    }
  }
  for (int i = 0; i < n; ++i) a[i] = temp[i];
  if (sig) for (int i = 0; i < n; ++i) a[i] = (i64)a[i] * inv % MOD;
}

}

int cnt[P], f[P][M];

void preprocessing() {
  for (int i = 0; i < p; ++i) {
    int first = -1;
    cnt[i] = 0;
    for (int j = 0, k = 1 % p; j < n; ++j, (k *= 10) %= p) {
      if (j > 3 * p) break;
      if (k != i) continue;
      if (~first) {
        int gap = j - first;
        int rem = n - 1 - first;
        cnt[i] += rem / gap;
        break;
      } else {
        first = j;
        cnt[i] = 1;
      }
    }
  }
  static int fact[M], inv[M];
  fact[0] = 1;
  for (int i = 1; i <= m; ++i) fact[i] = (i64)fact[i - 1] * i % MOD;
  inv[m] = power(fact[m], MOD - 2);
  for (int i = m - 1; i >= 0; --i) inv[i] = (i64)inv[i + 1] * (i + 1) % MOD;
  for (int i = 0; i < p; ++i) {
    for (int j = 0; j < i; ++j) {
      if (cnt[i] == cnt[j]) {
        for (int k = 0; k <= m; ++k) f[i][k] = f[j][k];
        goto next;
      }
    }
    static int pre[M], suf[M];
    pre[0] = suf[0] = 1;
    for (int j = 1; j <= m; ++j) {
      pre[j] = (i64)pre[j - 1] * (cnt[i] - j + 1) % MOD;
      suf[j] = (i64)suf[j - 1] * (cnt[i] + j - 1) % MOD;
    }
    for (int j = 0; j <= m; ++j) {
      pre[j] = (i64)pre[j] * inv[j] % MOD;
      suf[j] = (i64)suf[j] * inv[j] % MOD;
    }
    for (int j = 0; j <= m; ++j) {
      int sum = 0;
      for (int k = 0, sig = 1; 10 * k <= j; ++k, sig = -sig) {
        int cur = (i64)pre[k] * suf[j - 10 * k] % MOD;
        if (sig < 0) cur = MOD - cur;
        (sum += cur) %= MOD;
      }
      f[i][j] = sum;
    }
 next:
    continue;
  }
}

int main() {
  scanf("%d%d%d", &n, &p, &m);
  preprocessing();
  int tot = 2 * m + 1;
  NTT::preprocessing(p * tot);
  static int ans[N];
  for (int i = 0; i <= m; ++i) ans[i] = f[0][i];
  for (int i = 1; i < p; ++i) {
    static int temp[N];
    for (int j = 0; j < NTT::n; ++j) temp[j] = 0;
    for (int j = 0; j <= m; ++j) (temp[i * j % p * tot + j] += f[i][j]) %= MOD;
    NTT::ntt(ans);
    NTT::ntt(temp);
    for (int j = 0; j < NTT::n; ++j) temp[j] = (i64)ans[j] * temp[j] % MOD;
    NTT::ntt(temp, true);
    for (int j = 0; j < NTT::n; ++j) ans[j] = 0;
    for (int j = 0; j < p; ++j)
      for (int k = 0, det = j * tot; k <= m; ++k, ++det)
        ans[det] = temp[det];
    for (int j = p; j < 2 * p; ++j)
      for (int k = 0, a = (j - p) * tot, b = j * tot; k <= m; ++k, ++a, ++b)
        (ans[a] += temp[b]) %= MOD;
  }
  for (int i = 1; i <= m; ++i) (ans[i] += ans[i - 1]) %= MOD;
  for (int i = 0; i <= m; ++i) printf("%d%c", ans[i], i == m ? '\n' : ' ');
  return 0;
}
