#include <cmath>
#include <cstdio>
#include <vector>
#include <complex>

typedef std::complex<double> Complex;

#ifndef M_PI
const double M_PI = acos(-1.0);
#endif

const int N = 131072;

int n;

std::vector<int> adj[N];

bool flag[N];

void fft(Complex a[], int len, int sig) {
  for (int i = 0, j = 0; i < len; ++i) {
    if (i > j) std::swap(a[i], a[j]);
    for (int k = len >> 1; (j ^= k) < k; k >>= 1) {}
  }
  for (int m = 2; m <= len; m *= 2) {
    int gap = m / 2;
    double arg = sig * 2.0 * M_PI / m;
    Complex w(cos(arg), sin(arg));
    for (int i = 0; i < len; i += m) {
      Complex o(1.0, 0.0);
      for (int j = i; j < i + gap; ++j, o *= w) {
        Complex v = o * a[j + gap];
        a[j + gap] = a[j] - v;
        a[j] += v;
      }
    }
  }
  if (sig < 0) for (int i = 0; i < len; ++i) a[i] /= len;
}

void sqr(double val[], int len) {
  int t = 1;
  while (t <= 2 * len) t *= 2;
  static Complex temp[N];
  for (int i = len + 1; i < t; ++i) val[i] = 0.0;
  for (int i = 0; i < t; ++i) temp[i] = val[i];
  fft(temp, t, 1);
  for (int i = 0; i < t; ++i) temp[i] *= temp[i];
  fft(temp, t, -1);
  for (int i = 0; i < t; ++i) val[i] = temp[i].real();
}

int fa[N], dep[N], order[N];

void bfs(int s) {
  int &r = order[0] = 1;
  fa[order[r] = s] = 0;
  dep[s] = 1;
  for (int f = 1; f <= r; ++f) {
    int a = order[f];
    for (auto b : adj[a]) {
      if (!flag[b] && b != fa[a]) {
        fa[order[++r] = b] = a;
        dep[b] = dep[a] + 1;
      }
    }
  }
}

int centroid(int s) {
  bfs(s);
  int r = order[0];
  static int size[N];
  static int mx[N];
  for (int i = 1; i <= r; ++i) {
    int a = order[i];
    size[a] = 1;
    mx[a] = 0;
  }
  for (int i = r; i > 0; --i) {
    int a = order[i];
    size[fa[a]] += size[a];
    mx[a] = std::max(mx[a], r - size[a]);
    mx[fa[a]] = std::max(mx[fa[a]], size[a]);
  }
  int res = order[1];
  for (int i = 2; i <= r; ++i) if (mx[order[i]] < mx[res]) res = order[i];
  return res;
}

double ans[N];

void add(int s, int sig) {
  static double temp[N];
  bfs(s);
  int tot = order[0];
  int t = 1;
  for (int i = 2; i <= tot; ++i) t = std::max(t, dep[order[i]]);
  for (int i = 1; i <= t; ++i) temp[i] = 0.0;
  for (int i = 1; i <= tot; ++i) temp[dep[order[i]]] += 1.0;
  sqr(temp, t);
  for (int i = 2; i <= 2 * t; ++i) if (sig > 0) ans[i - 2] += temp[i]; else ans[i] -= temp[i];
}

void divide(int s) {
  int a = centroid(s);
  add(a, 1);
  flag[a] = true;
  for (auto b : adj[a]) if (!flag[b]) add(b, -1);
  for (auto b : adj[a]) if (!flag[b]) divide(b);
}

int main() {
  scanf("%d", &n);
  for (int i = n - 1; i--;) {
    int a, b;
    scanf("%d%d", &a, &b);
    adj[a].push_back(b);
    adj[b].push_back(a);
  }
  divide(1);
  double sum = 0.0;
  for (int i = 2; i <= n; ++i) {
    for (int j = 2; j * j <= i; ++j) if (i % j == 0) goto fail;
    sum += ans[i];
 fail:
    continue;
  }
  printf("%.10f\n", sum / n / (n - 1.0));
  return 0;
}
