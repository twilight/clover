#include <cstdio>
#include <cstring>
#include <vector>

typedef long long i64;
typedef std::pair<int, int> Query;

const int T = 50 + 1, N = 50 + 1, Q = 200000 + 10, K = 10000 + 10;
const int MOD = 1000000007;

int t, k, n[T], m[T], adj[T][N][N];

int q, ans[Q];
std::vector<Query> query[T][T];

int trace[T][K];

int power(int base, int exp, int mod = MOD) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) res = (i64)res * base % mod;
    base = (i64)base * base % mod;
  }
  return res;
}

namespace ntt {

int n;

const int LOG = 15, N = 1 << LOG;
const int mod[3] = {167772161, 998244353, 1004535809};

int crt(int x, int y, int z) {
  static const int u = power(mod[0], mod[1] - 2, mod[1]), v = power((i64)mod[0] * mod[1] % mod[2], mod[2] - 2, mod[2]);
  i64 t = (i64)u * (y - x + mod[1]) % mod[1] * mod[0] + x;
  return ((i64)v * (z - t % mod[2] + mod[2]) % mod[2] * mod[0] % MOD * mod[1] % MOD + t % MOD) % MOD;
}

int rev[N];

struct ntt {
  const int mod;
  int w[N + 1], u[N], v[N];
  ntt(int m): mod(m) {
    w[0] = 1;
    w[1] = power(3, (mod - 1) / N, mod);
    for (int i = 2; i <= N; ++i) w[i] = (i64)w[i - 1] * w[1] % mod;
  }
  void operator() (int val[], int sw = 1) {
    for (int i = 0; i < n; ++i) if (i < rev[i]) std::swap(val[i], val[rev[i]]);
    for (int m = 2; m <= n; m *= 2) {
      int gap = m / 2, layer = N / m;
      for (int i = 0; i < gap; ++i) {
        int o = w[sw > 0 ? i * layer : (N - i * layer)];
        for (int j = i; j < n; j += m) {
          int u = val[j], v = (i64)o * val[j + gap] % mod;
          val[j] = (u + v) % mod;
          val[j + gap] = (u - v + mod) % mod;
        }
      }
    }
    if (sw < 0) {
      int inv = power(n, mod - 2, mod);
      for (int i = 0; i < n; ++i) val[i] = (i64)val[i] * inv % mod;
    }
  }
} algo[] = {ntt(mod[0]), ntt(mod[1]), ntt(mod[2])};

void preprocessing() {
  int temp = 31 - __builtin_clz(n);
  for (int i = 0; i < n; ++i) {
    int j = 0;
    for (int t = i, cnt = temp; cnt--; t >>= 1) (j <<= 1) |= (t & 1);
    rev[i] = j;
  }
}

}

void multiply(int lhs[][N], const int rhs[][N], int n) {
  static int res[N][N];
  memset(res, 0, sizeof res);
  for (int i = 1; i <= n; ++i)
    for (int k = 1; k <= n; ++k)
      if (lhs[i][k])
        for (int j = 1; j <= n; ++j)
          if (rhs[k][j]) (res[i][j] += (i64)lhs[i][k] * rhs[k][j] % MOD) %= MOD;
  memcpy(lhs, res, sizeof res);
}

void calcTrace(int g) {
  static int temp[N][N];
  memset(temp, 0, sizeof temp);
  for (int i = 1; i <= n[g]; ++i) temp[i][i] = 1;
  for (int i = 0; i <= n[g]; ++i) {
    for (int j = 1; j <= n[g]; ++j) (trace[g][i] += temp[j][j]) %= MOD;
    multiply(temp, adj[g], n[g]);
  }
  static int e[N];
  e[0] = 1;
  for (int i = 1; i <= n[g]; ++i) {
    e[i] = 0;
    for (int j = 1; j <= i; ++j) {
      int val = (i64)e[i - j] * trace[g][j] % MOD;
      if (!(j & 1)) val = -val;
      (e[i] += val) %= MOD;
    }
    e[i] = (i64)(e[i] + MOD) * power(i, MOD - 2) % MOD;
  }
  static int c[N];
  for (int i = 0; i <= n[g]; ++i) {
    c[i] = e[n[g] - i];
    if ((n[g] - i) & 1) c[i] = (MOD - c[i]) % MOD;
  }
  for (int i = n[g] + 1; i <= k; ++i) {
    int &cur = trace[g][i] = 0;
    for (int j = 0; j < n[g]; ++j) cur = (cur + (i64)c[j] * trace[g][i - n[g] + j]) % MOD;
    cur = MOD - cur;
  }
}

int main() {
  scanf("%d", &t);
  for (int i = 1; i <= t; ++i) {
    scanf("%d%d", n + i, m + i);
    for (int cnt = m[i]; cnt--;) {
      int x, y;
      scanf("%d%d", &x, &y);
      ++adj[i][x][y], ++adj[i][y][x];
    }
    for (int j = 1; j <= n[i]; ++j) ++adj[i][j][j];
  }
  scanf("%d", &q);
  k = 0;
  for (int i = 1; i <= q; ++i) {
    int x, y, z;
    scanf("%d%d%d", &x, &y, &z);
    k = std::max(k, z);
    query[x][y].push_back(Query(z, i));
  }
  for (int i = 1; i <= t; ++i) calcTrace(i);
  ntt::n = 1 << (33 - __builtin_clz(k));
  ntt::preprocessing();
  static int f[K], g[3][ntt::N], fact[K], inv[K];
  fact[0] = 1;
  for (int i = 1; i < K; ++i) fact[i] = (i64)fact[i - 1] * i % MOD;
  inv[K - 1] = power(fact[K - 1], MOD - 2);
  for (int i = K - 2; i >= 0; --i) inv[i] = (i64)inv[i + 1] * (i + 1) % MOD;
  for (int i = 0; i <= k; ++i) g[0][i] = g[1][i] = g[2][i] = ((i & 1) ? (MOD - inv[i]) : inv[i]);
  for (int i = 0; i < 3; ++i) ntt::algo[i](g[i], 1);
  for (int l = 1; l <= t; ++l) {
    for (int i = 0; i <= k; ++i) f[i] = inv[i];
    for (int r = l; r <= t; ++r) {
      for (int i = 0; i <= k; ++i) f[i] = (i64)f[i] * trace[r][i] % MOD;
      if (query[l][r].empty()) continue;
      static int temp[3][ntt::N], res[ntt::N];
      for (int i = 0; i <= k; ++i) temp[0][i] = temp[1][i] = temp[2][i] = f[i];
      for (int i = k + 1; i < ntt::n; ++i) temp[0][i] = temp[1][i] = temp[2][i] = 0;
      for (int i = 0; i < 3; ++i) {
        ntt::algo[i](temp[i], 1);
        for (int j = 0; j < ntt::n; ++j) temp[i][j] = (i64)temp[i][j] * g[i][j] % ntt::mod[i];
        ntt::algo[i](temp[i], -1);
      }
      res[0] = 0;
      for (int i = 1; i <= k; ++i) res[i] = (res[i - 1] + (i64)ntt::crt(temp[0][i], temp[1][i], temp[2][i]) * fact[i]) % MOD;
      for (int i = 0; i < query[l][r].size(); ++i) ans[query[l][r][i].second] = res[query[l][r][i].first];
    }
  }
  for (int i = 1; i <= q; ++i) printf("%d\n", (ans[i] + MOD) % MOD);
  return 0;
}
