#include <cctype>
#include <cstdio>
#include <vector>
#include <algorithm>

const int N = 100000 + 10, LOG = 20;

int n, col[N];
std::vector<int> adj[N];

int fa[LOG][N], dep[N], left[N], right[N];
std::vector<int> dfn;

void DFS(int a) {
  for (std::vector<int>::iterator it = adj[a].begin(); it != adj[a].end(); ++it) {
    int b = *it;
    if (b != fa[0][a]) {
      fa[0][b] = a;
      dep[b] = dep[a] + 1;
      left[b] = dfn.size();
      dfn.push_back(b);
      DFS(b);
      right[b] = dfn.size();
      dfn.push_back(b);
    }
  }
}

class EulerTourTree {

  class node_t {
    inline int dir() {
      if (!p) return -1;
      return this == p->ch[1];
    }

    inline void rotate() {
      node_t *u = p;
      int d = dir();
      u->p->setc(this, u->dir());
      u->setc(ch[!d], d);
      setc(u, !d);
      u->update();
      update();
    }

   public:
    node_t *p, *ch[2];
    int size;

    inline void setc(node_t *c, int d) {
      if (this) ch[d] = c;
      if (c) c->p = this;
    }

    inline void update() {
      size = 1;
      if (ch[0]) size += ch[0]->size;
      if (ch[1]) size += ch[1]->size;
    }

    inline void splay(node_t *target = NULL) {
      while (p != target) {
        if (p->p != target) (dir() == p->dir() ? p : this)->rotate();
        rotate();
      }
    }
  } pool[2 * N];

 public:
  void init() {
    for (int i = 0; i <= n; ++i) {
      pool[left[i]].setc(pool + right[i], 1);
      pool[left[i]].update();
    }
  }

  void link(int u) {
    int v = fa[0][u];
    pool[left[v]].splay();
    pool[right[v]].splay(pool + left[v]);
    pool[left[u]].splay();
    pool[right[u]].splay(pool + left[u]);
    pool[left[v]].setc(pool + left[u], 1);
    pool[right[u]].setc(pool + right[v], 1);
    pool[right[v]].splay();
  }

  void cut(int u) {
    pool[left[u]].splay();
    pool[right[u]].splay(pool + left[u]);
    node_t *x = pool[left[u]].ch[0], *y = pool[right[u]].ch[1];
    pool[left[u]].ch[0] = x->p = NULL;
    pool[right[u]].ch[1] = y->p = NULL;
    pool[right[u]].update();
    pool[left[u]].update();
    if (!x || !y) return;
    while (x->ch[1]) x = x->ch[1];
    x->splay();
    x->setc(y, 1);
    x->update();
  }

  int query(int u) {
    node_t *temp = pool + left[u];
    for (temp->splay(); temp->ch[0];) temp = temp->ch[0];
    int x = dfn[temp - pool], y = u;
    for (int i = LOG - 1; i >= 0; --i)
      if (~fa[i][y] && dep[fa[i][y]] >= dep[x] && col[fa[i][y]] == col[u])
        y = fa[i][y];
    pool[left[y]].splay();
    pool[right[y]].splay(pool + left[y]);
    node_t *z = pool[right[y]].ch[0];
    return (z ? z->size / 2 : 0) + 1;
  }

} tree[2];

inline int READ() {
  static char ch;
  while (!isdigit(ch = getchar())) {}
  int res = ch - '0';
  while (isdigit(ch = getchar())) res = res * 10 + ch - '0';
  return res;
}

int main() {
  scanf("%d", &n);
  for (int ecnt = n - 1, u, v; ecnt--;) {
    scanf("%d%d", &u, &v);
    adj[u].push_back(v);
    adj[v].push_back(u);
  }
  adj[0].push_back(1);
  left[0] = 0;
  dfn.push_back(0);
  DFS(0);
  right[0] = dfn.size();
  dfn.push_back(0);
  tree[0].init();
  tree[1].init();
  col[0] = 2;
  for (int i = 1; i <= n; ++i) tree[col[i] = 0].link(i);
  fa[0][0] = -1;
  for (int j = 1; j < LOG; ++j)
    for (int i = 1; i <= n; ++i)
      fa[j][i] = fa[j - 1][fa[j - 1][i]];
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    int op, u;
    scanf("%d%d", &op, &u);
    switch (op) {
      case 0:
        printf("%d\n", tree[col[u]].query(u));
        break;
      case 1:
        tree[col[u]].cut(u);
        tree[col[u] ^= 1].link(u);
        break;
    }
  }
  return 0;
}
