#include <cstdio>
#include <vector>
#include <algorithm>

#define fst first
#define snd second

typedef std::pair<int, int> Info;
typedef long long i64;

const int N = 100000 + 10, LIM = 1000000 + 10;

int n, m, x[N], y[N], z[N], a[N], b[N];

int moe[LIM];
bool mark[LIM];
std::vector<int> prime;

void Preprocessing() {
  moe[1] = 1;
  for (int i = 2; i < LIM; ++i) {
    if (!mark[i]) {
      prime.push_back(i);
      moe[i] = -1;
    }
    for (int j = 0; j < prime.size() && i * prime[j] < LIM; ++j) {
      mark[i * prime[j]] = true;
      if (i % prime[j] == 0) {
        moe[i * prime[j]] = 0;
        break;
      }
      moe[i * prime[j]] = -moe[i];
    }
  }
}

std::vector<Info> edge[LIM];

void Append(const Info &cur, int val) {
  static std::vector<int> divisor;
  divisor.clear();
  divisor.push_back(1);
  for (int i = 0; i < prime.size() && prime[i] * prime[i] <= val; ++i) {
    int p = prime[i];
    if (val % p == 0) {
      for (int j = 0, lim = divisor.size(); j < lim; ++j) divisor.push_back(p * divisor[j]);
      while (val % p == 0) val /= p;
    }
  }
  if (val > 1) 
    for (int j = 0, lim = divisor.size(); j < lim; ++j) divisor.push_back(val * divisor[j]);
  for (std::vector<int>::iterator it = divisor.begin(); it != divisor.end(); ++it) edge[*it].push_back(cur);
}

int anc[N], size[N];

i64 cur;

inline int Find(int x) { return anc[x] == x ? x : Find(anc[x]); }

std::vector<int> stk[4];

inline void Union(int a, int b) {
  a = Find(a), b = Find(b);
  if (size[a] > size[b]) std::swap(a, b);
  stk[0].push_back(a);
  stk[1].push_back(b);
  stk[2].push_back(size[a]);
  stk[3].push_back(size[b]);
  cur -= (i64)size[a] * (size[a] - 1) / 2;
  cur -= (i64)size[b] * (size[b] - 1) / 2;
  size[b] += size[a];
  anc[a] = b;
  cur += (i64)size[b] * (size[b] - 1) / 2;
}

inline void Revert() {
  int a = stk[0].back(), b = stk[1].back(), c = stk[2].back(), d = stk[3].back();
  cur -= (i64)size[b] * (size[b] - 1) / 2;
  cur += (i64)c * (c - 1) / 2;
  cur += (i64)d * (d - 1) / 2;
  anc[a] = a;
  size[a] = c;
  size[b] = d;
  for (int i = 0; i < 4; ++i) stk[i].pop_back();
}

inline void Clear(int x) {
  anc[x] = x;
  size[x] = 1;
}

i64 ans[N];

inline void Solve(int p) {
  for (int i = 0; i < 4; ++i) stk[i].clear();
  for (int i = 0; i < edge[p].size(); ++i) {
    Clear(x[edge[p][i].snd]);
    Clear(y[edge[p][i].snd]);
  }
  cur = 0;
  for (int i = 0; i < edge[p].size();) {
    int j = i;
    while (j < edge[p].size() && edge[p][j].fst == edge[p][i].fst) ++j;
    for (int k = i; k < j; ++k) Union(x[edge[p][k].snd], y[edge[p][k].snd]);
    if (~edge[p][i].fst) {
      ans[edge[p][i].fst] += moe[p] * cur;
      for (int k = j - i; k--;) Revert();
    } else {
      static int tag[N];
      for (int k = j; k < edge[p].size(); ++k) tag[edge[p][k].fst] = p;
      for (int i = 0; i <= m; ++i) if (tag[i] != p) ans[i] += moe[p] * cur;
    }
    i = j;
  }
}

std::vector<int> pos, val;

inline void Query(int t) {
  for (int i = 0; i < pos.size(); ++i) Append(Info(t, pos[i]), val[i]);
}

int main() {
  Preprocessing();
  scanf("%d", &n);
  for (int i = 1; i < n; ++i) scanf("%d%d%d", x + i, y + i, z + i);
  scanf("%d", &m);
  static bool flag[N];
  for (int i = 1; i <= m; ++i) {
    scanf("%d%d", a + i, b + i);
    pos.push_back(a[i]);
    flag[a[i]] = true;
  }
  std::sort(pos.begin(), pos.end());
  pos.erase(std::unique(pos.begin(), pos.end()), pos.end());
  val.resize(pos.size());
  for (int i = 1; i < n; ++i) if (!flag[i]) Append(Info(-1, i), z[i]); else val[std::lower_bound(pos.begin(), pos.end(), i) - pos.begin()] = z[i];
  Query(0);
  for (int i = 1; i <= m; ++i) {
    val[std::lower_bound(pos.begin(), pos.end(), a[i]) - pos.begin()] = b[i];
    Query(i);
  }
  for (int i = 1; i < LIM; ++i) if (moe[i]) Solve(i);
  for (int i = 0; i <= m; ++i) printf("%lld\n", ans[i]);
  return 0;
}
