#include <cstdio>
#include <cstring>
#include <queue>

typedef std::pair<int, int> Info;

const int N = 100 + 10, M = N * N, INF = 0x3f3f3f3f;
const int V = 2 * N, E = 10 * M;

int n, m, row[N], col[N], a[2 * M][2 * N], b[2 * M], c[2 * N];

int adj[V];
int to[E], next[E], cap[E], cost[E], cnt;

namespace Graph {

int s, t;

inline void init() {
  cnt = 2;
  s = 2 * n + 1, t = 2 * n + 2;
  memset(adj, 0, sizeof adj);
}

inline void link(int a, int b, int c, int d) {
  to[cnt] = b, next[cnt] = adj[a], cap[cnt] = c, cost[cnt] = d, adj[a] = cnt++;
  to[cnt] = a, next[cnt] = adj[b], cap[cnt] = 0, cost[cnt] = -d, adj[b] = cnt++;
}

int dist[V], pre[V], e[V];

bool dijkstra() {
  static std::priority_queue<Info> heap;
  memset(dist, 0x3f, sizeof dist);
  for (heap.push(Info(-(dist[s] = 0), s)); !heap.empty();) {
    Info info(heap.top());
    heap.pop();
    int a = info.second;
    if (-info.first > dist[a])
      continue;
    for (int i = adj[a]; i; i = next[i]) {
      int b = to[i], c = cost[i];
      if (cap[i] && dist[a] + c < dist[b]) {
        dist[b] = dist[a] + c;
        pre[b] = a;
        e[b] = i;
        heap.push(Info(-dist[b], b));
      }
    }
  }
  return dist[t] < INF;
}

int flow() {
  int res = 0;
  while (dijkstra()) {
    int f = INF;
    for (int i = t; i != s; i = pre[i])
      f = std::min(f, cap[e[i]]);
    res += f * dist[t];
    for (int i = t; i != s; i = pre[i]) {
      cap[e[i]] -= f;
      cap[e[i] ^ 1] += f;
    }
  }
  return res;
}

void duality() {
  static int dis[V][V];
  memset(dis, 0x3f, sizeof dis);
  for (int a = 1; a <= 2 * n; ++a) {
    for (int i = adj[a]; i; i = next[i]) {
      int b = to[i];
      if (b <= 2 * n && cap[i]) dis[a][b] = std::min(dis[a][b], cost[i]);
    }
  }
  static int ans[V];
  std::queue<int> q;
  for (int i = 1; i <= 2 * n; ++i) ans[i] = 0, q.push(i);
  while (!q.empty()) {
    int a = q.front();
    q.pop();
    for (int b = 1; b <= 2 * n; ++b) {
      if (ans[a] + dis[a][b] < ans[b]) {
        ans[b] = ans[a] + dis[a][b];
        q.push(b);
      }
    }
  }
  int dif = 0;
  for (int i = 1; i <= 2 * n; ++i) dif = std::min(dif, ans[i]);
  for (int i = 1; i <= 2 * n; ++i) printf("%d%c", ans[i] - dif, (i % n) ? ' ' : '\n');
}

}

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d", &n, &m);
    Graph::init();
    memset(a, 0, sizeof a);
    memset(b, 0, sizeof b);
    memset(c, 0, sizeof c);
    int sum = 0;
    static int dis[V][V];
    memset(dis, 0x3f, sizeof dis);
    for (int i = 1; i <= 2 * n; ++i) dis[i][i] = 0;
    for (int i = 1; i <= m; ++i) {
      int x, y, l, s, t;
      scanf("%d%d%d%d%d", &x, &y, &l, &s, &t);
      sum += l;
      dis[x][y + n] = std::min(dis[x][y + n], l - s);
      dis[y + n][x] = std::min(dis[y + n][x], t - l);
      Graph::link(x, y + n, INF, l - s);
      Graph::link(y + n, x, INF, t - l);
      ++c[x], ++c[y + n];
    }
    for (int k = 1; k <= 2 * n; ++k)
      for (int i = 1; i <= 2 * n; ++i)
        for (int j = 1; j <= 2 * n; ++j)
          dis[i][j] = std::min(dis[i][j], dis[i][k] + dis[k][j]);
    for (int i = 1; i <= 2 * n; ++i) if (dis[i][i] < 0) goto fail;
    for (int i = 1; i <= n; ++i) Graph::link(i, Graph::t, c[i], 0);
    for (int i = n + 1; i <= 2 * n; ++i) Graph::link(Graph::s, i, c[i], 0);
    printf("%d\n", sum + Graph::flow());
    Graph::duality();
    continue;
 fail:
    puts("Unlike");
  }
  return 0;
}
