#include <cstdio>
#include <algorithm>

typedef long long i64;

const int N = 100000 + 10;

int n, a[N];

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d", &n);
    for (int i = 1; i <= n; ++i) scanf("%d", a + i);
    i64 ans = 0;
    for (int i = 1; i <= n; ++i) ans += std::max(0, a[i] - a[i - 1]);
    printf("%lld\n", ans);
  }
  return 0;
}
