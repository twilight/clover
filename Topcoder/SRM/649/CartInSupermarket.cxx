#include <bits/stdc++.h>

typedef long long i64;

std::vector<int> pool;

inline i64 split(int orig, int cnt) {
  int layer = 32 - __builtin_clz(cnt);
  orig -= (1 << layer) - cnt;
  int last = orig / cnt + (orig % cnt > 0);
  return last + layer;
}

i64 solve(int lim) {
  i64 res = 0;
  for (std::vector<int>::iterator it = pool.begin(); it != pool.end(); ++it) {
    int l = 1, r = *it + 1;
    while (l < r) {
      int mid = (l + r) / 2;
      if (split(*it, mid) <= lim) r = mid; else l = mid + 1;
    }
    if (split(*it, l) > lim) return 1LL << 50;
    res += l - 1;
  }
  return res;
}

class CartInSupermarket {
 public:
  int calcmin(std::vector<int> &a, int b) {
    pool = std::move(a);
    int l = 0, r = *std::max_element(pool.begin(), pool.end());
    while (l < r) {
      int mid = (l + r) / 2;
      if (solve(mid) <= b) r = mid; else l = mid + 1;
    }
    return l;
  }
};
