#include <cstdio>
#include <algorithm>

typedef long long i64;

const int N = 131072 + 10;

int a[N];

struct node_t {
  node_t *next[2];
  int cnt[2];
  i64 val[2];
  node_t() {
    next[0] = next[1] = NULL;
    cnt[0] = cnt[1] = val[0] = val[1] = 0;
  }
} *root = new node_t;

i64 cnt[2][50];

void solve(int dep, node_t *cur) {
  if (!cur) return;
  cnt[0][dep] += cur->val[0];
  cnt[1][dep] += cur->val[1];
  solve(dep - 1, cur->next[0]);
  solve(dep - 1, cur->next[1]);
}

class XorSequence {
 public:
  i64 getmax(int MOD, int sz, int u, int v, int p, int q, int r) {
    a[0] = u, a[1] = v;
    int k = __builtin_popcount(--MOD);
    for (int i = 2; i < sz; ++i) a[i] = ((i64)a[i - 2] * p + (i64)a[i - 1] * q + r) & MOD;
    for (int i = 0; i < sz; ++i) {
      node_t *cur = root;
      for (int j = k; j >= 0; --j) {
        int c = a[i] >> j & 1;
        cur->val[!c] += cur->cnt[!c];
        ++cur->cnt[c];
        if (!cur->next[c]) cur->next[c] = new node_t;
        cur = cur->next[c];
      }
    }
    solve(k, root);
    i64 res = 0;
    for (int i = 0; i <= k; ++i) res += std::max(cnt[0][i], cnt[1][i]);
    return res;
  }
};
