#include <bits/stdc++.h>

const int N = 50 + 1, K = 10;
const int dx[] = {0, 1, 0, -1}, dy[] = {-1, 0, 1, 0};

int n, m, tot;

int id[N][N];

int ray[N][N][4];
int dis[N][N][1 << K];

struct state_t {
  int x, y, s;
};

void BFS(int x, int y, int s) {
  memset(dis, -1, sizeof dis);
  dis[x][y][s] = 0;
  std::queue<state_t> q;
  for (q.push((state_t){x, y, s}); !q.empty(); q.pop()) {
    state_t &cur = q.front();
    for (int k = 0; k < 4; ++k) {
      if (~ray[cur.x][cur.y][k]) {
        int a = cur.x + dx[k], b = cur.y + dy[k], c = cur.s ^ ray[cur.x][cur.y][k];
        if (!~dis[a][b][c]) {
          dis[a][b][c] = dis[cur.x][cur.y][cur.s] + 1;
          q.push((state_t){a, b, c});
        }
      }
    }
  }
}

class VegetableGarden {
 public:
  std::vector<int> getMinDistances(std::vector<std::string> garden) {
    n = garden.size();
    m = garden[0].length();
    int forbid = 0;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        if (garden[i][j] != '.') id[i][j] = ++tot;
        if (garden[i][j] == 'X') forbid |= (1 << (tot - 1));
      }
    }
    for (int i = 0; i <= n; ++i) {
      for (int j = 0; j <= m; ++j) {
        for (int k = 0; k < 4; ++k) {
          int x = i + dx[k], y = j + dy[k];
          if (0 <= x && x <= n && 0 <= y && y <= m) {
            ray[i][j][k] = 0;
            if (k & 1) {
              int p = (k == 1 ? i : x);
              for (int q = j; q < m; ++q) 
                if (id[p][q]) ray[i][j][k] |= (1 << (id[p][q] - 1));
            }
          } else {
            ray[i][j][k] = -1;
          }
        }
      }
    }
    BFS(0, 0, 0);
    std::vector<int> res(tot - __builtin_popcount(forbid), INT_MAX);
    for (int i = 1; i < (1 << tot); ++i) {
      if ((i & forbid) || !~dis[0][0][i]) continue;
      int &cur = res[__builtin_popcount(i) - 1];
      cur = std::min(cur, dis[0][0][i]);
    }
    return res;
  }
};
