#include <cstdio>
#include <cstring>
#include <vector>

const int N = 1000 + 10, SZ = N * N, h = 0;

int n, m;
int L[SZ], R[SZ], U[SZ], D[SZ], C[SZ], S[SZ], no[SZ], cnt;

int first[N];

inline void init() {
  memset(first, 0, sizeof first);
  for (int i = 0; i <= m; ++i) {
    S[i] = 0;
    L[i + 1] = i;
    R[i] = i + 1;
    U[i] = D[i] = i;
  }
  R[m] = 0;
  cnt = m;
}

void insert(int x, int y) {
  int cur = ++cnt;
  no[cur] = x;
  U[cur] = y, D[cur] = D[y];
  U[D[y]] = cur, D[y] = cur;
  if (!first[x]) {
    first[x] = L[cur] = R[cur] = cur;
  } else {
    L[cur] = first[x], R[cur] = R[first[x]];
    L[R[first[x]]] = cur, R[first[x]] = cur;
  }
  S[C[cur] = y]++;
}

void cover(int c) {
  R[L[c]] = R[c], L[R[c]] = L[c];
  for (int i = D[c]; i != c; i = D[i]) {
    for (int j = R[i]; j != i; j = R[j]) {
      D[U[j]] = D[j];
      U[D[j]] = U[j];
      S[C[j]]--;
    }
  }
}

void uncover(int c) {
  R[L[c]] = L[R[c]] = c;
  for (int i = D[c]; i != c; i = D[i]) {
    for (int j = R[i]; j != i; j = R[j]) {
      D[U[j]] = U[D[j]] = j;
      S[C[j]]++;
    }
  }
}

std::vector<int> ans;

bool dfs() {
  if (R[h] == h) {
    printf("%d", ans.size());
    for (std::vector<int>::iterator it = ans.begin(); it < ans.end(); ++it)
      printf(" %d", *it);
    putchar('\n');
    return true;
  }
  int c = R[h];
  for (int t = R[h]; t != h; t = R[t]) if (S[t] < S[c]) c = t;
  cover(c);
  for (int r = D[c]; r != c; r = D[r]) {
    ans.push_back(no[r]);
    for (int j = R[r]; j != r; j = R[j]) cover(C[j]);
    if (dfs()) return true;
    for (int j = L[r]; j != r; j = L[j]) uncover(C[j]);
    ans.pop_back();
  }
  uncover(c);
  return false;
}

int main() {
  while (scanf("%d%d", &n, &m) != EOF) {
    init();
    for (int i = 1, j, k; i <= n; ++i) {
      scanf("%d", &k);
      while (k--) {
        scanf("%d", &j);
        insert(i, j);
      }
    }
    ans.clear();
    if (!dfs()) puts("NO");
  }
  return 0;
}
