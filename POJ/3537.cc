#include <cstdio>
#include <cstring>

const int N = 2000 + 10;

int n;
int sg[N];

int cal(int x) {
  if (x <= 0) return 0;
  if (~sg[x]) return sg[x];
  bool mex[N] = {false};
  for (int i = 1; i <= x; ++i) mex[cal(i - 3) ^ cal(x - i - 2)] = true;
  for (int i = 0;; ++i) if (!mex[i]) return sg[x] = i;
}

int main() {
  scanf("%d", &n);
  memset(sg, -1, sizeof sg);
  printf("%d\n", 2 - (bool)cal(n));
  return 0;
}
