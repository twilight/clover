#include <cstdio>
#include <algorithm>

const int N = 1000 + 10;

int n, pos[N];

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d", &n);
    for (int i = 1; i <= n; ++i) scanf("%d", pos + i);
    std::sort(pos + 1, pos + 1 + n);
    int stone[N] = {0}, ans = 0;
    for (int i = 1; i <= n; ++i) stone[i] = pos[i] - pos[i - 1] - 1;
    for (int i = n; i > 0; i -= 2) ans ^= stone[i];
    puts(ans ? "Georgia will win" : "Bob will win");
  }
  return 0;
}
