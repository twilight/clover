#include <cstdio>
#include <algorithm>

typedef std::pair<int, int> point;

#define x first
#define y second

inline int cross(const point &c, const point &a, const point &b) {
  return (a.x - c.x) * (b.y - c.y) - (a.y - c.y) * (b.x - c.x);
}

inline point mkvec(const point &a, const point &b) {
  return point(b.x - a.x, b.y - a.y);
}

inline int sgn(int x) {
  if (x == 0) return 0;
  return x > 0 ? 1 : -1;
}

inline bool intersect(const point &s1, const point &e1, const point &s2, const point &e2) {
  if (s1 == s2 || s1 == e2 || e1 == s2 || e1 == e2) return true;
  return
    std::min(s1.x, e1.x) <= std::max(s2.x, e2.x) &&
    std::min(s2.x, e2.x) <= std::max(s1.x, e1.x) &&
    std::min(s1.y, e1.y) <= std::max(s2.y, e2.y) &&
    std::min(s2.y, e2.y) <= std::max(s1.y, e1.y) &&
  sgn(cross(s1, s2, e2)) * sgn(cross(e1, s2, e2)) <= 0 &&
  sgn(cross(s2, s1, e1)) * sgn(cross(e2, s1, e1)) <= 0;
}

inline bool inside(const point &st, const point &ed, const point &p) {
  return st.x <= p.x && p.x <= ed.x && st.y <= p.y && p.y <= ed.y;
}

point st, ed;
point pool[5];

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d%d%d", &st.x, &st.y, &ed.x, &ed.y);
    int x1, y1, x2, y2;
    scanf("%d%d%d%d", &x1, &y1, &x2, &y2);
    pool[0].x = std::min(x1, x2), pool[0].y = std::min(y1, y2);
    pool[2].x = std::max(x1, x2), pool[2].y = std::max(y1, y2);
    pool[1].x = pool[2].x, pool[1].y = pool[0].y;
    pool[3].x = pool[0].x, pool[3].y = pool[2].y;
    pool[4] = pool[0];
    bool ans = false;
    for (int i = 1; i <= 4; ++i) ans |= intersect(st, ed, pool[i - 1], pool[i]);
    ans |= inside(pool[0], pool[2], st) && inside(pool[0], pool[2], ed);
    puts(ans ? "T" : "F");
  }
  return 0;
}
