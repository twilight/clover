#include <cstdio>
#include <vector>
#include <queue>
#include <functional>
#include <algorithm>

#define rep(i,p,q) for (i=p; i<=q; ++i)

using namespace std;

int main()
{
	int n1,n2,n;
	for (scanf("%d%d%d",&n1,&n2,&n); n; scanf("%d%d%d",&n1,&n2,&n)) 
	{
		double sum=0;
		int i,tmp;
		priority_queue< int > maxheap;
		priority_queue< int, vector<int>, greater<int> > minheap;
		rep(i,1,n)
		{
			scanf("%d",&tmp);
			if (i<=n1) minheap.push(tmp);
			if (i<=n2) maxheap.push(tmp);
			if (tmp>minheap.top()) minheap.pop(),minheap.push(tmp);
			if (tmp<maxheap.top()) maxheap.pop(),maxheap.push(tmp);
			sum+=tmp;
		}
		rep(i,1,n1) sum-=minheap.top(),minheap.pop();
		rep(i,1,n2) sum-=maxheap.top(),maxheap.pop();
		printf("%.6f\n",sum/(n-n1-n2));
	}
	return 0;
}
