#include <cstdio>

const int N = 1000 + 10;

int n, k[N];

int main() {
  while (scanf("%d", &n), n) {
    int sg = 0;
    for (int i = 1; i <= n; ++i) {
      scanf("%d", k + i);
      sg ^= k[i];
    }
    int ans = 0;
    for (int i = 1; i <= n; ++i) ans += (k[i] >= (sg ^ k[i]));
    if (!sg) ans = 0;
    printf("%d\n", ans);
  }
  return 0;
}
