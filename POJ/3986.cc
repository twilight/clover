#include <cstdio>
#include <cstring>

typedef long long i64;

const int N = 50 + 10, BIT = 32, MOD = 1000000003;

int n;
unsigned mask, m[N];

int main() {
  while (scanf("%d%u", &n, &mask), n) {
    static unsigned sum[N];
    for (int i = 1; i <= n; ++i) scanf("%u", m + i), sum[i] = sum[i - 1] ^ (++m[i]);
    static int f[N][BIT][2];
    memset(f, 0, sizeof f);
    f[0][0][0] = 1;
    for (int i = 0; i < n; ++i) {
      for (int j = BIT - 1; j >= 0; --j) {
        for (int k = 0; k < 2; ++k) {
          if (!f[i][j][k]) continue;
          for (int x = BIT - 1; x > j; --x) if (m[i + 1] >> x & 1) (f[i + 1][x][sum[i] >> x & 1] += ((i64)f[i][j][k] << j) % MOD) %= MOD;
          if (m[i + 1] >> j & 1) (f[i + 1][j][k] += ((i64)f[i][j][k] << j) % MOD) %= MOD;
          for (int x = j - 1; x >= 0; --x) if (m[i + 1] >> x & 1) (f[i + 1][j][k ^ (m[i + 1] >> j & 1)] += ((i64)f[i][j][k] << x) % MOD) %= MOD;
        }
      }
    }
    int ans = 0;
    for (int i = BIT - 1; i >= 0; --i) if ((mask >> (i + 1)) == (sum[n] >> (i + 1))) (ans += f[n][i][mask >> i & 1]) %= MOD;
    printf("%d\n", ans);
  }
  return 0;
}
