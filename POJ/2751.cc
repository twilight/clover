#include <cstdio>
#include <vector>
#include <algorithm>

using namespace std;

const int maxn=10000;

int a[maxn],b[maxn];

bool cmp1(const int p, const int q) { return a[p]<a[q]; }
bool cmp2(const int p, const int q) { return b[p]>b[q]; }

int main()
{
	int n;
	vector<int> l,l1,l2;
	for (scanf("%d",&n); n; scanf("%d",&n))
	{
		l1.clear(),l2.clear();
		for (int i=0; i<n; ++i)
		{
			scanf("%d%d",&a[i],&b[i]);
			a[i]<b[i] ? l1.push_back(i):l2.push_back(i);
		}
		sort(l1.begin(),l1.end(),cmp1);
		sort(l2.begin(),l2.end(),cmp2);
		l.clear();
		int i,t1=0,t2=0;
		for (i=0; i<l1.size(); ++i) l.push_back(l1[i]);
		for (i=0; i<l2.size(); ++i) l.push_back(l2[i]);
		for (i=0; i<l.size(); ++i)
		{
			t1+=a[l[i]];
			t2=max(t1,t2)+b[l[i]];
		}
		printf("%d\n",t2);
	}
	return 0;
}
