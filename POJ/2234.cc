#include <cstdio>

int main() {
  int n;
  while (scanf("%d", &n) != EOF) {
    int sg = 0;
    for (int x; n--;) {
      scanf("%d", &x);
      sg ^= x;
    }
    puts(sg ? "Yes" : "No");
  }
  return 0;
}
