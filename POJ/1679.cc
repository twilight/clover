#include <cstdio>
#include <algorithm>

using namespace std;

const int maxn=100+1,maxm=maxn*maxn;

int n,m,f[maxn];

struct node {
	int s,t,w;
} edge[maxm];

bool cmp(node p, node q) { return p.w<q.w; }

void init() {
	for (int i=1; i<=n; ++i) f[i]=i;
}

int find(int x) {
	if (f[x]==x) return f[x];
	return f[x]=find(f[x]);
}

int main() {
	int tcase;
	register int i;
	scanf("%d",&tcase);
	while (tcase--) {
		scanf("%d%d",&n,&m);
		for (i=0; i<m; ++i) scanf("%d%d%d",&edge[i].s,&edge[i].t,&edge[i].w);
		sort(edge,edge+m,cmp);
		node *p=&edge[0];
		init();
		for (i=0; i<m; ++i) if (edge[i].s > edge[i].t) swap(edge[i].s,edge[i].t);
		int ans=0;
		for (i=1; i < n && ans != -1; ++p) {
			if (find(p->s) != find(p->t)) {
				node *q=p;
				while (q->w == p->w && find(q->s)==find(p->s) && find(q->t) == find(p->t)) ++q;
				if (--q != p) {
					ans=-1;
					break;
				}
				f[find(p->s)]=find(p->t);
				ans+=p->w;
				++i;
			}
		}
		ans == -1 ? puts("Not Unique!") : printf("%d\n",ans);
	}
	return 0;
}
