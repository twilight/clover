#include <cstdio>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <algorithm>

typedef unsigned int int32;
typedef unsigned long long int64;
typedef std::pair<int64, int64> int128;

const int64 table[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29}, cnt = 10;

#define fst first
#define snd second

int128 mul64(int64 x, int64 y) {
  int128 a(x >> 32, x & 0xffffffff);
  int128 b(y >> 32, y & 0xffffffff);
  int64 rl = a.snd * b.snd;
  int64 c = a.snd * b.fst;
  int64 rh = (c >> 32);
  c <<= 32;
  rl += c;
  if (rl < c) ++rh;
  c = a.fst * b.snd;
  rh += (c >> 32);
  c <<= 32;
  rl += c;
  if (rl < c) ++rh;
  rh += a.fst * b.fst;
  return int128(rh, rl);
}

int64 mod64(int128 a, const int64 mod) {
  a.fst %= mod;
  a.snd %= mod;
  int64 r = fmodl(a.fst * pow(2.0, 64), mod);
  r += a.snd;
  if (r > mod) r -= mod;
  return r = fmodl(r, mod);
}

inline int64 pow(int64 base, int64 exp, const int64 mod) {
  int64 res = 1;
  while (exp) {
    if (exp & 1) res = mod64(mul64(res, base), mod);
    base = mod64(mul64(base, base), mod);
    exp >>= 1;
  }
  return res;
}

bool isprime(int64 x) {
  if (x == 1) return false;
  int64 s = 0, d;
  for (d = x - 1; (d & 1) == 0; d >>= 1) ++s;
  for (int i = 0; i < cnt; ++i) {
    if (table[i] >= x) break;
    if (pow(table[i], d, x) == 1) continue;
    bool flag = false;
    for (int64 r = 0, tmp; r < s; ++r) {
      tmp = mod64(mul64(d, pow(2, r, x)), x);
      if (pow(table[i], tmp, x) == x - 1) {
        flag = true;
        break;
      }
    }
    if (!flag)
      return false;
  }
  return true;
}

inline int64 rand(int64 x) { return (int64)(rand() * rand()) % x + 1; }

int64 gcd(int64 a, int64 b) {
  int64 r = a % b;
  while (r) {
    a = b;
    b = r;
    r = a % b;
  }
  return abs(b);
}

int64 rho(int64 x) {
  int64 a, b, k = 0, i = 1, d = 1;
  a = b = rand(x - 1);
  int64 det = rand();
  while (d == 1) {
    ++k;
    a = mod64(mul64(a, a), x);
    a = (a + det) % x;
    d = gcd(a - b, x);
    if (d > 1 && d < x) return d;
    if (k == i) b = a, i *= 2;
  }
  return x;
}

int64 find(int64 x) {
  if (x == 1) return ~0;
  if (isprime(x)) return x;
  int64 p = x;
  while (p >= x) p = rho(p);
  int64 a = find(p), b = find(x / p);
  return std::min(a, b);
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    int64 x;
    scanf("%llu", &x);
    if (isprime(x)) puts("Prime"); else printf("%llu\n", find(x));
  }
  return 0;
}
