#include <cstdio>
#include <cstring>

using namespace std;

const int N=1010,LEN=2*N;

struct node
{
	int size;
	char data[LEN];
	node *next;
	node(): next(NULL), size(0) { memset(data,0,sizeof(data)); }
} list[N],*root;

namespace List
{
	int cnt=0;

	void split(node *p)
	{
		memcpy(list[cnt].data,p->data+LEN/2,sizeof(p->data));
		list[cnt].size=LEN/2;
		p->size-=LEN/2;
		list[cnt].next=p->next;
		p->next=&list[cnt++];
	}

	void init(char str[])
	{
		int i;
		node *p;
		for (i=0,p=root; str[i]; p=p->next)
		{
			for (; p->size<LEN && str[i]; ++i)
				p->data[p->size++]=str[i];
			if (str[i]) p->next=&list[cnt++];
		}
		for (p=root; p!=NULL; p=p->next)
			if (p->size==LEN) split(p);
	}

	void insert(int pos, char ch)
	{
		node *p=root;
		while (pos>p->size && p->next!=NULL) pos-=p->size,p=p->next;
		if (pos>=p->size) p->data[p->size]=ch; else
		{
			for (int i=p->size; i>pos; --i) p->data[i]=p->data[i-1];
			p->data[pos]=ch;
		}
		p->size++;
		if (p->size==LEN) split(p);
	}

	char find(int pos)
	{
		node *p=root;
		while (pos>p->size) pos-=p->size,p=p->next;
		return p->data[pos-1];
	}
}

char str[N*N+10];

int main()
{
#ifndef ONLINE_JUDGE
	freopen("2887.in","r",stdin);
	freopen("2887.out","w",stdout);
#endif
	scanf("%s",str);
	root=&list[List::cnt++];
	List::init(str);
	int n,p;
	char order,ch;
	scanf("%d",&n);
	while (n--)
	{
		scanf(" %c",&order);
		switch (order)
		{
			case 'I':
				scanf(" %c %d",&ch,&p);
				List::insert(p-1,ch);
				break;
			case 'Q':
				scanf(" %d",&p);
				printf("%c\n",List::find(p));
				break;
		}
	}
	return 0;
}
