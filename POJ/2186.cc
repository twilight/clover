#include <cstdio>
#include <cstring>
#include <stack>
#include <algorithm>

using std::scanf;
using std::printf;

const int maxn=10001,maxm=50001;

struct 
{
	int adj,next;
} edge[maxm];

struct
{
	int src,dest;
} data[maxm];

int ehead[maxn],dis[maxn],low[maxn],contract[maxn],cnt,tot;
bool instack[maxn];
std::stack<int> stack;

inline void insert_edge(int p, int q)
{
	if (p==q) return;
	edge[cnt].adj=q;
	edge[cnt].next=ehead[p];
	ehead[p]=cnt++;
}

void dfs(int i)
{
	dis[i]=low[i]=++tot;
	stack.push(i);
	instack[i]=true;
	for (int p=ehead[i]; p!=-1; p=edge[p].next)
	{
		int j=edge[p].adj;
		if (!dis[j]) dfs(j);
		if (instack[j]) low[i]=std::min(low[i],low[j]);
	}
	if (dis[i]==low[i])
	{
		int j;
		do
		{
			j=stack.top();
			stack.pop();
			instack[j]=false;
			contract[j]=i;
		} while (j!=i);
	}
}

inline void init()
{
	std::memset(ehead,255,sizeof(ehead));
	cnt=tot=0;
}

int main()
{
	init();
	int n,m;
	scanf("%d%d",&n,&m);
	int x,y;
	for (int i=1; i<=m; ++i)
	{
		std::scanf("%d%d",&data[i].src,&data[i].dest);
		insert_edge(data[i].src,data[i].dest);
	}
	for (int i=1; i<=n; ++i) if (!dis[i]) dfs(i);
	init();
	for (int i=1; i<=m; ++i)
		insert_edge(contract[data[i].src],contract[data[i].dest]);
	int ans=0;
	for (int i=1; i<=n; ++i)
		if (contract[i]==i && ehead[i]==-1)
			if (ans==0) ans=i; else if (ans!=i) ans=-1;
	switch (ans)
	{
		case -1:
			printf("0\n");
			break;
		case 0:
			printf("%d\n",n);
			break;
		default:
			{
				int tmp=0;
				for (int i=1; i<=n; ++i) tmp+=(contract[i]==ans);
				printf("%d\n",tmp);
				break;
			}
	}
	return 0;
}
