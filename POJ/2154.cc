#include <cstdio>

int n, mod;

int pow(int base, int exp) {
  base %= mod;
  int res = 1;
  while (exp) {
    if (exp & 1) res = (res * base) % mod;
    base = (base * base) % mod;
    exp >>= 1;
  }
  return res;
}

int phi(int n) {
  int res = n;
  for (int i = 2; i * i <= n; ++i) {
    if ((n % i) == 0) {
      res = res / i * (i - 1);
      while ((n % i) == 0) n /= i;
    }
  }
  if (n > 1) res = res / n * (n - 1);
  return res %= mod;
}

int cal(int n) {
  int res = 0;
  for (int i = 1; i * i <= n; ++i) {
    if ((n % i) == 0) {
      res = (res + phi(n / i) * pow(n, i - 1)) % mod;
      if (i * i < n) res = (res + phi(i) * pow(n, n / i - 1)) % mod;
    }
  }
  return res;
}

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d", &n, &mod);
    printf("%d\n", cal(n));
  }
  return 0;
}
