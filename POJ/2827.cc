#include <cstdio>
#include <algorithm>
#include <utility>
#include <map>

#define rep(i,p,q) for (i=p; i<q; ++i)

const int maxn=20000+10;

int father[maxn],sum[maxn],cnt;
std::map<int,int> hash;

int find(int p)
{
	int tmp=father[p];
	if (tmp==p) return p;
	father[p]=find(tmp);
	sum[p]+=sum[tmp];
	return father[p];
}

void solve(int p, int q, int v)
{
	int fp=find(p),fq=find(q);
	if (fp==fq)
	{
		if (sum[q]-sum[p] == v) std::puts("Accept"); else
			std::printf("Bug Detected %d\n",sum[q]-sum[p]);
	}
	else
	{
		std::puts("Accept");
		father[fq]=fp;
		sum[fq]=v+sum[p]-sum[q];
	}
}

int main()
{
	int m;
	while (~std::scanf("%d",&m))
	{
		int i,j,v;
		hash.clear(),cnt=0;
		rep(i,0,maxn) father[i]=i,sum[i]=0;
		while (m--)
		{
			std::scanf("%d%d%d",&i,&j,&v);
			if (i>j) std::swap(i,j);
			--i;
			if (hash.count(i)==0) hash[i]=cnt++;
			if (hash.count(j)==0) hash[j]=cnt++;
			solve(hash[i],hash[j],v);
		}
	}
	return 0;
}
