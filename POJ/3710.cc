#include <cstdio>
#include <cstring>
#include <vector>
#include <stack>
#include <algorithm>

const int N = 100 + 10;

int n, m, cnt, adj[N][N];
int sg[N];

bool flag[N], cycle[N];
std::stack<int> stk;

int cal(int a, int fa) {
  stk.push(a);
  flag[a] = true;
  int res = 0;
  for (int b = 1; b <= n; ++b) {
    if (adj[a][b]) {
      adj[a][b]--;
      adj[b][a]--;
      int tmp;
      if (!flag[b]) {
        tmp = cal(b, a) + 1;
      } else {
        for (; !stk.empty() && stk.top() != b; stk.pop()) cycle[stk.top()] = true;
        return 1;
      }
      if (cycle[b]) res ^= (tmp & 1); else res ^= tmp;
    }
  }
  return res;
}

int main() {
  while (scanf("%d", &cnt) != EOF) {
    int ans = 0;
    while (cnt--) {
      scanf("%d%d", &n, &m);
      memset(adj, 0, sizeof adj);
      while (m--) {
        int a, b;
        scanf("%d%d", &a, &b);
        adj[a][b]++;
        adj[b][a]++;
      }
      while (!stk.empty()) stk.pop();
      memset(flag, false, sizeof flag);
      memset(cycle, false, sizeof cycle);
      ans ^= cal(1, 0);
    }
    puts(ans ? "Sally" : "Harry");
  }
  return 0;
}
