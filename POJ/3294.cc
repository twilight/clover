#include <cstdio>
#include <cstring>
#include <set>
#include <queue>
#include <string>
#include <vector>
#include <iostream>
#include <iterator>
#include <algorithm>

const int N = 200000 + 10;

int n, len;
int s[N], tag[N];

namespace SuffixArray {

int sa[N], rank[N], gap, h[N];

inline int sufComp(int lhs, int rhs) {
  if (rank[lhs] != rank[rhs]) return rank[lhs] < rank[rhs];
  lhs += gap, rhs += gap;
  return lhs <= len && rhs <= len ? rank[lhs] < rank[rhs] : lhs > rhs;
}

void build() {
  gap = 0;
  static int sum[N];
  memset(sum, 0, sizeof sum);
  for (int i = 1; i <= len; ++i) ++sum[rank[i] = s[i]];
  for (int i = 1; i < N; ++i) sum[i] += sum[i - 1];
  for (int i = 1; i <= len; ++i) sa[sum[rank[i]]--] = i;
  static int temp[N];
  for (int i = 1; i <= len; ++i) temp[sa[i]] = temp[sa[i - 1]] + sufComp(sa[i - 1], sa[i]);
  memcpy(rank, temp, sizeof temp);
  for (gap = 1;; gap *= 2) {
    for (int i = 1; i <= gap; ++i) temp[i] = len - i + 1;
    for (int i = 1, j = gap; i <= len; ++i) if (sa[i] > gap) temp[++j] = sa[i] - gap;
    memset(sum, 0, sizeof sum);
    for (int i = 1; i <= len; ++i) ++sum[rank[i]];
    for (int i = 1; i <= len; ++i) sum[i] += sum[i - 1];
    for (int i = len; i > 0; --i) sa[sum[rank[temp[i]]]--] = temp[i];
    for (int i = 1; i <= len; ++i) temp[sa[i]] = temp[sa[i - 1]] + sufComp(sa[i - 1], sa[i]);
    memcpy(rank, temp, sizeof temp);
    if (rank[sa[len]] == len) break;
  }
  for (int i = 1, k = 0; i <= len; ++i) {
    if (rank[i] == 1) continue;
    for (int j = sa[rank[i] - 1]; s[i + k] == s[j + k];) ++k;
    h[rank[i]] = k;
    if (k) --k;
  }
}

void solve() {
  static int count[N];
  memset(count, 0, sizeof count);
  int sum = 0;
  std::vector<int> ans;
  int mx = 0;
  typedef std::pair<int, int> Info;
  std::deque<Info> q;
  for (int i = 1, j = 1; i <= len; ++i) {
    if (!tag[sa[i]]) continue;
    while (!q.empty() && q.back().second > h[i]) q.pop_back();
    q.push_back(Info(i, h[i]));
    if (count[tag[sa[i]]]++ == 0) ++sum;
    while (sum - (count[tag[sa[j]]] == 1) > n / 2) sum -= (--count[tag[sa[j++]]] == 0);
    while (!q.empty() && q.front().first <= j) q.pop_front();
    if (sum > n / 2) {
      if (q.front().second > mx) {
        mx = q.front().second;
        ans.clear();
      }
      if (q.front().second == mx) ans.push_back(i);
    }
  }
  if (!mx) {
    puts("?");
    return;
  }
  std::set<std::string> pool;
  for (int i = 0; i < ans.size(); ++i) {
    std::string cur;
    for (int j = 0; j < mx; ++j) cur.push_back(s[sa[ans[i]] + j]);
    pool.insert(cur);
  }
  std::copy(pool.begin(), pool.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
}

}

int main() {
  freopen("in.txt", "r", stdin);
  while (scanf("%d", &n), n) {
    len = 0;
    for (int i = 1; i <= n; ++i) {
      static char temp[N];
      scanf(" %s", temp);
      if (n == 1) puts(temp);
      for (char *it = temp; *it; ++it) s[++len] = *it, tag[len] = i;
      s[++len] = N - i, tag[len] = 0;
    }
    if (n > 1) {
      SuffixArray::build();
      SuffixArray::solve();
    }
    puts("");
  }
  return 0;
}
