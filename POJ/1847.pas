const
  maxn=100;

var
  n,src,dest,i,j,k,tmp:longint;
  adj:array [0..maxn,0..maxn] of longint;

function min(p,q:longint):longint; inline;
begin
  if p<q then exit(p) else exit(q);
end;

begin
  readln(n,src,dest);
  fillchar(adj,sizeof(adj),$3A);
  for i:=1 to n do
    begin
      read(k);
      read(tmp);
      adj[i,tmp]:=0;
      for j:=2 to k do
        begin
          read(tmp);
          adj[i,tmp]:=1;
        end;
    end;
  for k:=1 to n do
    for i:=1 to n do
      for j:=1 to n do
        adj[i,j]:=min(adj[i,j],adj[i,k]+adj[k,j]);
  if adj[src,dest]<>$3A3A3A3A then writeln(adj[src,dest]) else writeln(-1);
end.