#include <cstdio>
#include <cstring>
#include <vector>

const int list[] = {2, 3, 5, 7, 13, 17, 19, 31}, SZ = 8;

int k;

int main() {
  while (scanf("%d", &k) != EOF) {
    static std::vector<int> state;
    state.clear();
    for (int i = 1; i <= k; ++i) {
      unsigned p;
      scanf("%u", &p);
      state.push_back(0);
      for (int j = SZ - 1; j >= 0; --j) {
        unsigned cur = (1U << list[j]) - 1;
        if (p % cur == 0) {
          p /= cur;
          if (p % cur == 0) break;
          state.back() |= (1 << j);
        }
      }
      if (p > 1) state.pop_back();
    }
    static bool flag[1 << SZ];
    memset(flag, false, sizeof flag);
    flag[0] = true;
    for (int i = 0; i < state.size(); ++i)
      for (int j = 0; j < (1 << SZ); ++j)
        if (flag[j] && !(j & state[i])) flag[state[i] | j] = true;
    int ans = 0;
    for (int i = 0; i < (1 << SZ); ++i) {
      if (flag[i]) {
        int cur = 0;
        for (int j = 0; j < SZ; ++j) if (i >> j & 1) cur += list[j];
        ans = std::max(ans, cur);
      }
    }
    if (ans) printf("%d\n", ans); else puts("NO");
  }
  return 0;
}
