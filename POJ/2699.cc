#include <cstring>
#include <iostream>
#include <string>
#include <sstream>
#include <algorithm>

const int N = 10 + 1, V = N * N + N + 10, E = V * 5;

int n, a[N];
int id[N][N], tot;

int adj[V], arc[V];
int to[E], next[E], cap[E], cnt;

inline void link(int a, int b, int c) {
  to[cnt] = b, next[cnt] = adj[a], cap[cnt] = c, adj[a] = cnt++;
  to[cnt] = a, next[cnt] = adj[b], cap[cnt] = 0, adj[b] = cnt++;
}

int h[V], gap[V];

int dfs(int a, int df, const int s, const int t) {
  if (a == t) return df;
  int cur = 0;
  for (int &it = arc[a]; it != -1; it = next[it]) {
    int b = to[it], c = cap[it];
    if (c && h[a] == h[b] + 1) {
      int f = dfs(b, std::min(c, df - cur), s, t);
      cap[it] -= f;
      cap[it ^ 1] += f;
      cur += f;
    }
    if (cur == df) return cur;
  }
  if (--gap[h[a]] == 0) h[s] = tot + 2;
  ++gap[++h[a]];
  arc[a] = adj[a];
  return cur;
}

int flow(const int s, const int t) {
  memset(h, 0, sizeof h);
  memset(gap, 0, sizeof gap);
  memcpy(arc, adj, sizeof arc);
  int res = 0;
  while (h[s] < tot + 2) res += dfs(s, 1e9, s, t);
  return res;
}

bool ok(int k) {
  memset(adj, -1, sizeof adj);
  cnt = 0;
  const int s = 0, t = tot + 1;
  for (int i = 1; i <= n; ++i) link(s, i, a[i]);
  for (int i = n + 1; i <= tot; ++i) link(i, t, 1);
  for (int i = 1; i <= n; ++i) {
    for (int j = i + 1; j <= n; ++j) {
      if (i >= n - k + 1 && j >= n - k + 1 && a[i] < a[j]) {
        link(i, id[i][j], 1);
      } else {
        link(i, id[i][j], 1);
        link(j, id[i][j], 1);
      }
    }
  }
  return flow(s, t) == n * (n - 1) / 2;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int tcase;
  for (std::cin >> tcase, std::cin.get(); tcase--;) {
    std::string s;
    std::getline(std::cin, s);
    std::stringstream ss(s);
    for (n = 0; ss >> a[n + 1];) ++n;
    tot = n;
    for (int i = 1; i <= n; ++i)
      for (int j = i + 1; j <= n; ++j)
        id[i][j] = id[j][i] = ++tot;
    int ans;
    for (ans = n; ans > 1 && !ok(ans);) --ans;
    std::cout << ans << std::endl;
  }
  return 0;
}
