#include <cstdio>
#include <cmath>

const long double PI=acos(-1.0);

using namespace std;

int main()
{
	double s;
	while (~scanf("%lf",&s))
	{
		double r=sqrt(s/PI/4),h=sqrt(2*s/PI);
		double l=sqrt(r*r+h*h);
		double v=PI*r*r*h/3.0;
		printf("%.2lf\n%.2lf\n%.2lf\n",v,h,r);
	}
	return 0;
}
