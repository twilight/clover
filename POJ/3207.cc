#include <cstdio>
#include <stack>
#include <vector>
#include <algorithm>

using namespace std;

const int maxn = 1010;

vector<int> epool[maxn];

struct node {
	int s,t;
} e[maxn];

inline bool cross(const node &p, const node &q) {
	if (p.s <= q.s && q.s <= p.t && p.t <= q.t) return true;
	if (q.s <= p.s && p.s <= q.t && q.t <= p.t) return true;
	return false;
}

int scc[maxn],dfn[maxn],low[maxn],cnt = 0,tot = 0;
bool instack[maxn];
stack<int> stk;

void tarjan(int i) {
	dfn[i] = low[i] = ++tot;
	stk.push(i);
	instack[i] = true;
	for (vector<int>::iterator iter = epool[i].begin(); iter != epool[i].end(); ++iter) {
		int j = *iter;
		if (!dfn[j]) tarjan(j);
		if (instack[j]) low[i] = min(low[i],low[j]);
	}
	if (dfn[i] == low[i]) {
		++cnt;
		int j;
		do {
			j = stk.top(), stk.pop();
			instack[j] = false;
			scc[j] = cnt;
		} while (j != i);
	}
}

int main() {
	int n;
	scanf("%d%d",&n,&n);
	for (int i=0; i<n; ++i) {
		scanf("%d%d",&e[i].s,&e[i].t);
		if (e[i].s > e[i].t) swap(e[i].s,e[i].t);
	}
	for (int i=0; i<n; ++i) {
		for (int j=i+1; j<n; ++j) {
			if (cross(e[i],e[j])) {
				epool[i].push_back(j+n);
				epool[j].push_back(i+n);
				epool[i+n].push_back(j);
				epool[j+n].push_back(i);
			}
		}
	}
	for (int i=0; i<n; ++i) if (!dfn[i]) tarjan(i);
	for (int i=0; i<n; ++i)
		if (scc[i] == scc[i+n])
			return puts("the evil panda is lying again"), 0;
	puts("panda is telling the truth...");
	return 0;
}
