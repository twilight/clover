#include <cstdio>
#include <cmath>

typedef long long int64;

int64 gcd(int64 a, int64 b) { return b ? gcd(b, a % b) : a; }

int64 cal(int c, int s) {
  int64 rot = 0, rev = 0;
  for (int i = 1; i <= s; ++i) rot += pow(c, gcd(s, i));
  if (s & 1)
    rev = s * pow(c, s / 2 + 1);
  else
    rev = s / 2 * pow(c, s / 2 + 1) + s / 2 * pow(c, s / 2);
  return (rot + rev) / (s * 2);
}

int main() {
  for (int c, s; scanf("%d%d", &c, &s), c | s;)
    printf("%lld\n", cal(c, s));
  return 0;
}
