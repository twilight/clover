#include <cstdio>
#include <cmath>
#include <vector>
#include <complex>
#include <algorithm>

const int N = 1000;

typedef std::complex<double> point;
typedef std::pair<point, point> line;
typedef std::vector<point> polygon;

#define x real()
#define y imag()
#define fst first
#define snd second

#ifndef M_PI
const double M_PI = acos(-1.0);
#endif

const double eps = 1e-10;

inline int sgn(double num) {
  if (fabs(num) < eps) return 0;
  return num > 0 ? 1 : -1;
}

inline double cross(const point &a, const point &b) {
  return a.x * b.y - a.y * b.x;
}

inline double cross(const point &o, const point &a, const point &b) {
  return cross(a - o, b - o);
}

inline point intersection(const line &m, const line &n) {
  point a(m.snd - m.fst), b(n.snd - n.fst), s(n.fst - m.fst);
  return m.fst + a * cross(s, b) / cross(a, b);
}

inline line rotate(const line &cur, double deg) {
  point mid((cur.fst + cur.snd) / 2.0);
  const point det(cos(deg), sin(deg));
  return line(mid + (cur.fst - mid) * det,
              mid + (cur.snd - mid) * det);
}

inline bool cmp(const line &a, const line &b) {
  return sgn(std::arg(a.snd - a.fst) - std::arg(b.snd - b.fst)) < 0;
}

inline bool parallel(const line &a, const line &b) {
  return sgn(cross(a.snd - a.fst, b.snd - b.fst)) == 0;
}

double halfplane(std::vector<line> &pool, const line &cur) {
  pool.push_back(cur);
  std::sort(pool.begin(), pool.end(), cmp);
  static line q[N], *qf, *qr;
  qf = q + 1, qr = q;
  #define SIZE (qr - qf + 1)
  for (int i = 0; i < pool.size(); ++i) {
    if (SIZE > 0 && parallel(*qr, pool[i])) {
      if (sgn(cross(qr->fst, qr->snd, pool[i].fst)) <= 0) continue;
      if (SIZE == 1) {
        *qr = pool[i];
        continue;
      }
    }
    while (SIZE >= 2 && sgn(cross(pool[i].fst, pool[i].snd, intersection(*(qr - 1), *qr))) <= 0) --qr;
    while (SIZE >= 2 && sgn(cross(pool[i].fst, pool[i].snd, intersection(*qf, *(qf + 1)))) <= 0) ++qf;
    *++qr = pool[i];
  }
  for (bool flag = true; flag;) {
    flag = false;
    while (SIZE >= 3 && sgn(cross(qr->fst, qr->snd, intersection(*qf, *(qf + 1)))) <= 0) ++qf, flag = true;
    while (SIZE >= 3 && sgn(cross(qf->fst, qf->snd, intersection(*(qr - 1), *qr))) <= 0) --qr, flag = true;
  }
  if (SIZE < 3) return 0;
  static polygon tmp;
  tmp.clear(), pool.clear();
  for (line *it = qf; it <= qr; ++it) pool.push_back(*it);
  for (line *it = qf; it < qr; ++it) tmp.push_back(intersection(*it, *(it + 1)));
  tmp.push_back(intersection(*qr, *qf));
  double res = cross(tmp.back(), tmp.front());
  for (int i = 0; i + 1 < tmp.size(); ++i) res += cross(tmp[i], tmp[i + 1]);
  return fabs(res) / 2.0;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  static std::vector<line> pool(4);
  pool[0] = line(point(0, 0), point(10, 0));
  pool[1] = line(point(10, 0), point(10, 10));
  pool[2] = line(point(10, 10), point(0, 10));
  pool[3] = line(point(0, 10), point(0, 0));
  static char op[10];
  bool zero = false;
  double ans = 0.0;
  for (point cur, prev(0, 0); scanf("%lf%lf %s", &cur.x, &cur.y, op) != EOF;) {
    if (!zero) {
      switch (op[0]) {
        case 'C':
          ans = halfplane(pool, rotate(line(prev, cur), M_PI / 2.0));
          break;
        case 'H':
          ans = halfplane(pool, rotate(line(prev, cur), -M_PI / 2.0));
          break;
        case 'S':
          if (zero = (prev != cur)) ans = 0.0;
          break;
      }
    }
    zero |= (sgn(ans) == 0);
    prev = cur;
    printf("%.2f\n", ans);
  }
  return 0;
}
