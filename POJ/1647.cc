#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>

#define fst first
#define snd second

typedef std::pair<int, int> Point;

const int N = 8;
const int dx[] = {-1, -1, -1, 0, 0, 1, 1, 1};
const int dy[] = {-1, 0, 1, -1, 1, -1, 0, 1};

Point pool[3];

inline bool Check(const Point &p) {
  return 0 <= p.fst && p.fst < N && 0 <= p.snd && p.snd < N;
}

inline bool One(const Point &u, const Point &v) {
  return abs(u.fst - v.fst) <= 1 && abs(u.snd - v.snd) <= 1;
}

inline bool Checkmate(const Point &k, const Point &q, const Point &enemy) {
  static bool forbid[N][N];
  if (One(k, enemy)) return false;
  memset(forbid, false, sizeof forbid);
  for (int i = 0; i < 8; ++i) {
    Point cur(k.fst + dx[i], k.snd + dy[i]);
    if (Check(cur)) forbid[cur.fst][cur.snd] = true;
  }
  for (int i = 0; i < 8; ++i) {
    for (Point cur(q.fst + dx[i], q.snd + dy[i]); Check(cur); cur.fst += dx[i], cur.snd += dy[i]) {
      forbid[cur.fst][cur.snd] = true;
      if (cur == k) break;
    }
  }
  if (!forbid[enemy.fst][enemy.snd]) return false;
  for (int i = 0; i < 8; ++i) {
    Point cur(enemy.fst + dx[i], enemy.snd + dy[i]);
    if (Check(cur) && !forbid[cur.fst][cur.snd]) return false;
  }
  return true;
}

int main() {
  for (int i = 0; i < 3; ++i) {
    char ch;
    int x;
    scanf(" %c%d", &ch, &x);
    pool[i].fst = ch - 'a';
    pool[i].snd = x - 1;
  }
  Point ans(10, 10);
  for (int i = 0; i < 8; ++i) {
    for (Point cur(pool[1].fst + dx[i], pool[1].snd + dy[i]); Check(cur); cur.fst += dx[i], cur.snd += dy[i]) {
      if (cur == pool[0]) break;
      if (cur == pool[2]) break;
      if (Checkmate(pool[0], cur, pool[2])) ans = std::min(ans, cur);
    }
  }
  if (ans.fst < 10) printf("%c%d\n", ans.fst + 'a', ans.snd + 1); else puts("no");
  return 0;
}
