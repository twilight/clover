#include <cstdio>
#include <cstring>
#include <cctype>
#include <iostream>
#include <queue>
#include <algorithm>

typedef long long int64;

const int N = 200000 + 10, SZ = 26 * 2;

int k;
char a[N], b[N];

inline int dispatch(char ch) { return isupper(ch) ? (ch - 'A' + 26) : (ch - 'a'); }

struct node_t {
  node_t *next[SZ], *pre;
  int right, cnt;
} pool[2 * N], *tot, *root, *last;

void Append(int ch) {
  node_t *p = last, *np = ++tot;
  np->right = p->right + 1;
  np->cnt = 1;
  for (; p && !p->next[ch]; p = p->pre) p->next[ch] = np;
  if (!p) {
    np->pre = root;
  } else {
    node_t *q = p->next[ch];
    if (q->right == p->right + 1) {
      np->pre = q;
    } else {
      node_t *nq = ++tot;
      *nq = *q;
      nq->cnt = 0;
      nq->right = p->right + 1;
      q->pre = np->pre = nq;
      for (; p && p->next[ch] == q; p = p->pre) p->next[ch] = nq;
    }
  }
  last = np;
}

int64 mem[2 * N];

int64 DFS(node_t *cur) {
  if (!cur || cur == root) return 0;
  int64 &res = mem[cur - pool];
  if (~res) return res;
  if (cur->pre->right >= k)
    res = (int64)(cur->right - cur->pre->right) * cur->cnt + DFS(cur->pre);
  else if (cur->right >= k)
    res = (int64)(cur->right - k + 1) * cur->cnt;
  else
    res = 0;
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  while (scanf("%d", &k), k) {
    scanf(" %s %s", a, b);
    memset(pool, 0, sizeof pool);
    tot = root = last = pool;
    for (char *it = a; *it != '\0'; ++it) Append(dispatch(*it));
    static int deg[2 * N];
    static std::queue<int> q;
    std::fill(deg, deg + (tot - pool) + 1, 0);
    for (node_t *it = pool + 1; it <= tot; ++it) ++deg[it->pre - pool];
    for (int i = 1; i <= tot - pool; ++i) if (!deg[i]) q.push(i);
    for (; !q.empty(); q.pop()) {
      int a = q.front();
      if (pool[a].pre) {
        pool[a].pre->cnt += pool[a].cnt;
        if (--deg[pool[a].pre - pool] == 0) q.push(pool[a].pre - pool);
      }
    }
    node_t *cur = root;
    int matched = 0;
    int64 ans = 0;
    std::fill(mem, mem + (tot - pool) + 1, -1);
    for (char *it = b; *it != '\0'; ++it) {
      int c = dispatch(*it);
      if (cur->next[c]) {
        cur = cur->next[c];
        ++matched;
      } else {
        while (cur && !cur->next[c]) cur = cur->pre;
        if (cur)
          matched = cur->right + 1, cur = cur->next[c];
        else
          matched = 0, cur = root;
      }
      if (matched < k) continue;
      if (cur->pre->right >= k)
        ans += DFS(cur->pre) + (int64)(matched - cur->pre->right) * cur->cnt;
      else
        ans += (int64)(matched - k + 1) * cur->cnt;
    }
    std::cout << ans << std::endl;
  }
  return 0;
}
