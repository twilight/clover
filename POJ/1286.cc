#include <cstdio>
#include <cmath>

typedef long long int64;

int64 gcd(int64 a, int64 b) { return b ? gcd(b, a % b) : a; }

int64 cal(int col, int n) {
  if (n == 0) return 0;
  int64 res = 0;
  for (int i = 1; i <= n; ++i) res += pow(col, gcd(i, n));
  if (n & 1)
    res += n * pow(col, n / 2 + 1);
  else
    res += (n / 2 * pow(col, n / 2 + 1) + n / 2 * pow(col, n / 2));
  return res /= (2 * n);
}

int main() {
  for (int n; scanf("%d", &n), ~n;) printf("%lld\n", cal(3, n));
  return 0;
}
