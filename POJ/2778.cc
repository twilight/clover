#include <cstdio>
#include <cstring>
#include <map>
#include <queue>

const int MOD = 100000, SZ = 100 + 10;

typedef long long int64;
typedef int matrix[SZ][SZ];

inline void INC(int &a, int b) { a += b; if (a > MOD) a -= MOD; }
inline int pdt(int a, int b) { return ((int64)a * b) % MOD; }

int m, n;
char s[10];

std::map<char, int> map;

matrix adj;

struct {
  int next[4], fail;
  bool flag;
} pool[1000];

int top = 1, root = 1;

void add(char s[]) {
  int u = root;
  for (char *it = s; *it != '\0'; ++it) {
    char ch = map[*it];
    if (!pool[u].next[ch]) pool[u].next[ch] = ++top;
    u = pool[u].next[ch];
  }
  pool[u].flag = true;
}

void build() {
  static std::queue<int> q;
  q.push(root);
  while (!q.empty()) {
    int a = q.front();
    q.pop();
    pool[a].flag |= pool[pool[a].fail].flag;
    for (int i = 0; i < 4; ++i) {
      int &b = pool[a].next[i];
      (b ? (q.push(b), pool[b].fail) : b) = (a == root) ? root : pool[pool[a].fail].next[i];
    }
  }
  for (int a = 1; a <= top; ++a) {
    if (!pool[a].flag) {
      for (int i = 0; i < 4; ++i) {
        int b = pool[a].next[i];
        if (!pool[b].flag) adj[a][b]++;
      }
    }
  }
}

void mul(matrix a, const matrix b) {
  static matrix c;
  memset(c, 0, SZ * SZ * sizeof(int));
  for (int i = 1; i <= top; ++i)
    for (int j = 1; j <= top; ++j)
      for (int k = 1; k <= top; ++k)
        INC(c[i][j], pdt(a[i][k], b[k][j]));
  memcpy(a, c, SZ * SZ * sizeof(int));
}

void pow(matrix base, int exp) {
  matrix res, tmp;
  for (int i = 1; i <= top; ++i) res[i][i] = 1;
  memcpy(tmp, base, SZ * SZ * sizeof(int));
  while (exp) {
    if (exp & 1) mul(res, tmp);
    mul(tmp, tmp);
    exp >>= 1;
  }
  memcpy(base, res, SZ * SZ * sizeof(int));
}

int main() {
  scanf("%d%d", &m, &n);
  map['A'] = 0, map['C'] = 1, map['T'] = 2, map['G'] = 3;
  pool[root].fail = root;
  for (int i = 1; i <= m; ++i) {
    scanf(" %s", s);
    add(s);
  }
  build();
  pow(adj, n);
  int ans = 0;
  for (int i = 1; i <= top; ++i) INC(ans, adj[root][i]);
  printf("%d\n", ans);
  return 0;
}
