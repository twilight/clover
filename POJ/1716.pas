{$M 16777216}
{$inline on}

const
  maxn=10010;
  oo=1000000;

var
  edge:array [0..maxn*10] of record
                               adj,next,cost:longint;
                             end;
  ehead:array [0..maxn] of longint;
  cnt,r,n,i,p,q,c:longint;

procedure insert_edge(p,q,c:longint); inline;
begin
  edge[cnt].adj:=q;
  edge[cnt].next:=ehead[p];
  edge[cnt].cost:=c;
  ehead[p]:=cnt;
  inc(cnt);
end;

function bellman_ford:longint;
var
  dis:array [0..maxn] of longint;
  queue:array [0..maxn*10] of longint;
  inqueue:array [0..maxn] of boolean;
  i,j,p,w,head,tail:longint;
  flag:boolean;

begin
  for i:=1 to r do dis[i]:=oo;
  fillchar(inqueue,sizeof(inqueue),false);
  dis[0]:=0;
  inqueue[0]:=true;
  head:=0;
  tail:=1;
  queue[1]:=0;
  repeat
    inc(head);
    i:=queue[head];
    p:=ehead[i];
    inqueue[i]:=false;
    while p<>-1 do
      begin
        j:=edge[p].adj;
        w:=edge[p].cost;
        if dis[i]+w<dis[j] then
          begin
            dis[j]:=dis[i]+w;
            if not inqueue[j] then
              begin
                inc(tail);
                queue[tail]:=j;
                inqueue[j]:=true;
              end;
          end;
        p:=edge[p].next;
      end;
  until head>=tail;
  exit(dis[r]-dis[1]);
end;

begin
  readln(n);
  fillchar(ehead,sizeof(ehead),255); //255=0xFF, longint=4*char=0xFFFFFFFF=-1
  for i:=1 to n do
    begin
      readln(p,q);
      inc(p);
      inc(q);
      if q+1>r then r:=q+1;
      insert_edge(q+1,p,-2);
    end;
  for i:=1 to r do
    begin
      insert_edge(0,i,0);
      insert_edge(i+1,i,0);
      insert_edge(i,i+1,1);
    end;
  writeln(bellman_ford);
end.