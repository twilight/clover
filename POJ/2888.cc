#include <cstdio>
#include <cstring>

typedef long long int64;
typedef int matrix[11][11];

const int MOD = 9973;

inline void INC(int &a, int b) { a = (a + b) % MOD; }
inline void MUL(int &a, int b) { a = (int64)a * b % MOD; }
inline int pdt(int a, int b) { return (int64)a * b % MOD; }

int n, m, k;
matrix adj;

inline void mul(matrix a, const matrix b) {
  static matrix c;
  for (int i = 1; i <= m; ++i) {
    for (int j = 1; j <= m; ++j) {
      c[i][j] = 0;
      for (int k = 1; k <= m; ++k)
        c[i][j] += a[i][k] * b[k][j];
      c[i][j] %= MOD;
    }
  }
  memcpy(a, c, sizeof(matrix));
}

inline void pow(matrix a, int exp) {
  static matrix res, base;
  memcpy(base, a, sizeof(matrix));
  memset(res, 0, sizeof(matrix));
  for (int i = 1; i <= m; ++i) res[i][i] = 1;
  while (exp) {
    if (exp & 1) mul(res, base);
    mul(base, base);
    exp >>= 1;
  }
  memcpy(a, res, sizeof(matrix));
}

inline int pow(int base, int exp) {
  int res = 1;
  while (exp) {
    if (exp & 1) MUL(res, base);
    MUL(base, base);
    exp >>= 1;
  }
  return res;
}

inline int inv(int a) { return pow(a, MOD - 2); }
inline int qtt(int a, int b) { return pdt(a, inv(b)); }
inline int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }

inline int phi(int n) {
  int res = n;
  for (int i = 2; i * i <= n; ++i) {
    if (n % i == 0) {
      res = res / i * (i - 1);
      while (n % i == 0) n /= i;
    }
  }
  if (n > 1) res = res / n * (n - 1);
  return res;
}

inline int loop(int len) {
  static matrix tmp;
  memcpy(tmp, adj, sizeof(matrix));
  pow(tmp, len);
  int res = 0;
  for (int i = 1; i <= m; ++i) INC(res, tmp[i][i]);
  return res;
}

int cal() {
  int res = 0;
  for (int i = 1; i * i <= n; ++i) {
    if (n % i == 0) {
      INC(res, pdt(phi(n / i), loop(i)));
      if (i * i < n) INC(res, pdt(phi(i), loop(n / i)));
    }
  }
  return qtt(res, n);
}

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d%d", &n, &m, &k);
    for (int i = 1; i <= m; ++i)
      for (int j = 1; j <= m; ++j)
        adj[i][j] = 1;
    while (k--) {
      int i, j;
      scanf("%d%d", &i, &j);
      adj[i][j] = adj[j][i] = 0;
    }
    printf("%d\n", cal());
  }
  return 0;
}
