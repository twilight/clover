#include <cmath>
#include <cstdio>
#include <cassert>
#include <cstring>
#include <algorithm>

const int N = 30 + 10, LIM = 10;

int n, m, player[2][4];

bool flag[2][N][N];
double prob[2][N][N];

inline double threepts(int p) {
  return (double)player[p][1] / (player[p][0] + player[p][1]);
}

inline double succeed(int p, int type) {
  return (type == 1 ? 0.8 : 1.0) * player[p][type] / (player[p][type] + player[p ^ 1][3]);
}

inline double rebound(int p) {
  return 0.8 * player[p][2] / (player[p][2] + player[p ^ 1][2]);
}

inline double det(double a, double b,
                  double c, double d) {
  return a * d - b * c;
}

inline void Solve(double a1, double b1, double c1,
           double a2, double b2, double c2,
           double &x, double &y) {
  double d = det(a1, b1, a2, b2), dx = det(-c1, b1, -c2, b2), dy = det(a1, -c1, a2, -c2);
  x = dx / d, y = dy / d;
}

double Calc(int cur, int u, int v) {
  if (u >= n) return 1.0;
  if (v >= n) return 0.0;
  double &p = prob[cur][u][v], &q = prob[cur ^ 1][v][u];
  if (flag[cur][u][v]) return p;
  flag[cur][u][v] = flag[cur ^ 1][v][u] = true;
  double x = threepts(cur);
  double y = succeed(cur, 1), z = succeed(cur, 0);
  double w = rebound(cur);
  double a1 = (x * (1.0 - y) + (1.0 - x) * (1.0 - z)) * w - 1.0;
  double b1 = -(x * (1.0 - y) + (1.0 - x) * (1.0 - z)) * (1.0 - w);
  double c1 = x * y * Calc(cur, u + 3, v) + (1.0 - x) * z * Calc(cur, u + 2, v) + (1.0 - x) * (1.0 - z) * (1.0 - w) + x * (1.0 - y) * (1.0 - w);
  x = threepts(cur ^ 1);
  y = succeed(cur ^ 1, 1), z = succeed(cur ^ 1, 0);
  w = rebound(cur ^ 1);
  double b2 = (x * (1.0 - y) + (1.0 - x) * (1.0 - z)) * w - 1.0;
  double a2 = -(x * (1.0 - y) + (1.0 - x) * (1.0 - z)) * (1.0 - w);
  double c2 = x * y * Calc(cur ^ 1, v + 3, u) + (1.0 - x) * z * Calc(cur ^ 1, v + 2, u) + (1.0 - x) * (1.0 - z) * (1.0 - w) + x * (1.0 - y) * (1.0 - w);
  Solve(a1, b1, c1, a2, b2, c2, p, q);
  return p;
}

int main() {
  while (scanf("%d%d", &n, &m) != EOF) {
    for (int i = 0; i < 4; ++i) scanf("%d", &player[1][i]);
    double ans = 0.0;
    for (int &i = player[0][0] = 1; i <= LIM && i <= m; ++i) {
      for (int &j = player[0][1] = 1; j <= LIM && i + j <= m; ++j) {
        for (int &k = player[0][2] = 1; k <= LIM && i + j + k <= m; ++k) {
          int rem = player[0][3] = m - i - j - k;
          if (!(1 <= rem && rem <= LIM)) continue;
          memset(flag, false, sizeof flag);
          ans = std::max(ans, Calc(0, 0, 0));
        }
      }
    }
    printf("%.3f\n", ans);
  }
  return 0;
}
