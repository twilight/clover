#include <cstdio>
#include <cstring>

const int N = 1 << 4;

typedef int Matrix[N][N];

int n, MOD;
Matrix trans, cur;

bool start[N];

void DFS(int dep, int s, const int c) {
  if (dep > 4) return;
  if (dep == 4) {
    ++trans[c][s];
    return;
  }
  if (!(c >> dep & 1)) {
    DFS(dep + 1, s | (1 << dep), c);
  } else {
    DFS(dep + 1, s, c);
    if (dep < 3 && (c >> (dep + 1) & 1)) DFS(dep + 2, s | (1 << dep) | (1 << (dep + 1)), c);
  }
}

void Multiply(Matrix a, const Matrix b) {
  static Matrix c;
  memset(c, 0, sizeof c);
  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j)
      for (int k = 0; k < N; ++k)
        (c[i][j] += a[i][k] * b[k][j] % MOD) %= MOD;
  memcpy(a, c, sizeof c);
}

void Pow(Matrix base, int exp) {
  static Matrix res;
  memset(res, 0, sizeof res);
  for (int i = 0; i < N; ++i) res[i][i] = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) Multiply(res, base);
    Multiply(base, base);
  }
  memcpy(base, res, sizeof res);
}

int main() {
  for (int i = 0; i < N; ++i) DFS(0, 0, i);
  start[0] = start[3] = start[6] = start[12] = start[15] = true;
  while (scanf("%d%d", &n, &MOD), MOD) {
    memcpy(cur, trans, sizeof trans);
    Pow(cur, n - 1);
    int ans = 0;
    for (int i = 0; i < N; ++i) if (start[i]) (ans += cur[i][N - 1]) %= MOD;
    printf("%d\n", ans);
  }
  return 0;
}
