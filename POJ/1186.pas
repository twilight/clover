const
  prime=8000037;

var
  n,m,i,mid,ans:longint;
  k,p:array [0..6] of longint;
  s:array [0..prime-1,1..2] of longint;

function locate(x:longint):longint;
var
  j:longint;

begin
  j:=x;
  while j<0 do inc(j,prime);
  while j>prime do dec(j,prime);
  while (s[j,1]<>x) and (s[j,2]<>0) do j:=(j+1) mod prime;
  locate:=j;
end;

procedure ins(k:longint);
var
  t:longint;

begin
  t:=locate(k);
  s[t,1]:=k;
  inc(s[t,2]);
end;

procedure firsthalf(dep,sum:longint);
var
  i,j,t:longint;

begin
  if dep>mid then
    begin
      ins(sum);
      exit;
    end;
  for i:=1 to m do
    begin
      t:=k[dep];
      if (i<>1) and (p[dep]<>0) then for j:=1 to p[dep] do t:=t*i;
      firsthalf(dep+1,sum+t);
    end;
end;

procedure halflast(dep,sum:longint);
var
  i,j,t:longint;

begin
  if dep>n then
    begin
      j:=locate(-sum);
      if s[j,1]=-sum then inc(ans,s[j,2]);
      exit;
    end;
  for i:=1 to m do
    begin
      t:=k[dep];
      if (i<>1) and (p[dep]<>0) then for j:=1 to p[dep] do t:=t*i;
      halflast(dep+1,sum+t);
    end;
end;

begin
  readln(n,m);
  mid:=n div 2;
  for i:=1 to n do readln(k[i],p[i]);
  firsthalf(1,0);
  halflast(mid+1,0);
  writeln(ans);
end.