#include <cstdio>
#include <cmath>
#include <complex>
#include <vector>
#include <algorithm>

const int N = 20000 + 10;
const double eps = 1e-10;

#ifndef M_PI
const double M_PI = acos(-1.0);
#endif

typedef std::complex<double> point;
typedef std::vector<point> polygon;
typedef std::pair<point, point> line;

#define x real()
#define y imag()
#define fst first
#define snd second

inline int sgn(double num) {
  if (fabs(num) < eps) return 0;
  return num > 0 ? 1 : -1;
}

inline double cross(const point &a, const point &b) {
  return a.x * b.y - a.y * b.x;
}

inline double cross(const point &o, const point &a, const point &b) {
  return cross(a - o, b - o);
}

inline point intersection(const line &m, const line &n) {
  point a = m.snd - m.fst, b = n.snd - n.fst, s = n.fst - m.fst;
  return m.fst + a * cross(b, s) / cross(b, a);
}

inline bool parallel(const line &a, const line &b) {
  return sgn(cross(a.snd - a.fst, b.snd - b.fst)) == 0;
}

inline bool cmp(const line &a, const line &b) {
  return std::arg(a.snd - a.fst) < std::arg(b.snd - b.fst);
}

void halfplane(std::vector<line> &pool, polygon &res) {
  res.clear();
  std::sort(pool.begin(), pool.end(), cmp);
  static line q[N], *qf, *qr;
  res.clear();
  qf = q + 1, qr = q;
  #define sz (qr - qf + 1)
  for (int i = 0; i < pool.size(); ++i) {
    if (sz >= 1 && parallel(pool[i], *qr)) {
      if (cross(pool[i].fst, pool[i].snd, qr->fst) >= 0) continue;
      if (sz == 1) {
        *qr = pool[i];
        continue;
      }
    }
    while (sz >= 2 && sgn(cross(pool[i].fst, pool[i].snd, intersection(*(qr - 1), *qr))) <= 0) --qr;
    while (sz >= 2 && sgn(cross(pool[i].fst, pool[i].snd, intersection(*qf, *(qf + 1)))) <= 0) ++qf;
    if (sz == 1 && std::arg(qr->snd - qr->fst) + M_PI <= std::arg(qf->snd - qf->fst)) return;
    *++qr = pool[i];
  }
  while (sz >= 2 && sgn(cross(qf->fst, qf->snd, intersection(*(qr - 1), *qr)) <= 0)) --qr;
  if (sz < 3) return;
  for (line *it = qf; it < qr; ++it) res.push_back(intersection(*it, *(it + 1)));
  res.push_back(intersection(*qf, *qr));
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int n;
  scanf("%d", &n);
  static std::vector<line> pool;
  pool.resize(n);
  for (int i = 0; i < n; ++i) {
    scanf("%lf%lf", &pool[i].fst.x, &pool[i].fst.y);
    scanf("%lf%lf", &pool[i].snd.x, &pool[i].snd.y);
  }
  pool.push_back(line(point(0, 0), point(10000, 0)));
  pool.push_back(line(point(10000, 0), point(10000, 10000)));
  pool.push_back(line(point(10000, 10000), point(0, 10000)));
  pool.push_back(line(point(0, 10000), point(0, 0)));
  static polygon res;
  halfplane(pool, res);
  double ans = (res.size() ? cross(res.back(), res.front()) : 0.0);
  for (int i = 0; i + 1 < res.size(); ++i) ans += cross(res[i], res[i + 1]);
  printf("%.1f\n", fabs(ans) / 2.0);
  return 0;
}
