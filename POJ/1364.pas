const
  maxn=1100;
  maxm=maxn*10;
  oo=100000000;
  trans:array [false..true] of string=('successful conspiracy','lamentable kingdom');

var
  n,m,a,b,k,r,cnt:longint;
  edge:array [0..maxm] of record
                            src,dest,cost:longint;
                          end;
  dis:array [0..maxn] of longint;
  order,ch:char;

procedure init; inline;
begin
  cnt:=0;
  r:=0;
end;

procedure insert_edge(p,q,c:longint); inline;
begin
   inc(cnt);
   edge[cnt].src:=p;
   edge[cnt].dest:=q;
   edge[cnt].cost:=c;
end;

function min(p,q:longint):longint; inline;
begin
  if p<q then exit(p) else exit(q);
end;

procedure relax(p,q,c:longint); inline;
begin
  if dis[p]+c<dis[q] then dis[q]:=dis[p]+c;
end;

function bellman_ford:boolean;
var
  i,k:longint;

begin
  for k:=1 to n do dis[k]:=oo;
  dis[0]:=0;
  for k:=1 to n do
    for i:=1 to cnt do
      with edge[i] do relax(src,dest,cost);
  for i:=1 to cnt do
    with edge[i] do
      if dis[src]+cost<dis[dest] then exit(false);
  exit(true);
end;

begin
  read(n);
  while (n>0) do
    begin
      readln(m);
      init;
      while m>0 do
        begin
          dec(m);
          readln(a,b,ch,order,ch,k);
          inc(b,a);
          dec(a);
          if b+1>r then r:=b+1;
          case order of
            'g':insert_edge(b,a,-k-1);
            'l':insert_edge(a,b,k-1);
          end;
        end;
      writeln(trans[bellman_ford]);
      read(n);
    end;
end.
