#include <cstdio>
#include <map>
#include <bitset>
#include <vector>
#include <algorithm>

#define fst first
#define snd second

const int N = 100 + 10;

typedef std::bitset<N> State;

int n;
std::vector<int> adj[N];

int col[N];

bool Paint(int a) {
  for (std::vector<int>::iterator it = adj[a].begin(); it != adj[a].end(); ++it) {
    int b = *it;
    if (~col[b]) {
      if (col[b] != (col[a] ^ 1)) return false;
    } else {
      col[b] = col[a] ^ 1;
      if (!Paint(b)) {
        col[b] = -1;
        return false;
      }
    }
  }
  return true;
}

bool flag[N];

std::pair<State, State> BFS(int s) {
  flag[s] = true;
  static std::vector<int> q;
  q.clear();
  q.push_back(s);
  for (int i = 0; i < q.size(); ++i) {
    int a = q[i];
    for (std::vector<int>::iterator it = adj[a].begin(); it != adj[a].end(); ++it) {
      int b = *it;
      if (!flag[b]) {
        flag[b] = true;
        q.push_back(b);
      }
    }
  }
  std::pair<State, State> res;
  for (int i = 0; i < q.size(); ++i) (col[q[i]] ? res.fst : res.snd)[q[i]] = 1;
  return res;
}

int main() {
  scanf("%d", &n);
  static bool know[N][N];
  for (int i = 1; i <= n; ++i) {
    know[i][i] = true;
    for (int j; scanf("%d", &j), j;) know[i][j] = true;
  }
  for (int i = 1; i <= n; ++i)
    for (int j = 1; j <= n; ++j)
      if (!(know[i][j] && know[j][i])) adj[i].push_back(j);
  std::fill(col + 1, col + n + 1, -1);
  for (int i = 1; i <= n; ++i) {
    if (col[i] == -1) {
      col[i] = 0;
      if (!Paint(i)) {
        col[i] = 1;
        if (!Paint(i)) {
          puts("No solution");
          return 0;
        }
      }
    }
  }
  static std::vector< std::vector<int> > pool;
  static std::map<int, State> bottle;
  bottle[0] = State();
  for (int i = 1; i <= n; ++i) {
    if (!flag[i]) {
      std::pair<State, State> cur = BFS(i);
      static std::map<int, State> temp;
      temp.clear();
      for (std::map<int, State>::iterator it = bottle.begin(); it != bottle.end(); ++it) {
        temp[it->fst + cur.fst.count()] = it->snd | cur.fst;
        temp[it->fst + cur.snd.count()] = it->snd | cur.snd;
      }
      bottle = temp;
    }
  }
  State div = bottle.lower_bound(n / 2)->snd;
  printf("%lu", div.count());
  for (int i = 1; i <= n; ++i) if (div[i]) printf(" %d", i);
  printf("\n%lu", n - div.count());
  for (int i = 1; i <= n; ++i) if (!div[i]) printf(" %d", i);
  putchar('\n');
  return 0;
}
