#include <cstdio>
#include <cstring>
#include <vector>

const int N = 1000 + 10;

int n, sg[N];
std::vector<int> e[N];

int cal(int a) {
  if (~sg[a]) return sg[a];
  bool mex[N] = {false};
  for (std::vector<int>::iterator it = e[a].begin(); it != e[a].end(); ++it) {
    int b = *it;
    mex[cal(b)] = true;
  }
  for (int i = 0;; ++i) if (!mex[i]) return sg[a] = i;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  while (scanf("%d", &n) != EOF) {
    for (int i = 0; i < n; ++i) {
      int x;
      scanf("%d", &x);
      e[i].clear();
      while (x--) {
        int j;
        scanf("%d", &j);
        e[i].push_back(j);
      }
    }
    memset(sg, -1, sizeof sg);
    int m;
    while (scanf("%d", &m), m) {
      int ans = 0;
      while(m--) {
        int x;
        scanf("%d", &x);
        ans ^= cal(x);
      }
      puts(ans ? "WIN" : "LOSE");
    }
  }
  return 0;
}
