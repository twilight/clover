#include <cstdio>
#include <vector>
#include <complex>
#include <algorithm>

typedef std::pair<int, int> point;

#define x first
#define y second

const int N = 1000 + 10;

inline int trapezoid(const point &a, const point &b) { return a.x * b.y - a.y * b.x; }

inline int convexity(const point &o, const point &a, const point &b) {
  return trapezoid(o, a) + trapezoid(a, b) + trapezoid(b, o);
}

inline double sqr(double a) { return a * a; }
inline double dist(const point &a) { return sqr(a.x) + sqr(a.y); }

inline bool cmp(const point &a, const point &b) {
  return trapezoid(a, b) > 0 || (trapezoid(a, b) == 0 && dist(a) < dist(b));
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    int n;
    scanf("%d", &n);
    static point a[N];
    int tmp = 0;
    for (int i = 1; i <= n; ++i) scanf("%d%d", &a[i].x, &a[i].y);
    if (n < 6) {
      puts("NO");
      continue;
    }
    std::sort(a + 1, a + n + 1);
    static std::vector<point> up, down;
    up.clear(), down.clear();
    up.push_back(a[1]), down.push_back(a[1]);
    for (int i = 2; i <= n; ++i) {
      while (up.size() > 1 && convexity(up[up.size() - 2], up.back(), a[i]) > 0) up.pop_back();
      while (down.size() > 1 && convexity(down[down.size() - 2], down.back(), a[i]) < 0) down.pop_back();
      up.push_back(a[i]);
      down.push_back(a[i]);
    }
    for (int i = up.size() - 2; i >= 0; --i) down.push_back(up[i]);
    for (int i = 1; i <= 2; ++i) down.push_back(down[i]);
    bool flag = true;
    for (int i = 1; i + 2 < down.size(); ++i) {
      if (convexity(down[i - 1], down[i], down[i + 1]) && convexity(down[i], down[i + 1], down[i + 2])) {
        flag = false;
        break;
      }
    }
    puts(flag ? "YES" : "NO");
  }
  return 0;
}
