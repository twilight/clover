#include <cstdio>
#include <vector>
#include <numeric>
#include <algorithm>
#include <functional>

const int N = 5000 + 10;

inline int lg2(int x) { return 31 - __builtin_clz(x); }

int n, k;

bool Check(int x) {
  static std::vector<int> q;
  static bool flag[N];
  std::fill(flag + 1, flag + n + 1, false);
  q.clear();
  flag[x] = true;
  q.push_back(x);
  for (int i = 0; i < q.size(); ++i) {
    int a = q[i];
    bool found = false;
    for (int b = std::max(a - k, 1); b <= n; ++b) {
      if (!flag[b]) {
        q.push_back(a);
        q.push_back(b);
        flag[b] = found = true;
        break;
      }
    }
    if (!found) return std::accumulate(flag + 1, flag + n + 1, true, std::logical_and<bool>());
  }
}

int main() {
  scanf("%d%d", &n, &k);
  int l = 1, r = n;
  while (l < r) {
    int mid = (l + r + 1) / 2;
    if (Check(mid)) l = mid; else r = mid - 1;
  }
  printf("%d\n", l);
  return 0;
}
