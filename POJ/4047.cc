#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>

const int N = 200000 + 10;

int n, m, k, val[N], sum[N];

int tag[2 * N], mx[2 * N];

inline int pos(int l, int r) { return (l + r) | (l != r); }

void Build(int l, int r) {
  int id = pos(l, r);
  if (l == r) {
    mx[id] = sum[l + k - 1] - sum[l - 1];
    return;
  }
  int mid = (l + r) / 2;
  Build(l, mid);
  Build(mid + 1, r);
  mx[id] = std::max(mx[pos(l, mid)], mx[pos(mid + 1, r)]);
}

void Add(int l, int r, int p, int q, int det) {
  int id = pos(l, r);
  if (p <= l && r <= q) {
    tag[id] += det;
    mx[id] += det;
    return;
  }
  int mid = (l + r) / 2;
  int lch = pos(l, mid), rch = pos(mid + 1, r);
  if (tag[id]) {
    tag[lch] += tag[id];
    tag[rch] += tag[id];
    mx[lch] += tag[id];
    mx[rch] += tag[id];
    tag[id] = 0;
  }
  if (p <= mid) Add(l, mid, p, q, det);
  if (q > mid) Add(mid + 1, r, p, q, det);
  mx[id] = std::max(mx[lch], mx[rch]);
}

int Query(int l, int r, int p, int q) {
  int id = pos(l, r);
  if (p <= l && r <= q) return mx[id];
  int mid = (l + r) / 2;
  int lch = pos(l, mid), rch = pos(mid + 1, r);
  if (tag[id]) {
    tag[lch] += tag[id];
    tag[rch] += tag[id];
    mx[lch] += tag[id];
    mx[rch] += tag[id];
    tag[id] = 0;
  }
  int res = -3e8;
  if (p <= mid) res = std::max(res, Query(l, mid, p, q));
  if (q > mid) res = std::max(res, Query(mid + 1, r, p, q));
  mx[id] = std::max(mx[lch], mx[rch]);
  return res;
}

inline void Modify(int x, int y) {
  Add(1, n - k + 1, std::max(1, x - k + 1), std::min(n - k + 1, x), y - val[x]);
  val[x] = y;
}

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d%d", &n, &m, &k);
    memset(mx, 0, sizeof mx);
    memset(tag, 0, sizeof tag);
    for (int i = 1; i <= n; ++i) {
      scanf("%d", val + i);
      sum[i] = sum[i - 1] + val[i];
    }
    Build(1, n - k + 1);
    while (m--) {
      int p, x, y;
      scanf("%d%d%d", &p, &x, &y);
      switch (p) {
        case 0:
          Modify(x, y);
          break;
        case 1: {
          int u = val[x], v = val[y];
          Modify(x, v);
          Modify(y, u);
          break;
        }
        case 2:
          printf("%d\n", Query(1, n - k + 1, x, y - k + 1));
          break;
      }
    }
  }
  return 0;
}
