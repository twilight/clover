#include <cstdio>
#include <cmath>
#include <vector>
#include <algorithm>

#define x first
#define y second

typedef std::pair<int, int> point;

const int N = 1000 + 10;
const double PI = acos(-1.0);

int n, l;
point a[N];

inline int trapezoid(const point &a, const point &b) {
  return a.x * b.y - a.y * b.x;
}

inline int convexity(const point &o, const point &a, const point &b) {
  return trapezoid(o, a) + trapezoid(a, b) + trapezoid(b, o);
}

inline double sqr(int a) { return (double)a * a; }

inline double dist(const point &a, const point &b) {
  return sqrt(sqr(a.x - b.x) + sqr(a.y - b.y));
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d%d", &n, &l);
  for (int i = 1; i <= n; ++i) scanf("%d%d", &a[i].x, &a[i].y);
  std::sort(a + 1, a + n + 1);
  static std::vector<point> up, down;
  up.push_back(a[1]), down.push_back(a[1]);
  for (int i = 2; i <= n; ++i) {
    while (up.size() > 1 && convexity(up[up.size() - 2], up.back(), a[i]) >= 0) up.pop_back();
    while (down.size() > 1 && convexity(down[down.size() - 2], down.back(), a[i]) <= 0) down.pop_back();
    up.push_back(a[i]);
    down.push_back(a[i]);
  }
  double c = 0;
  for (int i = 0; i + 1 < up.size(); ++i) c += dist(up[i], up[i + 1]);
  for (int i = 0; i + 1 < down.size(); ++i) c += dist(down[i], down[i + 1]);
  printf("%.0f\n", c + 2 * PI * l);
  return 0;
}
