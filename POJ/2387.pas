{$inline on}

const
  maxn=1000;
  maxm=2000;

var
  dis,ehead:array [0..maxn] of longint;
  vis:array [0..maxn] of boolean;
  edge:array [0..maxm*2] of record
                              adj,next,cost:longint;
                            end;
  cnt,n,m,i,j,k,p,q,c,w,min:longint;

procedure insert_edge(p,q,c:longint); inline;
begin
  edge[cnt].adj:=q;
  edge[cnt].next:=ehead[p];
  edge[cnt].cost:=c;
  ehead[p]:=cnt;
  inc(cnt);
end;

begin
  readln(m,n);
  fillchar(ehead,sizeof(ehead),255);
  for i:=1 to m do
    begin
      readln(p,q,c);
      insert_edge(p,q,c);
      insert_edge(q,p,c);
    end;
  fillchar(dis,sizeof(dis),255);
  dis[1]:=0;
  for i:=1 to n do
    begin
      min:=maxlongint;
      k:=0;
      for j:=1 to n do
        if (not vis[j]) and (dis[j]<>-1) and (dis[j]<min) then
          begin
            min:=dis[j];
            k:=j;
          end;
      if min=maxlongint then break;
      vis[k]:=true;
      p:=ehead[k];
      while p<>-1 do
        begin
          j:=edge[p].adj;
          w:=edge[p].cost;
          if (not vis[j]) and ((dis[k]+w<dis[j]) or (dis[j]=-1)) then
            dis[j]:=dis[k]+w;
          p:=edge[p].next;
        end;
    end;
  writeln(dis[n]);
end.
