#include <cstdio>
#include <algorithm>

const int N = 100 + 10;

int n, a[N];

int main() {
  while (scanf("%d", &n), n) {
    for (int i = 1; i <= n; ++i) scanf("%d", a + i);
    bool flag = true;
    if (n & 1) {
      flag = true;
    } else {
      std::sort(a + 1, a + 1 + n);
      flag = false;
      for (int i = 1; i <= n; i += 2) if (a[i] != a[i + 1]) flag = true;
    }
    puts(flag ? "1" : "0");
  }
  return 0;
}
