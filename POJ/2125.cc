#include <cstdio>
#include <cstring>
#include <algorithm>
#include <queue>

const int N = 100 * 2 + 10, E = N * N * 2, INF = 1 << 25;

int n, m;

inline int in(int x) { return 2 * x - 1; }
inline int out(int x) { return 2 * x; }

int adj[N];
int to[E], next[E], cap[E];

inline void link(int a, int b, int c) {
  static int cnt = 0;
  to[cnt] = b;
  next[cnt] = adj[a];
  cap[cnt] = c;
  adj[a] = cnt++;
  to[cnt] = a;
  next[cnt] = adj[b];
  cap[cnt] = 0;
  adj[b] = cnt++;
}

int h[N], gap[N + 1];

int dfs(int a, int df, const int s, const int t) {
  if (a == t) return df;
  for (int it = adj[a]; it != -1; it = next[it]) {
    int b = to[it];
    if (cap[it] && h[a] == h[b] + 1) {
      int f = dfs(b, std::min(df, cap[it]), s, t);
      if (f) {
        cap[it] -= f;
        cap[it ^ 1] += f;
        return f;
      }
    }
  }
  if (--gap[h[a]] == 0) h[s] = 2 * n + 2;
  ++gap[++h[a]];
  return 0;
}

inline int flow(const int s, const int t) {
  int res = 0;
  memset(h, 0, sizeof h);
  memset(gap, 0, sizeof gap);
  while (h[s] < 2 * n + 2) res += dfs(s, INF, s, t);
  return res;
}

bool flag[N];

inline void bfs(const int s) {
  memset(flag, false, sizeof flag);
  static std::queue<int> q;
  q.push(s);
  while (!q.empty()) {
    int a = q.front();
    q.pop();
    flag[a] = true;
    for (int it = adj[a]; it != -1; it = next[it]) {
      int b = to[it], c = cap[it];
      if (!flag[b] && c) q.push(b);
    }
  }
}

int main() {
  scanf("%d%d", &n, &m);
  memset(adj, -1, sizeof adj);
  const int s = 0, t = 2 * n + 1;
  for (int i = 1; i <= n; ++i) {
    int x;
    scanf("%d", &x);
    link(in(i), t, x);
  }
  for (int i = 1; i <= n; ++i) {
    int x;
    scanf("%d", &x);
    link(s, out(i), x);
  }
  while (m--) {
    int x, y;
    scanf("%d%d", &x, &y);
    link(out(x), in(y), INF);
  }
  printf("%d\n", flow(s, t));
  bfs(s);
  int tmp = 0;
  for (int i = 1; i <= n; ++i) tmp += flag[in(i)] + !flag[out(i)];
  printf("%d\n", tmp);
  for (int i = 1; i <= n; ++i) {
    if (flag[in(i)]) printf("%d +\n", i);
    if (!flag[out(i)]) printf("%d -\n", i);
  }
  return 0;
}
