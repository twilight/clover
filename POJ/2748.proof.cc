#include <cstdio>

using namespace std;

const int maxn=10000000;

int f[maxn]={0,1,1};

inline bool check(int k)
{
	for (int i=0; i<maxn; ++i) if (f[i]!=f[i%k]) return false;
	return true;
}

int main()
{
	int i;
	for (i=3; i<maxn; ++i)
		f[i]=(f[i-1]+f[i-2])%100000;
	for (i=3; i<maxn; ++i) if (check(i)) break;
	printf("%d\n",i);
	return 0;
}
