#include <cstdio>
#include <cmath>
#include <cstring>

typedef long long int64;

const int N = 100003;

int a, b, c;

inline int pow_m(int base, int exp, int MOD) {
  int res = 1;
  while (exp) {
    if (exp & 1) res = (int64)res * base % MOD;
    base = (int64)base * base % MOD;
    exp >>= 1;
  }
  return res;
}

int table[N], num[N];
inline int hash(int x) {
  int res = x % N;
  while (table[res] && table[res] != x) res = (res + 1) % N;
  return res;
}

inline void solve(int a, int b, int c) {
  memset(table, 0, sizeof table);
  const int m = ceil(sqrt(c));
  for (int i = 0, tmp = 1; i <= m; ++i) {
    int pos = hash(tmp);
    if (table[pos] != tmp) {
      table[pos] = tmp;
      num[pos] = i;
    }
    tmp = (int64)tmp * a % c;
  }
  int tmp = pow_m(a, c - 1 - m, c);
  for (int i = 0; i <= m; ++i) {
    int pos = hash((int64)pow_m(tmp, i, c) * b % c);
    if (table[pos]) {
      printf("%d\n", i * m + num[pos]);
      return;
    }
  }
  puts("no solution");
}

int main() {
  while (scanf("%d%d%d", &c, &a, &b) != EOF) solve(a, b, c);
  return 0;
}
