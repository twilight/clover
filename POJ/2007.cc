#include <cstdio>
#include <vector>
#include <algorithm>

#define x first
#define y second

typedef std::pair<int, int> point;

inline int trapezoid(const point &a, const point &b) { return a.x * b.y - a.y * b.x; }

std::vector<point> list;

inline bool cmp(const point &a, const point &b) {
  return trapezoid(a, b) > 0;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  for (int p, q; scanf("%d%d", &p, &q) != EOF;) list.push_back(std::pair<int, int>(p, q));
  std::sort(list.begin() + 1, list.end(), cmp);
  for (std::vector< std::pair<int, int> >::iterator it = list.begin(); it != list.end(); ++it) printf("(%d,%d)\n", it->first, it->second);
  return 0;
}
