type
  tree=^node;
  node=record
         data:char;
         lson,rson:tree;
       end;

var
  s:array [0..30] of string;
  exitflag:boolean;
  root:tree;
  n,i,j:longint;

procedure trinsert(ch:char; var root:tree);
var
  tmptree:tree;

begin
  if root=nil then
    begin
      new(tmptree);
      tmptree^.data:=ch;
      tmptree^.lson:=nil;
      tmptree^.rson:=nil;
      root:=tmptree;
      exit;
    end;
  if ch<root^.data then trinsert(ch,root^.lson);
  if ch>root^.data then trinsert(ch,root^.rson);
end;

procedure preorder(root:tree);
begin
  if root=nil then exit;
  write(root^.data);
  preorder(root^.lson);
  preorder(root^.rson);
end;

begin
  exitflag:=false;
  while not exitflag do
    begin
      n:=1;
      readln(s[n]);
      while (s[n]<>'*') and (s[n]<>'$') do
        begin
          inc(n);
          readln(s[n]);
        end;
      if s[n]='$' then exitflag:=true;
      dec(n);
      root:=nil;
      for i:=n downto 1 do
        for j:=1 to length(s[i]) do
          trinsert(s[i][j],root);
      preorder(root);
      writeln;
      dispose(root);
    end;
end.