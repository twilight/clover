{$inline on}

const
  prime=31;
  maxn=60000;

var
  s:array [0..maxn] of char;
  w,h,pos:array [0..maxn] of longint;
  tcase,i,p,q,l,m,r,n:longint;
  order,ch:char;

function hash(p,q:longint):longint; inline;
var
  k:longint;

begin
  k:=h[p+q-1]-h[p-1]*w[q];
  if k<0 then inc(k,maxlongint+1);
  exit(k);
end;

function max(p,q:longint):longint; inline;
begin
  if p>q then exit(p) else exit(q);
end;

begin
  n:=0;
  while not eoln do
    begin
      read(ch);
      inc(n);
      s[n]:=ch;
    end;
  readln(tcase);
  w[0]:=1;
  for i:=1 to n do
    begin
      w[i]:=w[i-1]*prime;
      h[i]:=h[i-1]*prime+ord(s[i])-ord('a')+1;
      pos[i]:=i;
    end;
  repeat
    read(order);
    dec(tcase);
    case order of
      'Q':begin
            readln(p,q);
            p:=pos[p];
            q:=pos[q];
            l:=0;
            r:=n-max(p,q)+2;
            while l+1<r do
              begin
                m:=(l+r) shr 1;
                if hash(p,m)=hash(q,m) then l:=m else r:=m;
              end;
            writeln(l);
          end;
      'I':begin
            readln(ch,ch,p);
            inc(n);
            if p>n then p:=n;
            for i:=1 to n do if pos[i]>=p then inc(pos[i]);
            for i:=n downto p+1 do s[i]:=s[i-1];
            s[p]:=ch;
            for i:=p to n do h[i]:=h[i-1]*prime+ord(s[i])-ord('a')+1;
          end;
    end;
  until tcase=0;
end.
