#include <cstdio>
#include <cstring>
#include <algorithm>

const int N = 128 + 10;

class BigInteger {
  int data[N];

 public:
  BigInteger(int num = 0) {
    memset(data, 0, sizeof data);
    data[++data[0]] = num;
  }

  inline int& size() { return data[0]; }
  inline const int& size() const { return data[0]; }
  inline int& operator[] (const size_t pos) { return data[pos]; }
  inline const int& operator[] (const size_t pos) const { return data[pos]; }

  inline BigInteger& operator+= (const BigInteger &rhs) {
    size() = std::max(size(), rhs.size());
    for (int i = size() + 1; i <= rhs.size(); ++i) data[i] = 0;
    for (int i = 1; i <= rhs.size(); ++i) data[i] += rhs[i];
    data[size() + 1] = 0;
    for (int i = 1; i <= size(); ++i) data[i + 1] += data[i] / 10;
    for (int i = 1; i <= size(); ++i) data[i] %= 10;
    if (data[size() + 1]) ++size();
    return *this;
  }

  inline void Output();
} Power[N];

BigInteger operator+ (const BigInteger &lhs, const BigInteger &rhs) {
  BigInteger res(lhs);
  res += rhs;
  return res;
}

void BigInteger::Output() {
  for (int i = size(); i > 0; --i) printf("%d", data[i]);
  putchar('\n');
}

int n, a[N], b[N];

BigInteger Calc(int a[], int k) {
  static int temp[N];
  for (int i = k; i <= n; ++i) temp[i] = 0;
  temp[k] = 1;
  while (k <= n && a[k] == temp[k]) ++k;
  if (k > n) return 0;
  if (k == n) return 1;
  ++k;
  return Power[n - k + 1] + Calc(a, k);
}

BigInteger Solve() {
  int k = 1;
  while (k <= n && a[k] == b[k]) ++k;
  if (k > n) return 0;
  if (k == n) return 1;
  ++k;
  return Calc(a, k) + Calc(b, k) + BigInteger(1);
}

int main() {
  int tcase;
  Power[0] = 1;
  for (int i = 1; i < N; ++i) Power[i] = Power[i - 1] + Power[i - 1];
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d", &n);
    for (int i = 1; i <= n; ++i) scanf("%d", a + i);
    for (int i = 1; i <= n; ++i) scanf("%d", b + i);
    Solve().Output();
  }
  return 0;
}
