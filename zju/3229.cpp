#include <bits/stdc++.h>

const int V = 3000 + 10, E = V * 100;

int n, m;

int adj[V];
int to[E], next[E], cap[E], cnt;

inline void link(int a, int b, int c) {
  to[cnt] = b, next[cnt] = adj[a], cap[cnt] = c, adj[a] = cnt++;
  to[cnt] = a, next[cnt] = adj[b], cap[cnt] = 0, adj[b] = cnt++;
}

int h[V], gap[V];

int dfs(int a, int df, const int s, const int t) {
  if (a == t) return df;
  int tot = 0;
  for (int it = adj[a]; ~it; it = next[it]) {
    int b = to[it];
    if (h[a] == h[b] + 1 && cap[it] > 0) {
      int f = dfs(b, std::min(cap[it], df - tot), s, t);
      cap[it] -= f;
      cap[it ^ 1] += f;
      tot += f;
    }
    if (tot == df) break;
  }
  if (!tot) {
    if (--gap[h[a]] == 0) h[s] = V;
    ++gap[++h[a]];
  }
  return tot;
}

int flow(const int s, const int t) {
  std::fill(h, h + V, 0);
  std::fill(gap, gap + V, 0);
  int res = 0;
  while (h[s] < V) res += dfs(s, INT_MAX, s, t);
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  while (scanf("%d%d", &n, &m) != EOF) {
    static std::vector<int> ans, det;
    ans.clear(), det.clear();
    cnt = 0;
    std::fill(adj, adj + V, -1);
    const int ss = n + m + 1, tt = n + m + 2, s = n + m + 3, t = n + m + 4;
    link(t, s, INT_MAX);
    int possible = 0;
    for (int i = 1; i <= m; ++i) {
      int g;
      scanf("%d", &g);
      link(n + i, t, INT_MAX);
      link(n + i, tt, g);
      link(ss, t, g);
      possible += g;
    }
    for (int i = 1; i <= n; ++i) {
      int c, d;
      scanf("%d%d", &c, &d);
      link(s, i, d);
      for (int j = 1; j <= c; ++j) {
        int p, l, r;
        scanf("%d%d%d", &p, &l, &r);
        p = n + p + 1;
        link(i, p, r - l);
        det.push_back(l);
        ans.push_back(cnt - 1);
        link(i, tt, l);
        link(ss, p, l);
        possible += l;
      }
    }
    if (flow(ss, tt) == possible) {
      cap[0] = cap[1] = 0;
      flow(s, t);
      for (int it = 0; it < ans.size(); ++it) ans[it] = cap[ans[it]] + det[it];
      printf("%d\n", std::accumulate(ans.begin(), ans.end(), 0));
      for (int it = 0; it < ans.size(); ++it) printf("%d\n", ans[it]);
    } else {
      puts("-1");
    }
    putchar('\n');
  }
  return 0;
}
