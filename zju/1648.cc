#include <cstdio>
#include <cmath>
#include <algorithm>

typedef std::pair<double, double> point;
typedef std::pair<point, point> line;

const int N = 2000 + 10;
const double eps = 1e-8;

#define fst first
#define snd second

#define x fst
#define y snd

double det(const point &a, const point &b, const point &c) {
  return (b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y);
}

inline int sgn(double x) {
  if (fabs(x) < eps) return 0;
  return x > eps ? 1 : -1;
}

bool isIntersect(const line &a, const line &b) {
  return
    sgn(det(a.fst, b.fst, a.snd)) * sgn(det(a.fst, b.snd, a.snd)) < 0 &&
    sgn(det(b.fst, a.fst, b.snd)) * sgn(det(b.fst, a.snd, b.snd)) < 0;
}

int main() {
  static line pool[N];
  for (int n; scanf("%d", &n) != EOF;) {
    for (int i = 1; i <= n; ++i)
      scanf("%lf%lf%lf%lf", &pool[i].fst.x, &pool[i].fst.y, &pool[i].snd.x, &pool[i].snd.y);
    bool flag = true;
    for (int i = 1; i < n; ++i) {
      for (int j = i + 1; j <= n; ++j) {
        if (isIntersect(pool[i], pool[j])) {
          flag = false;
          break;
        }
      }
    }
    puts(flag ? "ok!" : "burned!");
  }
  return 0;
}
