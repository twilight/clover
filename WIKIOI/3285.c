#include <stdio.h>

typedef long long int64;

int pow_mod(int base, int exp, int mod) {
	if (exp == 0) return 1;
	if (exp == 1) return base;
	int tmp = pow_mod(base,exp>>1,mod);
	tmp = ((int64)tmp * tmp) % mod;
	if (exp & 1) tmp = ((int64)tmp * base) % mod;
	return tmp;
}

int main() {
	int n,m,k,x;
	scanf("%d%d%d%d",&n,&m,&k,&x);
	printf("%d\n",((int64)x + m * pow_mod(10,k,n)) % n);
	return 0;
}
