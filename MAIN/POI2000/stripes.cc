#include <cstdio>
#include <algorithm>

const int N = 1000 + 10;

int a[3];
int sg[N];

int cal(int p) {
  if (~sg[p]) return sg[p];
  bool mex[N] = {false};
  for (int k = 0; k < 3; ++k)
    for (int q = 0; q <= p - a[k]; ++q)
      mex[cal(q) ^ cal(p - a[k] - q)] = true;
  for (int i = 0;; ++i) if (!mex[i]) return sg[p] = i;
}

int main() {
  scanf("%d%d%d", a, a + 1, a + 2);
  int tcase;
  memset(sg, -1, sizeof sg);
  for (scanf("%d", &tcase); tcase--;) {
    int p;
    scanf("%d", &p);
    printf("%d\n", 2 - (bool)cal(p));
  }
  return 0;
}
