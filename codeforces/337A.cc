#include <cstdio>
#include <algorithm>

using namespace std;

int main()
{
	int n,m;
	scanf("%d%d",&m,&n);
	int a[n+1];
	for (int i=1; i<=n; ++i) scanf("%d",&a[i]);
	sort(a+1,a+n+1);
	int ans=a[m]-a[1];
	for (int i=m+1; i<=n; ++i) ans=min(ans,a[i]-a[i-m+1]);
	printf("%d",ans);
	return 0;
}

