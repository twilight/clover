#include <bits/stdc++.h>

typedef long long int64;
typedef int64 matrix[5][5];

const int N = 100000 + 10, SZ = N * 20, root = 1;

int64 n;
int len;
char text[N];

int next[SZ][4], tot = root;

inline int lg2(int64 x) { return 63 - __builtin_clzll(x); }

inline void add(int l, int r) {
  int p = root;
  for (int i = l; i <= r; ++i) {
    int ch = text[i] - 'A';
    if (!next[p][ch]) next[p][ch] = ++tot;
    p = next[p][ch];
  }
}

matrix mat, dp[70];

void dfs(int a, int dep, const int ch) {
  for (int i = 0; i < 4; ++i)
    if (next[a][i])
      dfs(next[a][i], dep + 1, ch);
    else
      mat[ch][i] = std::min<int64>(mat[ch][i], dep);
}

inline void COPY(matrix a, matrix b) {
  for (int i = 0; i <= 4; ++i)
    for (int j = 0; j <= 4; ++j)
      a[i][j] = b[i][j];
}

inline void mul(matrix a, const matrix b) {
  static matrix c;
  memset(c, 0x3f, sizeof(matrix));
  for (int i = 0; i <= 4; ++i)
    for (int j = 0; j <= 4; ++j)
      for (int k = 0; k <= 4; ++k)
        c[i][j] = std::min(c[i][j], a[i][k] + b[k][j]);
  memcpy(a, c, sizeof(matrix));
}

int main() {
  scanf("%lld %s", &n, text + 1);
  len = strlen(text + 1);
  int lim = lg2(len);
  for (int i = 1; i <= len; ++i) add(i, std::min(i + lim, len));
  memset(mat, 0x3f, sizeof(matrix));
  for (int i = 0; i < 4; ++i) if (next[root][i]) dfs(next[root][i], 1, i + 1);
  dfs(root, 0, 0);
  memcpy(dp[0], mat, sizeof(matrix));
  for (int i = 1; i <= lg2(n); ++i) {
    memcpy(dp[i], dp[i - 1], sizeof(matrix));
    mul(dp[i], dp[i]);
  }
  int64 ans = 0;
  static matrix pool[2];
  int cur = 0, next = 1;
  memset(pool[cur], 0x3f, sizeof(matrix));
  for (int i = 0; i <= 4; ++i) pool[cur][i][i] = 0;
  for (int i = lg2(n); i >= 0; --i) {
    memcpy(pool[next], pool[cur], sizeof pool[cur]);
    mul(pool[next], dp[i]);
    int64 tmp = LONG_LONG_MAX;
    for (int j = 1; j <= 4; ++j) tmp = std::min(tmp, pool[next][0][j]);
    if (tmp < n) {
      ans += 1LL << i;
      std::swap(cur, next);
    }
  }
  printf("%lld\n", ans + 1);
  return 0;
}
