#include <bits/stdc++.h>

const int N = 200 + 10, E = N * N * 10;

int n, m;

int adj[N], arc[N];
int to[E], next[E], cap[E], cnt = 0;

void link(int a, int b, int c) {
  to[cnt] = b, next[cnt] = adj[a], cap[cnt] = c, adj[a] = cnt++;
  to[cnt] = a, next[cnt] = adj[b], cap[cnt] = 0, adj[b] = cnt++;
}

int h[N], gap[N];

int dfs(int a, int df, const int s, const int t) {
  if (a == t) return df;
  int tot = 0;
  bool flag = false;
  for (int it = arc[a]; ~it; it = next[it]) {
    int b = to[it], c = cap[it];
    if (c && h[a] == h[b] + 1) {
      int f = dfs(b, std::min(c, df - tot), s, t);
      if (f) {
        cap[it] -= f;
        cap[it ^ 1] += f;
        tot += f;
      }
      if (!flag) flag = true, arc[a] = it;
    }
    if (tot == df) return tot;
  }
  if (!flag) {
    if (--gap[h[a]] == 0) h[s] = n + 2;
    ++gap[++h[a]];
    arc[a] = adj[a];
  }
  return tot;
}

int flow(const int s, const int t) {
  memset(h, 0, sizeof h);
  memset(gap, 0, sizeof gap);
  memcpy(arc, adj, sizeof adj);
  int res = 0;
  while (h[s] < n + 2) res += dfs(s, 1e9, s, t);
  return res;
}

int main() {
  scanf("%d%d", &n, &m);
  const int s = n + 1, t = n + 2;
  memset(adj, -1, sizeof adj);
  int sum = 0;
  static std::vector< std::pair<int, int> > orig;
  for (int tmp = m; tmp--;) {
    int a, b, c, d;
    scanf("%d%d%d%d", &a, &b, &c, &d);
    sum += c;
    link(a, b, d - c);
    orig.push_back(std::pair<int, int>(c, cnt - 1));
    link(s, b, c);
    link(a, t, c);
  }
  if (flow(s, t) == sum) {
    puts("YES");
    for (std::vector< std::pair<int, int> >::iterator it = orig.begin(); it != orig.end(); ++it)
      printf("%d\n", it->first + cap[it->second]);
  } else {
    puts("NO");
  }
  return 0;
}
