#include <cstdio>
#include <cstring>
#include <queue>
#include <algorithm>

typedef long long i64;

const int N = 50 + 10, SZ = 1000 + 10, INF = 0x3f3f3f3f;

inline bool cmin(int &a, int b) { 
  if (b < a) {
    a = b;
    return true;
  } else {
    return false;
  }
}

int n, m, h[N], cost[N], mask[N];
int dist[2][N][N];

struct Info {
  int u, v, mask, dist;
  Info(int _u, int _v, int _m, int _d): u(_u), v(_v), mask(_m), dist(_d) {}
  inline bool operator< (const Info &rhs) const {
    return dist > rhs.dist;
  }
};

int main() {
  while (scanf("%d%d", &n, &m), n | m) {
    static int cnt[SZ];
    memset(cnt, 0, sizeof cnt);
    cost[1] = cost[n] = 0;
    h[1] = 0, h[n] = 1000;
    for (int i = 2; i < n; ++i) scanf("%d%d", cost + i, h + i);
    for (int i = 1; i <= n; ++i) mask[i] = (1 << cnt[h[i]]++);
    memset(dist, 0x3f, sizeof dist);
    for (int i = 1; i <= m; ++i) {
      int a, b, c;
      scanf("%d%d%d", &a, &b, &c);
      if (h[a] <= h[b]) cmin(dist[0][a][b], c);
      if (h[a] >= h[b]) cmin(dist[1][b][a], c);
    }
    static int f[N][N][1 << 10];
    memset(f, 0x3f, sizeof f);
    static std::priority_queue<Info> heap;
    int ans = INF;
    for (heap.push(Info(1, 1, mask[1], f[1][1][mask[1]] = 0)); !heap.empty();) {
      int u = heap.top().u, v = heap.top().v, c = heap.top().mask, d = heap.top().dist;
      heap.pop();
      if (d > f[u][v][c]) continue;
      if (u == n && v == n) cmin(ans, d);
      for (int i = 1; i <= n; ++i) {
        if (h[i] < std::max(h[u], h[v])) continue;
        if (dist[0][u][i] < INF) {
          int p = i, q = v, r = c, s = d + dist[0][u][i];
          if (h[i] > std::max(h[u], h[v])) {
            r = mask[i]; 
            s += cost[i];
          } else {
            r |= mask[i];
            if (!(c & mask[i])) s += cost[i];
          }
          if (cmin(f[p][q][r], s)) heap.push(Info(p, q, r, s));
        }
        if (dist[1][v][i] < INF) {
          int p = u, q = i, r = c, s = d + dist[1][v][i];
          if (h[i] > std::max(h[u], h[v])) {
            r = mask[i]; 
            s += cost[i];
          } else {
            r |= mask[i];
            if (!(c & mask[i])) s += cost[i];
          }
          if (cmin(f[p][q][r], s)) heap.push(Info(p, q, r, s));
        }
      }
    }
    if (ans < INF) printf("%d\n", ans); else puts("-1");
  }
  return 0;
}
