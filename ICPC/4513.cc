#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int maxn=40005,x=123;

char s[maxn];

unsigned long long h[maxn],p[maxn],hash[maxn];

int n=0,k,rank[maxn];

bool cmp(int p, int q)
{
	return h[p]<h[q] || (h[p]==h[q] && p<q);
}

void init()
{
	hash[n]=0;
	for (int i=n-1; i>=0; --i) hash[i]=hash[i+1]*x+s[i]-'a';
	p[0]=1;
	for (int i=1; i<=n; ++i) p[i]=p[i-1]*x;
}

int possible(int l)
{
	for (int i=0; i<n-l+1; ++i)
	{
		rank[i]=i;
		h[i]=hash[i]-hash[i+l]*p[l];
	}
	sort(rank,rank+n-l+1,cmp);
	int cnt=1,pos=-1;
	for (int i=0; i<n-l; ++i)
	{
		if (h[rank[i]]==h[rank[i+1]]) ++cnt; else
		{
			if (cnt>=k) pos=max(pos,rank[i]);
			cnt=1;
		}
	}
	if (cnt>=k) pos=max(pos,rank[n-l]);
	return pos;
}

int main()
{
	for (scanf(" %d",&k); k; scanf(" %d",&k))
	{
		scanf(" %s",s);
		n=strlen(s);
		init();
		if (possible(1)==-1) 
		{
			puts("none");
			continue;
		}
		int l=1,r=n,m,ans=l;
		while (l<r)
		{
			m=(l+r+1)>>1;
			if (possible(m)!=-1) ans=l=m; else r=m-1;
		}
		int pos=possible(ans);
		printf("%d %d\n",ans,pos);
	}
	return 0;
}
