#include <cstdio>
#include <cstring>
#include <queue>

const int N = 500 * 2 + 10, E = 100000 * 5 + 10, INF = 1 << 25;

int n, m, k;

int adj[N];
int to[E], next[E], cap[E];

void link(int a, int b, int c) {
  static int cnt = 0;
  to[cnt] = b, next[cnt] = adj[a], cap[cnt] = c, adj[a] = cnt++;
  to[cnt] = a, next[cnt] = adj[b], cap[cnt] = 0, adj[b] = cnt++;
}

int arc[N], h[N], gap[N];

int dfs(int a, int df, const int s, const int t) {
  if (a == t) return df;
  int tot = 0;
  for (int &it = arc[a]; it != -1; it = next[it]) {
    int b = to[it];
    if (cap[it] && h[a] == h[b] + 1) {
      int f = dfs(b, std::min(cap[it], df - tot), s, t);
      if (f) {
        cap[it] -= f;
        cap[it ^ 1] += f;
        tot += f;
      }
    }
    if (tot == df) return tot;
  }
  if (--gap[h[a]] == 0) h[s] = 2 * n + 2;
  ++gap[++h[a]];
  arc[a] = adj[a];
  return tot;
}

int flow(const int s, const int t) {
  memset(h, 0, sizeof h);
  memset(gap, 0, sizeof gap);
  memcpy(arc, adj, sizeof arc);
  int res = 0;
  while (h[s] < 2 * n + 2) res += dfs(s, INF, s, t);
  return res;
}

bool flag[N];

void cut(const int s, const int t) {
  static std::queue<int> q;
  static bool tmp[N];
  q.push(s);
  memset(tmp, true, sizeof tmp);
  while (!q.empty()) {
    int a = q.front();
    q.pop();
    tmp[a] = false;
    for (int it = adj[a]; it != -1; it = next[it]) {
      int b = to[it], c = cap[it];
      if (c && tmp[b]) q.push(b);
    }
  }
  memset(flag, false, sizeof flag);
  for (int it = adj[s]; it != -1; it = next[it]) {
    int b = to[it];
    if (tmp[b]) flag[b] = true;
  }
  for (int it = adj[t]; it != -1; it = next[it]) {
    int b = to[it];
    if (!tmp[b]) flag[b] = true;
  }
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d%d%d\n", &n, &k, &m);
  memset(adj, -1, sizeof adj);
  const int s = 0, t = 2 * n + 1;
  while (m--) {
    int u, v;
    scanf("%d%d", &u, &v);
    link(u, v + n, INF);
    link(v, u + n, INF);
  }
  for (int i = 1; i <= n; ++i) link(s, i, 1), link(i + n, t, 1);
  flow(s, t);
  cut(s, t);
  for (int i = 1; i <= n; ++i) {
    double ans = 0.0;
    if (flag[i]) ans += k / 2.0;
    if (flag[i + n]) ans += k / 2.0;
    printf("%.1lf\n", ans);
  }
  return 0;
}
