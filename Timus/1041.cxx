#include <vector>
#include <iostream>
#include <iterator>
#include <valarray>
#include <functional>

const int N = 2000 + 10, MOD = 37813;

int n, m, c[N];

std::valarray<int> a[N];
std::vector<int> pool, tag;

int Pow(int base, int exp) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) (res *= base) %= MOD;
    (base *= base) %= MOD;
  }
  return res;
}

bool gauss(int p) {
  std::valarray<int> &cur(a[p]);
  for (size_t i = 0; i < pool.size(); ++i)
    if (cur[tag[i]])
      (cur -= a[pool[i]] * (cur[tag[i]] * Pow(a[pool[i]][tag[i]], MOD - 2) % MOD)) %= MOD;
  int k = 0;
  ((cur %= MOD) += MOD) %= MOD;
  for (; k < m; ++k) if (cur[k]) break;
  if (k == m) return false;
  pool.push_back(p);
  tag.push_back(k);
  return true;
}

bool cmp(int a, int b) { return c[a] < c[b]; }

int main() {
  std::cin >> n >> m;
  for (int i = 0; i < n; ++i) {
    a[i].resize(m);
    for (auto &it : a[i]) std::cin >> it;
  }
  std::copy(std::istream_iterator<int>(std::cin), std::istream_iterator<int>(), c);
  static int id[N];
  for (int i = 0; i < n; ++i) id[i] = i;
  std::stable_sort(id, id + n, cmp);
  int ans = 0;
  for (int it = 0; it < n; ++it) {
    int i = id[it];
    if (gauss(i)) ans += c[i];
    if (pool.size() == m) break;
  }
  if (pool.size() == m) {
    std::cout << ans << std::endl;
    std::sort(pool.begin(), pool.end());
    std::for_each(pool.begin(), pool.end(), [](int &x)->void { ++x; });
    std::copy(pool.begin(), pool.end(), std::ostream_iterator<int>(std::cout, "\n"));
  } else {
    puts("0");
  }
  return 0;
}
