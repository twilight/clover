#include <bits/stdc++.h>

const int N = 1000 + 10, MOD = 10000;

int n, p[N], e[N];

int odd[N], even[N];

inline int pow(int base, int exp) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) (res *= base) %= MOD;
    (base *= base) %= MOD;
  }
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d", &n);
  int m = 1, a, b, c;
  a = b = c = 0;
  for (int i = 1; i <= n; ++i) {
    scanf("%d%d", p + i, e + i);
    (m *= pow(p[i], e[i])) %= MOD;
  }
  p[0] = 1;
  for (int i = 1; i <= n; ++i) {
    if ((p[i] & 1) == 0) continue;
    odd[i] = p[i] - 1, even[i] = 0;
    for (int j = 0; j < i; ++j) {
      if ((p[j] & 1) == 0) continue;
      (odd[i] += even[j] * (p[i] - 1)) %= MOD;
      (even[i] += odd[j] * (p[i] - 1)) %= MOD;
    }
    (b += odd[i]) %= MOD;
    (a += even[i]) %= MOD;
  }
  printf("%d\n%d\n%d\n", a, b, ((m - 1 - a - b) % MOD + MOD) % MOD);
  return 0;
}
