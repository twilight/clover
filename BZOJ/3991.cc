#include <cstdio>
#include <set>
#include <vector>

typedef long long i64;

const int N = 100000 + 10, LOG = 20;

int n, m;

struct edge_t {
  int adj, dis;
  edge_t *next;
  edge_t(int a, int d, edge_t *n): adj(a), dis(d), next(n) {}
} *e[N];

std::vector<int> order;

int dep[N], fa[LOG][N], left[N], right[N];
i64 dist[N];

void DFS(int a) {
  left[a] = order.size();
  order.push_back(a);
  for (edge_t *it = e[a]; it; it = it->next) {
    int b = it->adj, c = it->dis;
    if (b != fa[0][a]) {
      fa[0][b] = a;
      dep[b] = dep[a] + 1;
      dist[b] = dist[a] + c;
      DFS(b);
    }
  }
  right[a] = order.size();
  order.push_back(a);
}

int LCA(int u, int v) {
  if (dep[u] < dep[v]) std::swap(u, v);
  for (int i = LOG - 1; i >= 0; --i)
    if (dep[fa[i][u]] >= dep[v])
      u = fa[i][u];
  if (u == v) return u;
  for (int i = LOG - 1; i >= 0; --i)
    if (fa[i][u] && fa[i][v] && fa[i][u] != fa[i][v])
      u = fa[i][u], v = fa[i][v];
  return fa[0][u];
}

inline i64 Dist(int u, int v) { return dist[u] + dist[v] - 2 * dist[LCA(u, v)]; }

int col[N];
i64 len = 0;

std::set<int> dfn;

void Add(int pos) {
  if (dfn.empty()) {
    dfn.insert(pos);
    return;
  }
  if (pos < *dfn.begin()) {
    len += Dist(order[pos], order[*dfn.begin()]);
  } else if (pos > *dfn.rbegin()) {
    len += Dist(order[pos], order[*dfn.rbegin()]);
  } else {
    std::set<int>::iterator r = dfn.lower_bound(pos), l;
    --(l = r);
    len -= Dist(order[*l], order[*r]);
    len += Dist(order[*l], order[pos]);
    len += Dist(order[pos], order[*r]);
  }
  dfn.insert(pos);
}

void Remove(int pos) {
  if (dfn.size() <= 2) {
    dfn.erase(pos);
    return;
  }
  std::set<int>::iterator it = dfn.find(pos), l, r;
  --(l = it);
  ++(r = it);
  if (pos == *dfn.begin()) {
    len -= Dist(order[pos], order[*r]);
  } else if (pos == *dfn.rbegin()) {
    len -= Dist(order[pos], order[*l]);
  } else {
    len -= Dist(order[*l], order[pos]);
    len -= Dist(order[pos], order[*r]);
    len += Dist(order[*l], order[*r]);
  }
  dfn.erase(pos);
}

int main() {
  scanf("%d%d", &n, &m);
  for (int i = n - 1, a, b, c; i--;) {
    scanf("%d%d%d", &a, &b, &c);
    e[a] = new edge_t(b, c, e[a]);
    e[b] = new edge_t(a, c, e[b]);
  }
  dep[1] = 1;
  DFS(1);
  for (int i = 1; i < LOG; ++i)
    for (int j = 1; j <= n; ++j)
      fa[i][j] = fa[i - 1][fa[i - 1][j]];
  for (int i = 1, x; i <= m; ++i) {
    scanf("%d", &x);
    if (col[x] ^= 1) {
      Add(left[x]);
      Add(right[x]);
    } else {
      Remove(left[x]);
      Remove(right[x]);
    }
    i64 ans = len;
    if (!dfn.empty()) ans += Dist(order[*dfn.begin()], order[*dfn.rbegin()]);
    printf("%lld\n", ans);
  }
  return 0;
}
