#include <cstdio>
#include <algorithm>

typedef long long i64;

const int N = 100 + 10;

int n, a[N];

int main() {
  scanf("%d", &n);
  for (int i = 1; i <= n; ++i) scanf("%d", a + i);
  std::sort(a + 1, a + n + 1);
  static int base[N], pivot[N];
  int tot = 0;
  i64 ans = 0;
  for (int i = n; i > 0; --i) {
    int temp = a[i];
    for (int j = 1; j <= tot; ++j) if (temp >> pivot[j] & 1) temp ^= base[j];
    if (temp) {
      base[++tot] = temp;
      pivot[tot] = 31 - __builtin_clz(temp);
      for (int j = tot; j > 1; --j) {
        if (pivot[j] > pivot[j - 1]) {
          std::swap(pivot[j], pivot[j - 1]);
          std::swap(base[j], base[j - 1]);
        }
      }
    } else {
      ans += a[i];
    }
  }
  printf("%lld\n", ans);
  return 0;
}
