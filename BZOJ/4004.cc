#include <cstdio>
#include <vector>
#include <valarray>

const int N = 500 + 10;
const double eps = 1e-5;

int n, m, c[N];

std::valarray<double> a[N];
std::vector< std::valarray<double> > pool;
std::vector<int> tag;

bool gauss(int p) {
  std::valarray<double> temp(a[p]);
  for (int i = 0; i < pool.size(); ++i)
    if (fabs(temp[tag[i]]) > eps)
      temp -= pool[i] * (temp[tag[i]] / pool[i][tag[i]]);
  int k = 0;
  for (; k < m; ++k) if (fabs(temp[k]) > eps) break;
  if (k == m) return false;
  pool.push_back(temp);
  tag.push_back(k);
  return true;
}

bool cmp(int a, int b) { return c[a] < c[b]; }

int main() {
  scanf("%d%d", &n, &m);
  for (int i = 0; i < n; ++i) {
    a[i].resize(m);
    for (int j = 0; j < m; ++j)
      scanf("%lf", &a[i][j]);
  }
  int cnt = 0, ans = 0;
  static int id[N];
  for (int i = 0; i < n; ++i) scanf("%d", c + i), id[i] = i;
  std::sort(id, id + n, cmp);
  for (int it = 0; it < n; ++it) {
    int i = id[it];
    if (gauss(i)) {
      ++cnt;
      ans += c[i];
    }
  }
  printf("%d %d\n", cnt, ans);
  return 0;
}
