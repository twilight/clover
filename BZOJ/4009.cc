#include <cstdio>
#include <cstdlib>
#include <queue>
#include <vector>
#include <algorithm>

#define fst first
#define snd second

typedef std::pair<int, int> range_t;

const int N = 40000 + 10, SZ = N * 400;

int n, p, q, a[N], b[N], c[N];

int fa[N], dep[N], size[N], son[N], top[N], dfn[N];

std::vector<int> adj[N];

void DFS(int a) {
  size[a] = 1;
  for (std::vector<int>::iterator it = adj[a].begin(); it != adj[a].end(); ++it) {
    int b = *it;
    if (b != fa[a]) {
      fa[b] = a;
      dep[b] = dep[a] + 1;
      DFS(b);
      size[a] += size[b];
      if (size[b] > size[son[a]]) son[a] = b;
    }
  }
}

void Split(int a, int t) {
  static int tot;
  dfn[a] = ++tot;
  top[a] = t;
  if (son[a]) Split(son[a], t);
  for (std::vector<int>::iterator it = adj[a].begin(); it != adj[a].end(); ++it) {
    int b = *it;
    if (b != fa[a] && b != son[a]) Split(b, b);
  }
}

namespace PersistentTreap {

struct node_t {
  node_t *ch[2];
  int size, val;
  inline void update();
} pool[SZ], *tot = pool;

inline void node_t::update() {
  size = 1;
  if (ch[0]) size += ch[0]->size;
  if (ch[1]) size += ch[1]->size;
}

node_t *makenode(int v) {
  tot->val = v;
  tot->update();
  return tot++;
}

inline double frand() { return (double)rand() / RAND_MAX; }

node_t *Merge(node_t *a, node_t *b) {
  if (!a) return b;
  if (!b) return a;
  if (a->val > b->val) std::swap(a, b);
  node_t *res = ++tot;
  if (a->size / (double)(a->size + b->size) < frand()) {
    res->val = a->val;
    res->ch[0] = a->ch[0];
    res->ch[1] = Merge(a->ch[1], b);
  } else {
    res->val = b->val;
    res->ch[1] = b->ch[1];
    res->ch[0] = Merge(a, b->ch[0]);
  }
  res->update();
  return res;
}

void Split(node_t *cur, int v, node_t *&a, node_t *&b) {
  if (!cur) {
    a = b = NULL;
    return;
  }
  node_t *p = ++tot;
  *p = *cur;
  if (cur->val < v) {
    Split(cur->ch[1], v, a, b);
    p->ch[1] = a;
    p->update();
    a = p;
  } else {
    Split(cur->ch[0], v, a, b);
    p->ch[0] = b;
    p->update();
    b = p;
  }
}

node_t* Insert(node_t *a, int b) {
  node_t *u, *v, *x;
  x = ++tot;
  x->val = b;
  x->update();
  Split(a, b, u, v);
  return Merge(Merge(u, x), v);
}

#define SIZE(x) ((x) ? ((x)->size) : 0)

int Rank(node_t *root, int k) {
  int res = 0;
  for (node_t *cur = root; cur;) {
    if (cur->val > k) {
      cur = cur->ch[0];
    } else {
      res += SIZE(cur->ch[0]) + 1;
      cur = cur->ch[1];
    }
  }
  return res;
}

#undef SIZE

}

namespace PersistentSegmentTree {

int tot, lch[SZ], rch[SZ];
PersistentTreap::node_t *pool[SZ];

int Modify(int prev, int l, int r, int p, int v) {
  int res = ++tot;
  pool[res] = PersistentTreap::Insert(pool[prev], v);
  lch[res] = lch[prev];
  rch[res] = rch[prev];
  if (l == r) return res;
  int mid = (l + r) / 2;
  if (p <= mid) 
    lch[res] = Modify(lch[prev], l, mid, p, v);
  else
    rch[res] = Modify(rch[prev], mid + 1, r, p, v);
  return res;
}

int Query(int id, int l, int r, int p, int q, int k) {
  if (!id) return 0;
  if (p <= l && r <= q) return PersistentTreap::Rank(pool[id], k);
  int mid = (l + r) / 2, res = 0;
  if (p <= mid) res += Query(lch[id], l, mid, p, q, k);
  if (q > mid) res += Query(rch[id], mid + 1, r, p, q, k);
  return res;
}

}

int pool[N];

std::vector<range_t> range;
std::vector<int> plate[N];

int list[4];

inline void Select(int u, int v) {
  list[0] = pool[dfn[u]], list[1] = pool[dfn[v]];
  range.clear();
  while (top[u] != top[v]) {
    if (dep[top[u]] < dep[top[v]]) std::swap(u, v);
    range.push_back(range_t(dfn[top[u]], dfn[u]));
    u = fa[top[u]];
  }
  if (dep[u] > dep[v]) std::swap(u, v);
  range.push_back(range_t(dfn[u], dfn[v]));
  list[2] = pool[dfn[u]], list[3] = pool[dfn[fa[u]]];
}

inline int Query(int gap) {
  int res = 0;
  for (size_t i = 0; i < range.size(); ++i) {
    res += PersistentSegmentTree::Query(list[0], 1, n, range[i].fst, range[i].snd, gap);
    res += PersistentSegmentTree::Query(list[1], 1, n, range[i].fst, range[i].snd, gap);
    res -= PersistentSegmentTree::Query(list[2], 1, n, range[i].fst, range[i].snd, gap);
    res -= PersistentSegmentTree::Query(list[3], 1, n, range[i].fst, range[i].snd, gap);
  }
  return res;
}

inline void BFS(int s) {
  static std::queue<int> q;
  for (q.push(s); !q.empty();) {
    int a = q.front();
    q.pop();
    for (std::vector<int>::iterator it = plate[a].begin(); it != plate[a].end(); ++it) pool[dfn[a]] = PersistentSegmentTree::Modify(pool[dfn[a]], 1, n, dfn[b[*it]], c[*it]);
    for (std::vector<int>::iterator it = adj[a].begin(); it != adj[a].end(); ++it) {
      int b = *it;
      if (b != fa[a]) {
        q.push(b);
        pool[dfn[b]] = pool[dfn[a]];
      }
    }
  }
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d%d%d", &n, &p, &q);
  for (int cnt = n - 1; cnt--;) {
    int a, b;
    scanf("%d%d", &a, &b);
    adj[a].push_back(b);
    adj[b].push_back(a);
  }
  DFS(1);
  Split(1, 1);
  for (int i = 1; i <= p; ++i) {
    scanf("%d%d%d", a + i, b + i, c + i);
    if (dfn[a[i]] > dfn[b[i]]) std::swap(a[i], b[i]);
    plate[a[i]].push_back(i);
  }
  BFS(1);
  while (q--) {
    int u, v, k;
    scanf("%d%d%d", &u, &v, &k);
    Select(u, v);
    int l = 0, r = 1e9;
    while (l < r) {
      int mid = (l + r) / 2;
      if (Query(mid) < k) l = mid + 1; else r = mid;
    }
    printf("%d\n", l);
  }
  return 0;
}
