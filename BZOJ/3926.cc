#include <cstdio>
#include <cstring>
#include <queue>
#include <vector>

typedef long long i64;

const int N = 100000 + 10, SZ = 10;

int n, c, col[N];
std::vector<int> adj[N];

struct node_t {
  node_t *next[SZ], *pre;
  int len;
  i64 size;
  node_t *append(int);
} pool[50 * N], *tot = pool, *root = pool;

node_t *node_t::append(int ch) {
  node_t *np = ++tot, *p = this;
  np->len = p->len + 1;
  for (; p && !p->next[ch]; p = p->pre) p->next[ch] = np;
  if (!p) {
    np->pre = root;
  } else {
    node_t *q = p->next[ch];
    if (q->len == p->len + 1) {
      np->pre = q;
    } else {
      node_t *nq = ++tot;
      *nq = *q;
      nq->len = p->len + 1;
      q->pre = np->pre = nq;
      for (; p && p->next[ch] == q; p = p->pre) p->next[ch] = nq;
    }
  }
  return np;
}

int next[20 * N][SZ], trie = 1, cnt = 1;
node_t *node[20 * N];

void DFS(int a, int &id, int fa = 0) {
  if (!id) id = ++cnt;
  for (std::vector<int>::iterator it = adj[a].begin(); it != adj[a].end();
       ++it) {
    int b = *it;
    if (b != fa) DFS(b, next[id][col[b]], a);
  }
}

void BFS(int s) {
  static std::queue<int> q;
  node[s] = root;
  for (q.push(s); !q.empty(); q.pop()) {
    int a = q.front();
    for (int i = 0; i < SZ; ++i) {
      int b = next[a][i];
      if (b) {
        q.push(b);
        node[b] = node[a]->append(i);
      }
    }
  }
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d%d", &n, &c);
  for (int i = 1; i <= n; ++i) scanf("%d", col + i);
  for (int ecnt = n - 1, u, v; ecnt--;) {
    scanf("%d%d", &u, &v);
    adj[u].push_back(v);
    adj[v].push_back(u);
  }
  for (int i = 1; i <= n; ++i)
    if (adj[i].size() == 1) DFS(i, next[trie][col[i]]);
  BFS(1);
  static int cnt[N];
  for (node_t *it = root; it <= tot; ++it) ++cnt[it->len];
  for (int i = 1; i <= n; ++i) cnt[i] += cnt[i - 1];
  static node_t *temp[50 * N];
  for (node_t *it = root; it <= tot; ++it) temp[--cnt[it->len]] = it;
  for (int i = tot - pool; i >= 0; --i) {
    node_t *it = temp[i];
    it->size = 1;
    for (int i = 0; i < SZ; ++i)
      if (it->next[i]) it->size += it->next[i]->size;
  }
  printf("%lld\n", root->size - 1);
  return 0;
}
