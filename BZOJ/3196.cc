/**************************************************************
    Problem: 3196
    User: dennisyang
    Language: C++
    Result: Accepted
    Time:2992 ms
    Memory:83272 kb
****************************************************************/
 
#include <cstdio>
#include <map>
#include <vector>
#include <algorithm>
 
#pragma comment(linker, "/STACK:1000000000")
 
const int maxnode = 5e6 + 10, maxn = 5e4 + 10, maxm = 5e4 + 10;
 
struct node {
    int size,cnt;
    node* ch[2];
} pool[maxnode],*tot = &pool[0],*root[maxn];
 
int n,m,cur[maxn],rlim;
int l[maxm],r[maxm],opt[maxm],k[maxm];
 
std::vector<int> list;
std::map<int,int> table;
 
inline int lowbit(int x) { return x & -x; }
 
void get(std::vector<node*> &p, int r) {
    p.clear();
    while (r) {
        p.push_back(root[r]);
        r -= lowbit(r);
    }
}
 
void go(std::vector<node*> &p, int d) {
    std::vector<node*>::iterator iter;
    for (iter = p.begin(); iter != p.end(); ++iter)
        if (*iter) *iter = (*iter) -> ch[d];
}
 
int getlchsum(std::vector<node*> &p) {
    std::vector<node*>::iterator iter;
    int res = 0;
    for (iter = p.begin(); iter != p.end(); ++iter)
        if ((*iter) && (*iter) -> ch[0]) res += (*iter) -> ch[0] -> size;
    return res;
}
 
int getrchsum(std::vector<node*> &p) {
    std::vector<node*>::iterator iter;
    int res = 0;
    for (iter = p.begin(); iter != p.end(); ++iter)
        if ((*iter) && (*iter) -> ch[1]) res += (*iter) -> ch[1] -> size;
    return res;
}
 
node* Insert(node *orig, int l, int r, int pos, int val) {
    if (!orig) orig = tot++;
    orig->size += val;
    if (l == r) return orig;
    int m = (l+r) >> 1;
    if (pos <= m) orig->ch[0] = Insert(orig->ch[0],l,m,pos,val); else
        orig->ch[1] = Insert(orig->ch[1],m+1,r,pos,val);
    return orig;
}
 
int Rank(int l, int r, int pos, std::vector<node*> &ltree, std::vector<node*> &rtree) {
    if (l == pos && pos == r) return 0;
    int m = (l+r) >> 1, res = 0;
    bool d = pos > m;
    if (d) l = m+1, res += (getlchsum(rtree) - getlchsum(ltree)); else r = m;
    go(ltree,d), go(rtree,d);
    return res + Rank(l,r,pos,ltree,rtree);
}
 
int Select(int l, int r, int k, std::vector<node*> &ltree, std::vector<node*> &rtree) {
    if (l == r) return l;
    int m = (l+r) >> 1;
    int cnt = getlchsum(rtree) - getlchsum(ltree);
    bool d = k > cnt;
    go(ltree,d);
    go(rtree,d);
    if (d) l = m+1, k -= cnt; else r = m;
    return Select(l,r,k,ltree,rtree);
}
 
void update(int idx, int pos, int val) {
    while (idx <= n) {
        root[idx] = Insert(root[idx],1,rlim,pos,val);
        idx += lowbit(idx);
    }
}
 
int rank(int l, int r, int pos) {
    std::vector<node*> tree[2];
    get(tree[0],l-1);
    get(tree[1],r);
    return Rank(1,rlim,pos,tree[0],tree[1]) + 1;
}
 
int select(int l, int r, int k) {
    std::vector<node*> tree[2];
    get(tree[0],l-1);
    get(tree[1],r);
    return list[Select(1,rlim,k,tree[0],tree[1])-1];
}
 
int count(int l, int r, int pos) {
    std::vector<node*> tree[2];
    get(tree[0],l-1);
    get(tree[1],r);
    l = 1, r = rlim;
    while (l < r) {
        int m = (l+r) >> 1;
        bool d = (pos > m);
        go(tree[0],d), go(tree[1],d);
        if (d) l = m+1; else r = m;
    }
    std::vector<node*>::iterator iter;
    int res = 0;
    for (iter = tree[1].begin(); iter != tree[1].end(); ++iter)
        if (*iter) res += (*iter) -> size;
    for (iter = tree[0].begin(); iter != tree[0].end(); ++iter)
        if (*iter) res -= (*iter) -> size;
    return res;
}
 
int pred(int l, int r, int val) {
    int k = rank(l,r,val)-1;
    return select(l,r,k);
}
 
int succ(int l, int r, int val) {
    int k = rank(l,r,val),cnt = count(l,r,val);
    if (cnt) k += cnt;
    return select(l,r,k);
}
 
void Init() {
    std::scanf("%d%d",&n,&m);
    for (int i=1; i<=n; ++i) {
        std::scanf("%d",cur+i);
        list.push_back(cur[i]);
    }
    for (int i=1; i<=m; ++i) {
        std::scanf("%d%d",opt+i,l+i);
        if (opt[i] != 3) std::scanf("%d",r+i);
        std::scanf("%d",k+i);
        list.push_back(k[i]);
    }
    std::sort(list.begin(),list.end());
    std::vector<int>::iterator end = std::unique(list.begin(),list.end());
    list.erase(end,list.end());
    rlim = list.size();
    for (int i=0; i<rlim; ++i) table[list[i]] = i+1;
}
 
int main() {
    Init();
    for (int i=1; i<=n; ++i) {
        update(i,cur[i] = table[cur[i]],1);
    }
    for (int i=1,ans; i<=m; ++i) {
        switch (opt[i]) {
            case 1:
                ans = rank(l[i],r[i],table[k[i]]);
                break;
            case 2:
                ans = select(l[i],r[i],k[i]);
                break;
            case 3:
                update(l[i],cur[l[i]],-1);
                update(l[i],cur[l[i]] = table[k[i]],1);
                break;
            case 4:
                ans = pred(l[i],r[i],table[k[i]]);
                break;
            case 5:
                ans = succ(l[i],r[i],table[k[i]]);
                break;
        }
        if (opt[i] != 3) printf("%d\n",ans);
    }
    return 0;
}