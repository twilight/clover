#include <cstdio>
#include <cstring>
#include <algorithm>

const int N = 100000 * 2 + 10;

int n, gap, sa[N], rank[N];
char s[N];

inline bool sufcmp(int a, int b) {
  if (rank[a] != rank[b]) return rank[a] < rank[b];
  a += gap, b += gap;
  return (a <= n && b <= n) ? (rank[a] < rank[b]) : (a > b);
}

int main() {
  scanf(" %s", s + 1);
  n = strlen(s + 1);
  strncpy(s + 1 + n, s + 1, n);
  n *= 2;
  for (int i = 1; i <= n; ++i) sa[i] = i, rank[i] = s[i];
  for (gap = 1;; gap <<= 1) {
    std::sort(sa + 1, sa + 1 + n, sufcmp);
    static int tmp[N];
    tmp[1] = 1;
    for (int i = 2; i <= n; ++i) tmp[i] = tmp[i - 1] + sufcmp(sa[i - 1], sa[i]);
    for (int i = 1; i <= n; ++i) rank[sa[i]] = tmp[i];
    if (tmp[n] == n) break;
  }
  for (int i = 1; i <= n; ++i) if (sa[i] <= n / 2) putchar(s[sa[i] + n / 2 - 1]);
  putchar('\n');
  return 0;
}
