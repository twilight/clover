#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>

const int N = 20, dx[] = {-1, 0, 1, 0}, dy[] = {0, 1, 0, -1};
const int INF = 0x3f3f3f3f;

int n, x1, y1, x2, y2;

int mem[3 * N][N][N][N][N][2];

int dfs(int step, int x1, int y1, int x2, int y2, int dom) {
  if (x1 == x2 && y1 == y2) return dom ? INF : 0;
  if (step >= 3 * n) return INF;
  int &res = mem[step][x1][y1][x2][y2][dom];
  if (res != INF) return res;
  if (dom) {
    for (int k = 0; k < 4; ++k) {
      for (int b = 1; b <= 2; ++b) {
        int p = x2 + b * dx[k], q = y2 + b * dy[k];
        if (!(0 <= p && p < n && 0 <= q && q < n)) continue;
        res = std::min(res, dfs(step + 1, x1, y1, p, q, 0) + 1);
      }
    }
  } else {
    res = -1;
    for (int k = 0; k < 4; ++k) {
      int p = x1 + dx[k], q = y1 + dy[k];
      if (!(0 <= p && p < n && 0 <= q && q < n)) continue;
      res = std::max(res, dfs(step + 1, p, q, x2, y2, 1) + 1);
    }
  }
  return res;
}

int main() {
  scanf("%d%d%d%d%d", &n, &x1, &y1, &x2, &y2);
  --x1, --y1, --x2, --y2;
  if (abs(x1 - x2) + abs(y1 - y2) <= 1) {
    puts("WHITE 1");
  } else {
    memset(mem, 0x3f, sizeof mem);
    printf("BLACK %d\n", dfs(0, x1, y1, x2, y2, 0));
  }
  return 0;
}
