#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>

typedef long long i64;

const int N = 50000 + 10;

int n, m, op[N], a[N], b[N], c[N], ans[N];

unsigned sum[2 * N], tag[2 * N];

#define pos(l, r) (((l) + (r)) | ((l) != (r)))

inline int cross(int l, int r, int p, int q) { return std::min(r, q) - std::max(l, p) + 1; }

void modify(int l, int r, int p, int q) {
  int id = pos(l, r);
  if (p <= l && r <= q) {
    ++tag[id];
    return;
  }
  sum[id] += cross(l, r, p, q);
  int mid = (l + r) / 2;
  if (p <= mid) modify(l, mid, p, q);
  if (q > mid) modify(mid + 1, r, p, q);
}

unsigned query(int l, int r, int p, int q) {
  int id = pos(l, r);
  unsigned res = tag[id] * cross(l, r, p, q);
  if (p <= l && r <= q) return res + sum[id];
  int mid = (l + r) / 2;
  if (p <= mid) res += query(l, mid, p, q);
  if (q > mid) res += query(mid + 1, r, p, q);
  return res;
}

int sorted[N], order[N];

int mark[N];

inline bool Compare(int a) { return !mark[a]; }

void solve(int l, int r, int p, int q) {
  if (l == r) {
    for (; p <= q; ++p) ans[order[p]] = l;
    return;
  }
  int mid = (l + r) / 2;
  memset(sum, 0, sizeof sum);
  memset(tag, 0, sizeof tag);
  for (int it = p; it <= q; ++it) {
    int i = order[it];
    if (op[i] == 1) {
      if (c[i] > mid) {
        mark[i] = 1;
        modify(1, n, a[i], b[i]);
      } else {
        mark[i] = 0;
      }
    } else {
      unsigned temp = query(1, n, a[i], b[i]);
      if (c[i] > temp) {
        c[i] -= temp;
        mark[i] = 0;
      } else {
        mark[i] = 1;
      }
    }
  }
  int g = std::stable_partition(order + p, order + q + 1, Compare) - order - 1;
  solve(l, mid, p, g);
  solve(mid + 1, r, g + 1, q);
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= m; ++i) {
    scanf("%d%d%d%d", op + i, a + i, b + i, c + i);
    if (op[i] == 1) sorted[++sorted[0]] = c[i];
    order[i] = i;
  }
  std::sort(sorted + 1, sorted + sorted[0] + 1);
  sorted[0] = std::unique(sorted + 1, sorted + sorted[0] + 1) - sorted - 1;
  for (int i = 1; i <= m; ++i) if (op[i] == 1) c[i] = std::lower_bound(sorted + 1, sorted + sorted[0] + 1, c[i]) - sorted;
  solve(1, sorted[0], 1, m);
  for (int i = 1; i <= m; ++i) if (op[i] == 2) printf("%d\n", sorted[ans[i]]);

  return 0;
}
