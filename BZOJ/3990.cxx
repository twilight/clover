#include <cstdio>
#include <vector>
#include <algorithm>

const int N = 30;

typedef long long i64;

int n;
std::vector<int> a;

i64 cnt[N];

inline bool check(std::vector<int> &t, int a) {
  return !(t[a] & 1) && t[a] + 1 == t[a + 1];
}

void Solve(int n, int dep, std::vector<int> &a) {
  if (!n) {
    ++cnt[dep];
    return;
  }
  int u, v;
  u = v = -1;
  for (size_t i = 0; i < a.size(); i += 2) {
    if (!check(a, i)) {
      if (~u) {
        if (~v) return;
        v = i;
      } else {
        u = i;
      }
    }
  }
  std::vector<int> b(a.size() / 2);
  for (size_t i = 0; i < b.size(); ++i) b[i] = i;
  if (u == -1) {
    std::sort(b.begin(), b.end(), [&a](int u, int v) { return a[2 * u] < a[2 * v]; });
    Solve(n - 1, dep, b);
    return;
  } else if (v == -1) {
    std::swap(a[u], a[u + 1]);
    std::sort(b.begin(), b.end(), [&a](int u, int v) { return a[2 * u] < a[2 * v]; });
    Solve(n - 1, dep + 1, b);
    std::swap(a[u], a[u + 1]);
  } else {
    for (int i = 0; i < 2; ++i) {
      for (int j = 0; j < 2; ++j) {
        std::swap(a[u + i], a[v + j]);
        if (check(a, u) && check(a, v)) {
          std::sort(b.begin(), b.end(), [&a](int u, int v) { return a[2 * u] < a[2 * v]; });
          Solve(n - 1, dep + 1, b);
        }
        std::swap(a[u + i], a[v + j]);
      }
    }
  }
}

int main() {
  scanf("%d", &n);
  for (int i = 0, x; i < (1 << n); ++i) {
    scanf("%d", &x);
    a.push_back(x - 1);
  }
  Solve(n, 0, a);
  i64 temp = 1, ans = 0;
  for (int i = 0; i <= n; ++i) {
    if (i) temp *= i;
    ans += temp * cnt[i];
  }
  printf("%lld\n", ans);
  return 0;
}
