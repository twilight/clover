#include <bits/stdc++.h>

#define fst first
#define snd second

const int N = 100000 + 10, SZ = 3000000 + 10;
const double a = 0.7;

int n;

double tag[SZ];
std::pair<int, int> orig[SZ];
int ch[SZ][2], size[SZ], root, tot, pool[SZ];

int build(int p, int q, double l, double r) {
  if (p > q) return 0;
  double mid = (l + r) / 2.0;
  int m = (p + q) / 2;
  int cur = pool[m];
  tag[cur] = mid;
  ch[cur][0] = build(p, m - 1, l, mid);
  ch[cur][1] = build(m + 1, q, mid, r);
  size[cur] = size[ch[cur][0]] + size[ch[cur][1]] + 1;
  return cur;
}

void inorder(int cur) {
  if (!cur) return;
  inorder(ch[cur][0]), pool[++pool[0]] = cur, inorder(ch[cur][1]);
}

inline int rebuild(int cur, double l, double r) {
  pool[0] = 0;
  inorder(cur);
  return build(1, pool[0], l, r);
}

inline int sgn(int a, int b) {
  if (tag[a] == tag[b]) return 0;
  return tag[a] < tag[b] ? -1 : 1;
}

inline int cmp(const std::pair<int, int> &a, const std::pair<int, int> &b) {
  if (sgn(a.fst, b.fst)) return sgn(a.fst, b.fst);
  return sgn(a.snd, b.snd);
}

int res;

int insert(int cur, const std::pair<int, int> &now, double l, double r) {
  double mid = (l + r) / 2.0;
  if (!cur) {
    cur = ++tot;
    size[cur] = 1;
    tag[cur] = mid;
    orig[cur] = now;
    return res = cur;
  }
  int d = cmp(now, orig[cur]);
  if (!d) return res = cur;
  if (d == -1) ch[cur][0] = insert(ch[cur][0], now, l, mid); else ch[cur][1] = insert(ch[cur][1], now, mid, r);
  size[cur] = size[ch[cur][0]] + size[ch[cur][1]] + 1;
  if (std::max(size[ch[cur][0]], size[ch[cur][1]]) >= a * size[cur]) cur = rebuild(cur, l, r);
  return cur;
}

#define lch ((id) << 1)
#define rch ((id) << 1 | 1)

int no[N * 10], max[N * 10];

inline void update(int id) { if (tag[no[max[lch]]] >= tag[no[max[rch]]]) max[id] = max[lch]; else max[id] = max[rch]; }

void build(int id, int l, int r) {
  if (l == r) {
    max[id] = l;
    return;
  }
  int mid = (l + r) / 2;
  build(lch, l, mid);
  build(rch, mid + 1, r);
  update(id);
}

void touch(int id, int l, int r, int pos, const std::pair<double, double> &now) {
  if (l == r) {
    root = insert(root, now, 0.0, 1.0);
    no[l] = res;
    max[id] = l;
    return;
  }
  int mid = (l + r) / 2;
  if (pos <= mid) touch(lch, l, mid, pos, now); else touch(rch, mid + 1, r, pos, now);
  update(id);
}

int query(int id, int l, int r, int p, int q) {
  if (p <= l && r <= q) return max[id];
  int mid = (l + r) / 2, res = -1;
  if (p <= mid) res = query(lch, l, mid, p, q);
  if (q > mid) {
    int tmp = query(rch, mid + 1, r, p, q);
    if (!~res || tag[no[tmp]] > tag[no[res]]) res = tmp;
  }
  return res;
}

#undef lch
#undef rch

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  int m;
  scanf("%d%d", &n, &m);
  for (build(1, 1, n); m--;) {
    static char op;
    static int l, r, k;
    static std::pair<int, int> now;
    scanf(" %c%d%d", &op, &l, &r);
    switch (op) {
      case 'C':
        scanf("%d", &k);
        now.fst = no[l];
        now.snd = no[r];
        touch(1, 1, n, k, now);
        break;
      case 'Q':
        printf("%d\n", query(1, 1, n, l, r));
        break;
    }
  }
  return 0;
}
