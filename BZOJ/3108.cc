#include <cstdio>
#include <bitset>

const int N = 300 + 10;

int n, m;

std::bitset<N> mask[N];

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d", &n, &m);
    for (int i = 0; i < n; ++i) mask[i].reset();
    while (m--) {
      int x, y;
      scanf("%d%d", &x, &y);
      mask[x][y] = 1;
    }
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < n; ++j) {
        if ((mask[i] & mask[j]).none()) continue;
        if (mask[i] != mask[j]) goto fail;
      }
    }
    puts("Yes");
    continue;
 fail:
    puts("No");
  }
  return 0;
}
