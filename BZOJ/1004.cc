#include <cstdio>
#include <cstring>
#include <vector>

typedef long long int64;

const int N = 60 + 10;

int n, r, g, b, m, MOD;

inline void INC(int &a, int b) { a = (a + b) % MOD; }
inline void MUL(int &a, int b) { a = (int64)a * b % MOD; }
inline int pdt(int a, int b) { return (int64)a * b % MOD; }

inline int pow(int base, int exp) {
  int res = 1;
  while (exp) {
    if (exp & 1) MUL(res, base);
    MUL(base, base);
    exp >>= 1;
  }
  return res;
}

inline int inv(int x) { return pow(x, MOD - 2); }
inline int qtt(int a, int b) { return pdt(a, inv(b)); }

int f[N], size[N];
inline int find(int x) { return f[x] == x ? x : (f[x] = find(f[x])); }

inline int cal(int x[]) {
  static int dp[N][21][21];
  for (int i = 1; i <= n; ++i) f[i] = i, size[i] = 1;
  for (int i = 1; i <= n; ++i) {
    if (find(i) != find(x[i])) {
      size[find(x[i])] += size[find(i)];
      size[find(i)] = 0;
      f[find(i)] = find(x[i]);
    }
  }
  int cnt = 0;
  static std::vector<int> pool;
  pool.clear();
  for (int i = 1; i <= n; ++i) if (size[i]) pool.push_back(size[i]);
  memset(dp, 0, sizeof dp);
  int res = 0;
  dp[0][0][0] = 1;
  for (; !pool.empty(); pool.pop_back()) {
    for (int i = r; i >= 0; --i) {
      for (int j = g; j >= 0; --j) {
        for (int k = b; k >= 0; --k) {
          if (i >= pool.back()) INC(dp[i][j][k], dp[i - pool.back()][j][k]);
          if (j >= pool.back()) INC(dp[i][j][k], dp[i][j - pool.back()][k]);
          if (k >= pool.back()) INC(dp[i][j][k], dp[i][j][k - pool.back()]);
        }
      }
    }
  }
  return dp[r][g][b];
}

int main() {
  scanf("%d%d%d%d%d", &r, &b, &g, &m, &MOD);
  n = r + g + b;
  int ans = 0;
  static int x[N];
  for (int i = 1; i <= n; ++i) x[i] = i;
  INC(ans, cal(x));
  for (int _m = m; _m--;) {
    for (int i = 1; i <= n; ++i) scanf("%d", x + i);
    INC(ans, cal(x));
  }
  printf("%d\n", qtt(ans, m + 1));
  return 0;
}
