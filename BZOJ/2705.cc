#include <cstdio>

typedef long long int64;

const int N = 50;

int64 n;
int64 factor[N], cnt[N], tot;
int64 ans;

void cal(int dep, int64 num, int64 phi) {
  if (dep > tot) {
//  ans += phi / n * num * (n / num);
    ans += phi;
    return;
  }
  cal(dep + 1, num, phi);
  phi = phi / factor[dep] * (factor[dep] - 1);
  for (int i = 1; i <= cnt[dep]; ++i)
    cal(dep + 1, num *= factor[dep], phi);
}

int main() {
  scanf("%lld", &n);
  int m = n;
  for (int i = 2; i * i <= m; ++i) {
    if (m % i == 0) {
      factor[++tot] = i;
      for (; m % i == 0; m /= i) cnt[tot]++;
    }
  }
  if (m > 1) factor[++tot] = m, cnt[tot] = 1;
  ans = 0;
  cal(1, 1, n);
  printf("%lld\n", ans);
  return 0;
}
