#include <bits/stdc++.h>

#define x first
#define y second

typedef std::pair<double, double> point;

const int N = 100000 + 10;
const double eps = 1e-10;

int n;
double d, a[N], b[N];

inline double cross(const point &o, const point &a, const point &b) {
  return (a.x - o.x) * (b.y - o.y) - (a.y - o.y) * (b.x - o.x);
}

inline int sgn(double a) {
  if (std::abs(a) < eps) return 0;
  return a > 0.0 ? 1 : -1;
}

inline void maintain(std::vector<point> &pool, const point &a) {
  while (pool.size() > 2 && sgn(cross(pool[pool.size() - 2], pool.back(), a)) <= 0) pool.pop_back();
  pool.push_back(a);
}

inline double slope(const point &a, const point &b) { return (a.y - b.y) / (a.x - b.x); }

inline double search(const std::vector<point> &pool, const point &a) {
  int l = 0, r = pool.size() - 1;
  while (r - l > 3) {
    int p = l + (r - l) / 3, q = r - (r - l) / 3;
    if (slope(a, pool[p]) < slope(a, pool[q])) l = p; else r = q;
  }
  double res = 0.0;
  for (int i = l; i <= r; ++i) res = std::max(res, slope(pool[i], a));
  return res;
}

int main() {
  scanf("%d%lf", &n, &d);
  for (int i = 1; i <= n; ++i) scanf("%lf%lf", a + i, b + i);
  static std::vector<point> convex;
  double ans = 0.0;
  for (int i = 1; i <= n; ++i) {
    maintain(convex, point(i * d, a[i - 1]));
    a[i] += a[i - 1];
    ans += search(convex, point(b[i] + i * d, a[i]));
  }
  printf("%.0f\n", ans);
  return 0;
}
