#include <cstdio>
#include <cstring>
#include <algorithm>

const int N = 1100000 + 10, SZ = N * 2;
const int root = 1;

int n, m;

int next[SZ][3], pre[SZ], len[SZ];

inline void COPY(int a, int b) {
  memcpy(next[a], next[b], 3 * sizeof(int));
  pre[a] = pre[b], len[a] = len[b];
}

inline void append(int ch) {
  static int tot, last = ++tot;
  int p = last, np = ++tot;
  len[np] = len[p] + 1;
  for (; p && !next[p][ch]; p = pre[p]) next[p][ch] = np;
  if (!p) {
    pre[np] = root;
  } else {
    int q = next[p][ch];
    if (len[q] == len[p] + 1) {
      pre[np] = q;
    } else {
      int nq = ++tot;
      COPY(nq, q);
      len[nq] = len[p] + 1;
      pre[q] = pre[np] = nq;
      for (; p && next[p][ch] == q; p = pre[p]) next[p][ch] = nq;
    }
  }
  last = np;
}

int d[N];

void preprocessing(const char s[], const int n, int res[]) {
  int cur = 0, p = root;
  for (int i = 0; i < n; ++i) {
    int ch = s[i] - '0';
    if (next[p][ch]) {
      cur++;
      p = next[p][ch];
    } else {
      while (p && !next[p][ch]) p = pre[p];
      if (next[p][ch]) cur = len[p] + 1, p = next[p][ch]; else cur = 0, p = root; 
    }
    res[i + 1] = cur;
  }
}

bool check(const char s[], const int n, int lim) {
  static int dp[N], q[N];
  int *qf, *qr;
  qf = qr = q;
  for (int i = 1; i <= n; ++i) {
    if (i >= lim) {
      int tmp = i - lim;
      while (qf < qr && dp[*(qr - 1)] - *(qr - 1) < dp[tmp] - tmp) qr--;
      *qr++ = tmp;
    }
    while (qf < qr && *qf < i - d[i]) qf++;
    dp[i] = std::max(dp[i - 1], qf < qr ? (dp[*qf] + i - *qf) : 0);
  }
  return 10 * dp[n] >= 9 * n;
}

int solve(const char s[]) {
  int n = strlen(s);
  preprocessing(s, n, d);
  int l = 0, r = n;
  while (l < r) {
    int mid = (l + r + 1) / 2;
    if (check(s, n, mid)) l = mid; else r = mid - 1;
  }
  return l;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  scanf("%d%d", &n, &m);
  static char s[N];
  for (int tmp = m; tmp--;) {
    scanf(" %s", s);
    for (char *it = s; *it != '\0'; ++it) append(*it - '0');
    append(2);
  }
  for (int tmp = n; tmp--;) {
    scanf(" %s", s);
    printf("%d\n", solve(s));
  }
  return 0;
}
