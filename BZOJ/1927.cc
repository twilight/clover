#include <bits/stdc++.h>

typedef long long int64;

const int N = 800 + 10, V = N * 2, E = V * 100; 
const int INF = 1 << 25;
const int64 oo = 1LL << 50;

int n, m;

int adj[V];
int to[E], next[E], cap[E], cost[E];

inline void link(int a, int b, int c, int d) {
  static int cnt = 2;
  to[cnt] = b, next[cnt] = adj[a], cap[cnt] = c, cost[cnt] = d, adj[a] = cnt++;
  to[cnt] = a, next[cnt] = adj[b], cap[cnt] = 0, cost[cnt] = -d, adj[b] = cnt++;
}

int tot, pre[V], e[V];
int64 dis[V];

bool AugmentPath(const int s, const int t) {
  static int q[E * 10], *qf, *qr;
  static bool flag[V];
  qf = qr = q;
  std::fill(dis + 1, dis + tot + 1, oo);
  for (dis[*qr++ = s] = 0; qf < qr;) {
    int a = *qf++;
    flag[a] = false;
    for (int it = adj[a]; it; it = next[it]) {
      int b = to[it], c = cost[it];
      if (cap[it] && dis[a] + c < dis[b]) {
        dis[b] = dis[a] + c;
        pre[b] = a;
        e[b] = it;
        if (!flag[b]) flag[*qr++ = b] = true;
      }
    }
  }
  return dis[t] < oo;
}

int64 MinCostFlow(const int s, const int t) {
  int64 res = 0;
  int f = 0;
  while (AugmentPath(s, t)) {
    int df = INF;
    for (int i = t; i != s; i = pre[i]) df = std::min(df, cap[e[i]]);
    res += (int64)df * dis[t];
    for (int i = t; i != s; i = pre[i]) {
      cap[e[i]] -= df;
      cap[e[i] ^ 1] += df;
    }
    f += df;
  }
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d%d", &n, &m);
  tot = n * 2;
  const int s = ++tot, t = ++tot;
  const int ss = ++tot, tt = ++tot;
  for (int i = 1, val; i <= n; ++i) {
    scanf("%d", &val);
    link(i, tt, 1, 0);
    link(ss, i + n, 1, 0);
    link(s, i, INF, val);
    link(i + n, t, INF, 0);
  }
  for (int i = 1, u, v, w; i <= m; ++i) {
    scanf("%d%d%d", &u, &v, &w);
    if (u > v) std::swap(u, v);
    link(u + n, v, INF, w);
  }
  link(t, s, INF, 0);
  printf("%lld\n", MinCostFlow(ss, tt));
  return 0;
}
