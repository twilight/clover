#include <cstdio>
#include <cstdlib>
#include <algorithm>

const int N = 9;

#define pos(x, y) (N * (x) + (y))

int adj[N * N];
int to[N * N * N * N], next[N * N * N * N];

void link(int a, int b) {
  static int cnt = 2;
  to[cnt] = b;
  next[cnt] = adj[a];
  adj[a] = cnt++;
}

int ans[N * N];
bool cmp[N * N][N * N];

void readRow(int i) {
  for (int j = 0; j < N; j += 3) {
    char a, b;
    scanf(" %c %c", &a, &b);
    int x = pos(i, j), y = pos(i, j + 1), z = pos(i, j + 2);
    if (a == '>') cmp[x][y] = true; else cmp[y][x] = true;
    if (b == '>') cmp[y][z] = true; else cmp[z][y] = true;
  }
}

void readCol(int a, int b) {
  for (int j = 0; j < N; ++j) {
    char ch;
    scanf(" %c", &ch);
    int p = pos(a, j), q = pos(b, j);
    if (ch == '^') cmp[q][p] = true; else cmp[p][q] = true;
  }
}

int inf[N * N], sup[N * N];
int order[N * N], no[N * N];

inline bool Compare(int lhs, int rhs) { return inf[lhs] > inf[rhs]; }

void toposort() {
  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j)
      no[pos(i, j)] = i / 3 * 3 + j / 3;
  static int deg[N * N];
  for (int i = 0; i < N * N; ++i)
    for (int j = 0; j < N * N; ++j)
      if (cmp[i][j]) ++deg[j];
  for (int *qf = order, *qr = qf, it = 0; it < N; ++it) {
    for (int i = 0; i < N * N; ++i) if (!deg[i] && no[i] == it) *qr++ = i;
    while (qf < qr) {
      int a = *qf++;
      for (int b = 0; b < N * N; ++b) if (cmp[a][b] && --deg[b] == 0) *qr++ = b;
    }
  }
  for (int k = 0; k < N * N; ++k)
    for (int i = 0; i < N * N; ++i)
      for (int j = 0; j < N * N; ++j)
        if (cmp[i][k] && cmp[k][j]) cmp[i][j] = true;
  for (int i = 0; i < N * N; ++i)
    for (int j = 0; j < N * N; ++j)
      if (cmp[i][j]) link(i, j), ++inf[i];
}

int row[N], col[N], block[N];

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

void dfs(int dep) {
  if (dep == N * N) {
    for (int i = 0; i < N; ++i) {
      for (int j = 0; j < N; ++j) {
        if (j) putchar(' ');
        printf("%d", ans[pos(i, j)] + 1);
      }
      putchar('\n');
    }
    exit(0);
  }
  int p = order[dep], r = p / N, c = p % N, q = no[p];
  static int mem[N * N][N * N];
  for (int i = adj[p]; i; i = next[i]) {
    int j = to[i];
    mem[p][j] = sup[j];
  }
  for (int i = inf[p]; i < sup[p]; ++i) {
    int s = 1 << i;
    if (row[r] & s) continue;
    if (col[c] & s) continue;
    if (block[q] & s) continue;
    row[r] |= s, col[c] |= s, block[q] |= s;
    ans[p] = i;
    for (int e = adj[p]; e; e = next[e]) {
      int j = to[e];
      sup[j] = MIN(mem[p][j], i);
    }
    dfs(dep + 1);
    row[r] ^= s, col[c] ^= s, block[q] ^= s;
  }
  for (int i = adj[p]; i; i = next[i]) {
    int j = to[i];
    sup[j] = mem[p][j];
  }
}

int main() {
  for (int i = 0; i < N; i += 3) {
    readRow(i);
    readCol(i, i + 1);
    readRow(i + 1);
    readCol(i + 1, i + 2);
    readRow(i + 2);
  }
  toposort();
  for (int i = 0; i < N * N; ++i) sup[i] = N;
  dfs(0);
  return 0;
}
