#include <cstdio>
#include <cstring>
#include <algorithm>

const int N = 1000 + 10;

int n, m, map[N][N], f[N][N];

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= n; ++i)
      for (int j = 1; j <= m; ++j)
        scanf("%d", &map[i][j]);
    memset(f, 0, sizeof f);
    for (int i = n; i > 0; --i)
      for (int j = 1; j <= m; ++j)
        f[i][j] = std::max(f[i + 1][j - 1] + map[i][j], std::max(f[i + 1][j], f[i][j - 1]));
    printf("%d\n", f[1][m]);
  }
  return 0;
}
