#include <cstdio>
#include <vector>
#include <numeric>
#include <algorithm>

typedef long long i64;

const int N = 300000 + 10;

int n, m, type[N], c[N];
i64 h[N], v[N];

struct node_t {
  node_t *ch[2];
  i64 val, k, b;
  void apply(i64, i64);
  void release();
} pool[N], *heap[N];

inline void node_t::apply(i64 _k, i64 _b) {
  (val *= _k) += _b;
  k *= _k;
  b *= _k;
  b += _b;
}

inline void node_t::release() {
  if (k != 1 || b) {
    if (ch[0]) ch[0]->apply(k, b);
    if (ch[1]) ch[1]->apply(k, b);
    k = 1;
    b = 0;
  }
}

node_t *Merge(node_t *u, node_t *v) {
  if (!u) return v;
  if (!v) return u;
  u->release(), v->release();
  if (u->val > v->val) std::swap(u, v);
  u->ch[1] = Merge(u->ch[1], v);
  std::swap(u->ch[0], u->ch[1]);
  return u;
}

int dep[N];
std::vector<int> bfn, son[N];

void BFS(int s) {
  bfn.push_back(s);
  dep[s] = 1;
  for (size_t i = 0; i < bfn.size(); ++i) {
    int a = bfn[i];
    for (std::vector<int>::iterator it = son[a].begin(); it != son[a].end(); ++it) {
      int b = *it;
      dep[b] = dep[a] + 1;
      bfn.push_back(b);
    }
  }
}

int cnt[N], die[N];

void Solve() {
  for (int i = bfn.size() - 1; i >= 0; --i) {
    int a = bfn[i];
    for (std::vector<int>::iterator it = son[a].begin(); it != son[a].end(); ++it) heap[a] = Merge(heap[a], heap[*it]);
    while (heap[a] && heap[a]->val < h[a]) {
      ++cnt[a];
      die[heap[a] - pool] = a;
      heap[a]->release();
      heap[a] = Merge(heap[a]->ch[0], heap[a]->ch[1]);
    }
    if (heap[a]) {
      if (type[a] == 0) heap[a]->apply(1, v[a]); else heap[a]->apply(v[a], 0);
    }
  }
}

int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; ++i) scanf("%lld", h + i);
  for (int i = 2, fa; i <= n; ++i) {
    scanf("%d%d%lld", &fa, type + i, v + i);
    son[fa].push_back(i);
  }
  for (int i = 1; i <= m; ++i) {
    pool[i].k = 1;
    scanf("%lld%d", &pool[i].val, c + i);
    heap[c[i]] = Merge(heap[c[i]], pool + i);
  }
  BFS(1);
  Solve();
  for (int i = 1; i <= n; ++i) printf("%d\n", cnt[i]);
  for (int i = 1; i <= m; ++i) printf("%d\n", dep[c[i]] - dep[die[i]]);
  return 0;
}
