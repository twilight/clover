#include <cstdio>
#include <algorithm>

typedef long long int64;

const int N = 50000 + 10;

int a, b, c, d, k;
int miu[N], sum[N];

void preprocessing() {
  static int prime[N], cnt;
  static bool flag[N];
  miu[1] = 1;
  for (int i = 2; i < N; ++i) {
    if (!flag[i]) {
      miu[i] = -1;
      prime[++cnt] = i;
    }
    for (int j = 1; j <= cnt && i * prime[j] < N; ++j) {
      flag[i * prime[j]] = true;
      if (i % prime[j]) {
        miu[i * prime[j]] = -miu[i];
      } else {
        miu[i * prime[j]] = 0;
        break;
      }
    }
  }
  for (int i = 1; i < N; ++i) sum[i] = sum[i - 1] + miu[i];
}

int64 cal(int a, int b, int k) {
  int64 res = 0;
  a /= k, b /= k;
  if (a > b) std::swap(a, b);
  for (int i = 1, last; i <= a; i = last + 1) {
    last = std::min(a / (a / i), b / (b / i));
    res += (int64)(a / i) * (b / i) * (sum[last] - sum[i - 1]);
  }
  return res;
}

int main() {
  preprocessing();
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
      scanf("%d%d%d%d%d", &a, &b, &c, &d, &k);
      printf("%lld\n", cal(b, d, k)
                       - cal(a - 1, d, k) - cal(b, c - 1, k)
                       + cal(a - 1, c - 1, k));
  }
  return 0;
}
