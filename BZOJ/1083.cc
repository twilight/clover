/**************************************************************
    Problem: 1083
    User: dennisyang
    Language: C++
    Result: Accepted
    Time:32 ms
    Memory:1932 kb
****************************************************************/
 
#include <cstdio>
#include <algorithm>
 
const int maxn = 310, maxm = maxn * maxn;
 
struct edge {
    int s,t,w;
} epool[maxm];
 
int n,m,f[maxn];
 
bool cmp(const edge &p, const edge &q) { return p.w < q.w; }
 
int find(int x) {
    return f[x] == x ? x : f[x] = find(f[x]);
}
 
int main() {
    std::scanf("%d%d",&n,&m);
    for (int i = 1; i <= m; ++i)
        std::scanf("%d%d%d",&epool[i].s,&epool[i].t,&epool[i].w);
    std::sort(epool + 1, epool + 1 + m, cmp);
    for (int i = 1; i <= n; ++i) f[i] = i;
    int cnt = 0;
    for (int i = 1; i <= m; ++i) {
        if (find(epool[i].s) != find(epool[i].t)) {
            ++cnt;
            f[find(epool[i].s)] = find(epool[i].t);
            if (cnt == n - 1) {
                std::printf("%d %d\n",cnt,epool[i].w);
                break;
            }
        }
    }
    return 0;
}
