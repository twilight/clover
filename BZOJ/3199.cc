#include <bits/stdc++.h>

#define x real()
#define y imag()
#define fst first
#define snd second

typedef std::complex<double> point;
typedef std::pair<point, point> line;

const int N = 600 + 10;
const double INF = 1e20;

int n;
point bound, st, monitor[N];

std::vector<point> convex[N];

std::vector<int> e[N];

inline line pb(const point &a, const point &b) {
  point c = (a + b) / 2.0, s = b - a;
  std::swap(s.x, s.y);
  s.x *= -1;
  return line(c, c + s);
}

inline double cross(const point &a, const point &b) {
  return a.x * b.y - a.y * b.x;
}

inline double cross(const point &o, const point &a, const point &b) {
  return (a.x - o.x) * (b.y - o.y) - (b.x - o.x) * (a.y - o.y);
}

inline point intersection(const line &u, const line &v) {
  point a = u.snd - u.fst, b = v.snd - v.fst, s = v.fst - u.fst;
  return u.fst + a * cross(s, b) / cross(a, b);
}

int order[N];
std::vector<line> halfplane;
std::vector<double> angle;

inline bool cmp(int a, int b) { return angle[a] < angle[b]; }

void VoronoiDiagram(int id) {
  halfplane.clear();
  angle.clear();
  halfplane.push_back(line(point(0, 0), point(bound.x, 0)));
  halfplane.push_back(line(point(bound.x, 0), bound));
  halfplane.push_back(line(bound, point(0, bound.y)));
  halfplane.push_back(line(point(0, bound.y), point(0, 0)));
  for (int i = 1; i <= n; ++i) {
    if (i == id) continue;
    halfplane.push_back(pb(monitor[id], monitor[i]));
  }
  for (int i = 0; i < halfplane.size(); ++i) {
    angle.push_back(std::arg(halfplane[i].snd - halfplane[i].fst));
    order[i] = i;
  }
  std::sort(order, order + halfplane.size(), cmp);
  std::deque<int> q;
  for (int it = 0; it < halfplane.size(); ++it) {
    int i = order[it];
    const line &cur = halfplane[i];
    if (!q.empty() && angle[q.back()] == angle[i]) {
      if (cross(halfplane[q.back()].fst, halfplane[q.back()].snd, cur.fst) <= 0) continue;
      if (q.size() == 1) {
        q.back() = i;
        continue;
      }
    }
    while (q.size() > 1 && cross(cur.fst, cur.snd, intersection(halfplane[q.back()], halfplane[q[q.size() - 2]])) <= 0) q.pop_back();
    while (q.size() > 1 && cross(cur.fst, cur.snd, intersection(halfplane[q[0]], halfplane[q[1]])) <= 0) q.pop_front();
    q.push_back(i);
  }
  while (q.size() > 2 && cross(halfplane[q[0]].fst, halfplane[q[0]].snd, intersection(halfplane[q.back()], halfplane[q[q.size() - 2]])) <= 0) q.pop_back();
  convex[id].clear();
  e[id].clear();
  if (q.size() <= 2) return;
  q.push_back(q[0]);
  for (int i = 0; i + 1 < q.size(); ++i) {
    if (q[i] > 3) e[id].push_back(q[i] - 3 + (q[i] - 3 >= id)); else e[id].push_back(n + 1);
    convex[id].push_back(intersection(halfplane[q[i]], halfplane[q[i + 1]]));
  }
}

inline bool intersect(const line &a, const line &b) {
  return
    cross(a.fst, a.snd, b.fst) * cross(a.fst, a.snd, b.snd) < 0.0 &&
    cross(b.fst, b.snd, a.fst) * cross(b.fst, b.snd, a.snd) < 0.0;
}

inline bool Inside(const point &a, const std::vector<point> pool) {
  int cnt = 0;
  double deg = (double)rand() / RAND_MAX * 2.0 * M_PI;
  point b(a + INF * point(cos(deg), sin(deg)));
  for (int i = 0; i + 1 < pool.size(); ++i) cnt += intersect(line(pool[i], pool[i + 1]), line(a, b));
  cnt += intersect(line(pool[0], pool.back()), line(a, b));
  return cnt & 1;
}

inline int BFS(int s, int t) {
  std::queue<int> q;
  static int dist[N];
  std::fill(dist + 1, dist + n + 2, INT_MAX);
  for (q.push(s), dist[s] = 0; !q.empty(); q.pop()) {
    int a = q.front();
    for (std::vector<int>::iterator it = e[a].begin(); it != e[a].end(); ++it) {
      int b = *it;
      if (dist[a] + 1 < dist[b]) {
        q.push(b);
        dist[b] = dist[a] + 1;
      }
      if (b == n + 1) return dist[b];
    }
  }
  return 0;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d", &n);
    scanf("%lf%lf%lf%lf", &bound.x, &bound.y, &st.x, &st.y);
    for (int i = 1; i <= n; ++i) scanf("%lf%lf", &monitor[i].x, &monitor[i].y);
    int s = 0;
    for (int i = 1; i <= n; ++i) {
      VoronoiDiagram(i);
      if (Inside(st, convex[i])) s = i;
    }
    printf("%d\n", n ? BFS(s, n + 1) : 0);
  }
  return 0;
}
