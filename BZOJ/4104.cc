#include <cstdio>
#include <cstring>

const int N = 200000 + 10;

int n, m, t[N], s[N];

int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n + 1; ++i) scanf("%d", t + i);
  static int cnt[N], cur[N], map[N];
  for (int i = 1; i <= n + 1; ++i) ++cnt[t[i]];
  for (int i = 1; i <= m; ++i) cnt[i] += cnt[i - 1];
  for (int i = 1; i <= m; ++i) cur[i] = cnt[i - 1];
  memset(cnt, 0, sizeof cnt);
  for (int i = 1; i <= n + 1; ++i) map[i] = ++cnt[t[i]];
  for (int i = n, j = 1; i > 0; --i, j = cur[t[j]] + map[j]) s[i] = t[j];
  for (int i = 1; i <= n; ++i) printf("%d ", s[i]);
  putchar('\n');
  return 0;
}
