#include <cstdio>
#include <cstring>
#include <queue>

typedef std::pair<int, int> Info;

const int N = 1000 + 10, M = 10000 + 10, INF = 0x3f3f3f3f;
const int V = N, E = 10 * M;

int n, m;

int adj[V];
int to[E], next[E], cap[E], cost[E], cnt;

namespace Graph {

int s, t;

inline void init() {
  cnt = 2;
  s = n + 1, t = n + 2;
  memset(adj, 0, sizeof adj);
}

inline void link(int a, int b, int c, int d) {
  to[cnt] = b, next[cnt] = adj[a], cap[cnt] = c, cost[cnt] = d, adj[a] = cnt++;
  to[cnt] = a, next[cnt] = adj[b], cap[cnt] = 0, cost[cnt] = -d, adj[b] = cnt++;
}

int dist[V], cur = 0;

bool relabel() {
  static Info heap[10 * E];
  std::fill(dist, dist + t + 1, -INF);
  int top = 0;
  for (heap[++top] = Info(dist[t] = 0, t); top;) {
    std::pop_heap(heap + 1, heap + top + 1);
    Info info = heap[top--];
    int a = info.second;
    if (info.first < dist[a]) continue;
    for (int i = adj[a]; i; i = next[i]) {
      int b = to[i], c = cost[i ^ 1];
      if (cap[i ^ 1] && dist[a] + c > dist[b]) {
        heap[++top] = Info(dist[b] = dist[a] + c, b);
        std::push_heap(heap + 1, heap + top + 1);
      }
    }
  }
  if (dist[s] == -INF) return false;
  cur += dist[s];
  for (int a = 0; a <= t; ++a)
    for (int i = adj[a]; i; i = next[i])
      cost[i] += dist[to[i]] - dist[a];
  return true;
}

int tag[V], tot;

int dfs(int a, int df) {
  if (a == t) return df;
  tag[a] = tot;
  int res = 0;
  for (int i = adj[a]; i; i = next[i]) {
    int b = to[i];
    if (tag[b] != tot && cap[i] && !cost[i]) {
      int f = dfs(b, std::min(df - res, cap[i]));
      cap[i] -= f;
      cap[i ^ 1] += f;
      res += f;
    }
    if (df == res) break;
  }
  return res;
}

int flow() {
  int res = 0;
  while (relabel()) {
    do {
      ++tot;
      int f = dfs(s, INF);
      if (!f) break;
      res += f * cur;
    } while (1);
  }
  return res;
}

}

int main() {
  scanf("%d%d", &n, &m);
  Graph::init();
  static int c[N];
  for (int i = 1; i <= n; ++i) scanf("%d", c + i);
  c[0] = 0;
  for (int i = 0; i <= n; ++i) {
    int d = c[i] - c[i + 1];
    if (d > 0) Graph::link(i, Graph::t, d, 0); else Graph::link(Graph::s, i, -d, 0);
  }
  for (int i = 0; i < n; ++i) Graph::link(i, i + 1, INF, 0);
  while (m--) {
    int l, r, d;
    scanf("%d%d%d", &l, &r, &d);
    --l;
    Graph::link(l, r, INF, d);
  }
  printf("%d\n", Graph::flow());
  return 0;
}
