#include <cstdio>
#include <cstring>

typedef unsigned long long qword;
typedef qword matrix[2][2];

const matrix e = {
  {1, 0},
  {0, 1}
};

qword m, a, c, x, n, g;

inline void INC(qword &a, qword b) { a = (a + b) % m; }

qword mul(qword a, qword b) {
  qword res = 0, tmp = a;
  while (b) {
    if (b & 1) INC(res, tmp);
    INC(tmp, tmp);
    b >>= 1;
  }
  return res;
}

void mul(matrix a, matrix b) {
  static matrix tmp;
  memset(tmp, 0, sizeof(matrix));
  for (int i = 0; i < 2; ++i)
    for (int j = 0; j < 2; ++j)
      for (int k = 0; k < 2; ++k)
        INC(tmp[i][j], mul(a[i][k], b[k][j]));
  memcpy(a, tmp, sizeof(matrix));
}

void pow(matrix a, qword exp) {
  matrix base, res;
  memcpy(res, e, sizeof(matrix));
  memcpy(base, a, sizeof(matrix));
  while (exp) {
    if (exp & 1) mul(res, base);
    mul(base, base);
    exp >>= 1;
  }
  memcpy(a, res, sizeof(matrix));
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%llu%llu%llu%llu%llu%llu", &m, &a, &c, &x, &n, &g);
  matrix mat = {
    {a, 0},
    {c, 1}
  };
  pow(mat, n);
  qword ans = mul(mat[0][0], x);
  INC(ans, mat[1][0]);
  printf("%llu\n", ans % g);
  return 0;
}
