#include <cstdio>
#include <algorithm>

typedef long long int64;

const int N = 10000000 + 10;

int n;
int miu[N], sum[N];

void preprocessing() {
  static int prime[N], cnt;
  static bool flag[N];
  miu[1] = 1;
  for (int i = 2; i <= n; ++i) {
    if (!flag[i]) {
      prime[++cnt] = i;
      miu[i] = -1;
    }
    for (int j = 1; j <= cnt && i * prime[j] <= n; ++j) {
      flag[i * prime[j]] = true;
      if (i % prime[j]) {
        miu[i * prime[j]] = -miu[i];
      } else {
        miu[i * prime[j]] = 0;
        break;
      }
    }
  }
  for (int i = 1; i <= n; ++i) {
    if (!miu[i]) continue;
    for (int j = 1; j <= cnt && i * prime[j] <= n; ++j)
      sum[i * prime[j]] += miu[i];
  }
  for (int i = 1; i <= n; ++i) sum[i] += sum[i - 1];
}

int64 cal(int n) {
  int64 res = 0;
  for (int i = 1, last; i <= n; i = last + 1) {
    last = n / (n / i);
    res += (int64)(n / i) * (n / i) * (sum[last] - sum[i - 1]);
  }
  return res;
}

int main() {
  scanf("%d", &n);
  preprocessing();
  printf("%lld\n", cal(n));
  return 0;
}
