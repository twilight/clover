#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <cstring>

typedef long long i64;

const int N = 300000 + 10, MOD = 1000000007;

int n, m, alpha, beta, inv, a[5][N];

int pdt[3][3][N * 2], sum[3][2][N * 2], tag[2][N * 2];

#define pos(l, r) (((l) + (r)) | ((l) != (r)))

inline void Inc(int id, int len, int p) {
  for (int i = 0; i < 2; ++i) sum[i][p][id] = sum[i + 1][p][id];
  sum[2][p][id] = (sum[1][p][id] + (i64)alpha * sum[0][p][id] % MOD + (i64)len * beta % MOD) % MOD;
  if (p) {
    for (int i = 0; i < 3; ++i)
      for (int j = 0; j < 2; ++j)
        pdt[i][j][id] = pdt[i][j + 1][id];
    for (int i = 0; i < 3; ++i)
      pdt[i][2][id] = (pdt[i][1][id] + (i64)alpha * pdt[i][0][id] % MOD + (i64)beta * sum[i][0][id] % MOD) % MOD;
  } else {
    for (int i = 0; i < 3; ++i)
      for (int j = 0; j < 2; ++j)
        pdt[j][i][id] = pdt[j + 1][i][id];
    for (int i = 0; i < 3; ++i)
      pdt[2][i][id] = (pdt[1][i][id] + (i64)alpha * pdt[0][i][id] % MOD + (i64)beta * sum[i][1][id] % MOD) % MOD;
  }
}

inline void Dec(int id, int len, int p) {
  for (int i = 2; i > 0; --i) sum[i][p][id] = sum[i - 1][p][id];
  if (alpha) {
    sum[0][p][id] = ((sum[2][p][id] - sum[1][p][id] - (i64)beta * len % MOD) % MOD + MOD) % MOD * inv % MOD;
    if (p) {
      for (int i = 0; i < 3; ++i)
        for (int j = 2; j > 0; --j)
          pdt[i][j][id] = pdt[i][j - 1][id];
      for (int i = 0; i < 3; ++i) pdt[i][0][id] = ((pdt[i][2][id] - pdt[i][1][id] - (i64)beta * sum[i][0][id] % MOD) % MOD + MOD) % MOD * inv % MOD;
    } else {
      for (int i = 0; i < 3; ++i)
        for (int j = 2; j > 0; --j)
          pdt[j][i][id] = pdt[j - 1][i][id];
      for (int i = 0; i < 3; ++i) pdt[0][i][id] = ((pdt[2][i][id] - pdt[1][i][id] - (i64)beta * sum[i][1][id] % MOD) % MOD + MOD) % MOD * inv % MOD;
    }
  } else {
    sum[0][p][id] = (sum[1][p][id] - (i64)beta * len % MOD + MOD) % MOD;
    if (p) {
      for (int i = 0; i < 3; ++i)
        for (int j = 2; j > 0; --j)
          pdt[i][j][id] = pdt[i][j - 1][id];
      for (int i = 0; i < 3; ++i) pdt[i][0][id] = (pdt[i][1][id] - (i64)beta * sum[i][0][id] % MOD + MOD) % MOD;
    } else {
      for (int i = 0; i < 3; ++i)
        for (int j = 2; j > 0; --j)
          pdt[j][i][id] = pdt[j - 1][i][id];
      for (int i = 0; i < 3; ++i) pdt[0][i][id] = (pdt[1][i][id] - (i64)beta * sum[i][1][id] % MOD + MOD) % MOD;
    }
  }
}

inline void Update(int id, int lch, int rch) {
  for (int i = 0; i < 3; ++i)
    for (int j = 0; j < 3; ++j)
      pdt[i][j][id] = (pdt[i][j][lch] + pdt[i][j][rch]) % MOD;
  for (int i = 0; i < 3; ++i)
    for (int j = 0; j < 2; ++j)
      sum[i][j][id] = (sum[i][j][lch] + sum[i][j][rch]) % MOD;
}

void Shift(int l, int r, int p, int q, int u, int v) {
  int id = pos(l, r);
  if (p <= l && r <= q) {
    (v > 0 ? Inc : Dec)(id, r - l + 1, u);
    tag[u][id] += v;
    return;
  }
  int mid = (l + r) / 2, lch = pos(l, mid), rch = pos(mid + 1, r);
  for (int i = 0; i < 2; ++i) {
    if (tag[i][id] > 0) {
      for (int cnt = abs(tag[i][id]); cnt--;) {
        Inc(lch, mid - l + 1, i);
        Inc(rch, r - mid, i);
      }
    } else {
      for (int cnt = abs(tag[i][id]); cnt--;) {
        Dec(lch, mid - l + 1, i);
        Dec(rch, r - mid, i);
      }
    }
    tag[i][lch] += tag[i][id];
    tag[i][rch] += tag[i][id];
    tag[i][id] = 0;
  }
  if (p <= mid) Shift(l, mid, p, q, u, v);
  if (q > mid) Shift(mid + 1, r, p, q, u, v);
  Update(id, lch, rch);
}

int Query(int l, int r, int p, int q) {
  int id = pos(l, r);
  if (p <= l && r <= q) return pdt[1][1][id];
  int mid = (l + r) / 2, lch = pos(l, mid), rch = pos(mid + 1, r);
  for (int i = 0; i < 2; ++i) {
    if (tag[i][id] > 0) {
      for (int cnt = abs(tag[i][id]); cnt--;) {
        Inc(lch, mid - l + 1, i);
        Inc(rch, r - mid, i);
      }
    } else {
      for (int cnt = abs(tag[i][id]); cnt--;) {
        Dec(lch, mid - l + 1, i);
        Dec(rch, r - mid, i);
      }
    }
    tag[i][lch] += tag[i][id];
    tag[i][rch] += tag[i][id];
    tag[i][id] = 0;
  }
  int res = 0;
  if (p <= mid) res += Query(l, mid, p, q);
  if (q > mid) (res += Query(mid + 1, r, p, q)) %= MOD;
  Update(id, lch, rch);
  return res;
}

inline void Modify(int l, int r, int det) {
  Shift(1, n, l + 1, r + 1, 0, det);
  Shift(1, n, l - 1, r - 1, 1, det);
}

namespace Matrix {

const int k = 3;

typedef int Matrix[k][k];

void Mul(Matrix a, const Matrix b) {
  static Matrix c;
  memset(c, 0, sizeof c);
  for (register int i = 0; i < k; ++i)
    for (register int w = 0; w < k; ++w)
      for (register int j = 0; j < 2; ++j)
        c[i][j] = (c[i][j] + (i64)a[i][w] * b[w][j] % MOD) % MOD;
  memcpy(a, c, sizeof c);
  a[2][2] = 1;
}

Matrix trans[32];

inline void Init() {
  const Matrix orig = {
    {1, 1, 0},
    {alpha, 0, 0},
    {beta, 0, 1}
  };
  memcpy(trans[0], orig, sizeof orig);
  for (int i = 1; i < 32; ++i) {
    memcpy(trans[i], trans[i - 1], sizeof trans[i]);
    Mul(trans[i], trans[i]);
  }
}

void Power(Matrix res, int exp) {
  memset(res, 0, sizeof(Matrix));
  for (int i = 0; i < k; ++i) res[i][i] = 1;
  for (int i = 0; (1ULL << i) < exp; ++i) if (exp >> i & 1) Mul(res, trans[i]);
}

}

void Get(int &a, int &b, int x) {
  if (x == 1) {
    a = 1, b = 2;
    return;
  }
  if (x == 2) {
    a = 2, b = (2LL + alpha + beta) % MOD;
    return;
  }
  static Matrix::Matrix mat;
  Matrix::Power(mat, x - 2);
  a = (2LL * mat[0][0] % MOD + mat[1][0] % MOD + mat[2][0]) % MOD;
  Matrix::Mul(mat, Matrix::trans[0]);
  b = (2LL * mat[0][0] % MOD + mat[1][0] % MOD + mat[2][0]) % MOD;
}

inline void Build(int l, int r) {
  int id = pos(l, r);
  if (l == r) {
    if (l == 1 || l == n) return;
    for (int i = 0; i < 3; ++i) {
      sum[i][0][id] = a[i + 2][l - 1];
      sum[i][1][id] = a[i][r + 1];
    }
    for (int i = 0; i < 3; ++i)
      for (int j = 0; j < 3; ++j)
        pdt[i][j][id] = (i64)sum[i][0][id] * sum[j][1][id] % MOD;
    return;
  }
  int mid = (l + r) / 2;
  Build(l, mid);
  Build(mid + 1, r);
  Update(id, pos(l, mid), pos(mid + 1, r));
}

inline int Calc(int u, int v) { return (u + (i64)alpha * v % MOD + beta) % MOD; }

inline int Read() {
  static char ch;
  while (!isdigit(ch = getchar())) {}
  int res = ch - '0';
  while (isdigit(ch = getchar())) res = res * 10 + ch - '0';
  return res;
}

int main() {
  n = Read(), m = Read(), alpha = Read(), beta = Read();
  Matrix::Init();
  if (alpha) {
    inv = 1;
    int base = alpha;
    for (int exp = MOD - 2; exp; exp >>= 1) {
      if (exp & 1) inv = (i64)inv * base % MOD;
      base = (i64)base * base % MOD;
    }
  }
  for (int i = 1; i <= n; ++i) {
    Get(a[0][i], a[1][i], Read() - 2);
    for (int j = 2; j < 5; ++j) a[j][i] = Calc(a[j - 1][i], a[j - 2][i]);
  }
  Build(1, n);
  while (m--) {
    char op = getchar();
    while (!islower(op)) op = getchar();
    int l = Read(), r = Read();
    switch (op) {
      case 'p':
        Modify(l, r, 1);
        break;
      case 'm':
        Modify(l, r, -1);
        break;
      case 'q':
        printf("%d\n", r - l < 2 ? 0 : Query(1, n, l + 1, r - 1));
        break;
    }
  }
  return 0;
}
