#include <cstdio>
#include <algorithm>

typedef long long int64;

int n, k;

int main() {
  scanf("%d%d", &n, &k);
  int64 ans = 0;
  if (n > k) {
    ans += (int64)(n - k) * k;
    n = k;
  }
  for (int l = 1, r; l <= n; l = r + 1) {
    r = std::min(k / (k / l), n);
    ans += (int64)(r - l + 1) * k;
    ans -= (int64)(k / l) * (l + r) * (r - l + 1) / 2;
  }
  printf("%lld\n", ans);
  return 0;
}
