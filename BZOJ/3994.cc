#include <cstdio>
#include <vector>

typedef long long i64;

const int N = 50000 + 10;

int n, m;

int miu[N], sum[N];
i64 f[N];

void Preprocessing() {
  static std::vector<int> prime;
  static bool flag[N];
  static int cnt[N];
  miu[1] = f[1] = 1;
  for (int i = 2; i < N; ++i) {
    if (!flag[i]) {
      f[i] = 2;
      miu[i] = -1;
      cnt[i] = 1;
      prime.push_back(i);
    }
    for (size_t j = 0; j < prime.size() && i * prime[j] < N; ++j) {
      flag[i * prime[j]] = true;
      if (i % prime[j] == 0) {
        miu[i * prime[j]] = 0;
        cnt[i * prime[j]] = cnt[i] + 1;
        f[i * prime[j]] = f[i] / (cnt[i] + 1) * (cnt[i * prime[j]] + 1);
        break;
      }
      miu[i * prime[j]] = -miu[i];
      cnt[i * prime[j]] = 1;
      f[i * prime[j]] = f[i] * 2;
    }
  }
  for (int i = 1; i < N; ++i) f[i] += f[i - 1], sum[i] = sum[i - 1] + miu[i];
}

int main() {
  Preprocessing();
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d", &n, &m);
    if (n > m) std::swap(n, m);
    i64 ans = 0;
    for (int l = 1, r; l <= n; l = r + 1) {
      r = std::min(n / (n / l), m / (m / l));
      ans += (sum[r] - sum[l - 1]) * f[n / l] * f[m / l];
    }
    printf("%lld\n", ans);
  }
  return 0;
}
