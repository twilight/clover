#include <cstdio>
#include <cctype>
#include <cstring>
#include <climits>
#include <algorithm>

const int N = 100 + 10, V = N * N, E = V * 50;

inline int RD() {
  int res = 0;
  static char ch;
  for (ch = getchar(); !isdigit(ch); ch = getchar()) {}
  res = ch - '0';
  for (ch = getchar(); isdigit(ch); ch = getchar()) res = res * 10 + ch - '0';
  return res;
}

int n, m, s, t;

int adj[V];
int to[E], next[E], cap[E];

void link(int a, int b, int c) {
  static int cnt = 0;
  to[cnt] = b;
  next[cnt] = adj[a];
  cap[cnt] = c;
  adj[a] = cnt++;
  to[cnt] = a;
  next[cnt] = adj[b];
  cap[cnt] = 0;
  adj[b] = cnt++;
}

inline int map(int x, int y) { return (x - 1) * m + y; }

int dis[V];

bool bfs() {
  static int q[V];
  int *qf, *qr;
  qf = qr = q;
  memset(dis, -1, sizeof dis);
  dis[*qr++ = s] = 0;
  while (qf < qr) {
    int a = *qf++;
    for (int it = adj[a]; it != -1; it = next[it]) {
      int b = to[it], c = cap[it];
      if (c && dis[b] == -1) dis[*qr++ = b] = dis[a] + 1;
    }
  }
  return ~dis[t];
}

int aug[V];

int dfs(int a, int df) {
  if (a == t) return df;
  int res = 0;
  for (int &it = aug[a]; it != -1; it = next[it]) {
    int b = to[it], c = cap[it];
    if (c && dis[b] == dis[a] + 1) {
      int f = dfs(b, std::min(c, df));
      res += f;
      cap[it] -= f, cap[it ^ 1] += f;
      df -= f;
      if (!df) break;
    }
  }
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  n = RD(), m = RD();
  s = 0, t = n * m + 1;
  int ans = 0;
  memset(adj, -1, sizeof adj);
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= m; ++j) {
      int x = RD();
      link(s, map(i, j), x *= 2);
      ans += x;
    }
  }
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= m; ++j) {
      int x = RD();
      link(map(i, j), t, x *= 2);
      ans += x;
    }
  }
  for (int i = 1; i < n; ++i) {
    for (int j = 1; j <= m; ++j) {
      int a = map(i, j), b = map(i + 1, j), x = RD();
      link(s, a, x);
      link(s, b, x);
      link(a, b, x);
      link(b, a, x);
      ans += x * 2;
    }
  }
  for (int i = 1; i < n; ++i) {
    for (int j = 1; j <= m; ++j) {
      int a = map(i, j), b = map(i + 1, j), x = RD();
      link(a, t, x);
      link(b, t, x);
      link(a, b, x);
      link(b, a, x);
      ans += x * 2;
    }
  }
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j < m; ++j) {
      int a = map(i, j), b = map(i, j + 1), x = RD();
      link(s, a, x);
      link(s, b, x);
      link(a, b, x);
      link(b, a, x);
      ans += x * 2;
    }
  }
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j < m; ++j) {
      int a = map(i, j), b = map(i, j + 1), x = RD();
      link(a, t, x);
      link(b, t, x);
      link(a, b, x);
      link(b, a, x);
      ans += x * 2;
    }
  }
  while (bfs()) {
    memcpy(aug, adj, sizeof aug);
    ans -= dfs(s, INT_MAX);
  }
  printf("%d\n", ans / 2);
  return 0;
}
