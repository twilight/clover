#include <cstdio>
#include <cassert>
#include <vector>
#include <map>
#include <algorithm>

const int N = 50000 + 10, E = N * 2;

int n, m;
int x[E], y[E], a[E], b[E];

struct node {
  node *p, *ch[2], *mx;
  int val;
  bool rev;
  node(int v = 0):val(v) {
    rev = false;
    p = ch[0] = ch[1] = NULL;
    mx = this;
  }
  inline bool d();
  inline bool isroot();
  inline void relax();
  inline void update();
};

inline bool node::d() { return this == p->ch[1]; }
inline bool node::isroot() {
  return (!this->p) || (this != p->ch[0] && this != p->ch[1]);
}

inline void node::relax() {
  if (!this) return;
  if (rev) {
    if (ch[0]) ch[0]->rev ^= 1;
    if (ch[1]) ch[1]->rev ^= 1;
    std::swap(ch[0], ch[1]);
    rev = 0;
  }
}

inline void node::update() {
  if (!this) return;
  mx = this;
  if (ch[0] && ch[0]->mx->val > mx->val) mx = ch[0]->mx;
  if (ch[1] && ch[1]->mx->val > mx->val) mx = ch[1]->mx;
}

inline void rotate(node *u) {
  node *v = u->p;
  v->relax(), u->relax();
  bool d = u->d();
  if (!v->isroot()) v->p->ch[v->d()] = u;
  u->p = v->p;
  if (u->ch[!d]) u->ch[!d]->p = v;
  v->ch[d] = u->ch[!d];
  u->ch[!d] = v;
  v->p = u;
  v->update(), u->update();
}

inline void splay(node *u) {
  u->relax();
  while (!u->isroot()) {
    if (u->p->isroot())
      rotate(u);
    else
      (u->d() == u->p->d()) ? (rotate(u->p), rotate(u)) : (rotate(u), rotate(u));
  }
  u->update();
}

inline node* expose(node *u) {
  node *v;
  for (v = NULL; u; u = u->p) {
    splay(u);
    u->ch[1] = v;
    (v = u)->update();
  }
  return v;
}

inline void evert(node *u) {
  expose(u)->rev ^= 1;
  splay(u);
}

inline void link(node *u, node *v) {
  evert(u);
  u->p = v;
  expose(u);
}

inline void cut(node *u, node *v) {
  evert(u);
  expose(v);
  splay(v);
  u->p = v->ch[0] = NULL;
  v->update(), u->update();
}

inline int query(node *u, node *v) {
  evert(u);
  expose(v);
  splay(v);
  return v->mx->val;
}

inline node* pred(node *u) {
  splay(u);
  for (u = u->ch[0]; u->ch[1]; u = u->ch[1]) {}
  return u;
}

inline node* succ(node *u) {
  splay(u);
  for (u = u->ch[1]; u->ch[0]; u = u->ch[0]) {}
  return u;
}

int f[N];
inline int find(int x) { return (f[x] == x) ? x : (f[x] = find(f[x])); }

bool cmp(int p, int q) { return a[p] < a[q]; }

int main() {
#ifndef ONLINE_JUDGE
  freopen("forest.in", "r", stdin);
  freopen("forest.out", "w", stdout);
#endif
  scanf("%d%d", &n, &m);
  static std::vector<int> e[N];
  int l = N, r = 0;
  for (int i = 1; i <= m; ++i) {
    scanf("%d%d%d%d", x + i, y + i, a + i, b + i);
    e[a[i]].push_back(i);
    r = std::max(r, a[i]);
    l = std::min(l, a[i]);
  }
  static node *p[N];
  static std::map<node*, int> id;
  for (int i = 1; i <= n; ++i) p[i] = new node, f[i] = i;
  unsigned int ans = -1;
  for (int i = l; i <= r; ++i) {
    for (std::vector<int>::iterator it = e[i].begin(); it != e[i].end(); ++it) {
      int a = x[*it], b = y[*it], val = ::b[*it];
      node *u = p[a], *v = p[b];
      if (find(a) != find(b)) {
        node *e = new node(val);
        id[e] = *it;
        link(u, e);
        link(v, e);
        f[find(a)] = find(b);
      } else {
        evert(u);
        expose(v);
        splay(v);
        if (v->mx->val > val) {
          node *e = v->mx;
          splay(e);
          node *_u = p[x[id[e]]], *_v = p[y[id[e]]];
          cut(_u, e);
          cut(_v, e);
          delete e;
          e = new node(val);
          id[e] = *it;
          link(u, e);
          link(v, e);
        }
      }
    }
    if (find(1) == find(n)) ans = std::min(ans, (unsigned int)i + query(p[1], p[n]));
  }
  printf("%d\n", (int)ans);
  return 0;
}
