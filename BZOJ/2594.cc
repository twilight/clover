#include <cstdio>
#include <cctype>
#include <map>
#include <vector>
#include <algorithm>

const int N = 100000 + 10, E = N * 10;

int n, m, tcase;
int op[N], a[N], b[N];

struct edge {
  int x, y, t;
  edge() {}
  edge(int _x, int _y, int _t): x(_x), y(_y), t(_t) {}
};

edge epool[E];
bool flag[E];

inline bool operator< (const edge &a, const edge &b) {
  return a.x < b.x || (a.x == b.x && a.y < b.y);
}

struct node {
  node *p, *ch[2], *mx;
  int val;
  bool rev;
  node(int v = 0): val(v), rev(false) {
    p = ch[0] = ch[1] = NULL;
    mx = this;
  }
  inline bool d() { return this == p->ch[1]; }
  inline bool isroot() {
    if (!this) return true;
    return !p || (this != p->ch[0] && this != p->ch[1]);
  }
  inline void relax();
  inline void update();
};

node buffer[N * 2], *top;

inline void node::relax() {
  if (!this) return;
  if (rev) {
    if (ch[0]) ch[0]->rev ^= 1;
    if (ch[1]) ch[1]->rev ^= 1;
    std::swap(ch[0], ch[1]);
    rev = false;
  }
}

inline void node::update() {
  if (!this) return;
  mx = this;
  if (ch[0] && ch[0]->mx->val > mx->val) mx = ch[0]->mx;
  if (ch[1] && ch[1]->mx->val > mx->val) mx = ch[1]->mx;
}

inline void rotate(node *u) {
  node *v = u->p;
  v->relax(), u->relax();
  bool d = u->d();
  if (!v->isroot()) v->p->ch[v->d()] = u;
  u->p = v->p;
  v->ch[d] = u->ch[!d];
  if (u->ch[!d]) u->ch[!d]->p = v;
  u->ch[!d] = v;
  v->p = u;
  v->update(), u->update();
}

inline void splay(node *u) {
  u->relax();
  while (!u->isroot()) {
    if (u->p->isroot())
      rotate(u);
    else
      (u->d() == u->p->d()) ? (rotate(u->p), rotate(u)) : (rotate(u), rotate(u));
  }
  u->update();
}

inline node* expose(node *u) {
  node *v;
  for (v = NULL; u; u = u->p) {
    splay(u);
    u->ch[1] = v;
    (v = u)->update();
  }
  return v;
}

inline void evert(node *u) {
  expose(u)->rev ^= 1;
  splay(u);
}

inline void link(node *u, node *v) {
  evert(v);
  v->p = u;
  expose(v);
}

inline void cut(node *u, node *v) {
  evert(u);
  expose(v);
  splay(v);
  v->ch[0] = u->p = NULL;
  u->update(), v->update();
}

inline int query(node *u, node *v) {
  evert(u);
  expose(v);
  splay(u);
  return u->mx->val;
}

int f[N];
inline int find(int x) { return f[x] == x ? x : (f[x] = find(f[x])); }

std::map<node*, int> id;

inline void newedge(int i) {
  int a = epool[i].x, b = epool[i].y, c = epool[i].t;
  node *u = buffer + a, *v = buffer + b;
  if (find(a) != find(b)) {
    node *e = ++top;
    e->val = c;
    id[e] = i;
    link(u, e);
    link(v, e);
    f[find(a)] = find(b);
  } else {
    evert(u);
    expose(v);
    splay(u);
    if (u->mx->val > c) {
      node *e = u->mx;
      id[e] = i;
      node *_u = buffer + epool[id[e]].x, *_v = buffer + epool[id[e]].y;
      cut(_u, e);
      cut(_v, e);
      e->val = c;
      link(u, e);
      link(v, e);
    }
  }
}

inline int RD() {
  int res = 0;
  char ch;
  do ch = getchar();
  while (!isdigit(ch));
  res = ch - '0';
  while (isdigit(ch = getchar())) res = res * 10 + ch - '0';
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  n = RD(), m = RD(), tcase = RD();
  for (int i = 1; i <= m; ++i) {
    epool[i].x = RD();
    epool[i].y = RD();
    epool[i].t = RD();
  }
  std::sort(epool + 1, epool + 1 + m);
  static bool flag[E];
  static int no[N];
  for (int i = 1; i <= tcase; ++i) {
    op[i] = RD(), a[i] = RD(), b[i] = RD();
    if (op[i] == 2) {
      edge tmp(a[i], b[i], 0);
      flag[no[i] = std::lower_bound(epool + 1, epool + 1 + m, tmp) - epool] = true;
    }
  }
  for (int i = 1; i <= n; ++i) f[i] = i;
  top = buffer + n;
  for (int i = 1; i <= m; ++i) if (!flag[i]) newedge(i);
  static std::vector<int> ans;
  for (int i = tcase; i > 0; --i) {
    switch (op[i]) {
      case 1:
        ans.push_back(query(buffer + a[i], buffer + b[i]));
        break;
      case 2:
        newedge(no[i]);
        flag[no[i]] = false;
        break;
    }
  }
  for (std::vector<int>::reverse_iterator it = ans.rbegin(); it != ans.rend(); ++it) printf("%d\n", *it);
  return 0;
}
