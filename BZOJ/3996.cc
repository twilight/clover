#include <cstdio>
#include <algorithm>

typedef long long i64;

const int N = 500 + 10;
const i64 INF = 1LL << 50;

int n, s, t, tot, b[N][N], c[N];
i64 cap[N][N];

int h[N];

bool BFS(int s, int t) {
  static int q[N], *qf, *qr;
  qf = qr = q;
  std::fill(h, h + tot + 1, -1);
  for (h[*qr++ = s] = 0; qf < qr;) {
    int a = *qf++;
    for (int b = 0; b <= tot; ++b)
      if (cap[a][b] && h[b] == -1)
        h[*qr++ = b] = h[a] + 1;
  }
  return ~h[t];
}

i64 DFS(int a, i64 df) {
  if (a == t) return df;
  i64 res = 0;
  for (int b = 0; b <= tot; ++b) {
    if (h[a] + 1 == h[b]) {
      i64 f = DFS(b, std::min(df - res, cap[a][b]));
      cap[a][b] -= f;
      cap[b][a] += f;
      res += f;
    }
    if (res == df) break;
  }
  return res;
}

i64 Dinic(int s, int t) {
  i64 res = 0;
  while (BFS(s, t))
    for (i64 flow; (flow = DFS(s, INF));) res += flow;
  return res;
}

i64 Statistic() {
  static bool flag[N];
  static int q[N], *qf, *qr;
  qf = qr = q;
  for (flag[*qr++ = s] = true; qf < qr;) {
    int a = *qf++;
    for (int b = 0; b <= tot; ++b)
      if (cap[a][b] && !flag[b]) flag[*qr++ = b] = true;
  }
  i64 res = 0;
  for (int i = 1; i <= n; ++i) if (flag[i]) res -= c[i];
  for (int i = 1; i <= n; ++i)
    for (int j = 1; j <= n; ++j)
      if (flag[i] && flag[j]) res += b[i][j];
  return res;
}

int main() {
  scanf("%d", &n);
  s = 0, t = tot = n + 1;
  for (int i = 1; i <= n; ++i)
    for (int j = 1; j <= n; ++j)
      scanf("%d", &b[i][j]);
  for (int i = 1; i <= n; ++i) scanf("%d", c + i);
  i64 ans = 0;
  for (int i = 1; i <= n; ++i) {
    cap[i][t] = c[i];
    for (int j = 1; j <= n; ++j) {
      ans += b[i][j];
      cap[i][j] += b[i][j];
      cap[s][i] += b[i][j];
    }
  }
  printf("%lld\n", ans - Dinic(s, t));
  return 0;
}
