#include <cstdio>
#include <cstring>
#include <algorithm>

const int N = 100000 + 10, kCycle = 60 + 10;

int n, m, p, x[N];

int dist[N], cycle = 1;

inline int GCD(int a, int b) { return b ? GCD(b, a % b) : a; }
inline int LCM(int a, int b) { return a * b / GCD(a, b); }

int pre[N];

void Analysis(int a) {
  dist[a] = -2;
  int b = a * a % p;
  if (dist[b] == -1) {
    pre[b] = a;
    Analysis(b);
  } else if (dist[b] == -2) {
    int cnt = 1;
    for (int i = a; i != b; i = pre[i], ++cnt) dist[i] = 0;
    dist[b] = 0;
    cycle = LCM(cycle, cnt);
  }
  if (dist[a] == -2) dist[a] = dist[b] + 1;
}

inline int pos(int l, int r) { return (l + r) | (l != r); }

int tree[N * 2][kCycle], shift[N * 2]; 

inline void Merge(int u[], int a[], int b[]) {
  for (int i = 0; i < cycle; ++i) u[i] = a[i] + b[i];
}

inline void Shift(int id, int v) {
  static int temp[kCycle * 2];
  (shift[id] += v) %= cycle;
  for (int i = 0; i < cycle; ++i) temp[i] = tree[id][(i + v) % cycle];  
  std::copy(temp, temp + cycle, tree[id]);
}

void Modify(int l, int r, int u, int v) {
  int id = pos(l, r);
  for (int i = 0; i < cycle; ++i, (v *= v) %= p) tree[id][i] += v;
  if (l == r) return;
  int mid = (l + r) / 2;
  if (shift[id]) {
    Shift(pos(l, mid), shift[id]);
    Shift(pos(mid + 1, r), shift[id]);
    shift[id] = 0;
  }
  if (u <= mid) Modify(l, mid, u, v); else Modify(mid + 1, r, u, v);
}

void Cycle(int l, int r, int u, int v) {
  int id = pos(l, r);
  if (u <= l && r <= v) {
    Shift(id, 1);
    return;
  }
  int mid = (l + r) / 2;
  if (shift[id]) {
    Shift(pos(l, mid), shift[id]);
    Shift(pos(mid + 1, r), shift[id]);
    shift[id] = 0;
  }
  if (u <= mid) Cycle(l, mid, u, v);
  if (v > mid) Cycle(mid + 1, r, u, v);
  Merge(tree[id], tree[pos(l, mid)], tree[pos(mid + 1, r)]);
}

int Query(int l, int r, int u, int v) {
  int id = pos(l, r);
  if (u <= l && r <= v) return tree[id][0];
  int mid = (l + r) / 2, res = 0;
  if (shift[id]) {
    Shift(pos(l, mid), shift[id]);
    Shift(pos(mid + 1, r), shift[id]);
    shift[id] = 0;
  }
  if (u <= mid) res += Query(l, mid, u, v);
  if (v > mid) res += Query(mid + 1, r, u, v);
  return res;
}

class BIT {
  int data[N];

 public:
  inline void Add(int p, int v) { for (; p <= n; p += p & -p) data[p] += v; } 
  inline int Query(int p) {
    int res = 0;
    for (; p; p ^= p & -p) res += data[p];
    return res;
  }
  inline int Query(int l, int r) { return Query(r) - Query(l - 1); }

} cnt, sum;

void Sqr(int l, int r) {
  if (!cnt.Query(l, r)) return;
  if (l == r) {
    cnt.Add(l, -1);
    sum.Add(l, -x[l]);
    if (cnt.Query(l, l) == 0)
      Modify(1, n, l, (x[l] *= x[l]) %= p);
    else
      sum.Add(l, (x[l] *= x[l]) %= p);
  } else {
    int mid = (l + r) / 2;
    Sqr(l, mid);
    Sqr(mid + 1, r);
  }
}

int main() {
  scanf("%d%d%d", &n, &m, &p);
  for (int i = 1; i <= n; ++i) scanf("%d", x + i);
  memset(dist, -1, sizeof dist);
  for (int i = 0; i < p; ++i) if (dist[i] == -1) Analysis(i);
  for (int i = 1; i <= n; ++i) {
    if (dist[x[i]]) {
      sum.Add(i, x[i]);
      cnt.Add(i, dist[x[i]]);
    } else {
      Modify(1, n, i, x[i]);
    }
  }
  while (m--) {
    int op, l, r;
    scanf("%d%d%d", &op, &l, &r);
    switch (op) {
      case 0:
        Cycle(1, n, l, r);
        Sqr(l, r);
        break;
      case 1:
        printf("%d\n", Query(1, n, l, r) + sum.Query(l, r));
        break;
    }
  }
  return 0;
}
