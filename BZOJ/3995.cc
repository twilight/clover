#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>

#define fst first
#define snd second

const int N = 60000 + 10, SZ = 14;
const int state[] = {0, 10, 200, 210, 1010, 1110, 1210, 2200,
                     2210, 3000, 3010, 3110, 3200, 3210};

typedef int info_t[SZ];

int trans[SZ][SZ][3];

int n, m;
int vert[N], horz[3][N];

int anc[8];
info_t pool[4 * N];

inline void Decode(int state, int gap = 0) {
  for (int i = 0; i < 4; ++i) {
    anc[gap + i] = state % 10 + gap;
    state /= 10;
  }
}

inline int find(int x) { return anc[x] == x ? x : (anc[x] = find(anc[x])); }

inline bool check(int id) {
  return id == find(0) || id == find(1) || id == find(6) || id == find(7);
}

bool check() {
  return check(find(2)) && check(find(3)) && check(find(4)) && check(find(5));
}

int Encode() {
  static const int order[] = {0, 1, 6, 7};
  static const int base[] = {1, 10, 100, 1000};
  int res = 0;
  for (int i = 0; i < 4; ++i) {
    int j;
    for (j = 0; j <= i; ++j) 
      if (find(order[i]) == find(order[j])) break;
    res += j * base[i];
  }
  return res;
}

inline void cmin(int &lhs, int rhs) { if (!~lhs || rhs < lhs) lhs = rhs; }

void Preprocessing() {
  for (int i = 0; i < SZ; ++i) {
    for (int j = 0; j < SZ; ++j) {
      Decode(state[i]);
      Decode(state[j], 4);
      static int temp[8];
      memcpy(temp, anc, sizeof temp);
      anc[find(2)] = find(4);
      if (check()) trans[i][j][0] = std::lower_bound(state, state + SZ, Encode()) - state;
      anc[find(3)] = find(5);
      if (check()) trans[i][j][2] = std::lower_bound(state, state + SZ, Encode()) - state;
      memcpy(anc, temp, sizeof temp);
      anc[find(3)] = find(5);
      if (check()) trans[i][j][1] = std::lower_bound(state, state + SZ, Encode()) - state;
    }
  }
}

inline void merge(info_t res, const info_t lhs, const info_t rhs, int p, int q) {
  memset(res, -1, SZ * sizeof(int));
  for (int i = 0; i < SZ; ++i) {
    if (!~lhs[i]) continue;
    for (int j = 0; j < SZ; ++j) {
      if (!~rhs[j]) continue;
      if (~trans[i][j][0]) cmin(res[trans[i][j][0]], lhs[i] + rhs[j] + p);
      if (~trans[i][j][1]) cmin(res[trans[i][j][1]], lhs[i] + rhs[j] + q);
      if (~trans[i][j][2]) cmin(res[trans[i][j][2]], lhs[i] + rhs[j] + p + q);
    }
  }
}

void Toggle(int id, int l, int r, int pos) {
  if (l == r) {
    memset(pool[id], -1, sizeof pool[id]);
    pool[id][0] = vert[l];
    pool[id][4] = 0;
    return;
  }
  int mid = (l + r) / 2;
  if (pos <= mid) Toggle(id << 1, l, mid, pos); else Toggle(id << 1 | 1, mid + 1, r, pos);
  merge(pool[id], pool[id << 1], pool[id << 1 | 1], horz[1][mid], horz[2][mid]);
}

void Build(int id, int l, int r) {
  if (l == r) {
    memset(pool[id], -1, SZ * sizeof(int));
    pool[id][0] = vert[l];
    pool[id][4] = 0;
    return;
  }
  int mid = (l + r) / 2;
  Build(id << 1, l, mid);
  Build(id << 1 | 1, mid + 1, r);
  merge(pool[id], pool[id << 1], pool[id << 1 | 1], horz[1][mid], horz[2][mid]);
}

void Modify(int x0, int y0, int x1, int y1, int w) {
  if (x0 == x1) horz[x0][std::min(y0, y1)] = w; else vert[y0] = w;
  Toggle(1, 1, n, y0);
  Toggle(1, 1, n, y1);
}

info_t ans;

void Query(int id, int l, int r, int p, int q) {
  if (p <= l && r <= q) {
    if (~ans[0]) {
      static info_t temp;
      memcpy(temp, ans, sizeof ans);
      merge(ans, temp, pool[id], horz[1][l - 1], horz[2][l - 1]);
    } else {
      memcpy(ans, pool[id], sizeof ans);
    }
    return;
  }
  int mid = (l + r) / 2;
  if (p <= mid) Query(id << 1, l, mid, p, q);
  if (q > mid) Query(id << 1 | 1, mid + 1, r, p, q);
}

int main() {
  memset(trans, -1, sizeof trans);
  Preprocessing();
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= 2; ++i)
    for (int j = 1; j < n; ++j)
      scanf("%d", &horz[i][j]);
  for (int i = 1; i <= n; ++i) scanf("%d", vert + i);
  Build(1, 1, n);
  while (m--) {
    char op;
    int x0, y0, x1, y1, w, l, r;
    scanf(" %c", &op);
    switch (op) {
      case 'C':
        scanf("%d%d%d%d%d", &x0, &y0, &x1, &y1, &w);
        Modify(x0, y0, x1, y1, w);
        break;
      case 'Q':
        scanf("%d%d", &l, &r);
        memset(ans, -1, sizeof ans);
        Query(1, 1, n, l, r);
        printf("%d\n", ans[0]);
        break;
    }
  }
  return 0;
}
