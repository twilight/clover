#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>

const int N = 10, INF = 0x3f3f3f3f;

int n, m, gap;
int val[2][1 << N][N];

std::vector< std::vector<int> > mem[1 << N];

#define lg2(x) (31 - __builtin_clz(x))

int DFS(int i, int state, int w) {
  if (w > (1 << (n - lg2(i) - 1))) return -INF;
  if (mem[i].empty()) {
    mem[i].resize(1 << lg2(i));
    for (std::vector< std::vector<int> >::iterator it = mem[i].begin(); it != mem[i].end(); ++it) it->resize((1 << (n - lg2(i) - 1)) + 1, -1);
  }
  int &res = mem[i][state][w];
  if (~res) return res;
  res = 0;
  if (i >= gap) {
    for (int j = 0; j < n - 1; ++j) {
      int k = state >> j & 1;
      if (k == w) res += val[w][i][j];
    }
    return res;
  }
  for (int j = 0; j < 2; ++j)
    for (int k = 0; k <= w; ++k)
      res = std::max(res, DFS(i << 1, state << 1 | j, k) + DFS(i << 1 | 1, state << 1 | j, w - k));
  return res;
}

int main() {
  scanf("%d%d", &n, &m);
  gap = 1 << (n - 1);
  for (int k = 1; k >= 0; --k) 
    for (int i = 0; i < gap; ++i)
      for (int j = 0; j < n - 1; ++j)
        scanf("%d", &val[k][i + gap][j]);
  int ans = 0;
  for (int i = 0; i <= m; ++i) ans = std::max(ans, DFS(1, 0, i));
  printf("%d\n", ans);
  return 0;
}
