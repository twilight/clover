#include <cstdio>
#include <cstdlib>
#include <map>
#include <algorithm>

#define x first
#define y second
#define fst first
#define snd second

typedef std::pair<int, int> Point;

const int N = 200000 + 10;

int n, c;
Point point[N];

int anc[N], size[N];

inline int Find(int a) { return anc[a] == a ? a : (anc[a] = Find(anc[a])); }

inline void Union(int a, int b) {
  if ((a = Find(a)) != (b = Find(b))) {
    size[b] += size[a];
    anc[a] = b;
  }
}

int main() {
  scanf("%d%d", &n, &c);
  for (int i = 1; i <= n; ++i) {
    int a, b;
    scanf("%d%d", &a, &b);
    point[i].x = a + b, point[i].y = a - b;
  }
  std::sort(point + 1, point + n + 1);
  static std::map<int, int> pos;
  for (int i = 1; i <= n; ++i) size[anc[i] = i] = 1;
  for (int i = 1; i <= n; ++i) {
    std::map<int, int>::iterator it = pos.lower_bound(point[i].y);
    while (it != pos.end() && abs(point[i].x - point[it->snd].x) > c) pos.erase(it++);
    if (it != pos.end() && abs(it->fst - point[i].y) <= c) Union(it->snd, i);
    if (it != pos.begin()) {
      --it;
      while (it != pos.begin() && abs(point[i].x - point[it->snd].x) > c) pos.erase(it--);
      if (abs(point[i].x - point[it->snd].x) > c)
        pos.erase(it);
      else if (abs(it->fst - point[i].y) <= c)
        Union(it->snd, i);
    }
    pos[point[i].y] = i;
  }
  int cnt = 0, mx = 0;
  for (int i = 1; i <= n; ++i) if (Find(i) == i) ++cnt, mx = std::max(mx, size[i]);
  printf("%d %d\n", cnt, mx);
  return 0;
}
