#include <cstdio>
#include <cstring>
#include <algorithm>

const int N = 100 + 10, K = 10 + 1, INF = 1 << 30;

int n, m, k, a[N][N];

int Solve(int x) {
  static int f[N][2 * K][N];
  int res = -INF;
  memset(f, 0xaa, sizeof f);
  for (int y = 1; y <= m; ++y) {
    static int sum[N];
    sum[x + 1] = 0;
    for (int i = x; i > 0; --i) sum[i] = sum[i + 1] + a[i][y];
    for (int h = 1; h <= x; ++h) f[y][0][h] = std::max(f[y - 1][0][h], 0) + sum[h];
    for (int z = 1; z <= k; ++z) {
      static int pre[N], suf[N];
      pre[0] = suf[x + 1] = -INF;
      for (int j = 1; j <= x; ++j) pre[j] = std::max(pre[j - 1], f[y - 1][z - 1][j]);
      for (int j = x; j > 0; --j) suf[j] = std::max(suf[j + 1], f[y - 1][z - 1][j]);
      for (int h = 1; h <= x; ++h) f[y][z][h] = std::max(f[y - 1][z][h], (z & 1) ? pre[h - 1] : suf[h + 1]) + sum[h];
    }
    for (int h = 1; h <= x; ++h) res = std::max(res, f[y][k][h]);
  }
  return res;
}

int main() {
  scanf("%d%d%d", &n, &m, &k);
  k *= 2;
  for (int i = 1; i <= n; ++i)
    for (int j = 1; j <= m; ++j)
      scanf("%d", &a[i][j]);
  int ans = -INF;
  for (int i = 1; i <= n; ++i) ans = std::max(ans, Solve(i));
  printf("%d\n", ans);
  return 0;
}
