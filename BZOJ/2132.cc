#include <cstdio>
#include <cstring>
#include <algorithm>

const int N = 100 + 10, V = N * N, E = 30 * V, INF = 1 << 27, D = 2000;
const int dx[] = {-1, 0, 1, 0}, dy[] = {0, 1, 0, -1};

int n, m, a[N][N], b[N][N], c[N][N];

int adj[V], s, t;
int to[E], next[E], cap[E];

void link(int a, int b, int c) {
  static int cnt = 2;
  to[cnt] = b, next[cnt] = adj[a], cap[cnt] = c, adj[a] = cnt++;
  to[cnt] = a, next[cnt] = adj[b], cap[cnt] = 0, adj[b] = cnt++;
}

int h[V], gap[V], arc[V];

int dfs(int a, int df) {
  if (a == t) return df;
  int res = 0;
  for (int i = adj[a]; i; i = next[i]) {
    int b = to[i];
    if (cap[i] && h[a] == h[b] + 1) {
      int f = dfs(b, std::min(df - res, cap[i]));
      cap[i] -= f;
      cap[i ^ 1] += f;
      res += f;
    }
    if (res == df) return res;
  }
  if (--gap[h[a]] == 0) h[s] = V;
  ++gap[++h[a]];
  arc[a] = adj[a];
  return res;
}

int flow() {
  memset(h, 0, sizeof h);
  memset(gap, 0, sizeof gap);
  memcpy(arc, adj, sizeof adj);
  int res = 0;
  while (h[s] < V) res += dfs(s, INF);
  return res;
}

int pos(int x, int y) { return x * (m + 1) + y; }

int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; ++i)
    for (int j = 1; j <= m; ++j)
      scanf("%d", &a[i][j]);
  for (int i = 1; i <= n; ++i)
    for (int j = 1; j <= m; ++j)
      scanf("%d", &b[i][j]);
  for (int i = 1; i <= n; ++i)
    for (int j = 1; j <= m; ++j)
      scanf("%d", &c[i][j]);
  s = 0, t = 1;
  int tot = 0;
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= m; ++j) {
      int p = pos(i, j);
      tot += a[i][j] + b[i][j];
      if ((i + j) & 1) {
        link(s, p, a[i][j]);
        link(p, t, b[i][j]);
        for (int k = 0; k < 4; ++k) {
          int x = i + dx[k], y = j + dy[k];
          if (1 <= x && x <= n && 1 <= y && y <= m) {
            int q = pos(x, y);
            link(p, q, c[i][j] + c[x][y]);
            link(q, p, c[i][j] + c[x][y]);
            tot += c[i][j] + c[x][y];
          }
        }
      } else {
        link(s, p, b[i][j]);
        link(p, t, a[i][j]);
      }
    }
  }
  printf("%d\n", tot - flow());
  return 0;
}
