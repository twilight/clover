#include <cstdio>
#include <climits>
#include <cstdlib>
#include <vector>
#include <utility>
#include <algorithm>

typedef std::pair<unsigned, unsigned> Hash;

const int N = 16900 + 10, L = 130 + 10;

int n, m, l;
unsigned mem[2 * N * L], *randome[2 * N];

int Intersection(int lhs, int rhs) {
  int res = 0;
  for (int i = 0, j = 0; i < l && j < l;) {
    if (randome[lhs][i] == randome[rhs][j]) {
      ++res;
      ++i, ++j;
    } else if (randome[lhs][i] < randome[rhs][j]) {
      ++i;
    } else {
      ++j;
    }
  }
  return res;
}

inline void Check(unsigned &lhs, unsigned rhs) { if (rhs < lhs) lhs = rhs; }

Hash hash[2 * N];

inline bool Compare(int u, int v) { return hash[u] < hash[v]; }

int main() {
  scanf("%d%d%d %s", &n, &m, &l, (char*)mem);
  for (int i = 0; i < 2 * n; ++i) {
    randome[i] = mem + i * l;
    std::sort(randome[i], randome[i] + l);
  }
  static int ans[2 * N];
  static std::vector<int> pool;
  for (int i = 0; i < 2 * n; ++i) pool.push_back(i);
  while (!pool.empty()) {
    Hash a(random(), random()), b(random(), random());
    for (int i = 0; i < pool.size(); ++i) {
      hash[pool[i]].first = hash[pool[i]].second = UINT_MAX;
      for (int j = 0; j < l; ++j) {
        Check(hash[pool[i]].first, a.first * randome[pool[i]][j] + b.first);
        Check(hash[pool[i]].second, a.second * randome[pool[i]][j] + b.second);
      }
    }
    std::sort(pool.begin(), pool.end(), Compare);
    static std::vector<int> temp;
    temp.clear();
    for (int it = 0; it < pool.size(); ++it) {
      if (it + 1 < pool.size()) {
        int i = pool[it], j = pool[it + 1];
        if (hash[i] == hash[j] && Intersection(i, j) == l / 2) {
          ans[i] = j;
          ans[j] = i;
          ++it;
          continue;
        }
      }
      temp.push_back(pool[it]);
    }
    pool = temp;
  }
  for (int i = 0; i < 2 * n; ++i) printf("%d\n", ans[i] + 1);
  return 0;
}
