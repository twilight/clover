#include <cstdio>
#include <cstring>
#include <cassert>
#include <vector>
#include <queue>

typedef long long int64;

const int N = 200000 + 10, E = N * 2;
const int64 INF = 0x3f3f3f3f3f3f3f3fLL;

inline void cmin(int64 &a, int64 b) { if (b < a) a = b; }

int n;
int fa[N];
int64 p[N], q[N], s[N], lim[N];

std::vector<int> e[N];

int64 dis[N];
int size[N], son[N], top[N], dep[N], no[N], cnt = 0;
std::vector<int> pool[N];
std::vector<int> convex[N * 10];

void dfs(int a) {
  size[a] = 1;
  for (std::vector<int>::iterator it = e[a].begin(); it != e[a].end(); ++it) {
    int b = *it;
    dep[b] = dep[a] + 1;
    dis[b] = dis[a] + s[b];
    dfs(b);
    size[a] += size[b];
    if (size[b] > size[son[a]]) son[a] = b;
  }
}

void split(int a, int tp) {
  top[a] = tp;
  no[a] = ++cnt;
  if (son[a]) split(son[a], tp);
  for (std::vector<int>::iterator it = e[a].begin(); it != e[a].end(); ++it) {
    int b = *it;
    if (b != son[a]) split(b, b);
  }
}

int64 ans[N];

double slope(int a, int b) { return (ans[a] - ans[b]) / (double)(dis[a] - dis[b]); }
int64 f(int j, int i) { return ans[j] + (dis[i] - dis[j]) * p[i] + q[i]; }

int64 slopesearch(const std::vector<int> &pool, int a) {
  int l = 0, r = pool.size() - 1;
  while (l < r) {
    int mid = (l + r) / 2;
    if (slope(pool[mid + 1], pool[mid]) <= p[a]) l = mid + 1; else r = mid;
  }
  return f(pool[l], a);
}

int dissearch(const std::vector<int> &pool, int a) {
  int l = 0, r = pool.size() - 1;
  while (l < r) {
    int mid = (l + r) / 2;
    if (dis[a] - dis[pool[mid]] <= lim[a]) r = mid; else l = mid + 1;
  }
  return pool[l];
}

inline void append(std::vector<int> &cur, int val) {
  while (cur.size() > 1 && slope(cur[cur.size() - 2], cur[cur.size() - 1]) >= slope(cur[cur.size() - 1], val)) cur.pop_back();
  cur.push_back(val);
}

void insert(int id, int l, int r, int pos, int val) {
  for (;;) {
    append(convex[id], val);
    if (l == r) break;
    int mid = (l + r) / 2;
    id <<= 1;
    if (pos <= mid) {
      r = mid;
    } else {
      l = mid + 1;
      id |= 1;
    }
  }
}

int64 query(int id, int l, int r, int p, int q, int val) {
  if (p <= l && r <= q) {
    int64 res = slopesearch(convex[id], val);
    cmin(res, f(convex[id][0], val));
    cmin(res, f(convex[id][convex[id].size() - 1], val));
    return res;
  }
  int64 res = INF;
  int mid = (l + r) / 2;
  if (p <= mid) cmin(res, query(id << 1, l, mid, p, q, val));
  if (q > mid) cmin(res, query(id << 1 | 1, mid + 1, r, p, q, val));
  return res;
}

void solve(int a) {
  int v = fa[a];
  int u = top[v];
  while (u && dis[a] - dis[u] <= lim[a]) {
    cmin(ans[a], query(1, 1, cnt, no[u], no[v], a));
    u = top[v = fa[u]];
  }
  if (u) {
    int pos = dissearch(pool[u], a);
    cmin(ans[a], query(1, 1, cnt, no[pos], no[v], a));
  }
  pool[top[a]].push_back(a);
  insert(1, 1, cnt, no[a], a);
}

void bfs(int s) {
  static std::queue<int> q;
  q.push(s);
  while (!q.empty()) {
    int a = q.front();
    q.pop();
    solve(a);
    for (std::vector<int>::iterator it = e[a].begin(); it != e[a].end(); ++it) q.push(*it);
  }
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("ticket.in", "r", stdin);
  freopen("ticket.out", "w", stdout);
#endif
  int type;
  scanf("%d%d", &n, &type);
  for (int i = 2; i <= n; ++i) {
    scanf("%d%lld%lld%lld%lld", fa + i, s + i, p + i, q + i, lim + i);
    e[fa[i]].push_back(i);
  }
  dfs(1);
  split(1, 1);
  memset(ans, 0x3f, sizeof ans);
  ans[1] = 0;
  lim[1] = -1;
  bfs(1);
  for (int i = 2; i <= n; ++i) printf("%lld\n", ans[i]);
  return 0;
}
