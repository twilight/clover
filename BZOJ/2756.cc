#include <bits/stdc++.h>

typedef long long int64;

const int N = 40 + 10, V = N * N, E = V * 10;
const int dx[] = {-1, 0, 1, 0}, dy[] = {0, 1, 0, -1};
const int64 INF = 1LL << 40;

int n, m, s, t, tot;
int64 val[N][N];

int adj[V];
int to[E], next[E], cnt;
int64 cap[E];

void link(int a, int b, int64 c) {
  to[cnt] = b, next[cnt] = adj[a], cap[cnt] = c, adj[a] = cnt++;
  to[cnt] = a, next[cnt] = adj[b], cap[cnt] = 0, adj[b] = cnt++;
}

inline int map(int x, int y) { return (x - 1) * m + y; }

inline int64 Calc(int64 x) {
  int64 res = 0;
  for (int i = 1; i <= n; ++i)
    for (int j = 1; j <= m; ++j)
      res += (x - val[i][j]);
  return res / 2;
}

int h[V], gap[V];

int64 DFS(int a, int64 df) {
  if (a == t) return df;
  int64 flow = 0;
  for (int it = adj[a]; it; it = next[it]) {
    int b = to[it];
    if (cap[it] && h[a] == h[b] + 1) {
      int64 f = DFS(b, std::min(cap[it], df - flow));
      cap[it] -= f;
      cap[it ^ 1] += f;
      flow += f;
    }
    if (flow == df) break;
  }
  if (!flow) {
    if (--gap[h[a]] == 0) h[s] = tot + 1;
    ++gap[++h[a]];
  }
  return flow;
}

int64 MaxFlow() {
  int64 res = 0;
  std::fill(h + 1, h + tot + 1, 0);
  std::fill(gap + 1, gap + tot + 2, 0);
  while (h[s] <= tot) res += DFS(s, INF);
  return res;
}

bool Check(int64 x) {
  cnt = 2;
  std::fill(adj + 1, adj + tot + 1, 0);
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= m; ++j) {
      if ((i + j) & 1) {
        link(s, map(i, j), x - val[i][j]);
        for (int k = 0; k < 4; ++k) {
          int x = i + dx[k], y = j + dy[k];
          if (1 <= x && x <= n && 1 <= y && y <= m)
            link(map(i, j), map(x, y), INF);
        }
      } else {
        link(map(i, j), t, x - val[i][j]);
      }
    }
  }
  return MaxFlow() == Calc(x);
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d", &n, &m);
    int64 maxval = LONG_LONG_MIN;
    int64 sumx, sumy, cntx, cnty;
    sumx = sumy = cntx = cnty = 0;
    for (int i = 1; i <= n; ++i) {
      for (int j = 1; j <= m; ++j) {
        scanf("%lld", &val[i][j]);
        if ((i + j) & 1) sumx += val[i][j], cntx++; else sumy += val[i][j], cnty++;
        maxval = std::max(maxval, val[i][j]);
      }
    }
    s = n * m + 1, t = tot = n * m + 2;
    #define FAIL { puts("-1"); continue; }
    if ((n * m) & 1) {
      if ((sumx - sumy) % (cntx - cnty)) FAIL;
      int64 x = (sumx - sumy) / (cntx - cnty);
      if (x < maxval) FAIL;
      if (Check(x)) printf("%lld\n", Calc(x)); else FAIL;
    } else {
      int64 l = maxval, r = INF;
      while (l < r) {
        int64 mid = (l + r) / 2;
        if (Check(mid)) r = mid; else l = mid + 1;
      }
      if (Check(l)) printf("%lld\n", Calc(l)); else FAIL;
    }
  }
  return 0;
}
