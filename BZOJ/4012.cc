#include <cstdio>
#include <vector>
#include <algorithm>

const int N = 150000 + 10, LOG = 20;

typedef long long i64;
typedef std::pair<int, int> info_t;

int n, tcase, lim, x[N];

struct edge_t {
  int adj, w;
  edge_t *next;
  edge_t(int _a, int _w, edge_t *_n): adj(_a), w(_w), next(_n) {}
} *e[N];

int fa[N], dep[N], left[N], right[N];
i64 dist[N];

std::vector<int> table[LOG];

void DFS(int s) {
  static std::vector<int> stk;
  std::fill(left + 1, left + n + 1, -1);
  for (stk.push_back(s); !stk.empty();) {
    int a = stk.back();
    if (left[a] == -1) {
      left[a] = right[a] = table[0].size();
      table[0].push_back(a);
    }
    bool flag = false;
    for (edge_t *it = e[a]; it; it = it->next) {
      int b = it->adj;
      if (b != fa[a]) {
        if (fa[b] != a) {
          fa[b] = a;
          dep[b] = dep[a] + 1;
          dist[b] = dist[a] + it->w;
          e[a] = it;
          stk.push_back(b);
          flag = true;
          break;
        } else {
          right[a] = table[0].size();
          table[0].push_back(a);
        }
      }
    }
    if (!flag) stk.pop_back();
  }
}

void Preprocessing() {
  for (int i = 1; i < LOG; ++i) {
    table[i].resize(table[0].size());
    for (size_t j = 0; j + (1 << i) <= table[0].size(); ++j) {
      int u = table[i - 1][j], v = table[i - 1][j + (1 << (i - 1))];
      table[i][j] = (dep[u] < dep[v] ? u : v);
    }
  }
}

int LCA(int u, int v) {
  u = left[u], v = left[v];
  if (u > v) std::swap(u, v);
  int layer = 31 - __builtin_clz(v - u + 1);
  int a = table[layer][u], b = table[layer][v - (1 << layer) + 1];
  return dep[a] < dep[b] ? a : b;
}

std::vector<int> son[N];

class node_t {
 private:
  std::vector<info_t> dfn;
  std::vector<int> size;
  std::vector<i64> up, down;

  void BFS();

 public:
  std::vector<int> pool;
  void Build();
  i64 Query(int);
} pool[2 * N];

inline bool cmp(int a, int b) { return left[a] < left[b]; }

void node_t::BFS() {
  static std::vector<int> q;
  q.clear();
  q.push_back(0);
  for (size_t i = 0; i < q.size(); ++i) {
    int a = q[i];
    for (std::vector<int>::iterator it = son[a].begin(); it != son[a].end(); ++it) {
      int b = *it;
      q.push_back(b);
    }
  }
  for (int i = q.size() - 1; i >= 0; --i) {
    int a = q[i];
    for (std::vector<int>::iterator it = son[a].begin(); it != son[a].end(); ++it) {
      int b = *it;
      i64 dis = dist[pool[b]] - dist[pool[a]];
      size[a] += size[b];
      down[a] += down[b] + size[b] * dis;
    }
  }
  for (size_t i = 0; i < q.size(); ++i) {
    int a = q[i];
    for (std::vector<int>::iterator it = son[a].begin(); it != son[a].end(); ++it) {
      int b = *it;
      i64 dis = dist[pool[b]] - dist[pool[a]];
      up[b] = up[a] + down[a] - (down[b] + size[b] * dis) + (size[0] - size[b]) * dis;
    }
  }
}

void node_t::Build() {
  static int flag[N], cnt;
  ++cnt;
  for (size_t i = 0; i < pool.size(); ++i) flag[pool[i]] = cnt;
  std::sort(pool.begin(), pool.end(), cmp);
  for (size_t i = 1, lim = pool.size(); i < lim; ++i)
    pool.push_back(LCA(pool[i - 1], pool[i]));
  std::sort(pool.begin(), pool.end());
  pool.erase(std::unique(pool.begin(), pool.end()), pool.end());
  std::sort(pool.begin(), pool.end(), cmp);
  size.resize(pool.size());
  up.resize(pool.size());
  down.resize(pool.size());
  for (size_t i = 0; i < pool.size(); ++i) size[i] = (flag[pool[i]] == cnt);
  dfn.clear();
  for (size_t i = 0; i < pool.size(); ++i) son[i].clear();
  static std::vector<int> stk;
  stk.clear();
  for (size_t i = 0; i < pool.size(); ++i) {
    dfn.push_back(info_t(left[pool[i]], i));
    dfn.push_back(info_t(right[pool[i]], i));
    while (!stk.empty() && right[pool[stk.back()]] < right[pool[i]]) stk.pop_back();
    if (!stk.empty()) son[stk.back()].push_back(i);
    stk.push_back(i);
  }
  BFS();
  std::sort(dfn.begin(), dfn.end());
}

i64 node_t::Query(int u) {
  std::vector<info_t>::iterator it = std::lower_bound(dfn.begin(), dfn.end(), info_t(left[u], u));
  if (it == dfn.end()) --it;
  int a = LCA(pool[it->second], u), b = it->second;
  if (it != dfn.begin()) {
    --it;
    int c = LCA(pool[it->second], u);
    if (dep[c] > dep[a]) {
      a = c;
      b = it->second;
    }
  }
  int p = size[b], q = size[0] - size[b];
  i64 res = up[b] + q * (dist[u] - dist[pool[b]]);
  res += down[b] + p * (dist[pool[b]] + dist[u] - 2 * dist[a]);
  return res;
}

#define pos(l, r) (((l) + (r)) | ((l) != (r)))

void Insert(int l, int r, int p, int v) {
  pool[pos(l, r)].pool.push_back(v);
  if (l < r) {
    int mid = (l + r) / 2;
    if (p <= mid) Insert(l, mid, p, v); else Insert(mid + 1, r, p, v);
  }
}

void Build(int l, int r) {
  pool[pos(l, r)].Build();
  int mid = (l + r) / 2;
  if (l < r) {
    Build(l, mid);
    Build(mid + 1, r);
  }
}

i64 Query(int l, int r, int p, int q, int u) {
  if (p <= l && r <= q) return pool[pos(l, r)].Query(u);
  int mid = (l + r) / 2;
  i64 res = 0;
  if (p <= mid) res += Query(l, mid, p, q, u);
  if (q > mid) res += Query(mid + 1, r, p, q, u);
  return res;
}

int main() {
  scanf("%d%d%d", &n, &tcase, &lim);
  static std::vector<int> sorted;
  for (int i = 1; i <= n; ++i) {
    scanf("%d", x + i);
    sorted.push_back(x[i]);
  }
  std::sort(sorted.begin(), sorted.end());
  sorted.erase(std::unique(sorted.begin(), sorted.end()), sorted.end());
  for (int cnt = n - 1; cnt--;) {
    int a, b, c;
    scanf("%d%d%d", &a, &b, &c);
    e[a] = new edge_t(b, c, e[a]);
    e[b] = new edge_t(a, c, e[b]);
  }
  dep[1] = 1;
  DFS(1);
  Preprocessing();
  for (int i = 1; i <= n; ++i) {
    x[i] = std::lower_bound(sorted.begin(), sorted.end(), x[i]) - sorted.begin() + 1;
    Insert(1, sorted.size(), x[i], i);
  }
  Build(1, sorted.size());
  i64 ans = 0;
  while (tcase--) {
    int u, a, b;
    scanf("%d%d%d", &u, &a, &b);
    int l = std::min((a + ans) % lim, (b + ans) % lim);
    int r = std::max((a + ans) % lim, (b + ans) % lim);
    int p = std::lower_bound(sorted.begin(), sorted.end(), l) - sorted.begin() + 1;
    int q = std::upper_bound(sorted.begin(), sorted.end(), r) - sorted.begin();
    printf("%lld\n", ans = (p <= q ? Query(1, sorted.size(), p, q, u) : 0));
  }
  return 0;
}
