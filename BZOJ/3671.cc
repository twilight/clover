#include <cstdio>
#include <algorithm>

typedef long long int64;

const int N = 5000 + 1;

inline void cmin(int &a, int b) { if (b < a) a = b; }
inline void cmax(int &a, int b) { if (b > a) a = b; }

int n, m, k, q;
int x, a, b, c, d;
int num[N * N];

inline int random(int64 x) {
  return (a * x * x + b * x + c) % d;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("random.in", "r", stdin);
  freopen("random.out", "w", stdout);
#endif
  scanf("%d%d%d%d%d", &x, &a, &b, &c, &d);
  scanf("%d%d%d", &n, &m, &q);
  for (int i = 1; i <= n * m; ++i) num[i] = i;
  k = n * m;
  for (int i = 1; i <= k; ++i) {
    x = random(x);
    std::swap(num[i], num[x % i + 1]);
  }
  while (q--) {
    int u, v;
    scanf("%d%d", &u, &v);
    std::swap(num[u], num[v]);
  }
  static int pos[N * N], cnt, sup[N], inf[N];
  for (int i = 1; i <= k; ++i) pos[num[i]] = i;
  for (int i = 1; i <= n; ++i) inf[i] = 1, sup[i] = m;
  for (int i = 1; i <= k; ++i) {
    int a = pos[i] / m + 1, b = pos[i] % m;
    if (!b) b = m, --a;
    if (inf[a] <= b && b <= sup[a]) {
      printf("%d%c", i, ++cnt == n + m - 1 ? '\n' : ' ');
      for (int x = 1; x <= n; ++x) {
        if (x < a) cmin(sup[x], b);
        if (x > a) cmax(inf[x], b);
      }
    }
  }
  putchar('\n');
  return 0;
}
