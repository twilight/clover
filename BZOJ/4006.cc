#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>

const int N = 1000 + 10, S = 10 + 1, INF = 0x3f3f3f3f;

int n, m, p, state[S + 1], mask[N];
int dis[1 << S][N], mx[1 << S];

struct edge_t {
  int adj, cost;
  edge_t *next;
  edge_t(int _a, int _c, edge_t *_n): adj(_a), cost(_c), next(_n) {}
} *e[N];

void Dijkstra(int dis[]) {
  typedef std::pair<int, int> info_t;
#define fst first
#define snd second
  static std::vector<info_t> q;
  q.clear();
  for (int i = 1; i <= n; ++i) q.push_back(info_t(-dis[i], i));
  std::make_heap(q.begin(), q.end());
  while (!q.empty()) {
    info_t temp(q[0]);
    std::pop_heap(q.begin(), q.end());
    q.pop_back();
    int a = temp.snd;
    if (-temp.fst > dis[a]) continue;
    for (edge_t *it = e[a]; it; it = it->next) {
      int b = it->adj, c = it->cost;
      if (dis[b] > dis[a] + c) {
        q.push_back(info_t(-(dis[b] = dis[a] + c), b));
        std::push_heap(q.begin(), q.end());
      }
    }
  }
}

int ans = INF;

void DFS(int dep, int cur) {
  if (cur >= ans) return;
  if (dep > p) {
    ans = cur;
    return;
  }
  if (!state[dep]) {
    DFS(dep + 1, cur);
    return;
  }
  static std::vector<int> pool;
  pool.push_back(state[dep]);
  DFS(dep + 1, cur + mx[state[dep]]);
  pool.pop_back();
  for (std::vector<int>::iterator it = pool.begin(); it != pool.end(); ++it) {
    int temp = *it;
    DFS(dep + 1, cur - mx[temp] + mx[*it |= state[dep]]);
    *it = temp;
  }
}

int main() {
  scanf("%d%d%d", &n, &m, &p);
  while (m--) {
    int u, v, w;
    scanf("%d%d%d", &u, &v, &w);
    e[u] = new edge_t(v, w, e[u]);
    e[v] = new edge_t(u, w, e[v]);
  }
  for (int i = 0; i < p; ++i) {
    int c, d;
    scanf("%d%d", &c, &d);
    state[c] |= (mask[d] = 1 << i);
  }
  memset(dis, 0x3f, sizeof dis);
  for (int i = 1; i <= n; ++i) dis[mask[i]][i] = 0;
  for (int s = 1; s < (1 << p); ++s) {
    for (int t = s; t; t = (t - 1) & s)
      for (int i = 1; i <= n; ++i)
        dis[s][i] = std::min(dis[s][i], dis[t][i] + dis[s ^ t][i]);
    Dijkstra(dis[s]);
  }
  for (int s = 0; s < (1 << p); ++s) mx[s] = *std::min_element(dis[s] + 1, dis[s] + n + 1);
  DFS(1, 0);
  printf("%d\n", ans);
  return 0;
}
