#include <cstdio>
#include <climits>
#include <numeric>

typedef long long int64;

int MOD;

inline void INC(int &a, int b) { a = ((int64)a + b) % MOD; }
inline void MUL(int &a, int b) { a = (int64)a * b % MOD; }
inline int sum(int a, int b) { return ((int64)a + b) % MOD; }
inline int pdt(int a, int b) { return (int64)a * b % MOD; }

const int N = 100;

int p[N], c[N], num[N], cnt;

inline void Factor(int n) {
  cnt = 0;
  for (int i = 2; i * i <= n; ++i) {
    if (n % i == 0) {
      p[++cnt] = i;
      num[cnt] = 1;
      for (c[cnt] = 0; n % i == 0; n /= i) c[cnt]++, num[cnt] *= i;
    }
  }
  if (n > 1) {
    c[++cnt] = 1;
    num[cnt] = p[cnt] = n;
  }
}

inline int pow(int base, int exp) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) MUL(res, base);
    MUL(base, base);
  }
  return res;
}

inline int exgcd(int a, int b, int &x, int &y) {
  if (!b) {
    x = 1, y = 0;
    return a;
  }
  int res = exgcd(b, a % b, x, y);
  int t = x;
  x = y;
  y = t - a / b * y;
  return res;
}

inline int inv(int a) {
  int x, y;
  exgcd(a, MOD, x, y);
  return (x % MOD + MOD) % MOD;
}

inline int fac(int n, int p) {
  int res = 1;
  for (int i = 1; i <= n; ++i) if (i % p) MUL(res, i);
  return res;
}

inline int fact(int n, int p, int &tot) {
  if (n < p) return fac(n, p);
  int seg = n / MOD, rem = n % MOD;
  int res = pdt(pow(fac(MOD, p), seg), fac(rem, p));
  tot += n / p;
  MUL(res, fact(n / p, p, tot));
  return res;
}

inline int Binomial(int n, int m, int p) {
  int res = 1, a, b, c;
  a = b = c = 0;
  MUL(res, fact(n, p, a));
  MUL(res, inv(fact(m, p, b)));
  MUL(res, inv(fact(n - m, p, c)));
  MUL(res, pow(p, a - (b + c)));
  return res;
}

int BinomialMod(int n, int m, int t) {
  if (n < 0) return 0;
  static int a[N];
  int res = 1;
  for (int i = 1; i <= cnt; ++i) {
    MOD = num[i];
    a[i] = Binomial(n, m, p[i]);
  }
  int last = 1;
  for (int i = 2; i <= cnt; ++i) {
    int x, y;
    exgcd(last *= num[i - 1], num[i], x, y);
    a[i] = (((int64)num[i] * -y * (a[i] - a[i - 1]) + a[i]) % t + t) % t;
  }
  return a[cnt];
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int n, m, tmp;
  static int w[N];
  scanf("%d", &tmp);
  Factor(tmp);
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= m; ++i) scanf("%d", w + i);
  if (std::accumulate(w + 1, w + m + 1, 0) > n) {
    puts("Impossible");
  } else {
    int res = 1;
    for (int i = 1; i <= m; ++i) res = (int64)res * BinomialMod(n -= w[i - 1], w[i], tmp) % tmp;
    printf("%d\n", res);
  }
  return 0;
}
