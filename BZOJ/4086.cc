#include <cstdio>
#include <cstring>
#include <bitset>
#include <vector>
#include <iterator>
#include <algorithm>

const int N = 1000 + 10, M = 5000 + 10;

int n, m, k, x[M], y[M];
std::vector<int> adj[N];

bool flag[N][N];
char ans[N][N];

inline void Solve2() {
  for (int i = 1; i <= n; ++i)
    for (int j = 0; j < adj[i].size(); ++j)
      ans[i][adj[i][j]] = 'Y';
}

inline void Solve3() {
  for (int i = 1; i <= n; ++i)
    for (int j = 0; j < adj[i].size(); ++j)
      for (int k = j + 1; k < adj[i].size(); ++k)
        ans[adj[i][j]][adj[i][k]] = ans[adj[i][k]][adj[i][j]] = 'Y';
}

inline void Solve4() {
  for (int i = 1; i <= m; ++i) {
    for (int j = 1; j <= m; ++j) {
      if (x[i] == x[j] || x[i] == y[j] || y[i] == x[j] || y[i] == y[j]) continue;
      if (flag[x[i]][x[j]]) ans[y[i]][y[j]] = ans[y[j]][y[i]] = 'Y';
      if (flag[x[i]][y[j]]) ans[y[i]][x[j]] = ans[x[j]][y[i]] = 'Y';
      if (flag[y[i]][x[j]]) ans[x[i]][y[j]] = ans[y[j]][x[i]] = 'Y';
      if (flag[y[i]][y[j]]) ans[x[i]][x[j]] = ans[x[j]][x[i]] = 'Y';
    }
  }
}

void Solve5() {
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= n; ++j) {
      if (i == j) continue;
      static std::vector<int> temp;
      temp.clear();
      std::set_intersection(adj[i].begin(), adj[i].end(), adj[j].begin(), adj[j].end(), std::back_inserter(temp));
      if (temp.empty()) continue;
      for (int p = 0; p < adj[i].size(); ++p) {
        int a = adj[i][p];
        if (a == j) continue;
        for (int q = 0; q < adj[j].size(); ++q) {
          int b = adj[j][q];
          if (b == i) continue;
          if (temp.size() > 2 || 
              (temp[0] != a && temp[0] != b) ||
              (temp.size() == 2 && temp[1] != a && temp[1] != b))
            ans[a][b] = ans[b][a] = 'Y';
        }
      }
    }
  }
}

void Solve6() {
  static int tag[N][N], tot = 0;
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= n; ++j) {
      if (i == j) continue;
      int w = 0;
      static int cnt[N];
      ++tot;
      for (int p = 0; p < adj[i].size(); ++p) cnt[adj[i][p]] = 0;
      for (int q = 0; q < adj[j].size(); ++q) cnt[adj[j][q]] = 0;
      for (int p = 0; p < adj[i].size(); ++p) {
        int a = adj[i][p];
        if (a == j) continue;
        for (int q = 0; q < adj[j].size(); ++q) {
          int b = adj[j][q];
          if (b == i) continue;
          if (flag[a][b]) {
            ++w, ++cnt[a], ++cnt[b];
            tag[a][b] = tot;
          }
        }
      }
      for (int p = 0; p < adj[i].size(); ++p) {
        int a = adj[i][p];
        if (a == j) continue;
        for (int q = 0; q < adj[j].size(); ++q) {
          int b = adj[j][q];
          if (b == i || a == b) continue;
          if (w - cnt[a] - cnt[b] + (tag[a][b] == tot) > 0) ans[a][b] = ans[b][a] = 'Y';
        }
      }
    }
  }
}

void Solve7() {
  static int tag[N][N];
  static std::vector<int> tri[N][N];
  memset(tag, 0, sizeof tag);
  for (int i = 1; i <= n; ++i)
    for (int j = 1; j <= n; ++j)
      tri[i][j].clear();
  for (int i = 1; i <= n; ++i) {
    for (int j = 0; j < adj[i].size(); ++j) {
      int a = adj[i][j];
      for (int k = j + 1; k < adj[i].size(); ++k) {
        int b = adj[i][k];
        tri[a][b].push_back(i);
        tri[b][a].push_back(i);
      }
    }
  }
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= n; ++j) {
      if (i == j) continue;
      int tot = 0;
      static int cnt[N], med[N], end[N][N], actual[N][N];
      cnt[i] = med[i] = cnt[j] = med[j] = 0;
      for (int p = 0; p < adj[i].size(); ++p) {
        int a = adj[i][p];
        cnt[a] = med[a] = 0;
      }
      for (int p = 0; p < adj[j].size(); ++p) {
        int a = adj[j][p];
        cnt[a] = med[a] = 0;
      }
      for (int p = 0; p < adj[i].size(); ++p) {
        int a = adj[i][p];
        if (a == j) continue;
        for (int q = 0; q < adj[j].size(); ++q) {
          int b = adj[j][q];
          if (b == i) continue;
          for (int d = 0; d < tri[a][b].size(); ++d) {
            int c = tri[a][b][d];
            if (c == i || c == j) continue;
            ++actual[a][b];
            ++tot;
            ++med[c];
            ++cnt[a], ++cnt[b];
            ++end[a][c], ++end[b][c];
          }
        }
      }
      for (int p = 0; p < adj[i].size(); ++p) {
        int a = adj[i][p];
        if (a == j) continue;
        for (int q = 0; q < adj[j].size(); ++q) {
          int b = adj[j][q];
          if (b == i) continue;
          if (tot - cnt[a] - cnt[b] - med[a] - med[b] + end[a][b] + end[b][a] + actual[a][b]) ans[a][b] = ans[b][a] = 'Y';
          if (ans[4][250] == 'Y') {
//          puts("STOP");
          }
        }
      }
      for (int p = 0; p < adj[i].size(); ++p) {
        int a = adj[i][p];
        for (int q = 0; q < adj[j].size(); ++q) {
          int b = adj[j][q];
          for (int d = 0; d < tri[a][b].size(); ++d) {
            int c = tri[a][b][d];
            actual[a][b] = 0;
            end[a][c] = end[b][c] = 0;
          }
        }
      }
    }
  }
}

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d%d", &n, &m, &k);
    for (int i = 1; i <= n; ++i) adj[i].clear();
    memset(flag, false, sizeof flag);
    for (int i = 1; i <= m; ++i) {
      scanf("%d%d", x + i, y + i);
      if (x[i] == y[i]) {
        --i, --m;
        continue;
      }
      flag[x[i]][y[i]] = flag[y[i]][x[i]] = true;
      adj[x[i]].push_back(y[i]);
      adj[y[i]].push_back(x[i]);
    }
    for (int i = 1; i <= n; ++i) {
      std::sort(adj[i].begin(), adj[i].end());
      adj[i].erase(std::unique(adj[i].begin(), adj[i].end()), adj[i].end());
    }
    memset(ans, 0, sizeof ans);
    for (int i = 1; i <= n; ++i)
      for (int j = 1; j <= n; ++j)
        ans[i][j] = 'N';
    switch (k) {
      case 2:
        Solve2();
        break;
      case 3:
        Solve3();
        break;
      case 4:
        Solve4();
        break;
      case 5:
        Solve5();
        break;
      case 6:
        Solve6();
        break;
      case 7:
        Solve7();
        break;
    }
    for (int i = 1; i <= n; ++i) ans[i][i] = 'N';
    for (int i = 1; i <= n; ++i) puts(ans[i] + 1);
  }
  return 0;
}
