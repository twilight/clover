#include <cstdio>
#include <cassert>
#include <cstring>
#include <algorithm>

typedef long long int64;

const int N = 1000000 + 10, MOD = 1000000007;

inline void MUL(int &a, int b) { a = (int64)a * b % MOD; }
inline int low_bit(int x) { return x & -x; }

int n;
char s[N];

int num[N];

void cal(char s[]) {
  static int next[N][20], dep[N];
  memset(num, 0, sizeof num);
  memset(dep, 0, sizeof dep);
  for (int i = 1; i <= n; ++i) {
    int j = i - 1;
    while (j && s[next[j][0] + 1] != s[i]) j = next[j][0];
    if (j && s[next[j][0] + 1] == s[i]) j = next[j][0] + 1;
    next[i][0] = j;
    for (int k = 1; k < 20; ++k) next[i][k] = next[next[i][k - 1]][k - 1];
    dep[i] = dep[j] + 1;
    for (int k = 19; k >= 0; --k) if (next[j][k] > i / 2) j = next[j][k];
    if (j > i / 2) j = next[j][0];
    num[i] += dep[j];
  }
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("zoo.in", "r", stdin);
  freopen("zoo.out", "w", stdout);
#endif
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf(" %s", s + 1);
    n = strlen(s + 1);
    cal(s);
    int ans = 1;
    for (int i = 1; i <= n; ++i) MUL(ans, num[i] + 1);
    printf("%d\n", ans);
  }
  return 0;
}
