#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>
#include <functional>

typedef long long i64;

const int LOG = 15, N = 1 << LOG, MOD = 479 * (1 << 21) + 1;

int n, m, x;
std::vector<int> s;

int log[N];

int PrimitiveRoot(int m) {
  for (int i = 1; i < m; ++i) {
    int t = 1;
    bool flag = true;
    for (int j = m - 2; j--;) {
      (t *= i) %= m;
      if (t == 1) {
        flag = false;
        break;
      }
    }
    if (flag) {
      for (int j = 0, t = 1; j < m - 1; ++j, (t *= i) %= m) log[t] = j;
      return i;
    }
  }
}

int w[N + 1], inv;

void NTT(int a[], int sw) {
  static int temp[N];
  for (int i = 0; i < N; ++i) {
    int k = 0;
    for (int j = 0; j < LOG; ++j) (k <<= 1) |= (i >> j & 1);
    temp[k] = a[i];
  }
  for (int i = 2; i <= N; i *= 2) {
    int gap = i / 2, layer = N / i;
    for (int j = 0; j < gap; ++j) {
      int w = (sw > 0 ? ::w[j * layer] : ::w[N - j * layer]);
      for (int k = j; k < N; k += i) {
        int u = temp[k], v = temp[k + gap];
        temp[k] = (u + (i64)w * v % MOD) % MOD;
        temp[k + gap] = (u - (i64)w * v % MOD + MOD) % MOD;
      }
    }
  }
  memcpy(a, temp, sizeof temp);
  if (sw < 0)
    for (int i = 0; i < N; ++i) a[i] = (i64)a[i] * inv % MOD;
}

void Convolution(int a[], const int b[]) {
  static int u[N], v[N];
  memcpy(u, a, sizeof u);
  memcpy(v, b, sizeof v);
  NTT(u, 1);
  NTT(v, 1);
  for (int i = 0; i < N; ++i) u[i] = (i64)u[i] * v[i] % MOD;
  NTT(u, -1);
  memset(a, 0, sizeof u);
  for (int i = 0; i < N; ++i)
    (((i >= log[0]) ? a[log[0]] : a[i % (m - 1)]) += u[i]) %= MOD;
}

int Pow(int base, int exp) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) res = (i64)res * base % MOD;
    base = (i64)base * base % MOD;
  }
  return res;
}

void Pow(int base[], int exp) {
  static int res[N];
  memset(res, 0, sizeof res);
  res[0] = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) Convolution(res, base);
    Convolution(base, base);
  }
  memcpy(base, res, sizeof res);
}

int main() {
  inv = Pow(N, MOD - 2);
  w[0] = 1, w[1] = Pow(3, (MOD - 1) / N);
  for (int i = 2; i <= N; ++i) w[i] = (i64)w[i - 1] * w[1] % MOD;
  int cnt;
  for (scanf("%d%d%d%d", &n, &m, &x, &cnt); cnt--;) {
    int t;
    scanf("%d", &t);
    s.push_back(t);
  }
  PrimitiveRoot(m);
  log[0] = 2 * m;
  static int temp[N];
  for (std::vector<int>::iterator it = s.begin(); it != s.end(); ++it) ++temp[log[*it]];
  Pow(temp, n);
  printf("%d\n", temp[log[x]]);
  return 0;
}
