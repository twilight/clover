#include <cstdio>
#include <cassert>
#include <cstring>
#include <algorithm>

#define bitcount(x) (__builtin_popcount(x))
#define lg2(x) (31 - __builtin_clz(x))

const int N = 32;

int len, a, b, c, ans;

bool Check(int gap) {
  static bool f[N][N][N][N][2];
  memset(f, 0, sizeof f);
  if (!(gap == 0 && (ans & 1))) {
    f[0][0][0][0][0] = true;
    f[0][1][1][0][1] = true;
  }
  if (!(gap == 0 && !(ans & 1))) {
    f[0][1][0][1][0] = true;
    f[0][0][1][1][0] = true;
  }
  for (int p = 0; p < len; ++p) {
    for (int i = 0; i <= a; ++i) {
      for (int j = 0; j <= b; ++j) {
        for (int k = 0; k <= c; ++k) {
          for (int add = 0; add < 2; ++add) {
            if (!f[p][i][j][k][add]) continue;
            for (int x = 0; x < 2; ++x) {
              for (int y = 0; y < 2; ++y) {
                for (int z = 0; z < 2; ++z) {
                  int u = i + x, v = j + y, w = k + z;
                  if (u > a || v > b || w > c) continue;
                  int g = x + y + add;
                  if ((g & 1) != z) continue;
                  if (p + 1 >= gap && (g & 1) != (ans >> (p + 1) & 1)) continue;
                  f[p + 1][u][v][w][g / 2] = true;
                }
              }
            }
          }
        }
      }
    }
  }
  return f[len][a][b][c][0];
}

int main() {
  scanf("%d%d%d", &a, &b, &c);
  if (!c) {
    puts(a || b ? "-1" : "0");
  } else if (!a || !b) {
    if (a > b) std::swap(a, b);
    printf("%d\n", (bitcount(b) == bitcount(c)) ? ((1 << bitcount(c)) - 1) : -1);
  } else {
    len = std::max(lg2(a), std::max(lg2(b), lg2(c)));
    a = bitcount(a), b = bitcount(b), c = bitcount(c), ans = 0;
    if (!Check(len + 1)) {
      puts("-1");
      return 0;
    }
    for (int i = len; i >= 0; --i) {
      if (!Check(i)) {
        ans |= (1 << i);
        assert(Check(i));
      }
    }
    printf("%d\n", ans);
  }
  return 0;
}
