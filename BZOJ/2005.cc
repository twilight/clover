#include <bits/stdc++.h>

typedef long long int64;

const int N = 100000 + 10;

int miu[N];

void preprocessing(int n) {
  memset(miu, 0, sizeof miu);
  static int prime[N], cnt;
  static bool flag[N];
  miu[1] = 1;
  for (int i = 2; i <= n; ++i) {
    if (!flag[i]) {
      prime[++cnt] = i;
      miu[i] = -1;
    }
    for (int j = 1; j <= cnt && i * prime[j] <= n; ++j) {
      flag[i * prime[j]] = true;
      if (i % prime[j]) {
        miu[i * prime[j]] = -miu[i];
      } else {
        miu[i * prime[j]] = 0;
        break;
      }
    }
  }
}

int64 cal(int n, int m) {
  int64 res = 0;
  static int prime[N], cnt;
  static bool flag[N];
  if (n > m) std::swap(n, m);
  preprocessing(n);
  for (int i = 1; i <= n; ++i) {
    int64 tmp = 0;
    for (int j = i; j <= n; j += i)
      tmp += (int64)(n / j) * (m / j) * miu[j / i];
    res += tmp * ((i - 1) * 2 + 1);
  }
  return res;
}

int main() {
  int n, m;
  scanf("%d%d", &n, &m);
  printf("%lld\n", cal(n, m));
  return 0;
}
