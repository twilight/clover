#include <cstdio>
#include <cstdlib>
#include <map>
#include <vector>
#include <algorithm>

typedef long long i64;
typedef std::pair<int, int> Info;

const int N = 10000000, BASE = 31;
const Info MOD(1000000007, 1000000009);

int n, m, x, y, z;

inline Info operator* (const Info &lhs, int rhs) {
  return Info((i64)lhs.first * rhs % MOD.first, (i64)lhs.second * rhs % MOD.second);
}

inline Info operator+ (const Info &lhs, int rhs) {
  return Info((lhs.first + rhs) % MOD.first, (lhs.second + rhs) % MOD.second);
}

inline Info operator- (const Info &lhs, const Info &rhs) {
  return Info(((lhs.first - rhs.first) % MOD.first + MOD.first) % MOD.first,
              ((lhs.second - rhs.second) % MOD.second + MOD.second) % MOD.second);
}

int main() {
  scanf("%d%d%d%d", &n, &m, &x, &y);
  static char *pool[2][N];
  for (int i = 1; i <= n; ++i) pool[0][i] = (char*)malloc((x + 10) * sizeof(char));
  for (int i = 1; i <= m; ++i) pool[1][i] = (char*)malloc((y + 10) * sizeof(char));
  for (int i = 1; i <= n; ++i) scanf(" %s", pool[0][i]);
  for (int i = 1; i <= m; ++i) scanf(" %s", pool[1][i]);
  z = (x + y) / 2;
  int u = 0, v = 1;
  static Info power[2];
  if (x < z) {
    std::swap(x, y);
    std::swap(u, v);
    std::swap(n, m);
    for (int i = 1; i <= n; ++i) std::reverse(pool[u][i], pool[u][i] + x);
    for (int i = 1; i <= m; ++i) std::reverse(pool[v][i], pool[v][i] + y);
  }
  power[0].first = power[0].second = power[1].first = power[1].second = 1;
  for (int i = x - z; i--;) power[0] = power[0] * BASE;
  for (int i = y; i--;) power[1] = power[1] * BASE;
  static std::map<Info, int> cnt;
  for (int it = 1; it <= n; ++it) {
    char *s = pool[u][it];
    Info pat(0, 0), text(0, 0), t(0, 0);
    for (int i = z; i < x; ++i) pat = pat * BASE + (s[i] - 'a' + 1);
    for (int i = 0; i < x - z; ++i) text = text * BASE + (s[i] - 'a' + 1);
    for (int i = x - z; i < z; ++i) t = t * BASE + (s[i] - 'a' + 1);
    static std::vector<Info> cur;
    cur.clear();
    for (int i = 0; i < z; ++i) {
      if (pat == text) cur.push_back(t);
      text = text * BASE - power[0] * (s[i] - 'a' + 1) + (s[(i + x - z) % z] - 'a' + 1);
      t = t * BASE - power[1] * (s[(i + x - z) % z] - 'a' + 1) + (s[i] - 'a' + 1);
    }
    std::sort(cur.begin(), cur.end());
    cur.erase(std::unique(cur.begin(), cur.end()), cur.end());
    for (int i = 0; i < cur.size(); ++i) ++cnt[cur[i]];
  }
  i64 ans = 0;
  for (int it = 1; it <= m; ++it) {
    char *s = pool[v][it];
    Info cur(0, 0);
    for (int i = 0; i < y; ++i) cur = cur * BASE + (s[i] - 'a' + 1);
    ans += cnt[cur];
  }
  printf("%lld\n", ans);
  return 0;
}
