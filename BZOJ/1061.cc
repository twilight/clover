#include <cstdio>
#include <cstring>
#include <queue>
#include <algorithm>

const int N = 1000 + 10, E = N * 100, INF = 0x01010101;

inline void cmin(int &a, int b) { if (b < a) a = b; }

int n, m, a[N];

int adj[N];
int to[E], next[E], cap[E], cost[E];

inline void link(int a, int b, int c, int d) {
  static int cnt = 0;
  to[cnt] = b;
  next[cnt] = adj[a];
  cap[cnt] = c;
  cost[cnt] = d;
  adj[a] = cnt++;
  to[cnt] = a;
  next[cnt] = adj[b];
  cap[cnt] = 0;
  cost[cnt] = -d;
  adj[b] = cnt++;
}

int dis[N], pre[N], e[N];

bool spfa(const int s, const int t) {
  static std::queue<int> q;
  static bool flag[N];
  q.push(s);
  memset(dis, 0x1, sizeof dis);
  memset(pre, -1, sizeof pre);
  memset(e, -1, sizeof e);
  memset(flag, false, sizeof flag);
  dis[s] = 0, pre[s] = 0;
  while (!q.empty()) {
    int a = q.front();
    q.pop();
    flag[a] = false;
    for (int it = adj[a]; it != -1; it = next[it]) {
      int b = to[it], c = cap[it], d = cost[it];
      if (c > 0 && dis[a] + d < dis[b]) {
        dis[b] = dis[a] + d;
        pre[b] = a;
        e[b] = it;
        if (!flag[b]) {
          q.push(b);
          flag[b] = true;
        }
      }
    }
  }
  return dis[t] < INF;
}

int flow(const int s, const int t) {
  int res = 0;
  while (spfa(s, t)) {
    int df = INF;
    for (int i = t; i != s; i = pre[i]) cmin(df, cap[e[i]]);
    res += df * dis[t];
    for (int i = t; i != s; i = pre[i]) {
      cap[e[i]] -= df;
      cap[e[i] ^ 1] += df;
    }
  }
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; ++i) scanf("%d", a + i);
  memset(adj, -1, sizeof adj);
  for (int i = 1; i <= m; ++i) {
    int s, t, c;
    scanf("%d%d%d", &s, &t, &c);
    link(s, t + 1, INF, c);
  }
  const int s = 0, t = n + 1;
  for (int i = 1; i <= n + 1; ++i) {
    link(i - 1, i, INF, 0);
    int det = a[i] - a[i - 1];
    det > 0 ? link(s, i, det, 0) : link(i, t, -det, 0);
  }
  printf("%d\n", flow(s, t));
  return 0;
}
