#include <cstdio>
#include <cstring>

typedef long long i64;

const int N = 500000 + 10;

char s[N];
int t;
i64 k;

struct node_t {
  node_t *next[26], *pre;
  int len, val;
  i64 size;
  bool mark;
} pool[2 * N], *tot = pool, *root = pool;

void Append(int ch) {
  static node_t *last = pool;
  node_t *np = ++tot, *p = last;
  np->len = p->len + 1;
  np->mark = true;
  for (; p && !p->next[ch]; p = p->pre) p->next[ch] = np;
  if (!p) {
    np->pre = root;
  } else {
    node_t *q = p->next[ch];
    if (q->len == p->len + 1) {
      np->pre = q;
    } else {
      node_t *nq = ++tot;
      *nq = *q;
      nq->mark = false;
      nq->len = p->len + 1;
      q->pre = np->pre = nq;
      for (; p && p->next[ch] == q; p = p->pre) p->next[ch] = nq;
    }
  }
  last = np;
}

int main() {
  scanf(" %s%d%lld", s, &t, &k);
  int n = strlen(s);
  for (char *it = s; *it != '\0'; ++it) Append(*it - 'a');
  static int cnt[N];
  for (node_t *it = pool; it <= tot; ++it) ++cnt[it->len];
  for (int i = 1; i <= n; ++i) cnt[i] += cnt[i - 1];
  static node_t *q[2 * N];
  for (node_t *it = pool; it <= tot; ++it) q[--cnt[it->len]] = it;
  if (t) {
    for (node_t *it = pool; it <= tot; ++it) if (it->mark) it->val = 1;
    for (int i = tot - pool; i > 0; --i) q[i]->pre->val += q[i]->val;
  } else {
    for (node_t *it = pool; it <= tot; ++it) it->val = 1;
  }
  root->val = 0;
  for (int i = tot - pool; i >= 0; --i) {
    q[i]->size = q[i]->val;
    for (int j = 0; j < 26; ++j)
      if (q[i]->next[j]) q[i]->size += q[i]->next[j]->size;
  }
  if (root->size < k || k <= 0) {
    puts("-1");
  } else {
    for (node_t *p = root;;) {
      if (k <= p->val) break;
      k -= p->val;
      for (int i = 0; i < 26; ++i) {
        if (p->next[i]) {
          if (k <= p->next[i]->size) {
            p = p->next[i];
            putchar(i + 'a');
            break;
          }
          k -= p->next[i]->size;
        }
      }
    }
    putchar('\n');
  }
  return 0;
}
