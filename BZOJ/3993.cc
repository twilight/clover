#include <cstdio>
#include <cstring>
#include <algorithm>

typedef long long i64;

const int N = 50 * 2 + 10, BASE = 100000;
const i64 INF = 1LL << 55;

int n, m;
i64 a[N], b[N];

int map[N][N];

int tot, s, t;
i64 cap[N][N];

int h[N], gap[N];

i64 DFS(int a, i64 df) {
  if (a == t) return df;
  i64 sum = 0;
  for (int b = 1; b <= tot; ++b) {
    if (h[a] == h[b] + 1 && cap[a][b]) {
      i64 f = DFS(b, std::min(cap[a][b], df - sum));
      cap[a][b] -= f;
      cap[b][a] += f;
      sum += f;
    }
    if (sum == df) return sum;
  }
  if (--gap[h[a]] == 0) h[s] = tot;
  ++gap[++h[a]];
  return sum;
}

bool Check(i64 g) {
  memset(cap, 0, sizeof cap);
  for (int i = 1; i <= m; ++i) cap[s][i] = b[i] * g;
  i64 sum = 0;
  for (int i = 1; i <= n; ++i) sum += (cap[i + m][t] = a[i]);
  for (int i = 1; i <= m; ++i)
    for (int j = 1; j <= n; ++j)
      if (map[i][j]) cap[i][j + m] = INF;
  memset(h, 0, sizeof h);
  memset(gap, 0, sizeof gap);
  while (h[s] < tot) sum -= DFS(s, INF);
  return !sum;
}

int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; ++i) scanf("%lld", a + i), a[i] *= BASE;
  for (int i = 1; i <= m; ++i) scanf("%lld", b + i);
  for (int i = 1; i <= m; ++i)
    for (int j = 1; j <= n; ++j)
      scanf("%d", &map[i][j]);
  s = n + m + 1, t = tot = n + m + 2;
  i64 l = 0, r = (i64)N * BASE * BASE;
  while (l < r) {
    i64 mid = (l + r) / 2;
    if (Check(mid)) r = mid; else l = mid + 1;
  }
  printf("%.5f\n", (double)l / BASE);
  return 0;
}
