#include <bits/stdc++.h>

#define x real()
#define y imag()
#define fst first
#define snd second

typedef std::complex<double> point;
typedef std::pair<point, point> line;

const int N = 200000 + 10;
const double eps = 1e-20, oo = 1e15;

int n;
double a[N], inf[N], sup[N];

inline double sqr(double a) { return a * a; }

inline double Solve(double a, double b, double c, double d) {
  return (-c + b * d) / a;
}

line HalfPlane(double a, double b, double c, bool sw) { // ax + by <= c
  point p(1.0, Solve(a, b, c, 1.0));
  point q;
  if (sw) q.x = 2.0; else q.x = 0.0;
  q.y = Solve(a, b, c, q.x);
  return line(p, q);
}

std::vector<line> halfplane;
std::vector<double> angle;

inline bool cmp(int a, int b) { return angle[a] < angle[b]; }

inline double cross(const point &a, const point &b) {
  return a.x * b.y - a.y * b.x;
}

inline double cross(const point &o, const point &a, const point &b) {
  return (a.x - o.x) * (b.y - o.y) - (a.y - o.y) * (b.x - o.x);
}

inline point Intersection(const line &u, const line &v) {
  point a(u.snd - u.fst), b(v.snd - v.fst), s(v.fst - u.fst);
  double d = cross(s, b) / cross(a, b);
  return u.fst + a * d;
}

inline int fcmp(double a, double b) {
  if (fabs(a - b) < eps) return 0;
  return a > b ? 1 : -1;
}

bool Check(int lim) {
  static int order[N];
  for (int i = 0; i < lim; ++i) order[i] = i;
  for (int i = 0; i < 4; ++i) order[lim + i] = n + i;
  std::sort(order, order + lim, cmp);
  std::deque<int> q;
  for (int it = 0; it < lim; ++it) {
    const line &cur = halfplane[order[it]];
    if (!q.empty() && fcmp(angle[q.back()] - angle[order[it]], 0.0) == 0) {
      if (fcmp(cross(cur.fst, cur.snd, halfplane[q.back()].fst), 0.0) < 0) continue;
      if (q.size() == 1) {
        q.back() = order[it];
        continue;
      }
    }
    while (q.size() > 1 && cross(cur.fst, cur.snd, Intersection(halfplane[q[q.size() - 2]], halfplane[q.back()])) >= 0) q.pop_back();
    while (q.size() > 1 && cross(cur.fst, cur.snd, Intersection(halfplane[q[0]], halfplane[q[1]])) >= 0) q.pop_front();
    q.push_back(order[it]);
  }
  while (q.size() > 2 && cross(halfplane[q.back()].fst, halfplane[q.back()].snd, Intersection(halfplane[q[0]], halfplane[q[1]])) >= 0) q.pop_back();
  while (q.size() > 2 && cross(halfplane[q[0]].fst, halfplane[q[0]].snd, Intersection(halfplane[q[q.size() - 2]], halfplane[q[q.size() - 1]])) >= 0) q.pop_back();
  return q.size() > 2;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d", &n);
  for (int i = 1; i <= n; ++i) scanf("%lf%lf%lf", a + i, inf + i, sup + i);
  int l = 1, r = n;
  for (int i = 1; i <= n; ++i) {
    halfplane.push_back(HalfPlane(sqr(a[i]), a[i], sup[i], 0));
    halfplane.push_back(HalfPlane(sqr(a[i]), a[i], inf[i], 1));
  }
  halfplane.push_back(line(point(-oo, -oo), point(oo, -oo)));
  halfplane.push_back(line(point(oo, -oo), point(oo, oo)));
  halfplane.push_back(line(point(oo, oo), point(-oo, oo)));
  halfplane.push_back(line(point(-oo, oo), point(-oo, -oo)));
  for (int i = 0; i < halfplane.size(); ++i) angle.push_back(std::arg(halfplane[i].snd - halfplane[i].fst));
  while (l < r) {
    int mid = (l + r + 1) / 2;
    if (Check(mid * 2)) l = mid; else r = mid - 1;
  }
  printf("%d\n", l);
  return 0;
}
