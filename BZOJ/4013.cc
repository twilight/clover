#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <algorithm>

typedef long long i64;

const int N = 100 + 10, MOD = 1000000007;

int n, m, x[N], y[N];
char op[N];

int anc[N];

int find(int u) { return anc[u] == u ? u : (anc[u] = find(anc[u])); }

std::vector<int> adj[N];

int mem[N][N];

int C(int n, int m) {
  if (m > n) return 0;
  if (~mem[n][m]) return mem[n][m];
  if (n - m < m) return C(n, n - m);
  if (!m) return 1;
  return mem[n][m] = (C(n - 1, m) + C(n - 1, m - 1)) % MOD;
}

int f[N][N];

void merge(int u, int v) {
  static int temp[N];
  memset(temp, 0, sizeof temp);
  for (int i = 1; i <= n; ++i) {
    if (!f[u][i]) continue;
    for (int j = 1; j <= n; ++j) {
      if (!f[v][j]) continue;
      for (int k = std::max(i, j); k <= i + j; ++k)
        (temp[k] += (i64)C(k, i) * C(i, i - (k - j)) % MOD * f[u][i] % MOD * f[v][j] % MOD) %= MOD;
    }
  }
  std::copy(temp + 1, temp + n + 1, f[u] + 1);
}

void DFS(int a) {
  static bool flag[N];
  if (flag[a]) {
    puts("0");
    exit(0);
  }
  flag[a] = true;
  bool exist = false;
  for (std::vector<int>::iterator it = adj[a].begin(); it != adj[a].end(); ++it) {
    int b = *it;
    DFS(b);
    if (!exist) {
      exist = true;
      std::copy(f[b] + 1, f[b] + n + 1, f[a] + 1);
    } else {
      merge(a, b);
    }
  }
  if (a) {
    for (int i = n; i > 0; --i) f[a][i] = f[a][i - 1];
    if (adj[a].empty()) f[a][1] = 1;
  }
}

int main() {
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; ++i) anc[i] = i;
  for (int i = 1; i <= m; ++i) {
    scanf("%d %c%d", x + i, op + i, y + i);
    if (op[i] == '=') anc[find(x[i])] = find(y[i]);
  }
  for (int i = 1; i <= m; ++i) if (op[i] == '<') adj[find(x[i])].push_back(find(y[i]));
  static int deg[N];
  for (int i = 1; i <= n; ++i) {
    std::sort(adj[i].begin(), adj[i].end());
    adj[i].erase(std::unique(adj[i].begin(), adj[i].end()), adj[i].end());
    for (std::vector<int>::iterator it = adj[i].begin(); it != adj[i].end(); ++it) ++deg[*it];
  }
  for (int i = 1; i <= n; ++i) if (find(i) == i && !deg[i]) adj[0].push_back(i);
  memset(mem, -1, sizeof mem);
  DFS(0);
  int ans = 0;
  for (int i = 1; i <= n; ++i) (ans += f[0][i]) %= MOD;
  printf("%d\n", ans);
  return 0;
}
