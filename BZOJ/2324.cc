#include <bits/stdc++.h>

const int N = 150 + 10, V = N * 2, E = V * V * 10, INF = 0x3f3f3f3f;

int n, m, k;
int dist[V][V];

int adj[V];
int to[E], next[E], cap[E], cost[E];

inline void link(int a, int b, int c, int d) {
  static int cnt = 2;
  to[cnt] = b, next[cnt] = adj[a], cap[cnt] = c, cost[cnt] = d, adj[a] = cnt++;
  to[cnt] = a, next[cnt] = adj[b], cap[cnt] = 0, cost[cnt] = -d, adj[b] = cnt++;
}

int dis[V], e[V], pre[V];

bool AugmentPath(const int s, const int t) {
  static int q[E], *qf, *qr;
  static bool flag[V];
  qf = qr = q;
  memset(dis, 0x3f, sizeof dis);
  for (dis[*qr++ = s] = 0; qf < qr;) {
    int a = *qf++;
    flag[a] = false;
    for (int it = adj[a]; it; it = next[it]) {
      int b = to[it], c = cost[it];
      if (cap[it] && dis[a] + c < dis[b]) {
        dis[b] = dis[a] + c;
        pre[b] = a;
        e[b] = it;
        if (!flag[b]) flag[*qr++ = b] = true;
      }
    }
  }
  return dis[t] < INF;
}

int MinCostFlow(const int s, const int t) {
  int res = 0;
  while (AugmentPath(s, t)) {
    int df = INF;
    for (int i = t; i != s; i = pre[i]) if (cap[e[i]] < df) df = cap[e[i]];
    res += dis[t] * df;
    for (int i = t; i != s; i = pre[i]) {
      cap[e[i]] -= df;
      cap[e[i] ^ 1] += df;
    }
  }
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d%d%d", &n, &m, &k);
  static int map[2][N], tot;
  for (int i = 0; i <= n; ++i) map[0][i] = ++tot, map[1][i] = ++tot;
  memset(dist, 0x3f, sizeof dist);
  for (int tmp = m, a, b, c; tmp--;) {
    scanf("%d%d%d", &a, &b, &c);
    if (c < dist[a][b]) dist[a][b] = c;
    dist[b][a] = dist[a][b];
  }
  for (int i = 0; i <= n; ++i) dist[i][i] = 0;
  for (int u = 0; u <= n; ++u)
    for (int i = 0; i <= n; ++i)
      for (int j = 0; j <= n; ++j)
        if (u <= std::max(i, j))
          dist[i][j] = std::min(dist[i][j], dist[i][u] + dist[u][j]);
  for (int i = 0; i <= n; ++i)
    for (int j = i + 1; j <= n; ++j)
      if (dist[i][j] < INF) link(map[1][i], map[0][j], INF, dist[i][j]);
  link(map[0][0], map[1][0], k, 0);
  const int ss = ++tot, tt = ++tot;
  for (int i = 1; i <= n; ++i) {
    link(map[1][0], map[0][i], INF, dist[0][i]);
    link(map[1][i], map[1][n], INF, 0);
    link(map[0][i], map[1][i], INF, 0);
    link(ss, map[1][i], 1, 0);
    link(map[0][i], tt, 1, 0);
  }
  link(map[1][n], map[0][0], INF, 0);
  printf("%d\n", MinCostFlow(ss, tt));
  return 0;
}
