/**************************************************************
    Problem: 3223
    User: dennisyang
    Language: C++
    Result: Accepted
    Time:2220 ms
    Memory:3616 kb
****************************************************************/
 
#include <iostream>
#include <algorithm>
 
const int maxn = 1e5 + 10;
 
int n,m;
 
struct node {
    int val,size;
    bool rev;
    node *p,*ch[2];
    void revIt() { rev ^= 1; }
    void relax() {
        if (rev) {
            std::swap(ch[0],ch[1]);
            ch[0]->revIt();
            ch[1]->revIt();
            rev = 0;
        }
    }
    void update() { size = ch[0]->size + ch[1]->size + 1; }
    void sc(node* c, int d) { ch[d] = c; c->p = this; }
    inline bool d() { return this == p->ch[1]; }
};
 
node pool[maxn],*tot = &pool[0],*nil = tot++,*root;
 
node* make(int x) {
    tot->ch[0] = tot->ch[1] = nil;
    tot->rev = false;
    tot->size = 1;
    tot->val = x;
    return tot++;
}
 
void rotate(node *cur) {
    node *p = cur->p;
    p->relax();
    cur->relax();
    int d = cur->d();
    p->p->sc(cur,p->d());
    p->sc(cur->ch[d^1],d);
    cur->sc(p,d^1);
    p->update(), cur->update();
    if (root == p) root = cur;
}
 
void splay(node *cur, node *target = nil) {
    while (cur->p != target) {
        if (cur->p->p == target) rotate(cur); else
            (cur->p->d() == cur->d()) ? (rotate(cur->p), rotate(cur)) : (rotate(cur), rotate(cur));
    }
    cur->update();
}
 
node* Init(int n) {
    root = make(0);
    root->p = nil;
    node *p,*q = root;
    for (int i=1; i<=n; ++i) {
        p = make(i);
        q->sc(p,1);
        q = p;
    }
    node *last = make(0);
    q->sc(last,1);
    splay(last);
}
 
node* select(int x) {
    for (node* cur = root;;) {
        cur->relax();
        int k = cur->ch[0]->size;
        if (x == k) return cur;
        if (x > k) x -= k+1, cur = cur->ch[1]; else cur = cur->ch[0];
    }
}
 
void Reverse(int l, int r) {
    node *x = select(l-1);
    node *y = select(r+1);
    splay(x);
    splay(y,root);
    y->ch[0]->revIt();
}
 
void Inorder(node *cur) {
    if (!cur || cur == nil) return;
    cur->relax();
    Inorder(cur->ch[0]);
    if (cur->val) std::cout<<cur->val<<' ';
    Inorder(cur->ch[1]);
}
 
int main() {
    std::cin>>n>>m;
    Init(n);
    int l,r;
    while (m--) {
        std::cin>>l>>r;
        Reverse(l,r);
    }
    Inorder(root);
    return 0;
}