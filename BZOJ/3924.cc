#include <cstdio>
#include <vector>

typedef long long i64;

const int N = 100000 + 10, E = N * 2, LOG = 20;

int n, tcase, root;
i64 tot = 0;

struct edge_t {
  int adj, cost;
  edge_t *next;
} *e[N], pool[E];

void link(int a, int b, int c) {
  static edge_t *tot = pool;
  tot->adj = b;
  tot->next = e[a];
  tot->cost = c;
  e[a] = tot++;
}

bool forbid[N];

int Centroid(int s) {
  static std::vector<int> q;
  static int size[N], fa[N];
  q.clear();
  q.push_back(s);
  fa[s] = 0;
  for (int i = 0; i < q.size(); ++i) {
    int a = q[i];
    size[a] = 1;
    for (edge_t *it = e[a]; it; it = it->next) {
      int b = it->adj;
      if (!forbid[b] && b != fa[a]) {
        q.push_back(b);
        fa[b] = a;
      }
    }
  }
  for (int i = q.size() - 1; i >= 0; --i) size[fa[q[i]]] += size[q[i]];
  int tot = size[s];
  for (int i = 0; i < q.size(); ++i) {
    int a = q[i];
    bool flag = (tot - size[a]) <= tot / 2;
    for (edge_t *it = e[a]; flag && it; it = it->next) {
      int b = it->adj;
      if (!forbid[b] && b != fa[a]) flag &= (size[b] <= tot / 2);
    }
    if (flag)
      return a;
  }
  return s;
}

int anc[N], from[N];
std::vector<int> succ[N];

int Divide(int a) {
  forbid[a = Centroid(a)] = true;
  for (edge_t *it = e[a]; it; it = it->next) {
    int b = it->adj;
    if (!forbid[b]) {
      int c = Divide(b);
      succ[a].push_back(c);
      anc[c] = a;
      from[c] = b;
    }
  }
  return a;
}

int fa[N], dep[N], left[N];
i64 sum[N], dist[N], f[N], sub[N];

std::vector<int> dfn, table[LOG];

void DFS(int a) {
  left[a] = dfn.size();
  dfn.push_back(a);
  for (edge_t *it = e[a]; it; it = it->next) {
    int b = it->adj;
    if (b != fa[a]) {
      fa[b] = a;
      dep[b] = dep[a] + 1;
      dist[b] = dist[a] + it->cost;
      DFS(b);
      dfn.push_back(a);
    }
  }
}

void BuildSparseTable() {
  table[0] = dfn;
  for (int i = 1; i < LOG; ++i) {
    table[i].resize(dfn.size());
    for (int j = 0; j + (1 << (i - 1)) <= dfn.size(); ++j) {
      int a = table[i - 1][j], b = table[i - 1][j + (1 << (i - 1))];
      table[i][j] = (dep[a] < dep[b] ? a : b);
    }
  }
}

inline int lg2(int x) { return 31 - __builtin_clz(x); }

int LCA(int a, int b) {
  a = left[a], b = left[b];
  if (a > b) std::swap(a, b);
  int layer = lg2(b - a + 1);
  int u = table[layer][a], v = table[layer][b - (1 << layer) + 1];
  return dep[u] < dep[v] ? u : v;
}

i64 Dist(int u, int v) { return dist[u] + dist[v] - 2 * dist[LCA(u, v)]; }

int FindCentroid(int a) {
  for (std::vector<int>::iterator it = succ[a].begin(); it != succ[a].end(); ++it) {
    int b = *it;
    if (sum[b] > tot / 2) {
      i64 out = sum[a] - sum[b];
      for (int t = from[b]; t; t = anc[t]) sum[t] += out;
      int res = FindCentroid(b);
      for (int t = from[b]; t; t = anc[t]) sum[t] -= out;
      return res;
    }
  }
  return a;
}

i64 Solve(int u, int e) {
  tot += e;
  for (int a = u; a; a = anc[a]) {
    sum[a] += e;
    if (anc[a]) sub[a] += e * Dist(u, anc[a]);
    f[a] += e * Dist(u, a);
  }
  int c = FindCentroid(root);
  i64 res = 0;
  for (int a = c, b = 0; a; b = a, a = anc[a]) {
    res += (f[a] - sub[b]) + (sum[a] - sum[b]) * Dist(c, a);
  }
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d%d", &n, &tcase);
  for (int cnt = n - 1, a, b, c; cnt--;) {
    scanf("%d%d%d", &a, &b, &c);
    link(a, b, c);
    link(b, a, c);
  }
  root = Divide(1);
  DFS(1);
  BuildSparseTable();
  while (tcase--) {
    int u, e;
    scanf("%d%d", &u, &e);
    printf("%lld\n", Solve(u, e));
  }
  return 0;
}
