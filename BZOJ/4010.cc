#include <cstdio>
#include <cstring>
#include <queue>
#include <vector>

const int N = 100000 + 10, E = N * 2;

int n, m, a[N], b[N];

std::vector<int> adj[N];

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d", &n, &m);
    static int deg[N];
    for (int i = 1; i <= n; ++i) {
      adj[i].clear();
      deg[i] = 0;
    }
    for (int i = 1; i <= m; ++i) {
      scanf("%d%d", a + i, b + i);
      adj[b[i]].push_back(a[i]);
      ++deg[a[i]];
    }
    static std::priority_queue<int> q;
    while (!q.empty()) q.pop();
    for (int i = 1; i <= n; ++i) if (deg[i] == 0) q.push(i);
    static bool flag[N];
    memset(flag, false, sizeof flag);
    static std::vector<int> ans;
    ans.clear();
    while (!q.empty()) {
      int a = q.top();
      q.pop();
      ans.push_back(a);
      for (std::vector<int>::iterator it = adj[a].begin(); it != adj[a].end(); ++it) {
        int b = *it;
        if (--deg[b] == 0) {
          if (flag[b]) goto fail;
          q.push(b);
          flag[b] = true;
        }
      }
    }
    if (ans.size() < n) goto fail;
    for (std::vector<int>::reverse_iterator it = ans.rbegin(); it != ans.rend(); ++it) printf("%d ", *it);
    putchar('\n');
    continue;
fail:
    puts("Impossible!");
  }
  return 0;
}
