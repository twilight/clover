#include <cstdio>
#include <vector>

typedef long long i64;

const int N = 3000000 + 10, MOD = 1000000007;

int n, m;

int Pow(int base, int exp) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) res = (i64)res * base % MOD;
    base = (i64)base * base % MOD;
  }
  return res;
}

int val[N], inv[N];

void Preprocessing() {
  static std::vector<int> prime;
  static bool flag[N];
  for (int i = 2; i < N; ++i) {
    if (!flag[i]) {
      prime.push_back(i);
      inv[i] = Pow(i, MOD - 2);
    }
    for (size_t j = 0; j < prime.size() && i * prime[j] < N; ++j) {
      flag[i * prime[j]] = true;
      inv[i * prime[j]] = (i64)inv[i] * inv[prime[j]] % MOD;
      if (i % prime[j] == 0) break;
    }
  }
  inv[0] = val[0] = inv[1] = val[1] = 1;
  for (int i = 2; i < N; ++i) {
    val[i] = (i64)val[i - 1] * i % MOD;
    inv[i] = (i64)inv[i - 1] * inv[i] % MOD;
  }
}

int C(int n, int m) {
  if (m > n || m < 0) return 0;
  return (i64)val[n] * inv[m] % MOD * inv[n - m] % MOD;
}

int Calc(int n, int m) { return C(n + m - 2, m - 1); }

int Solve(int n, int m) {
  int res = 0;
  for (int sw = -1, a = n, b = m;; sw = -sw) {
    if (~sw) {
      ++a;
      --b;
    } else {
      a += m - n + 1;
      b -= m - n + 1;
    }
    if (a <= 0 || b <= 0) break;
    (res += (sw * Calc(a, b) + MOD) % MOD) %= MOD;
  }
  for (int sw = -1, a = n, b = m;; sw = -sw) {
    if (~sw) {
      a -= m - n + 1;
      b += m - n + 1;
    } else {
      --a;
      ++b;
    }
    if (a <= 0 || b <= 0) break;
    (res += (sw * Calc(a, b) + MOD) % MOD) %= MOD;
  }
  return res;
}

int main() {
  scanf("%d%d", &n, &m);
  n += 1, m += 2;
  Preprocessing();
  printf("%d\n", ((Calc(n, n + m - 1) + Solve(n, n + m - 1)) % MOD + MOD) % MOD);
  return 0;
}
