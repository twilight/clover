#include <cstdio>
#include <algorithm>

const int N = 2e5 + 10;

int n, m;

struct node_t {
  node_t *lch, *rch;
  int id, rank, fa;
};

node_t *state[N], *cur;

node_t *build(int l, int r) {
  if (l > r) return NULL;
  node_t *res = new node_t;
  int mid = (l + r) / 2;
  res->lch = build(l, mid - 1);
  res->rch = build(mid + 1, r);
  res->rank = 0, res->fa = res->id = mid;
  return res;
}

node_t* find(node_t *cur, int k) {
  if (k == cur->id) return cur;
  return (k < cur->id) ? find(cur->lch, k) : find(cur->rch, k);
}

node_t* modify(node_t *cur, node_t *u) {
  if (cur->id == u->id) return u;
  node_t *res = new node_t;
  *res = *cur;
  if (u->id < res->id) res->lch = modify(res->lch, u); else res->rch = modify(res->rch, u);
  return res;
}

node_t* join(node_t *cur, int a, int b) {
  node_t *u = find(cur, a), *v = find(cur, b);
  while (u->fa != u->id) u = find(cur, u->fa);
  while (v->fa != v->id) v = find(cur, v->fa);
  if (u == v) return cur;
  node_t *res = cur;
  if (u->rank == v->rank) {
    node_t *x = new node_t, *y = new node_t;
    *x = *u, *y = *v;
    x->rank++, y->fa = x->id;
    res = modify(res, x);
    res = modify(res, y);
  } else {
    if (u->rank < v->rank) std::swap(u, v);
    node_t *x = new node_t;
    *x = *v;
    x->fa = u->id;
    res = modify(res, x);
  }
  return res;
}

inline int query(node_t *cur, int a, int b) {
  node_t *u = find(cur, a), *v = find(cur, b);
  while (u->fa != u->id) u = find(cur, u->fa);
  while (v->fa != v->id) v = find(cur, v->fa);
  return u == v;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d%d", &n, &m);
  cur = state[0] = build(1, n);
  int lastans = 0;
  for (int i = 1; i <= m; ++i) {
    int op, a, b;
    scanf("%d%d", &op, &a);
#ifdef ENCRYPTED
    a ^= lastans;
#endif
    switch (op) {
      case 1:
        scanf("%d", &b);
#ifdef ENCRYPTED
        b ^= lastans;
#endif
        cur = join(cur, a, b);
        break;
      case 2:
        cur = state[a];
        break;
      case 3:
        scanf("%d", &b);
#ifdef ENCRYPTED
        b ^= lastans;
#endif
        printf("%d\n", lastans = query(cur, a, b));
        break;
    }
    state[i] = cur;
  }
  return 0;
}
