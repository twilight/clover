#include <cstdio>
#include <algorithm>

const int N = 40 + 10, V = N * N * N, E = 20 * V, INF = 0x3f3f3f3f;
const int dx[] = {-1, 0, 1, 0}, dy[] = {0, 1, 0, -1};

int adj[V], s, t;
int to[E], next[E], cap[E];

inline void link(int a, int b, int c) {
  static int cnt = 2;
  to[cnt] = b, next[cnt] = adj[a], cap[cnt] = c, adj[a] = cnt++;
  to[cnt] = a, next[cnt] = adj[b], cap[cnt] = 0, adj[b] = cnt++;
}

namespace Graph {

int h[V], gap[V];

int dfs(int a, int df) {
  if (a == t) return df;
  int res = 0;
  for (int i = adj[a]; i; i = next[i]) {
    int b = to[i];
    if (cap[i] && h[a] == h[b] + 1) {
      int f = dfs(b, std::min(df - res, cap[i]));
      cap[i] -= f;
      cap[i ^ 1] += f;
      res += f;
    }
    if (res == df) return res;
  }
  if (--gap[h[a]] == 0) h[s] = V;
  ++gap[++h[a]];
  return res;
}

int flow() {
  int res = 0;
  while (h[s] < V) res += dfs(s, INF);
  return res;
}

}

int n, m, h, d, v[N][N][N];

inline int pos(int z, int x, int y) { return z * n * m + (x - 1) * m + y - 1; }

int main() {
  scanf("%d%d%d%d", &n, &m, &h, &d);
  for (int k = 1; k <= h; ++k)
    for (int i = 1; i <= n; ++i)
      for (int j = 1; j <= m; ++j)
        scanf("%d", &v[i][j][k]);
  s = (h + 1) * n * m, t = s + 1;
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= m; ++j) {
      for (int k = 1; k <= h; ++k) link(pos(k - 1, i, j), pos(k, i, j), v[i][j][k]);
      link(s, pos(0, i, j), INF);
      link(pos(h, i, j), t, INF);
    }
  }
  for (int i = 1; i <= n; ++i) {
    for (int j = 1; j <= m; ++j) {
      for (int dir = 0; dir < 4; ++dir) {
        int x = i + dx[dir], y = j + dy[dir];
        if (!(1 <= x && x <= n && 1 <= y && y <= m)) continue;
        for (int k = d; k <= h; ++k) link(pos(k, i, j), pos(k - d, x, y), INF);
      }
    }
  }
  printf("%d\n", Graph::flow());
  return 0;
}
