#include <cstdio>
#include <cmath>
#include <algorithm>

const int N = 100000 + 10;

int n, m, len, t[N];
char op[N][3];

inline int _1(int x) { return 1 << x; }
inline bool _1(int x, int pos) { return x & _1(pos); }

inline int cal(int pos, int val) {
  for (int i = 1; i <= n; ++i) {
    switch (op[i][0]) {
      case 'A':
        val &= _1(t[i], pos);
        break;
      case 'O':
        val |= _1(t[i], pos);
        break;
      case 'X':
        val ^= _1(t[i], pos);
        break;
    }
  }
  return val;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("sleep.in", "r", stdin);
  freopen("sleep.out", "w", stdout);
#endif
  scanf("%d%d", &n, &m);
  len = m;
  for (int i = 1; i <= n; ++i) {
    scanf(" %s%d", op[i], t + i);
    len = std::max(len, t[i]);
  }
  len = log(len) / log(2.0);
  bool lo = false;
  int ans = 0;
  for (int i = len; i >= 0; --i) {
    int t1 = cal(i, 0), t2 = -1;
    if (lo || (!lo && _1(m, i))) t2 = cal(i, 1);
    int t = t1 >= t2 ? 0 : 1;
    if (t < _1(m, i)) lo = true;
    if (std::max(t1, t2)) ans |= _1(i);
  }
  printf("%d\n", ans);
  return 0;
}
