#include <cstdio>
#include <numeric>

typedef long long int64;

const int N = 40000 + 10;

int n;
int phi[N];

void preprocessing() {
  static int prime[N], cnt;
  static bool flag[N];
  phi[1] = 1;
  for (int i = 2; i <= n; ++i) {
    if (!flag[i]) {
      prime[++cnt] = i;
      phi[i] = i - 1;
    }
    for (int j = 1; j <= cnt && i * prime[j] <= n; ++j) {
      flag[i * prime[j]] = true;
      if (i % prime[j]) {
        phi[i * prime[j]] = phi[i] * (prime[j] - 1);
      } else {
        phi[i * prime[j]] = phi[i] * prime[j];
        break;
      }
    }
  }
}

int main() {
  scanf("%d", &n);
  preprocessing();
  printf("%d\n", n ? std::accumulate(phi + 1, phi + n, 0) * 2 + 1: 0);
  return 0;
}
