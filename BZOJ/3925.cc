#include <cstdio>
#include <vector>
#include <algorithm>

const int N = 10, E = N * N;

typedef std::vector<__float128> Polynomial;

int n, m, a[E], b[E];

Polynomial operator* (const Polynomial &lhs, const Polynomial &rhs) {
  Polynomial temp(lhs.size() + rhs.size(), 0);
  for (int i = 0; i < lhs.size(); ++i)
    for (int j = 0; j < rhs.size(); ++j)
      temp[i + j] += lhs[i] * rhs[j];
  return temp;
}

Polynomial& operator+= (Polynomial &lhs, const Polynomial &rhs) {
  lhs.resize(std::max(lhs.size(), rhs.size()));
  for (int i = 0; i < rhs.size(); ++i) lhs[i] += rhs[i];
  return lhs;
}

Polynomial Pow(const Polynomial &a, int k) {
  Polynomial res(1, 1);
  while (k--) res = res * a;
  return res;
}

Polynomial f[1 << N];

inline double Solve(Polynomial &poly) {
  __float128 res = 0.0;
  for (int i = 1; i < poly.size(); ++i) res += poly[i] * i / (i + 1.0);
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= m; ++i) scanf("%d%d", a + i, b + i);
  Polynomial temp;
  temp.push_back(1);
  temp.push_back(-1);
  for (int i = 0; i < (1 << n); ++i) f[i].resize(1);
  for (int i = 0; i < (1 << n); ++i) {
    if (!(i & 1)) continue;
    for (int s = (i - 1) & i; s; s = (s - 1) & i) {
      int k = 0;
      if (!(s & 1)) continue;
      int t = i ^ s;
      for (int j = 1; j <= m; ++j) {
        int x = a[j] - 1, y = b[j] - 1;
        k += ((s >> x & 1) && (t >> y & 1)) ||
             ((s >> y & 1) && (t >> x & 1));
      }
      f[i] += f[s] * Pow(temp, k);
    }
    for (Polynomial::iterator it = f[i].begin(); it != f[i].end(); ++it) *it = -*it;
    ++f[i][0];
  }
  printf("%.6f\n", Solve(f[(1 << n) - 1]));
  return 0;
}
