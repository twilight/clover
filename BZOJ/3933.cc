#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>

#define fst first
#define snd second

typedef long long i64;
typedef std::pair<int, int> matrix;

const int N = 30000 + 10, MOD = 3389, BASE = 10000;

class BigInteger {
  std::vector<int> data;

 public:
  BigInteger(int num = 0) {
    data.clear();
    for (; num; num /= MOD) data.push_back(num % MOD);
    if (data.empty()) data.push_back(0);
  }

  inline size_t size() const { return data.size(); }
  inline int& operator[] (const size_t pos) { return data[pos]; }
  inline const int& operator[] (const size_t pos) const { return data[pos]; }

  inline BigInteger& operator+= (const BigInteger &rhs) {
    data.resize(std::max(data.size(), rhs.size()) + 1);
    for (size_t i = 0; i < rhs.size(); ++i) data[i] += rhs[i];
    for (size_t i = 1; i < data.size(); ++i) data[i] += data[i - 1] / BASE;
    for (size_t i = 0; i < data.size(); ++i) data[i] %= BASE;
    while (data.size() > 1 && data.back() == 0) data.pop_back();
    return *this;
  }

  inline BigInteger& operator++ () { return *this += 1; }

  inline BigInteger& operator*= (const BigInteger &rhs) {
    static std::vector<i64> temp;
    temp.resize(data.size() + rhs.size() + 1);
    std::fill(temp.begin(), temp.end(), 0);
    for (size_t i = 0; i < data.size(); ++i)
      for (size_t j = 0; j < rhs.size(); ++j)
        temp[i + j] += data[i] * rhs[j];
    for (size_t i = 1; i < temp.size(); ++i) temp[i] += temp[i - 1] / BASE;
    data.resize(temp.size());
    for (size_t i = 0; i < data.size(); ++i) data[i] = temp[i] % BASE;
    while (data.size() > 1 && data.back() == 0) data.pop_back();
    return *this;
  }

  inline BigInteger& operator/= (int rhs) {
    for (int i = data.size() - 1; i >= 0; --i) {
      if (i) data[i - 1] += data[i] % rhs * BASE;
      data[i] /= rhs;
    }
    while (data.size() > 1 && data.back() == 0) data.pop_back();
    return *this;
  }

  inline void Input();
  inline void Output();
} n, t, m;

BigInteger operator+ (const BigInteger &lhs, const BigInteger &rhs) {
  BigInteger res(lhs);
  res += rhs;
  return res;
}

int operator- (const BigInteger &lhs, const BigInteger &rhs) {
  int res = 0;
  for (int i = lhs.size() - 1; i >= 0; --i)
    (res *= BASE) += (lhs[i] - (i < rhs.size() ? rhs[i] : 0));
  return res;
}

BigInteger operator* (const BigInteger &lhs, const BigInteger &rhs) {
  BigInteger temp(lhs);
  temp *= rhs;
  return temp;
}

int operator% (const BigInteger &lhs, int rhs) {
  int res = 0;
  for (int i = lhs.size() - 1; i >= 0; --i) res = ((i64)res * BASE + lhs[i]) % rhs;
  return res;
}

void BigInteger::Input() {
  static char s[N];
  static const int base[] = {1, 10, 100, 1000};
  data.clear();
  scanf(" %s", s);
  int len = strlen(s);
  std::reverse(s, s + len);
  int cur = 0;
  for (int i = 0; i < len; ++i) {
    if (i && i % 4 == 0) {
      data.push_back(cur);
      cur = 0;
    }
    cur += (s[i] - '0') * base[i % 4];
  }
  if (cur || data.empty()) data.push_back(cur);
}

void BigInteger::Output() {
  printf("%d", data.back());
  for (int i = data.size() - 2; i >= 0; --i) printf("%04d", data[i]);
  putchar('\n');
}

int main() {
  n.Input(), t.Input(), m.Input();
  static int a[MOD];
  a[0] = 1;
  for (int i = 1; i < MOD; ++i) a[i] = (1234LL * a[i - 1] + 5678) % MOD;
  static BigInteger ans, C, temp;
  C = temp = 1;
  for (int i = 0, dif = n - m; i <= dif; ++i, ++m) {
    if (i) (C *= m) /= i, temp *= t;
    ans += C * temp * a[m % (MOD - 1)];
  }
  ans.Output();
  return 0;
}
