#include <cstdio>
#include <vector>
#include <algorithm>

typedef long long i64;

const int N = 100000 + 10, M = 200000 + 10, MOD = 1000000007;

int n, m, x, y, u[M], v[M];
std::vector<int> adj[N];

int Pow(int base, int exp) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) res = (i64)res * base % MOD;
    base = (i64)base * base % MOD;
  }
  return res;
}

int main() {
  scanf("%d%d%d%d", &n, &m, &x, &y);
  static int in[N], deg[N];
  in[y] = 1;
  for (int i = 1; i <= m; ++i) {
    scanf("%d%d", u + i, v + i);
    adj[u[i]].push_back(v[i]);
    ++in[v[i]];
  }
  int ans = 1;
  for (int i = 2; i <= n; ++i) ans = (i64)ans * in[i] % MOD;
  static std::vector<int> q;
  static bool flag[N];
  flag[y] = true;
  q.push_back(y);
  for (size_t i = 0; i < q.size(); ++i) {
    int a = q[i];
    for (std::vector<int>::iterator it = adj[a].begin(); it != adj[a].end(); ++it) {
      int b = *it;
      if (!flag[b]) {
        flag[b] = true;
        q.push_back(b);
      }
    }
  }
  for (int i = 1; i <= n; ++i) adj[i].clear();
  for (int i = 1; i <= m; ++i) {
    if (flag[u[i]] && flag[v[i]]) {
      adj[u[i]].push_back(v[i]);
      ++deg[v[i]];
    }
  }
  static int val[N], f[N];
  for (int i = 2; i <= n; ++i) val[i] = Pow(in[i], MOD - 2);
  f[y] = val[y];
  q.clear();
  q.push_back(y);
  for (size_t i = 0; i < q.size(); ++i) {
    int a = q[i];
    for (std::vector<int>::iterator it = adj[a].begin(); it != adj[a].end(); ++it) {
      int b = *it;
      f[b] = (f[b] + (i64)f[a] * val[b]) % MOD;
      if (--deg[b] == 0) q.push_back(b);
    }
  }
  printf("%lld\n", (ans - (i64)ans * f[x] % MOD + MOD) % MOD);
  return 0;
}
