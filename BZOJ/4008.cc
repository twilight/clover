#include <cmath>
#include <cstdio>
#include <cstring>

const int N = 300 + 10;

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    static int n, r, d[N];
    static double p[N];
    scanf("%d%d", &n, &r);
    for (int i = 1; i <= n; ++i) scanf("%lf%d", p + i, d + i);
    static double f[N][N];
    memset(f, 0, sizeof f);
    double ans = 0.0;
    f[0][r] = 1.0;
    for (int i = 0; i < n; ++i) {
      for (int j = r; j > 0; --j) {
        double skip = pow(1.0 - p[i + 1], j);
        f[i + 1][j] += f[i][j] * skip;
        f[i + 1][j - 1] += f[i][j] * (1.0 - skip);
        ans += f[i][j] * (1.0 - skip) * d[i + 1];
      }
    }
    printf("%.10f\n", ans);
  }
  return 0;
}
