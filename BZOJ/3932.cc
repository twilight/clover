#include <cstdio>
#include <vector>
#include <algorithm>

typedef long long i64;

const int N = 100000 + 10, SZ = N * 50;

int n, m, s[N], e[N], p[N];

struct node_t {
  node_t *lch, *rch;
  int size;
  i64 sum;
  i64 Query();
} pool[SZ], *tot = pool, *tree[N];

node_t *Modify(node_t *orig, int l, int r, int pos, int val) {
  node_t *res = ++tot;
  if (orig) *res = *orig;
  res->size += (val > 0 ? 1 : -1);
  res->sum += val;
  if (l < r) {
    int mid = (l + r) / 2;
    if (pos <= mid) res->lch = Modify(res->lch, l, mid, pos, val); else res->rch = Modify(res->rch, mid + 1, r, pos, val);
  }
  return res;
}

#define SIZE(p) ((p) ? ((p)->size) : 0)

std::vector<int> sorted;

i64 Query(node_t *cur, int l, int r, int k) {
  if (!cur || k <= 0) return 0;
  if (k >= cur->size) return cur->sum;
  if (l == r) return k * sorted[l - 1];
  int mid = (l + r) / 2;
  return Query(cur->lch, l, mid, k) + Query(cur->rch, mid + 1, r, k - SIZE(cur->lch));
}

int main() {
  scanf("%d%d", &n, &m);
  static std::vector<int> event[N];
  for (int i = 1; i <= n; ++i) {
    scanf("%d%d%d", s + i, e + i, p + i);
    sorted.push_back(p[i]);
    event[s[i]].push_back(i);
    event[e[i] + 1].push_back(-i);
  }
  std::sort(sorted.begin(), sorted.end());
  sorted.erase(std::unique(sorted.begin(), sorted.end()), sorted.end());
  for (int i = 1; i <= m; ++i) {
    tree[i] = tree[i - 1];
    for (std::vector<int>::iterator it = event[i].begin(); it != event[i].end(); ++it) {
      int val = p[*it > 0 ? *it : -*it];
      int pos = std::lower_bound(sorted.begin(), sorted.end(), val) - sorted.begin() + 1;
      tree[i] = Modify(tree[i], 1, sorted.size(), pos, *it > 0 ? val : -val);
    }
  }
  i64 pre = 1;
  for (int tcase = m; tcase--;) {
    int x, a, b, c;
    scanf("%d%d%d%d", &x, &a, &b, &c);
    printf("%lld\n", pre = Query(tree[x], 1, sorted.size(), 1 + (a * pre % c + b) % c));
  }
  return 0;
}
