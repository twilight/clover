#include <cstdio>
#include <cstring>

const int N = 50 + 5, MOD = 10000;

inline void INC(int &a, int b) { a = (a + b) % MOD; }
inline int pdt(int a, int b) { return (a * b) % MOD; }

typedef int matrix[N][N];

int n, m, k, st, ed, cnt, period[N];
matrix fish, map, graph[12 + 1];

inline void mul(matrix a, const matrix b) {
  static matrix c;
  memset(c, 0, sizeof(matrix));
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      for (int k = 0; k < n; ++k)
        INC(c[i][j], pdt(a[i][k], b[k][j]));
  memcpy(a, c, sizeof(matrix));
}

inline void pow(matrix a, int exp) {
  static matrix base, res;
  memcpy(base, a, sizeof(matrix));
  memset(res, 0, sizeof res);
  for (int i = 0; i < n; ++i) res[i][i] = 1;
  while (exp) {
    if (exp & 1) mul(res, base);
    mul(base, base);
    exp >>= 1;
  }
  memcpy(a, res, sizeof(matrix));
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d%d%d%d%d", &n, &m, &st, &ed, &k);
  while (m--) {
    int x, y;
    scanf("%d%d", &x, &y);
    map[x][y]++, map[y][x]++;
  }
  scanf("%d", &cnt);
  for (int i = 1; i <= cnt; ++i) {
    scanf("%d", period + i);
    for (int j = 0; j < period[i]; ++j) scanf("%d", &fish[i][j]);
  }
  for (int i = 0; i < n; ++i) graph[0][i][i] = 1;
  for (int i = 1; i <= 12; ++i) {
    static matrix tmp;
    memcpy(tmp, map, sizeof(matrix));
    for (int j = 1; j <= cnt; ++j) {
      int t = fish[j][i % period[j]];
      for (int k = 0; k < n; ++k) tmp[k][t] = 0;
    }
    memcpy(graph[i], graph[i - 1], sizeof(matrix));
    mul(graph[i], tmp);
  }
  static matrix ans;
  memcpy(ans, graph[12], sizeof(matrix));
  pow(ans, k / 12);
  mul(ans, graph[k % 12]);
  printf("%d\n", ans[st][ed]);
  return 0;
}
