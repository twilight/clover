{$inline on}
{$M 10000000}
{$DEFINE ONLINE_JUDGE}
const
  infinity=$3F3F3F3F;
  maxn=100010;

var
  root,i,n,tot,t,v,tmp,ans:longint;
  tree:array [0..maxn] of record
                            child:array [0..1] of longint;
                            data:longint;
                          end;

//Implementation of Splay Tree

function cmp(x,root:longint):longint; inline;
//To get which subtree the num x (should) exist
begin
  if tree[root].data=x then exit(-1);
  if tree[root].data>x then exit(0);
  exit(1);
end;

procedure rotate(f:longint; var root:longint); inline;
//f:flag (0:LeftRotate, 1:RightRotate)
var
  k:longint;

begin
  if root=0 then exit;
  k:=tree[root].child[f xor 1];
  tree[root].child[f xor 1]:=tree[k].child[f];
  tree[k].child[f]:=root;
  root:=k;
end;

procedure splay(x:longint; var root:longint);
var
  t1,t2,p:longint;

begin
  if root=0 then exit;
  t1:=cmp(x,root);
  if t1<>-1 then
    begin
      p:=tree[root].child[t1];
      t2:=cmp(x,p);
      if t2<>-1 then
        begin
          splay(x,tree[p].child[t2]);
          if t1=t2 then rotate(t1 xor 1,root) else rotate(t2 xor 1,tree[root].child[t1]);
        end;
      rotate(t1 xor 1,root);
    end;
end;

procedure ins(x:longint; var root:longint);
var
  t:longint;

begin
  if root=0 then
    begin
      inc(tot);
      root:=tot;
      tree[root].data:=x;
      exit;
    end;
  t:=cmp(x,root);
  if t=-1 then t:=random(2);
  ins(x,tree[root].child[t]);
end;

function min(p,q:longint):longint;
begin
  if p<q then exit(p) else exit(q);
end;

function findmin(root:longint):longint; inline;
begin
  if root=0 then exit(infinity);
  while tree[root].child[0]<>0 do root:=tree[root].child[0];
  exit(tree[root].data);
end;

function findmax(root:longint):longint; inline;
begin
  if root=0 then exit(-infinity);
  while tree[root].child[1]<>0 do root:=tree[root].child[1];
  exit(tree[root].data);
end;

//End of Splay Tree

begin
{$IFNDEF ONLINE_JUDGE}
  assign(input,'turnover.in');
  assign(output,'turnover.out');
  reset(input);
  rewrite(output);
{$ENDIF}
  randomize;
  readln(n);
  root:=0;
  tot:=0;
  for i:=1 to n do
    begin
      if eof then v:=0 else read(v);
      ins(v,root);
      splay(v,root); //notice: now tree[root].data=v
      if i=1 then begin ans:=v; continue; end;
      tmp:=infinity;
      tmp:=min(tmp,findmin(tree[root].child[1])-v);
      tmp:=min(tmp,v-findmax(tree[root].child[0]));
      inc(ans,tmp);
    end;
  writeln(ans);
{$IFNDEF ONLINE_JUDGE}
  close(input);
  close(output);
{$ENDIF}
end.
