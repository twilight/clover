#include <bits/stdc++.h>

const int N = 40 + 1, M = 100 + 1, V = N * M * 20, E = V * 10;
const int s = V - 2, t = V - 1;
const int INF = 0x3f3f3f3f;

int n, m, p[N], sum, w[N][M];

int adj[V];
int to[E], next[E], cap[E], cost[E];

inline void link(int a, int b, int c, int d) {
  static int cnt = 2;
  to[cnt] = b, next[cnt] = adj[a], cap[cnt] = c, cost[cnt] = d, adj[a] = cnt++;
  to[cnt] = a, next[cnt] = adj[b], cap[cnt] = 0, cost[cnt] = -d, adj[b] = cnt++;
}

inline int map(int a, int b) { return (a - 1) * sum + b + n; }
inline int calc(int id) {
  if (id <= n || id == s || id == t) return -1;
  return (id - n - 1) / sum + 1; 
}

int layer[M], cur[M], last[M];

inline void expand(int id) {
  cur[id] = map(id, ++layer[id]);
  for (int i = 1; i <= n; ++i)
    link(i, map(id, layer[id]), 1, w[i][id] * layer[id]);
  link(map(id, layer[id]), t, 1, 0);
}

int dis[V], e[V], pre[V];

bool sssp(const int s, const int t) {
  for (int i = 1; i <= m; ++i) if (last[i] == cur[i]) expand(i);
  static int q[E], *qf, *qr;
  static bool flag[V];
  memset(dis, 0x3f, sizeof dis);
  memset(flag, false, sizeof flag);
  qf = qr = q;
  for (dis[*qr++ = s] = 0; qf < qr;) {
    int a = *qf++;
    flag[a] = false;
    for (int it = adj[a]; it; it = next[it]) {
      int b = to[it], c = cost[it];
      if (cap[it] && dis[a] + c < dis[b]) {
        dis[b] = dis[a] + c;
        pre[b] = a;
        e[b] = it;
        if (!flag[b]) flag[*qr++ = b] = true;
      }
    }
  }
  return dis[t] < INF;
}

int MinCostFlow(const int s, const int t) {
  int res = 0;
  while (sssp(s, t)) {
    int df = INT_MAX;
    for (int i = t; i != s; i = pre[i]) df = std::min(df, cap[e[i]]);
    res += df * dis[t];
    for (int i = t; i != s; i = pre[i]) {
      int id = calc(i);
      if (~id) last[id] = std::max(last[id], i);
      cap[e[i]] -= df;
      cap[e[i] ^ 1] += df;
    }
  }
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d%d", &n, &m);
  for (int i = 1; i <= n; ++i) scanf("%d", p + i);
  sum = std::accumulate(p + 1, p + n + 1, 0);
  for (int i = 1; i <= n; ++i)
    for (int j = 1; j <= m; ++j)
      scanf("%d", &w[i][j]);
  for (int i = 1; i <= n; ++i) link(s, i, p[i], 0);
  printf("%d\n", MinCostFlow(s, t));
  return 0;
}
