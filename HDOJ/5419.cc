#include <cstdio>
#include <cstring>
#include <vector>
#include <iostream>

const int N = 50000 + 10;

typedef long long i64;

int n, m, w[N];

int tree[N];

inline void add(int p, int v) {
  for (; p <= n; p += p & -p) tree[p] += v;
}

inline int query(int p) {
  int res = 0;
  for (; p; p ^= p & -p) res += tree[p];
  return res;
}

i64 GCD(i64 a, i64 b) { return b ? GCD(b, a % b) : a; }

inline i64 calc(int x) {
  if (x < 3) return 0;
  return x * (x - 1LL) * (x - 2LL) / 6LL;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= n; ++i) scanf("%d", w + i);
    static std::vector<int> seg[N];
    for (int i = 1; i <= n; ++i) seg[i].clear();
    i64 denom = calc(m);
    while (m--) {
      int l, r;
      scanf("%d%d", &l, &r);
      seg[l].push_back(r);
    }
    if (denom == 0) {
      std::cout << 0 << std::endl;
      continue;
    }
    memset(tree, 0, sizeof tree);
    i64 ans = 0;
    for (int i = 1; i <= n; ++i) {
      for (int j = 0; j < seg[i].size(); ++j) add(n - seg[i][j] + 1, 1);
      ans += w[i] * calc(query(n - i + 1));
    }
    i64 temp = GCD(ans, denom);
    ans /= temp, denom /= temp;
    std::cout << ans;
    if (denom > 1) std::cout << '/' << denom;
    std::cout << std::endl;
  }
  return 0;
}
