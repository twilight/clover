#include <cstdio>

int x, y, w, n;

int main() {
  while (scanf("%d%d%d%d", &x, &y, &w, &n) != EOF) {
    for (int ans = 0, cnt = 0;; ++ans) {
      if (ans % (x + y) == 0) {
        cnt = 0;
        if (--n == 0) {
          printf("%d\n", ans);
          break;
        }
      } else if (ans % (x + y) <= x) {
        if (++cnt == w) {
          cnt = 0;
          if (--n == 0) {
            printf("%d\n", ans);
            break;
          }
        }
      }
    }
  }
  return 0;
}
