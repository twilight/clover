#include <cstdio>
#include <vector>
#include <algorithm>

const int N = 500 + 10, BASE = 10000;

int n;

struct BigInteger {
  std::vector<int> data;
  BigInteger(int x = 0) {
    data.clear();
    for (; x; x /= BASE) data.push_back(x % BASE);
    if (data.empty()) data.push_back(0);
  }
  inline size_t size() const { return data.size(); }
  inline int &operator[] (size_t pos) { return data[pos]; }
  inline int operator[] (size_t pos) const { return data[pos]; }
  BigInteger& operator+= (const BigInteger&);
  BigInteger& operator*= (int);
  BigInteger& operator/= (int);
  inline void Print();
};

BigInteger& BigInteger::operator+= (const BigInteger &rhs) {
  data.resize(std::max(size(), rhs.size()) + 1);
  for (int i = 0; i < rhs.size(); ++i) data[i] += rhs[i];
  for (int i = 1; i < size(); ++i) data[i] += data[i - 1] / BASE;
  for (int i = 0; i < size(); ++i) data[i] %= BASE;
  while (!data.empty() && data.back() == 0) data.pop_back();
  return *this;
}

BigInteger& BigInteger::operator*= (int rhs) {
  for (int i = 0; i < size(); ++i) data[i] *= rhs;
  for (int i = 0; i + 1 < size(); ++i) data[i + 1] += data[i] / BASE;
  for (int i = 0; i + 1 < size(); ++i) data[i] %= BASE;
  while (data.back() > BASE) {
    int temp = data.back() / BASE;
    data.back() %= BASE;
    data.push_back(temp);
  }
  return *this;
}

BigInteger operator* (const BigInteger &lhs, int rhs) {
  BigInteger res(lhs);
  res *= rhs;
  return res;
}

inline int operator% (const BigInteger &lhs, int rhs) {
  int res = 0;
  for (int i = lhs.size() - 1; i >= 0; --i) (res = res * BASE + lhs[i]) %= rhs;
  return res;
}

BigInteger& BigInteger::operator/= (int rhs) {
  for (int i = size() - 1; i > 0; --i) {
    data[i - 1] += data[i] % rhs * BASE;
    data[i] /= rhs;
  }
  data[0] /= rhs;
  while (!data.empty() && data.back() == 0) data.pop_back();
  return *this;
}

void Div(BigInteger &numer, BigInteger &denom, int x) {
  for (int i = 2; i * i <= x; ++i)
    for (; x % i == 0; x /= i)
      if (numer % i == 0) numer /= i; else denom *= i;
  if (numer % x == 0) numer /= x; else denom *= x;
}

inline void BigInteger::Print() {
  printf("%d", data.back());
  for (int i = data.size() - 2; i >= 0; --i) printf("%04d", data[i]);
}

int main() {
  static BigInteger fact[N];
  fact[1] = 1;
  for (int i = 2; i < N; ++i) fact[i] = fact[i - 1] * (2 * i - 1);
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d", &n);
    static BigInteger numer, denom;
    numer = fact[n];
    denom = 1;
    for (int i = n; i--;) Div(numer, denom, 2);
    for (int i = 1; i <= n; ++i) Div(numer, denom, i);
    numer.Print();
    putchar('/');
    denom.Print();
    putchar('\n');
  }
  return 0;
}
