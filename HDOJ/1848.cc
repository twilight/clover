#include <cstdio>
#include <cstring>

const int N = 1000 + 10;

int a, b, c;
int fib[N], sg[N], cnt;

int dfs(int cur) {
  if (~sg[cur]) return sg[cur];
  bool mex[N] = {false};
  for (int i = 1; i <= cnt && cur - fib[i] >= 0; ++i) mex[dfs(cur - fib[i])] = true;
  for (int i = 0; i < N; ++i) if (!mex[i]) return sg[cur] = i;
}

void cal() {
  fib[1] = 1, fib[2] = 2;
  for (cnt = 3; (fib[cnt] = fib[cnt - 1] + fib[cnt - 2]) <= N; ++cnt) {}
  memset(sg, -1, sizeof sg);
  sg[0] = 0;
  for (int i = 1; i < N; ++i) if (!~sg[i]) sg[i] = dfs(i);
}

int main() {
  cal();
  for (scanf("%d%d%d", &a, &b, &c); a | b | c; scanf("%d%d%d", &a, &b, &c))
    puts(sg[a] ^ sg[b] ^ sg[c] ? "Fibo" : "Nacci");
  return 0;
}
