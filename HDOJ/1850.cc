#include <cstdio>

const int N = 1000 + 10;

int n, x[N];

int main() {
  while (scanf("%d", &n), n) {
    int sg = 0, ans = 0;
    for (int i = 1; i <= n; ++i) {
      scanf("%d", x + i);
      sg ^= x[i];
    }
    if (sg) for (int i = 1; i <= n; ++i) ans += (x[i] >= (sg ^ x[i]));
    printf("%d\n", ans);
  }
  return 0;
}
