#include <cstdio>
#include <cstring>
#include <vector>
#include <algorithm>

typedef long long i64;

const int N = 200000 + 10;

int n, a[N];

int mx[N * 2], tag[N * 2];
i64 sum[N * 2];

inline int pos(int l, int r) { return (l + r) | (l != r); }

inline void Apply(int l, int r, int val) {
  int id = pos(l, r);
  mx[id] = tag[id] = val;
  sum[id] = (i64)(r - l + 1) * val;
}

void Modify(int l, int r, int p, int q, int val) {
  int id = pos(l, r);
  if (p <= l && r <= q) {
    Apply(l, r, val);
    return;
  }
  int mid = (l + r) / 2, lch = pos(l, mid), rch = pos(mid + 1, r);
  if (~tag[id]) {
    Apply(l, mid, tag[id]);
    Apply(mid + 1, r, tag[id]);
    tag[id] = -1;
  }
  if (p <= mid) Modify(l, mid, p, q, val);
  if (q > mid) Modify(mid + 1, r, p, q, val);
  mx[id] = std::max(mx[lch], mx[rch]);
  sum[id] = sum[lch] + sum[rch];
}

int Find(int l, int r, int val) {
  if (l == r) return l;
  int mid = (l + r) / 2;
  int lch = pos(l, mid);
  return mx[lch] > val ? Find(l, mid, val) : Find(mid + 1, r, val);
}

int main() {
  while (scanf("%d", &n), n) {
    static std::vector<int> sorted;
    sorted.clear();
    sorted.push_back(0);
    for (int i = 1; i <= n; ++i) {
      scanf("%d", a + i);
      sorted.push_back(a[i]);
      sorted.push_back(a[i] + 1);
    }
    std::sort(sorted.begin(), sorted.end());
    sorted.erase(std::unique(sorted.begin(), sorted.end()), sorted.end());
    static bool flag[N * 2];
    memset(tag, -1, sizeof tag);
    memset(flag, 0, sizeof flag);
    for (int i = 1, j = 0; i <= n; ++i) {
      flag[a[i] = std::lower_bound(sorted.begin(), sorted.end(), a[i]) - sorted.begin()] = true;
      while (j < sorted.size() && flag[j]) ++j;
      Modify(1, n, i, i, sorted[j]);
    }
    static int succ[N], last[N * 2];
    memset(succ, 0, sizeof succ);
    memset(last, -1, sizeof last);
    for (int i = 1; i <= n; ++i) {
      if (~last[a[i]]) succ[last[a[i]]] = i;
      last[a[i]] = i;
    }
    for (int i = 1; i <= n; ++i) if (!succ[i]) succ[i] = n + 1;
    i64 ans = sum[pos(1, n)];
    for (int i = 2; i <= n; ++i) {
      Modify(1, n, i - 1, i - 1, 0);
      int det = a[i - 1];
      int p = (mx[pos(1, n)] <= sorted[det] ? n + 1 : Find(1, n, sorted[det]));
      if (p < succ[i - 1]) Modify(1, n, p, succ[i - 1] - 1, sorted[det]);
      ans += sum[pos(1, n)];
    }
    printf("%I64d\n", ans);
  }
  return 0;
}
