#include <cstdio>
#include <cstring>
#include <algorithm>

const int N = 100 + 10;

int n, m, s[N];

int sg[N * N];

int cal(int x) {
  bool mex[N] = {false};
  if (~sg[x]) return sg[x];
  if (x < s[1]) return sg[x] = 0;
  for (int i = 1; i <= n && x >= s[i]; ++i) mex[cal(x - s[i])] = true;
  for (int i = 0;; ++i) if (!mex[i]) return sg[x] = i;
}

int main() {
  for (scanf("%d", &n); n; scanf("%d", &n)) {
    memset(sg, -1, sizeof sg);
    for (int i = 1; i <= n; ++i) scanf("%d", s + i);
    std::sort(s + 1, s + 1 + n);
    for (scanf("%d", &m); m--;) {
      int tmp, ans = 0;
      for (scanf("%d", &tmp); tmp--;) {
        int x;
        scanf("%d", &x);
        ans ^= cal(x);
      }
      putchar(ans ? 'W' : 'L');
    }
    putchar('\n');
  }
  return 0;
}
