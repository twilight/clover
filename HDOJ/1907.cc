#include <cstdio>

const int N = 50;

int n, a[N];

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d", &n);
    int cnt = 0, sg = 0;
    for (int i = 1; i <= n; ++i) {
      scanf("%d", a + i);
      sg ^= a[i];
      cnt += (a[i] == 1);
    }
    bool flag;
    if (cnt == n) flag = (sg == 0); else flag = (bool)sg;
    puts(flag ? "John" : "Brother");
  }
  return 0;
}
