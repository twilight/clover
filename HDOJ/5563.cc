#include <cmath>
#include <cstdio>
#include <algorithm>

const double eps = 1e-4;

int sgn(double x) {
  if (fabs(x) < eps) return 0.0;
  return x > 0.0 ? 1 : -1;
}

double x[5], y[5];

double dist(int a, int b) {
  return sqrt((x[a] - x[b]) * (x[a] - x[b]) + (y[a] - y[b]) * (y[a] - y[b]));
}

double calc(int a, int b, int c) {
  double t1 = x[a] - x[b], t2 = y[a] - y[b];
  double t3 = x[c] - x[b], t4 = y[c] - y[b];
  return acos((t1 * t3 + t2 * t4) / sqrt(t1 * t1 + t2 * t2) / sqrt(t3 * t3 + t4 * t4));
}

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    static int pal[5];
    for (int i = 0; i < 5; ++i) scanf("%lf%lf", x + i, y + i), pal[i] = i;
    bool flag = true;
    for (int i = 0; i < 5; ++i)
      for (int j = 0; j < 5; ++j)
        flag &= (sgn(x[i] - x[j]) == 0 && sgn(y[i] - y[j]) == 0);
    if (flag) goto success;
    do {
      bool flag = true;
      for (int i = 0; i < 5; ++i) {
        int a = pal[i], b = pal[(i + 1) % 5], c = pal[(i + 2) % 5], d = pal[(i + 3) % 5];
        if (!(sgn(dist(a, b) - dist(b, c)) == 0 && sgn(calc(a, b, c) - calc(b, c, d)) == 0)) {
          flag = false;
          break;
        }
      }
      if (flag) goto success;
    } while (std::next_permutation(pal, pal + 5));
    puts("No");
    continue;
 success:
    puts("Yes");
  }
  return 0;
}
