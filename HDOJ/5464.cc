#include <cstdio>
#include <cstring>

const int N = 1000 + 10, MOD = 1000000007;

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    static int f[N];
    memset(f, 0, sizeof f);
    int n, p;
    scanf("%d%d", &n, &p);
    f[0] = 1;
    while (n--) {
      int x;
      scanf("%d", &x);
      x = (x % p + p) % p;
      static int g[N];
      memcpy(g, f, sizeof g);
      for (int i = 0; i < p; ++i) (g[(i + x) % p] += f[i]) %= MOD;
      memcpy(f, g, sizeof f);
    }
    printf("%d\n", f[0]);
  }
  return 0;
}
