#include <cstdio>
#include <cmath>
#include <complex>
#include <vector>
#include <algorithm>

#define x real()
#define y imag()

typedef std::complex<double> point;

const int N = 100000 + 10;
const double oo = 1e20;

inline double sqr(double a) { return a * a; }

int n;
point p[N];

inline bool cmpx(const point &a, const point &b) { return a.x < b.x; }
inline bool cmpy(const point &a, const point &b) { return a.y < b.y; }

inline double dist(const point &a, const point &b) {
  return sqrt(sqr(a.x - b.x) + sqr(a.y - b.y));
}

double solve(int l, int r) {
  if (l >= r) return oo;
  int mid = (l + r) / 2;
  double res = std::min(solve(l, mid), solve(mid + 1, r));
  static std::vector<point> tmp;
  tmp.clear();
  for (int i = mid; l <= i && abs(p[i].x - p[mid].x) < res; --i) tmp.push_back(p[i]);
  for (int i = mid + 1; i <= r && abs(p[i].x - p[mid + 1].x) < res; ++i) tmp.push_back(p[i]);
  std::sort(tmp.begin(), tmp.end(), cmpy);
  for (int i = 0; i < tmp.size(); ++i)
    for (int j = 1; j <= 3 && i + j < tmp.size(); ++j)
      res = std::min(res, dist(tmp[i], tmp[i + j]));
  return res;
}

int main() {
  while (scanf("%d", &n), n) {
    for (int i = 1; i <= n; ++i) scanf("%lf%lf", &p[i].x, &p[i].y);
    std::sort(p + 1, p + n + 1, cmpx);
    printf("%.2lf\n", solve(1, n) / 2.0);
  }
  return 0;
}
