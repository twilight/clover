#include <cstdio>
#include <cstring>

typedef long long i64;

const int N = 80, MOD = 1000000007;

typedef int Matrix[N][N];

void multiply(Matrix lhs, const Matrix rhs) {
  static Matrix res;
  memset(res, 0, sizeof res);
  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j)
      for (int k = 0; k < N; ++k)
        (res[i][j] += (i64)rhs[i][k] * lhs[k][j] % MOD) %= MOD;
  memcpy(lhs, res, sizeof res);
}

void power(Matrix base, int exp) {
  static Matrix res;
  memset(res, 0, sizeof res);
  for (int i = 0; i < N; ++i) res[i][i] = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) multiply(res, base);
    multiply(base, base);
  }
  memcpy(base, res, sizeof res);
}

inline int pos(int x, int y) { return x * 10 + y; }

int calc(int lim, int k) {
  if (!lim) return 0;
  static Matrix trans[6], cycle;
  static const int val[] = {3, 2, 6, 4, 5, 1};
  memset(trans, 0, sizeof trans);
  for (int i = 0; i < 6; ++i) {
    for (int a = 0; a < 7; ++a) {
      for (int b = 0; b < 10; ++b) {
        for (int c = 0; c < 10; ++c)
          if (b + c != k) trans[i][pos((a + val[i] * c) % 7, c)][pos(a, b)] += 1;
        if (b) trans[i][N - 1][b] = 1;
      }
    }
    trans[i][N - 1][N - 1] = 1;
  }
  memcpy(cycle, trans[0], sizeof cycle);
  for (int i = 1; i < 6; ++i) multiply(cycle, trans[i]);
  power(cycle, lim / 6);
  for (int i = 0; i < lim % 6; ++i) multiply(cycle, trans[i]);
  int res = 0;
  for (int i = 0; i < 10; ++i) (res += cycle[N - 1][pos(i % 7, i)]) %= MOD;
  return res;
}

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    int l, r, k;
    scanf("%d%d%d", &l, &r, &k);
    printf("%d\n", (calc(r, k) - calc(l - 1, k) + MOD) % MOD);
  }
  return 0;
}
