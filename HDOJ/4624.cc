#include <cstring>
#include <iomanip>
#include <iostream>

typedef long long i64;

const int N = 50 + 10;

int n;

i64 pool[N][2][N * N];

inline int calc(int x) { return x * (x + 1) / 2; }

int main() {
  int tcase;
  for (std::cin >> tcase; tcase--;) {
    std::cin >> n;
    memset(pool, 0, sizeof pool);
    int tot = calc(n);
    pool[0][1][0] = 1;
    for (int i = 0; i <= n; ++i) {
      for (int j = 0; j < 2; ++j) {
        for (int k = 0; k <= tot; ++k) {
          if (!pool[i][j][k]) continue;
          for (int x = i + 1; x <= n + 1; ++x)
            pool[x][j ^ 1][k + calc(x - i - 1)] += pool[i][j][k];
        }
      }
    }
    __float128 ans = 0.0;
    for (int k = 0; k < 2; ++k) {
      int sw = (k ? 1 : -1);
      for (int i = 0; i < tot; ++i) {
        i64 cur = pool[n + 1][k][i];
        if (cur) ans += sw * cur * 1.0 / (1.0 - (__float128)i / tot);
      }
    }
    std::cout << std::fixed << std::setprecision(15) << (long double)ans << std::endl;
  }
  return 0;
}
