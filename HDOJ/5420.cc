#include <cstdio>
#include <stack>
#include <vector>
#include <iostream>
#include <algorithm>

typedef long long i64;

const int N = 100000 + 10, V = N * 100, E = V * 5;

int n, fa[N], dep[N];

int adj[V], to[E], next[E], cnt;

int tot;

inline void link(int a, int b) {
  if (!a || !b) return;
  to[cnt] = b;
  next[cnt] = adj[a];
  adj[a] = cnt++;
}

int root[N], lch[V], rch[V];

int append(int orig, int l, int r, int p, int v) {
  int res = ++tot;
  adj[res] = 0;
  link(res, orig);
  link(res, v);
  lch[res] = lch[orig], rch[res] = rch[orig];
  if (l == r) return res;
  int mid = (l + r) / 2;
  if (p <= mid) lch[res] = append(lch[orig], l, mid, p, v); else rch[res] = append(rch[orig], mid + 1, r, p, v);
  return res;
}

int merge(int lhs, int rhs, int l, int r) {
  if (!lhs) return rhs;
  if (!rhs) return lhs;
  int res = ++tot;
  lch[res] = 0, rch[res] = 0, adj[res] = 0;
  link(res, lhs);
  link(res, rhs);
  if (l == r) return res;
  int mid = (l + r) / 2;
  lch[res] = merge(lch[lhs], lch[rhs], l, mid);
  rch[res] = merge(rch[lhs], rch[rhs], mid + 1, r);
  return res;
}

void query(int id, int l, int r, int p, int q, int v) {
  if (!id) return;
  if (p <= l && r <= q) {
    link(v, id);
    return;
  }
  int mid = (l + r) / 2;
  if (p <= mid) query(lch[id], l, mid, p, q, v);
  if (q > mid) query(rch[id], mid + 1, r, p, q, v);
}

int dfn[V], low[V], stk[V], top, scc, size[V], cur;

bool flag[V];
int arc[V];

void tarjan(int s) {
  std::stack<int> stack;
  static int tag[V], e[V], cnt;
  ++cnt;
  for (stack.push(s); !stack.empty();) {
    int a = stack.top();
    if (tag[a] != cnt) {
      tag[a] = cnt;
      flag[stk[++top] = a] = true;
      dfn[a] = low[a] = ++cur;
    }
    int i = arc[a];
    if (!i) {
      if (low[a] == dfn[a]) {
        size[++scc] = 0;
        do {
          size[scc] += (stk[top] <= n);
          flag[stk[top]] = false;
        } while (stk[top--] != a);
      }
      stack.pop();
      continue;
    }
    int b = to[i];
    if (!dfn[b]) {
      stack.push(b);
    } else {
      arc[a] = next[i];
      if (flag[b]) low[a] = std::min(low[a], low[b]);
    }
  }
}

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d", &n);
    cnt = 2;
    dep[1] = 1;
    for (int i = 1; i <= n; ++i) adj[i] = 0;
    for (int i = 2; i <= n; ++i) {
      scanf("%d", fa + i);
      dep[i] = dep[fa[i]] + 1;
    }
    tot = n;
    for (int i = 1; i <= n; ++i) root[i] = 0;
    for (int i = n; i > 0; --i) {
      root[i] = append(root[i], 1, n, dep[i], i);
      if (fa[i]) root[fa[i]] = merge(root[fa[i]], root[i], 1, n);
    }
    for (int i = 1; i <= n; ++i) {
      int x, d;
      scanf("%d%d", &x, &d);
      query(root[x], 1, n, dep[x], std::min(dep[x] + d, n), i);
    }
    for (int i = 1; i <= tot; ++i) dfn[i] = low[i] = 0, arc[i] = adj[i], flag[i] = false;
    scc = cur = 0;
    for (int i = 1; i <= n; ++i) if (!dfn[i]) tarjan(i);
    i64 ans = 0;
    for (int i = 1; i <= scc; ++i) ans += size[i] * (size[i] - 1LL) / 2LL;
    std::cout << ans << std::endl;
  }
  return 0;
}
