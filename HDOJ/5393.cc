#include <cstdio>
#include <vector>

typedef long long i64;

const int N = 100000 + 10;

int x, a, c, p;

int GCD(int a, int b) {
  do {
    int r = a % b;
    a = b, b = r;
  } while (b);
  return a;
}

inline int LCM(int a, int b) { return a / GCD(a, b) * b; }

std::vector<int> prime;

void Preprocessing() {
  static bool flag[N];
  for (int i = 2; i < N; ++i) {
    if (!flag[i]) prime.push_back(i);
    for (int j = 0; j < prime.size() && (i64)i * prime[j] < N; ++j) {
      flag[i * prime[j]] = true;
      if (i % prime[j] == 0) break;
    }
  }
}

int f(int k, int m) {
  int res = x % m, u = a % m, v = c % m;
  for (; k; k >>= 1) {
    if (k & 1) res = ((i64)res * u + v) % m;
    v = (i64)v * (u + 1) % m;
    u = (i64)u * u % m;
  }
  return res;
}

int Power(int base, int exp) {
  int res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) res *= base;
    base *= base;
  }
  return res;
}

int Calc(int u, int v) {
  int m = Power(u, v);
  int res = m;
  if (GCD(a - 1, m) == 1) res = res / u * (u - 1);
  int cur = f(m, m), t = res;
  for (int i = 0; i < prime.size() && prime[i] * prime[i] <= t; ++i) {
    while (res % prime[i] == 0 && cur == f(m + res / prime[i], m))
      res /= prime[i];
    while (t % prime[i] == 0) t /= prime[i];
  }
  if (t > 1) while (res % t == 0 && cur == f(m + res / t, m)) res /= t;
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  Preprocessing();
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d%d%d", &a, &c, &x, &p);
    int ans = 1, q = p;
    for (int i = 0; i < prime.size() && prime[i] * prime[i] <= q; ++i) {
      int u = prime[i], v = 0;
      for (; q % prime[i] == 0; q /= prime[i]) ++v;
      if (v) ans = LCM(ans, Calc(u, v));
    }
    if (q > 1) ans = LCM(ans, Calc(q, 1));
    if (f(ans, p) != x % p) puts("-1"); else printf("%d\n", ans);
  }
  return 0;
}
