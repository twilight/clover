#include <cstdio>
#include <cstring>

const int N = 50 + 1, LIM = 1000 + 1;

int sg[N][N * LIM];

int solve(int cnt, int sum) {
  int &res = sg[cnt][sum];
  if (~res) return res;
  if (sum == 1) return res = solve(cnt + 1, 0);
  if (cnt == 0) return res = sum & 1;
  bool flag = true;
  if (sum > 0) flag &= solve(cnt, sum - 1);
  if (cnt > 0) flag &= solve(cnt - 1, sum);
  if (sum > 0 && cnt > 0) flag &= solve(cnt - 1, sum + 1);
  if (cnt > 1) flag &= solve(cnt - 2, sum + 2 + (sum > 0));
  return res = !flag;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int tcase;
  scanf("%d", &tcase);
  memset(sg, -1, sizeof sg);
  for (int t = 1; t <= tcase; ++t) {
    static int n, a[N];
    scanf("%d", &n);
    int cnt, sum;
    cnt = sum = 0;
    for (int i = 1; i <= n; ++i) {
      scanf("%d", a + i);
      if (a[i] > 1) sum += (a[i] + 1); else cnt++;
    }
    if (sum) sum--;
    printf("Case #%d: %s\n", t, solve(cnt, sum) ? "Alice" : "Bob");
  }
  return 0;
}
