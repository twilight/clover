#include <cstdio>
#include <cstring>

const int N = 1000 + 10;

int n, m;
int sg[N];

int cal(int x) {
  if (~sg[x]) return sg[x];
  bool mex[N] = {false};
  for (int i = 1; i <= m && x >= i; ++i) mex[cal(x - i)] = true;
  for (int i = 0; i < N; ++i) if (!mex[i]) return sg[x] = i;
}

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d", &n, &m);
    memset(sg, -1, sizeof sg);
    sg[0] = 0;
    puts(cal(n) ? "first" : "second");
  }
  return 0;
}
