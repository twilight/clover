#include <cstdio>
#include <cstring>

using namespace std;

const int maxn=300000,num=2012;

struct SAM
{
	SAM *pre,*next[11];
	int cnt,sum,k;
	SAM(): cnt(0), sum(0), k(0), pre(NULL) {memset(next,0,sizeof(next));}
} *root,*last,sam[maxn],*a[maxn];

int tot,len,sum[maxn];

char s[100000];

void add(int ch, int l)
{
	SAM *p=last,*np=&sam[tot++];
	np->k=l;
	for (; p && p->next[ch]==NULL; p=p->pre) p->next[ch]=np;
	if (p==NULL) np->pre=root; else
	{
		SAM *q=p->next[ch];
		if (p->k+1==q->k) np->pre=q; else
		{
			SAM *nq=&sam[tot++];
			*nq=*q;
			nq->k=p->k+1;
			np->pre=q->pre=nq;
			for (; p && p->next[ch]==q; p=p->pre) p->next[ch]=nq;
		}
	}
	last=np;
}

int solve()
{
	memset(sum,0,sizeof(sum));
	for (int i=0; i<tot; ++i) ++sum[sam[i].k];
	for (int i=1; i<len; ++i) sum[i]+=sum[i-1];
	memset(a,0,sizeof(a));
	for (int i=0; i<tot; ++i) a[--sum[sam[i].k]]=&sam[i];
	root->cnt=1,root->sum=0;
	int res=0;
	for (int i=0; i<tot; ++i)
	{
		SAM *p=a[i];
		for (int j=0; j<10; ++j)
		{
			if (i==0 && j==0) continue;
			if (p->next[j])
			{
				SAM *q=p->next[j];
				q->cnt=(q->cnt+p->cnt)%num;
				q->sum=(q->sum+p->sum*10+p->cnt*j)%num;
			}
		}
		res=(res+p->sum)%num;
	}
	return res;
}

int main()
{
	int n;
	while (~scanf("%d",&n))
	{
		tot=0,len=1;
		root=last=&sam[tot++];
		memset(sam,0,sizeof(sam));
		while (n--)
		{
			scanf("%s",s);
			for (int i=0; s[i]; ++i) add(s[i]-'0',len++);
			add(10,len++);
		}
		printf("%d\n",solve());
	}
	return 0;
}
