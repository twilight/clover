#include <cstdio>
#include <cstring>
#include <algorithm>

const int N = 16, INF = 0x3f3f3f3f;

int n, m;

void check(int &lhs, int rhs) {
  if (rhs < lhs) lhs = rhs;
}

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    static int dist[N][N];
    memset(dist, 0x3f, sizeof dist);
    scanf("%d%d", &n, &m);
    while (m--) {
      int u, v, w;
      scanf("%d%d%d", &u, &v, &w);
      --u, --v;
      check(dist[u][v], w);
      check(dist[v][u], w);
    }
    for (int i =0; i < n; ++i) dist[i][i] = 0;
    for (int k = 0; k < n; ++k)
      for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
          check(dist[i][j], dist[i][k] + dist[k][j]);
    static int f[N][1 << N];
    memset(f, 0x3f, sizeof f);
    f[0][1] = 0;
    for (int s = 0; s < (1 << n); ++s) {
      for (int i = 0; i < n; ++i) {
        if (f[i][s] == INF) continue;
        for (int j = 0; j < n; ++j) {
          check(f[j][s | (1 << j)], f[i][s] + dist[i][j]);
        }
      }
    }
    printf("%d\n", f[0][(1 << n) - 1]);
  }
  return 0;
}
