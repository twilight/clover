#include <cstdio>
#include <cstring>

const int N = 1000 + 10;

int a;
int sg[N], cnt;

int dfs(int cur) {
  if (~sg[cur]) return sg[cur];
  bool mex[N] = {false};
  for (int i = 1; cur - i >= 0; i *= 2) mex[dfs(cur - i)] = true;
  for (int i = 0; i < N; ++i) if (!mex[i]) return sg[cur] = i;
}

void cal() {
  memset(sg, -1, sizeof sg);
  sg[0] = 0;
  for (int i = 1; i < N; ++i) if (!~sg[i]) sg[i] = dfs(i);
}

int main() {
  cal();
  while (scanf("%d", &a) != EOF) puts(sg[a] ? "Kiki" : "Cici");
  return 0;
}
