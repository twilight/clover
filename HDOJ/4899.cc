#include <cstdio>
#include <cstring>
#include <algorithm>

typedef long long i64;

const int N = 16, MOD = 1000000007;
const char pool[] = "ACGT";

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    int m;
    static char s[N];
    scanf(" %s%d", s, &m);
    int n = strlen(s);
    static int f[2][1 << N], succ[1 << N][4];
    memset(succ, 0, sizeof succ);
    for (int i = 0; i < (1 << n); ++i) {
      static int g[N];
      for (int j = 0; j < n; ++j) g[j] = i >> j & 1;
      for (int j = 1; j < n; ++j) g[j] += g[j - 1];
      for (int j = 0; j < 4; ++j) {
        static int temp[N];
        for (int k = 0; k < n; ++k) {
          int prev = k ? g[k - 1] : 0;
          temp[k] = (pool[j] == s[k] ? (prev + 1) : (std::max(g[k], k ? temp[k - 1] : 0)));
        }
        int t = temp[0];
        for (int k = 1; k < n; ++k) if (temp[k] != temp[k - 1]) t |= 1 << k;
        succ[i][j] = t;
      }
    }
    int *last = f[0], *cur = f[1];
    memset(last, 0, (1 << n) * sizeof(int));
    last[0] = 1;
    for (int i = 0; i < m; ++i) {
      memset(cur, 0, (1 << n) * sizeof(int));
      for (int j = 0; j < (1 << n); ++j)
        for (int k = 0; k < 4; ++k)
          (cur[succ[j][k]] += last[j]) %= MOD;
      std::swap(cur, last);
    }
    static int cnt[N];
    memset(cnt, 0, sizeof cnt);
    for (int i = 0; i < (1 << n); ++i) (cnt[__builtin_popcount(i)] += last[i]) %= MOD;
    for (int i = 0; i <= n; ++i) printf("%d\n", cnt[i]);
  }
  return 0;
}
