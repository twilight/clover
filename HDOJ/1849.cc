#include <cstdio>
#include <cstring>

int main() {
  int n;
  while (scanf("%d", &n), n) {
    int sg = 0;
    for (int x; n--;) {
      scanf("%d", &x);
      sg ^= x;
    }
    puts(sg ? "Rabbit Win!" : "Grass Win!");
  }
  return 0;
}
