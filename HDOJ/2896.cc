#include <cstdio>
#include <cstring>
#include <set>
#include <queue>

const int N = 10000 + 10;

int n, m;
char s[N];

struct node {
  node *next[128], *fail;
  std::set<int> id;
  node() {
    id.clear();
    fail = NULL;
    memset(next, NULL, sizeof next);
  }
};

node *root = new node;

void insert(char s[], int id) {
  node *tmp = root;
  for (char *it = s; *it != '\0'; ++it) {
    if (!tmp->next[*it]) tmp->next[*it] = new node;
    tmp = tmp->next[*it];
  }
  tmp->id.insert(id);
}

void build(node *root) {
  static std::queue<node*> q;
  q.push(root);
  while (!q.empty()) {
    node *a = q.front();
    q.pop();
    for (int i = 0; i < 128; ++i) {
      if (a->next[i]) {
        node *b = a->next[i];
        q.push(b);
        if (a == root) {
          b->fail = root;
        } else {
          node *p = a->fail;
          while (p != root && !p->next[i]) p = p->fail;
          if (p->next[i]) p = p->next[i];
          b->fail = p;
        }
      }
    }
  }
}

inline void merge(std::set<int> &a, const std::set<int> &b) {
  for (std::set<int>::const_iterator it = b.begin(); it != b.end(); ++it)
    if (a.find(*it) == a.end()) a.insert(*it);
}

void match(char s[], std::set<int> &ans) {
  ans.clear();
  node *p = root;
  for (char *it = s; *it != '\0'; ++it) {
    while (p != root && !p->next[*it]) p = p->fail;
    if (p->next[*it]) p = p->next[*it];
    for (node *tmp = p; tmp != root; tmp = tmp->fail) merge(ans, tmp->id);
  }
}

int main() {
  scanf("%d", &n);
  for (int i = 1; i <= n; ++i) {
    scanf(" %s", s);
    insert(s, i);
  }
  build(root);
  scanf("%d", &m);
  int cnt = 0;
  for (int i = 1; i <= m; ++i) {
    scanf(" %s", s);
    static std::set<int> ans;
    match(s, ans);
    if (ans.size()) {
      ++cnt;
      printf("web %d:", i);
      for (std::set<int>::iterator it = ans.begin(); it != ans.end(); ++it) printf(" %d", *it);
      putchar('\n');
    }
  }
  printf("total: %d\n", cnt);
  return 0;
}
