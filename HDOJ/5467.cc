#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <algorithm>

typedef long long i64;

const int N = 300000 + 10, MOD = 1000000007;

struct Node {
  Node *p, *ch[2];
  int val, pdt;
  bool flag;
  inline int dir();
  inline void flip();
  void release();
  void update();
  void rotate();
  void splay();
  Node *expose();
  void evert();
} pool[N];

inline int Node::dir() {
  if (!p || (this != p->ch[0] && this != p->ch[1])) return -1;
  return this == p->ch[1];
}

inline void Node::flip() {
  flag ^= 1;
  std::swap(ch[0], ch[1]);
}

void Node::release() {
  if (flag) {
    if (ch[0]) ch[0]->flip();
    if (ch[1]) ch[1]->flip();
    flag = false;
  }
}

void Node::update() {
  pdt = val;
  if (ch[0]) pdt = (i64)pdt * ch[0]->pdt % MOD;
  if (ch[1]) pdt = (i64)pdt * ch[1]->pdt % MOD;
}

void Node::rotate() {
  Node *v = p;
  v->release();
  release();
  int d = dir();
  if (~v->dir()) v->p->ch[v->dir()] = this;
  p = v->p;
  v->ch[d] = ch[!d];
  if (ch[!d]) ch[!d]->p = v;
  ch[!d] = v;
  v->p = this;
  v->update(), update();
}

void Node::splay() {
  while (~dir()) {
    if (~p->dir()) (dir() == p->dir() ? p : this)->rotate();
    rotate();
  }
  update();
}

Node *Node::expose() {
  Node *v, *u = this;
  for (v = NULL; u; u = u->p) {
    u->splay();
    u->release();
    u->ch[1] = v;
    (v = u)->update();
  }
  return v;
}

inline void Node::evert() {
  expose()->flip();
  splay();
}

bool link(int u, int v) {
  if (u == v) return false;
  Node *x = pool + u, *y = pool + v;
  x->evert();
  x->splay();
  y->evert();
  y->splay();
  if (~x->dir()) return false;
  y->p = x;
  y->expose();
  y->splay();
  return true;
}

bool cut(int u, int v) {
  if (u == v) return false;
  Node *x = pool + u, *y = pool + v;
  x->evert();
  x->splay();
  y->evert();
  y->splay();
  if (~x->dir()) {
    x->splay();
    y->splay();
    if (x->p != y || x->ch[!x->dir()]) return false;
    y->ch[x->dir()] = NULL;
    y->update();
    x->p = NULL;
    x->splay();
    x->update();
    return true;
  }
  return false;
}

int query(int u, int v) {
  if (u == v) return pool[u].val;
  Node *x = pool + u, *y = pool + v;
  x->evert();
  x->splay();
  y->expose();
  y->splay();
  return ~x->dir() ? y->pdt : 0;
}

int n, q;

inline int calc(int x) {
  if (x < 2) return 0;
  return x * (x - 1) / 2;
}

int op[N], u[N], v[N], h[N], s[N], ans[N];
std::vector<int> succ[N];

void dfs() {
  static int stk[N], top;
  static int pos[N], orig[N];
  static bool first[N];
  for (int i = 0; i <= q; ++i) pos[i] = 0, first[i] = true;
  for (stk[top = 1] = 0; top;) {
    int a = stk[top];
    int x = u[a], y = v[a];
    if (first[a]) {
      if (op[a] == 1) orig[a] = link(x, y);
      if (op[a] == 2) orig[a] = cut(x, y);
      if (op[a] == 4) ans[a] = query(x, y);
      if (op[a] == 5) {
        orig[a] = pool[x].val;
        pool[x].splay();
        pool[x].val = s[a];
        pool[x].update();
      }
      first[a] = false;
    }
    if (pos[a] < succ[a].size()) {
      stk[++top] = succ[a][pos[a]++];
      continue;
    }
    if (op[a] == 1 && orig[a]) cut(x, y);
    if (op[a] == 2 && orig[a]) link(x, y);
    if (op[a] == 5) {
      pool[x].splay();
      pool[x].val = orig[a];
      pool[x].update();
    }
    --top;
  }
}

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d", &n, &q);
    memset(pool, 0, sizeof pool);
    for (int i = 1; i <= n; ++i) {
      int a;
      scanf("%d", &a);
      pool[i].val = calc(a);
    }
    for (int i = 0; i <= q; ++i) succ[i].clear();
    for (int i = 1; i <= q; ++i) {
      scanf("%d", op + i);
      if (op[i] == 1 || op[i] == 2 || op[i] == 4) {
        scanf("%d%d", u + i, v + i);
      } else if (op[i] == 3) {
        scanf("%d", h + i);
      } else {
        scanf("%d%d", u + i, s + i);
        s[i] = calc(s[i]);
      }
      if (op[i] == 3) succ[h[i]].push_back(i); else succ[i - 1].push_back(i);
    }
    dfs();
    for (int i = 1; i <= q; ++i) if (op[i] == 4) printf("%d\n", ans[i]);
  }
  return 0;
}
