#include <cstdio>
#include <cstring>

const int N = 500 + 10;

int n, m, q;

int tree[N][N], c[N][N];

void add(int x, int y, int v) {
  for (; x <= n; x += x & -x)
    for (int t = y; t <= m; t += t & -t)
      tree[x][t] ^= v;
}

int query(int x, int y) {
  int res = 0;
  for (; x; x ^= x & -x)
    for (int t = y; t; t ^= t & -t)
      res ^= tree[x][t];
  return res;
}

int main() {
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    memset(tree, 0, sizeof tree);
    scanf("%d%d%d", &n, &m, &q);
    for (int i = 1; i <= n; ++i)
      for (int j = 1; j <= m; ++j)
        scanf("%d", &c[i][j]);
    for (int i = 1; i <= n; ++i)
      for (int j = 1; j <= m; ++j)
        add(i, j, c[i][j]);
    while (q--) {
      int op, x1, y1, x2, y2, x, y, z;
      scanf("%d", &op);
      switch (op) {
        case 1:
          scanf("%d%d%d%d", &x1, &y1, &x2, &y2);
          puts((query(x2, y2) ^ query(x1 - 1, y2) ^ query(x2, y1 - 1) ^ query(x1 - 1, y1 - 1)) ? "Yes" : "No");
          break;
        case 2:
          scanf("%d%d%d", &x, &y, &z);
          add(x, y, c[x][y]);
          add(x, y, c[x][y] = z);
          break;
      }
    }
  }
  return 0;
}
