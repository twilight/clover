#include <cstdio>
#include <cstring>
#include <queue>

typedef long long int64;

const int N = 50000 + 10, M = 200000 + 10, SZ = 26;

char s[N];
int n, m, k;
int t[M];
char ch[M];

struct node_t {
  node_t *pre, *next[SZ], *self;
  int right, cnt;
} pool[2 * (N + M)], *tot, *root, *last;

void append(int ch) {
  ch -= 'a';
  node_t *np = ++tot, *p = last;
  np->right = p->right + 1;
  np->cnt = 1;
  for (; p && !p->next[ch]; p = p->pre) p->next[ch] = np;
  if (!p) {
    np->pre = root;
  } else {
    node_t *q = p->next[ch];
    if (q->right == p->right + 1) {
      np->pre = q;
    } else {
      node_t *nq = ++tot;
      *nq = *q;
      nq->right = p->right + 1;
      nq->cnt = 0;
      np->pre = q->pre = nq;
      for (; p && p->next[ch] == q; p = p->pre) p->next[ch] = nq;
    }
  }
  last = np;
}

int anc[2 * (N + M)], tag[2 * (N + M)];

inline int eval(int x) { return anc[x] == x ? x : (anc[x] = eval(anc[x])); }

int64 preprocessing() {
  int64 res = 0;
  static int deg[2 * (N + M)];
  memset(deg, 0, sizeof deg);
  root->self = root;
  for (node_t *it = pool + 1; it <= tot; ++it) {
    ++deg[it->pre - pool];
    it->self = it;
  }
  static std::queue<int> q;
  anc[0] = tag[0] = 0;
  for (int i = 1; i <= tot - pool; ++i) {
    anc[i] = i;
    tag[i] = 0;
    if (!deg[i]) q.push(i);
  }
  for (; !q.empty(); q.pop()) {
    int a = q.front();
    if (pool[a].pre) {
      int fa = pool[a].pre - pool;
      pool[fa].cnt += pool[a].cnt;
      if (--deg[fa] == 0) q.push(fa);
    }
  }
  for (node_t *it = pool + 1; it <= tot; ++it) if (it->cnt >= k) res += it->right - it->pre->right;
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  while (scanf("%d%d%d %s", &n, &m, &k, s) != EOF) {
    memset(pool, 0, sizeof pool);
    tot = root = last = pool;
    static int mem[M];
    static int64 ans[M];
    for (char *it = s; *it != '\0'; ++it) append(*it);
    for (int i = 1; i <= m; ++i) {
      scanf("%d", t + i);
      if (t[i] == 1) scanf(" %c", ch + i), append(ch[i]), mem[i] = last - pool;
    }
    ans[m + 1] = preprocessing();
    for (int i = m; i > 0; --i) {
      ans[i] = ans[i + 1];
      if (t[i] == 1) {
        int a = mem[i];
        while (pool[a = eval(a)].cnt < k) a = anc[a] = eval(pool[a].pre - pool);
        ++tag[a];
        while ((a = eval(a)) && pool[a].cnt - tag[a] < k) {
          if (!pool[a].pre) break;
          int fa = eval(pool[a].pre - pool);
          ans[i] -= pool[a].right - pool[fa].right;
          tag[fa] += tag[a];
          a = anc[a] = fa;
        }
      }
    }
    for (int i = 1; i <= m; ++i) if (t[i] == 2) printf("%I64d\n", ans[i]);
  }
  return 0;
}
