#include <cstdio>
#include <cstring>
#include <queue>

const int MOD = 20090717, N = 100 + 10;

inline void INC(int &a, int b) { a += b; if (a > MOD) a -= MOD; }

inline int _1(int x) { return 1 << x; }
inline int bitcount(int x) {
  int res = 0;
  for (; x; x >>= 1) res += x & 1;
  return res;
}

int n, m, k;

int next[N][26], fail[N], flag[N], root, cnt;

void add(char s[], int id) {
  int u = root;
  for (char *it = s; *it != '\0'; ++it) {
    char ch = *it - 'a';
    if (!next[u][ch]) next[u][ch] = ++cnt;
    u = next[u][ch];
  }
  flag[u] |= _1(id);
}

void build() {
  static std::queue<int> q;
  q.push(root);
  while (!q.empty()) {
    int a = q.front();
    q.pop();
    flag[a] |= flag[fail[a]];
    for (int ch = 0; ch < 26; ++ch) {
      int &b = next[a][ch];
      (b ? (q.push(b), fail[b]) : b) = (a == root ? a : next[fail[a]][ch]);
    }
  }
}

int solve() {
  static int dp[2][N][1 << 10];
  memset(dp[0], 0, sizeof dp[0]);
  dp[0][root][0] = 1;
  for (int i = 0; i < n; ++i) {
    int cur = i & 1;
    memset(dp[cur ^ 1], 0, sizeof dp[cur ^ 1]);
    for (int j = 1; j <= cnt; ++j)
      for (int s = 0; s < _1(10); ++s)
        if (dp[cur][j][s])
          for (int ch = 0; ch < 26; ++ch)
            INC(dp[cur ^ 1][next[j][ch]][s | flag[next[j][ch]]], dp[cur][j][s]);
  }
  int cur = n & 1, res = 0;
  for (int i = 1; i <= cnt; ++i)
    for (int s = 0; s < _1(10); ++s)
      if (bitcount(s) >= k) INC(res, dp[cur][i][s]);
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  for (scanf("%d%d%d", &n, &m, &k); n; scanf("%d%d%d", &n, &m, &k)) {
    memset(next, 0, sizeof next);
    memset(fail, 0, sizeof fail);
    memset(flag, 0, sizeof flag);
    root = cnt = 1;
    for (int i = 0; i < m; ++i) {
      static char s[10];
      scanf(" %s", s);
      add(s, i);
    }
    build();
    printf("%d\n", solve());
  }
  return 0;
}
