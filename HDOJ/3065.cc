#include <cstdio>
#include <cstring>
#include <cctype>
#include <set>
#include <map>
#include <queue>

const int N = 2000000 + 10;

int n;
char pattern[1000 + 10][50];
char text[N];

struct node {
  node *next[26], *fail;
  std::set<int> id;
  node() {
    id.clear();
    fail = NULL;
    memset(next, NULL, sizeof next);
  }
};

void add(node *root, int id) {
  node *u = root;
  for (char *it = pattern[id]; *it != '\0'; ++it) {
    char ch = *it - 'A';
    if (!u->next[ch]) u->next[ch] = new node;
    u = u->next[ch];
  }
  u->id.insert(id);
}

void build(node *root) {
  static std::queue<node*> q;
  q.push(root);
  while (!q.empty()) {
    node *u = q.front();
    q.pop();
    for (int i = 0; i < 26; ++i) {
      node *v = u->next[i];
      if (v) {
        q.push(v);
        if (u == root) {
          v->fail = root;
        } else {
          node *p = u->fail;
          while (p != root && !p->next[i]) p = p->fail;
          if (p->next[i]) p = p->next[i];
          v->fail = p;
        }
      }
    }
  }
}

void match(node *root, std::map<int, int> &ans) {
  ans.clear();
  node *u = root;
  for (char *it = text; *it != '\0'; ++it) {
    if (!isupper(*it)) {
      u = root;
      continue;
    }
    char ch = *it - 'A';
    while (u != root && !u->next[ch]) u = u->fail;
    if (u->next[ch]) u = u->next[ch];
    for (node *tmp = u; tmp != root; tmp = tmp->fail)
      for (std::set<int>::iterator it = tmp->id.begin(); it != tmp->id.end(); ++it) ans[*it]++;
  }
}

int main() {
  while (scanf("%d", &n) != EOF) {
    node *root = new node;
    for (int i = 1; i <= n; ++i) {
      scanf(" %s", pattern[i]);
      add(root, i);
    }
    build(root);
    scanf(" %s", text);
    static std::map<int, int> ans;
    match(root, ans);
    for (int i = 1; i <= n; ++i) if (ans[i]) printf("%s: %d\n", pattern[i], ans[i]);
  }
  return 0;
}
