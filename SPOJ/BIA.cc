#include <bits/stdc++.h>

const int N = 5000 + 10;

inline void cmin(int &a, int b) { if (b < a) a = b; }

int n, m;
std::vector<int> e[N], pre[N], dom[N];

int dfn[N], id[N], fa[N], cnt;
int idom[N], semi[N];

int anc[N], low[N];

inline int eval(int x) {
  if (anc[x] == x) return x;
  int y = eval(anc[x]);
  if (semi[low[anc[x]]] < semi[low[x]]) low[x] = low[anc[x]];
  return anc[x] = y;
}

inline void preprocessing() {
  cnt = 0;
  for (int i = 1; i <= n; ++i) {
    fa[i] = dfn[i] = idom[i] = 0;
    anc[i] = low[i] = semi[i] = i;
    e[i].clear(), pre[i].clear(), dom[i].clear();
  }
}

void dfs(int a) {
  id[dfn[a] = ++cnt] = a;
  for (std::vector<int>::iterator it = e[a].begin(); it != e[a].end(); ++it) {
    int b = *it;
    if (!dfn[b]) {
      dfs(b);
      fa[dfn[b]] = dfn[a];
    }
  }
}

void tarjan() {
  for (int i = n; i > 1; --i) {
    for (std::vector<int>::iterator it = pre[id[i]].begin(); it != pre[id[i]].end(); ++it) {
      int j = dfn[*it];
      if (!j) continue;
      eval(j);
      cmin(semi[i], semi[low[j]]);
    }
    dom[semi[i]].push_back(i);
    int p = anc[i] = fa[i];
    for (std::vector<int>::iterator it = dom[p].begin(); it != dom[p].end(); ++it) {
      int j = *it;
      eval(j);
      idom[j] = (semi[low[j]] < p ? low[j] : p);
    }
    dom[p].clear();
  }
  for (int i = 2; i <= n; ++i) {
    if (idom[i] != semi[i]) idom[i] = idom[idom[i]];
    dom[idom[i]].push_back(i);
  }
  idom[1] = 0;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  while (scanf("%d%d", &n, &m) != EOF) {
    preprocessing();
    for (int tmp = m, a, b; tmp--;) {
      scanf("%d%d", &a, &b);
      e[a].push_back(b), pre[b].push_back(a);
    }
    dfs(1);
    tarjan();
    static std::vector<int> ans;
    ans.clear();
    for (int i = 1; i <= n; ++i) if (dom[i].size()) ans.push_back(id[i]);
    std::sort(ans.begin(), ans.end());
    printf("%d\n", ans.size());
    for (std::vector<int>::iterator it = ans.begin(); it != ans.end(); ++it) printf("%d ", *it);
    putchar('\n');
  }
  return 0;
}
