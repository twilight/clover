#include <cstdio>
#include <cmath>
#define RD(x) (scanf("%lld",&x))
typedef long long int64;
int main() {int64 tcase,a;for (RD(tcase);tcase--;){RD(a);int64 c=a+sqrt(1.0*a*a+1.0);while((c*c+1)%(c-a)!=0)--c;printf("%lld\n",(c*c+1)/(c-a));}return 0;}
