#include <bits/stdc++.h>

const int N = 80000 * 2 + 10;

class node {
  node *p, *ch[2];
  int tag, count;

  inline int dir();
  inline void apply(int);
  inline void release();
  inline void rotate();
  inline void splay();
  inline node* expose();
  friend inline int dir();
  friend inline void apply(int);
  friend inline void rotate();
  friend inline void release();

 public:
  inline void reset(int);
  inline void link(int);
  inline void cut();
  inline int query();
} pool[N];

inline int node::dir() {
  if (!this || !this->p) return -1;
  return (this == p->ch[0] || this == p->ch[1]) ? this == p->ch[1] : -1;
}

inline void node::apply(int det) {
  tag += det;
  count += det;
}

inline void node::release() {
  if (!this) return;
  if (tag) {
    if (ch[0]) ch[0]->apply(tag);
    if (ch[1]) ch[1]->apply(tag);
    tag = 0;
  }
}

inline void node::rotate() {
  node *fa = p;
  fa->release(), release();
  int d = dir();
  if (~fa->dir()) fa->p->ch[fa->dir()] = this;
  p = fa->p;
  fa->p = this;
  if (ch[!d]) ch[!d]->p = fa;
  fa->ch[d] = ch[!d];
  ch[!d] = fa;
}

inline void node::splay() {
  release();
  while (~dir()) {
    if (~p->dir())
      (dir() == p->dir()) ? (p->rotate(), rotate()) : (rotate(), rotate());
    else
      rotate();
  }
}

inline node* node::expose() {
  node *v = NULL;
  for (node *u = this; u; v = u, u = u->p) {
    u->splay();
    u->ch[1] = v;
  }
  return v;
}

inline void node::reset(int c) {
  p = ch[0] = ch[1] = NULL;
  count = c, tag = 0;
}

inline void node::link(int id) {
  node* fa = pool + id;
  fa->expose()->apply(count);
  fa->splay();
  p = fa;
}

inline void node::cut() {
  expose(), splay();
  ch[0]->apply(-count);
  ch[0]->p = NULL;
  ch[0] = NULL;
}

inline int node::query() {
  expose(), splay();
  return count;
}

char text[N], s[N];
int len[N], pre[N], next[26][N];

inline void append(int ch) {
  static int tot = 1, last = 1;
  int np = ++tot, p = last;
  pool[np].reset(1);
  for (len[np] = len[p] + 1; p && !next[ch][p]; p = pre[p]) next[ch][p] = np;
  if (!p) {
    pool[np].link(pre[np] = 1);
  } else {
    int q = next[ch][p];
    if (len[q] == len[p] + 1) {
      pool[np].link(pre[np] = q);
    } else {
      int nq = ++tot;
      pool[nq].link(pre[nq] = pre[q]), len[nq] = len[p] + 1;
      for (int i = 0; i < 26; ++i) next[i][nq] = next[i][q];
      pool[q].cut();
      pool[q].link(pre[q] = nq);
      pool[np].link(pre[np] = nq);
      for (; p && next[ch][p] == q; p = pre[p]) next[ch][p] = nq;
    }
  }
  last = np;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  scanf(" %s", text);
  for (char* it = text; *it != '\0'; ++it) append(*it - 'a');
  int tcase, a, b;
  for (scanf("%d%d%d", &tcase, &a, &b); tcase--;) {
    scanf(" %s", s);
    int ans = 0, p = 1;
    for (char* it = s; *it != '\0'; ++it) if (!(p = next[*it - 'a'][p])) break;
    if (p) ans = pool[p].query();
    printf("%d\n", ans);
    append((a * ans + b) % 26);
  }
  return 0;
}
