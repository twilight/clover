#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

const int maxn = 1e4 + 10, maxm = maxn << 1;

int to[maxm], next[maxm], cost[maxm], etot;
int adj[maxn];

void link(int a, int b, int c) {
	to[etot] = b;
	next[etot] = adj[a];
	cost[etot] = c;
	adj[a] = etot++;
}

struct node {
	node *p, *ch[2];
	int val, max;
	inline void update() { max = std::max(val, std::max(ch[0]->max, ch[1]->max)); }
	inline bool dir() { return this == p->ch[1]; }
	inline bool isroot() { return !(this == p->ch[0] || this == p->ch[1]); }
};

node pool[maxn], *tot, *nil = new node, *id[maxn], *idx[maxn];

node* makenode(int x) {
	tot->p = tot->ch[0] = tot->ch[1] = nil;
	tot->val = tot->max = x;
	return tot++;
}

void rotate(node *u) {
	node *p = u->p;
	bool d = u->dir();
	u->p = p->p;
	if (!p->isroot()) p->p->ch[p->dir()] = u;
	if (u->ch[d^1] != nil) u->ch[d^1]->p = p;
	p->p = u, p->ch[d] = u->ch[d^1], u->ch[d^1] = p;
	p->update(), u->update();
}

void splay(node *u) {
	while (!u->isroot()) {
		if (u->p->isroot())
			rotate(u);
		else
			(u->dir() == u->p->dir()) ? (rotate(u->p), rotate(u)) : (rotate(u), rotate(u));
	}
	u->update();
}

node* expose(node *u) {
	node *v;
	for (v = nil; u != nil; u = u->p) {
		splay(u);
		u->ch[1] = v;
		(v = u)->update();
	}
	return v;
}

void bfs(int s) {
	static int q[maxn];
	int *qf, *qr;
	qf = qr = q;
	memset(id, 0, sizeof id);
	memset(idx, 0, sizeof idx);
	id[s] = makenode(0);
	*qr++ = s;
	while (qf < qr) {
		int a = *qf++;
		for (int i = adj[a]; i != -1; i = next[i]) {
			int b = to[i], c = cost[i];
			if (!id[b]) {
				id[b] = makenode(c);
				id[b]->p = id[a];
				idx[(i / 2) + 1] = id[b];
				*qr++ = b;
			}
		}
	}
}

int main() {
	int tcase, n;
	nil->ch[0] = nil->ch[1] = nil->p = nil;
	nil->val = nil->max = 0;
	scanf("%d", &tcase);
	while (tcase--) {
		scanf("%d", &n);
		int a, b, c;
		memset(adj, -1, sizeof adj);
		etot = 0;
		for (int i = 1; i < n; ++i) {
			scanf("%d%d%d", &a, &b, &c);
			link(a, b, c);
			link(b, a, c);
		}
		tot = pool;
		bfs(1);
		node *lca;
		char op[10];
		for (bool g = true; g;) {
			scanf(" %s", op);
			switch (op[0]) {
				case 'Q':
					scanf("%d%d", &a, &b);
					if (a == b) puts("0"); else {
						expose(id[a]);
						lca = expose(id[b]);
						splay(id[a]);
						printf("%d\n", max(id[a] == lca ? 0 : id[a]->max, lca->ch[1]->max));
					}
					break;
				case 'C':
					scanf("%d%d", &a, &b);	
					splay(idx[a]);
					idx[a]->val = b;
					idx[a]->update();
					break;
				case 'D':
					g = false;
					break;
			}
		}
	}
	delete nil;
	return 0;
}
