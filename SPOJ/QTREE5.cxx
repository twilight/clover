#include <cstdio>
#include <climits>
#include <cstdlib>
#include <vector>
#include <queue>
#include <algorithm>

const int N = 100000 + 10, SZ = 20, INF = 1e8;

int n;
bool col[N];
std::vector<int> adj[N];

bool forbid[N];
int dis[SZ][N], tag[SZ][N], mark[SZ][N];

struct info_t {
  int id, dis;
  info_t() {}
  info_t(int _id, int _dis): id(_id), dis(_dis) {}
  bool operator< (const info_t &rhs) const { return dis > rhs.dis; }
};

typedef std::priority_queue<info_t> heap_t;

int val[SZ][N];
heap_t center[N], son[SZ][N];

int centroid(int root) {
  static int q[N], *qf, *qr;
  static int mark[N], fa[N], heavy[N], size[N], cnt;
  qf = qr = q;
  for (mark[*qr++ = root] = ++cnt; qf < qr;) {
    int a = *qf++;
    size[a] = 1;
    heavy[a] = 0;
    for (auto b : adj[a])
      if (!forbid[b] && mark[b] != cnt) mark[*qr++ = b] = cnt, fa[b] = a;
  }
  for (int *it = qr - 1; it > q; --it) {
    int a = *it;
    size[fa[a]] += size[a];
    heavy[fa[a]] = std::max(heavy[fa[a]], size[a]);
  }
  int tot = size[root], res = root;
  for (int *it = q; it < qr; ++it) {
    heavy[*it] = std::max(heavy[*it], tot - size[*it]);
    if (heavy[*it] < heavy[res]) res = *it;
  }
  return res;
}

void BFS(int s, const int c, const int son, const int dep) {
  std::queue<int> q;
  for (q.push(s); !q.empty(); q.pop()) {
    int a = q.front();
    tag[dep][a] = c;
    mark[dep][a] = son;
    for (auto b : adj[a]) {
      if (!forbid[b] && mark[dep][b] != son) {
        dis[dep][b] = dis[dep][a] + 1;
        q.push(b);
      }
    }
  }
}

void Divide(int e, int dep = 0) {
  int root = centroid(e);
  dis[dep][root] = 0;
  tag[dep][root] = mark[dep][root] = root;
  forbid[root] = true;
  for (auto b : adj[root]) {
    if (!forbid[b]) {
      dis[dep][b] = 1;
      val[dep][b] = INF;
      BFS(b, root, b, dep);
      Divide(b, dep + 1);
    }
  }
}

void Toggle(int a) {
  col[a] ^= 1;
  for (int dep = SZ - 1; dep >= 0; --dep) {
    int c = tag[dep][a], t = mark[dep][a];
    if (!t) continue;
    heap_t &cur = son[dep][t];
    while (!cur.empty() && !col[cur.top().id]) cur.pop();
    if (col[a]) cur.push(info_t(a, dis[dep][a]));
    center[c].push(info_t(t, val[dep][t] = (cur.empty() ? INF : cur.top().dis)));
  }
}

int Query(int a) {
  if (col[a]) return 0;
  int res = INF;
  for (int dep = SZ - 1; dep >= 0; --dep) {
    int c = tag[dep][a], t = mark[dep][a];
    heap_t &cur = center[c];
    while (!cur.empty() && val[dep][cur.top().id] != cur.top().dis) cur.pop();
    if (!cur.empty()) {
      info_t tmp = cur.top();
      cur.pop();
      if (tmp.id == t) {
        while (!cur.empty() && (cur.top().id == t || val[dep][cur.top().id] != cur.top().dis)) cur.pop();
        if (!cur.empty()) res = std::min(res, cur.top().dis + dis[dep][a]);
      } else {
        res = std::min(res, tmp.dis + dis[dep][a]);
      }
      cur.push(tmp);
    }
  }
  return (res >= INF) ? -1 : res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  scanf("%d", &n);
  for (int ecnt = n - 1, a, b; ecnt--;) {
    scanf("%d%d", &a, &b);
    adj[a].push_back(b);
    adj[b].push_back(a);
  }
  Divide(1);
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    int op, a;
    scanf("%d%d", &op, &a);
    switch (op) {
      case 0:
        Toggle(a);
        break;
      case 1:
        printf("%d\n", Query(a));
        break;
    }
  }
  return 0;
}
