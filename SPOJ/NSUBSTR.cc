#define LOCAL

/** template.h ver 0.5.2 */

#include <cassert>
#include <cctype>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <vector>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <bitset>
#include <complex>
#include <functional>
#include <limits>
#include <numeric>
#include <string>
#include <typeinfo>
#include <utility>
#include <valarray>

#define REP(i, n) for (int i = 0; i < int(n); ++i)
#define FOR(i, a, b) for (int i = int(a); i < int(b); ++i)
#define DWN(i, b, a) for (int i = int(b - 1); i >= int(a); --i)
#define REP_1(i, n) for (int i = 1; i <= int(n); ++i)
#define FOR_1(i, a, b) for (int i = int(a); i <= int(b); ++i)
#define DWN_1(i, b, a) for (int i = int(b); i >= int(a); --i)
#define REP_C(i, n) for (int n____ = int(n), i = 0; i < n____; ++i)
#define FOR_C(i, a, b) for (int b____ = int(b), i = a; i < b____; ++i)
#define DWN_C(i, b, a) for (int a____ = int(a), i = b - 1; i >= a____; --i)
#define REP_N(i, n) for (i = 0; i < int(n); ++i)
#define FOR_N(i, a, b) for (i = int(a); i < int(b); ++i)
#define DWN_N(i, b, a) for (i = int(b - 1); i >= int(a); --i)
#define REP_1_C(i, n) for (int n____ = int(n), i = 1; i <= n____; ++i)
#define FOR_1_C(i, a, b) for (int b____ = int(b), i = a; i <= b____; ++i)
#define DWN_1_C(i, b, a) for (int a____ = int(a), i = b; i >= a____; --i)
#define REP_1_N(i, n) for (i = 1; i <= int(n); ++i)
#define FOR_1_N(i, a, b) for (i = int(a); i <= int(b); ++i)
#define DWN_1_N(i, b, a) for (i = int(b); i >= int(a); --i)
#define REP_C_N(i, n) for (n____ = int(n), i = 0; i < n____; ++i)
#define FOR_C_N(i, a, b) for (b____ = int(b), i = a; i < b____; ++i)
#define DWN_C_N(i, b, a) for (a____ = int(a), i = b - 1; i >= a____; --i)
#define REP_1_C_N(i, n) for (n____ = int(n), i = 1; i <= n____; ++i)
#define FOR_1_C_N(i, a, b) for (b____ = int(b), i = a; i <= b____; ++i)
#define DWN_1_C_N(i, b, a) for (a____ = int(a), i = b; i >= a____; --i)

#define ECH(it, A) for (__typeof(A.begin()) it = A.begin(); it != A.end(); ++it)
#define REP_S(it, str) for (char *it = str; *it; ++it)
#define REP_G(it, u) for (int it = adj[u]; it != -1; it = next[it])
#define DO(n) for (int ____n##__line__ = n; ____n##__line__--;)
#define REP_2(i, j, n, m) REP(i, n) REP(j, m)
#define REP_2_1(i, j, n, m) REP_1(i, n) REP_1(j, m)
#define REP_3(i, j, k, n, m, l) REP(i, n) REP(j, m) REP(k, l)
#define REP_3_1(i, j, k, n, m, l) REP_1(i, n) REP_1(j, m) REP_1(k, l)

#define COPY(a, b) memcpy(a, b, sizeof a)
#define Rush int T____; RD(T____); DO(T____)
#define Display(A, n, m) {                   \
  REP(i, n) {                                \
    REP(j, m) std::cout << A[i][j] << " ";   \
    std::cout << endl;                       \
  }                                          \
}

#define Display_1(A, n, m) {                 \
  REP_1(i, n) {                              \
    REP_1(j, m) std::cout << A[i][j] << " "; \
    std::cout << endl;                       \
  }                                          \
}
#pragma comment(linker, "/STACK:16777216")

typedef unsigned uint;
typedef long long int64;
typedef unsigned long long uint64;

const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0, 1, 0, -1};

const int MOD = 1000000007;
const int INF = 0x3f3f3f3f;
//const int64 INF = 1LL << 60;
const double eps = 1e-9;
const double oo = 1e20;

#ifdef M_PI
const double PI = M_PI;
#else
const double PI = acos(-1.0);
#endif

template <typename T> inline T& RD(T &);
template <typename T> inline void OT(const T &);
inline int64 RD() { int64 x; return RD(x); }
inline char& RC(char &c) { scanf(" %c", &c); return c; }
inline char RC() { char c; return RC(c); }
//inline char& RC(char &c) { c = getchar(); return c; }
//inline char RC() { return getchar(); }
inline double& RF(double &x) { scanf("%lf", &x); return x; }
inline double RF() { double x; return RF(x); }
inline char* RS(char *s) { scanf("%s", s); return s; }

template <typename T0, typename T1> inline T0& RD(T0 &x0, T1 &x1) { RD(x0), RD(x1); return x0; }
template <typename T0, typename T1, typename T2> inline T0& RD(T0 &x0, T1 &x1, T2 &x2) { RD(x0), RD(x1), RD(x2); return x0; }
template <typename T0, typename T1, typename T2, typename T3> inline T0& RD(T0 &x0, T1 &x1, T2 &x2, T3 &x3) { RD(x0), RD(x1), RD(x2), RD(x3); return x0; }
template <typename T0, typename T1, typename T2, typename T3, typename T4> inline T0& RD(T0 &x0, T1 &x1, T2 &x2, T3 &x3, T4 &x4) { RD(x0), RD(x1), RD(x2), RD(x3), RD(x4); return x0; }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5> inline T0& RD(T0 &x0, T1 &x1, T2 &x2, T3 &x3, T4 &x4, T5 &x5) { RD(x0), RD(x1), RD(x2), RD(x3), RD(x4), RD(x5); return x0; }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6> inline T0& RD(T0 &x0, T1 &x1, T2 &x2, T3 &x3, T4 &x4, T5 &x5, T6 &x6) { RD(x0), RD(x1), RD(x2), RD(x3), RD(x4), RD(x5), RD(x6); return x0; }
template <typename T0, typename T1> inline void OT(const T0 &x0, const T1 &x1) { OT(x0), OT(x1); }
template <typename T0, typename T1, typename T2> inline void OT(const T0 &x0, const T1 &x1, const T2 &x2) { OT(x0), OT(x1), OT(x2); }
template <typename T0, typename T1, typename T2, typename T3> inline void OT(const T0 &x0, const T1 &x1, const T2 &x2, const T3 &x3) { OT(x0), OT(x1), OT(x2), OT(x3); }
template <typename T0, typename T1, typename T2, typename T3, typename T4> inline void OT(const T0 &x0, const T1 &x1, const T2 &x2, const T3 &x3, const T4 &x4) { OT(x0), OT(x1), OT(x2), OT(x3), OT(x4); }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5> inline void OT(const T0 &x0, const T1 &x1, const T2 &x2, const T3 &x3, const T4 &x4, const T5 &x5) { OT(x0), OT(x1), OT(x2), OT(x3), OT(x4), OT(x5); }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6> inline void OT(const T0 &x0, const T1 &x1, const T2 &x2, const T3 &x3, const T4 &x4, const T5 &x5, const T6 &x6) { OT(x0), OT(x1), OT(x2), OT(x3), OT(x4), OT(x5), OT(x6); }
inline char& RC(char &a, char &b) { RC(a), RC(b); return a; }
inline char& RC(char &a, char &b, char &c) { RC(a), RC(b), RC(c); return a; }
inline char& RC(char &a, char &b, char &c, char &d) { RC(a), RC(b), RC(c), RC(d); return a; }
inline char& RC(char &a, char &b, char &c, char &d, char &e) { RC(a), RC(b), RC(c), RC(d), RC(e); return a; }
inline char& RC(char &a, char &b, char &c, char &d, char &e, char &f) { RC(a), RC(b), RC(c), RC(d), RC(e), RC(f); return a; }
inline char& RC(char &a, char &b, char &c, char &d, char &e, char &f, char &g) { RC(a), RC(b), RC(c), RC(d), RC(e), RC(f), RC(g); return a; }
inline double& RF(double &a, double &b) { RF(a), RF(b); return a; }
inline double& RF(double &a, double &b, double &c) { RF(a), RF(b), RF(c); return a; }
inline double& RF(double &a, double &b, double &c, double &d) { RF(a), RF(b), RF(c), RF(d); return a; }
inline double& RF(double &a, double &b, double &c, double &d, double &e) { RF(a), RF(b), RF(c), RF(d), RF(e); return a; }
inline double& RF(double &a, double &b, double &c, double &d, double &e, double &f) { RF(a), RF(b), RF(c), RF(d), RF(e), RF(f); return a; }
inline double& RF(double &a, double &b, double &c, double &d, double &e, double &f, double &g) { RF(a), RF(b), RF(c), RF(d), RF(e), RF(f), RF(g); return a; }
inline void RS(char *s1, char *s2) { RS(s1), RS(s2); }
inline void RS(char *s1, char *s2, char *s3) { RS(s1), RS(s2), RS(s3); }

template <typename T> inline void RST(T &A) { memset(A, 0, sizeof(A)); }
template <typename T0, typename T1> inline void RST(T0 &A0, T1 &A1) { RST(A0), RST(A1); }
template <typename T0, typename T1, typename T2> inline void RST(T0 &A0, T1 &A1, T2 &A2) { RST(A0), RST(A1), RST(A2); }
template <typename T0, typename T1, typename T2, typename T3> inline void RST(T0 &A0, T1 &A1, T2 &A2, T3 &A3) { RST(A0), RST(A1), RST(A2), RST(A3); }
template <typename T0, typename T1, typename T2, typename T3, typename T4> inline void RST(T0 &A0, T1 &A1, T2 &A2, T3 &A3, T4 &A4) { RST(A0), RST(A1), RST(A2), RST(A3), RST(A4); }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5> inline void RST(T0 &A0, T1 &A1, T2 &A2, T3 &A3, T4 &A4, T5 &A5) { RST(A0), RST(A1), RST(A2), RST(A3), RST(A4), RST(A5); }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6> inline void RST(T0 &A0, T1 &A1, T2 &A2, T3 &A3, T4 &A4, T5 &A5, T6 &A6) { RST(A0), RST(A1), RST(A2), RST(A3), RST(A4), RST(A5), RST(A6); }
template <typename T> inline void FILL(T &A, int x) { memset(A, x, sizeof(A)); }
template <typename T0, typename T1> inline void FILL(T0 &A0, T1 &A1, int x) { FILL(A0, x), FILL(A1, x); }
template <typename T0, typename T1, typename T2> inline void FILL(T0 &A0, T1 &A1, T2 &A2, int x) { FILL(A0, x), FILL(A1, x), FILL(A2, x); }
template <typename T0, typename T1, typename T2, typename T3> inline void FILL(T0 &A0, T1 &A1, T2 &A2, T3 &A3, int x) { FILL(A0, x), FILL(A1, x), FILL(A2, x), FILL(A3, x); }
template <typename T0, typename T1, typename T2, typename T3, typename T4> inline void FILL(T0 &A0, T1 &A1, T2 &A2, T3 &A3, T4 &A4, int x) { FILL(A0, x), FILL(A1, x), FILL(A2, x), FILL(A3, x), FILL(A4, x); }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5> inline void FILL(T0 &A0, T1 &A1, T2 &A2, T3 &A3, T4 &A4, T5 &A5, int x) { FILL(A0, x), FILL(A1, x), FILL(A2, x), FILL(A3, x), FILL(A4, x), FILL(A5, x); }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6> inline void FILL(T0 &A0, T1 &A1, T2 &A2, T3 &A3, T4 &A4, T5 &A5, T6 &A6, int x) { FILL(A0, x), FILL(A1, x), FILL(A2, x), FILL(A3, x), FILL(A4, x), FILL(A5, x), FILL(A6, x); }
template <typename T> inline void CLR(std::priority_queue <T, std::vector <T> , std::less <T> > &q) { while (!q.empty()) q.pop(); }
template <typename T> inline void CLR(std::priority_queue <T, std::vector <T> , std::greater <T> > &q) { while (!q.empty()) q.pop(); }
template <typename T> inline void CLR(T &A) { A.clear(); }
template <typename T0, typename T1> inline void CLR(T0 &A0, T1 &A1) { CLR(A0), CLR(A1); }
template <typename T0, typename T1, typename T2> inline void CLR(T0 &A0, T1 &A1, T2 &A2) { CLR(A0), CLR(A1), CLR(A2); }
template <typename T0, typename T1, typename T2, typename T3> inline void CLR(T0 &A0, T1 &A1, T2 &A2, T3 &A3) { CLR(A0), CLR(A1), CLR(A2), CLR(A3); }
template <typename T0, typename T1, typename T2, typename T3, typename T4> inline void CLR(T0 &A0, T1 &A1, T2 &A2, T3 &A3, T4 &A4) { CLR(A0), CLR(A1), CLR(A2), CLR(A3), CLR(A4); }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5> inline void CLR(T0 &A0, T1 &A1, T2 &A2, T3 &A3, T4 &A4, T5 &A5) { CLR(A0), CLR(A1), CLR(A2), CLR(A3), CLR(A4), CLR(A5); }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6> inline void CLR(T0 &A0, T1 &A1, T2 &A2, T3 &A3, T4 &A4, T5 &A5, T6 &A6) { CLR(A0), CLR(A1), CLR(A2), CLR(A3), CLR(A4), CLR(A5), CLR(A6); }
template <typename T> inline void CLR(T &A, int n) { REP(i, n) CLR(A[i]); }

template <typename T> inline void cmax(T &x, T ano) { if (!(ano < x)) x = ano; }
template <typename T> inline void cmin(T &x, T ano) { if (ano < x) x = ano; }
template <typename T> inline T sqr(T x) { return x * x; }

namespace BO {

inline bool _1(int x, int i) { return (bool)(x & 1 << i); }
inline bool _1(int64 x, int i) { return (bool)(x & 1LL << i); }
inline int64 _1(int i) { return 1LL << i; }
inline int64 _U(int i) { return _1(i) - 1; }
inline int reverse_bits(int x) {
  x = ((x >> 1) & 0x55555555) | ((x << 1) & 0xaaaaaaaa);
  x = ((x >> 2) & 0x33333333) | ((x << 2) & 0xcccccccc);
  x = ((x >> 4) & 0x0f0f0f0f) | ((x << 4) & 0xf0f0f0f0);
  x = ((x >> 8) & 0x00ff00ff) | ((x << 8) & 0xff00ff00);
  x = ((x >> 16) & 0x0000ffff) | ((x << 16) & 0xffff0000);
  return x;
}
inline int64 reverse_bits(int64 x) {
  x = ((x >> 1) & 0x5555555555555555LL) | ((x << 1) & 0xaaaaaaaaaaaaaaaaLL);
  x = ((x >> 2) & 0x3333333333333333LL) | ((x << 2) & 0xccccccccccccccccLL);
  x = ((x >> 4) & 0x0f0f0f0f0f0f0f0fLL) | ((x << 4) & 0xf0f0f0f0f0f0f0f0LL);
  x = ((x >> 8) & 0x00ff00ff00ff00ffLL) | ((x << 8) & 0xff00ff00ff00ff00LL);
  x = ((x >> 16) & 0x0000ffff0000ffffLL) | ((x << 16) & 0xffff0000ffff0000LL);
  x = ((x >> 32) & 0x00000000ffffffffLL) | ((x << 32) & 0xffffffff00000000LL);
  return x;
}
template <typename T> inline bool odd(T x) { return x & 1; }
template <typename T> inline bool even(T x) { return !odd(x); }
template <typename T> inline T low_bit(T x) { return x & (x ^ (x - 1)); }
template <typename T> inline T high_bit(T x) { T p = low_bit(x); while (p != x) x -= p, p = low_bit(x); return p; }
template <typename T> inline T cover_bit(T x) { T p = 1; while (p < x) p <<= 1; return p; }
inline int low_idx(int x) { return __builtin_ffs(x); }
inline int low_idx(int64 x) { return __builtin_ffsll(x); }
inline int high_idx(int x) { return low_idx(reverse_bits(x)); }
inline int high_idx(int64 x) { return low_idx(reverse_bits(x)); }
inline int clz(int x) { return __builtin_clz(x); }
inline int clz(int64 x) { return __builtin_clzll(x); }
inline int ctz(int x) { return __builtin_ctz(x); }
inline int ctz(int64 x) { return __builtin_ctzll(x); }
inline int parity(int x) { return __builtin_parity(x); }
inline int parity(int64 x) { return __builtin_parityll(x); }
inline int lg2(int a) { return 31 - __builtin_clz(a); }
inline int count_bits(int x) { return __builtin_popcount(x); }
inline int count_bits(int64 x) { return __builtin_popcountll(x); }

} using namespace BO;

namespace NT {

inline void INC(int &a, int b) { a += b; if (a >= MOD) a -= MOD; }
inline int sum(int a, int b) { a += b; if (a >= MOD) a -= MOD; return a; }
inline void DEC(int &a, int b) { a -= b; if (a < 0) a += MOD; }
inline int dif(int a, int b) { a -= b; if (a < 0) a += MOD; return a; }
inline void MUL(int &a, int b) { a = (int64)a * b % MOD; }
inline int pdt(int a, int b) { return (int64)a * b % MOD; }
inline int sum(int a, int b, int c) { return sum(sum(a, b), c); }
inline int sum(int a, int b, int c, int d) { return sum(sum(a, b), sum(c, d)); }
inline int pdt(int a, int b, int c) { return pdt(pdt(a, b), c); }
inline int pdt(int a, int b, int c, int d) { return pdt(pdt(pdt(a, b), c), d); }

template <typename T>
inline T pow(T a, int64 b) {
  T c(1);
  while (b) {
    if (b & 1) c *= a;
    a *= a, b >>= 1;
  }
  return c;
}

inline int _I(int b) {
  int a = MOD, x1 = 0, x2 = 1, q;
  while (true) {
    q = a / b, a %= b;
    if (!a) return (x2 + MOD) % MOD;
    DEC(x1, pdt(q, x2));
    q = b / a, b %= a;
    if (!b) return (x1 + MOD) % MOD;
    DEC(x2, pdt(q, x1));
  }
}

inline void DIV(int &a, int b) { MUL(a, _I(b)); }
inline int qtt(int a, int b) { return pdt(a, _I(b)); }

inline int phi(int n) {
  int res = n;
  for (int i = 2; sqr(i) <= n; ++i) {
    if (!(n % i)) {
      DEC(res, qtt(res, i));
      do { n /= i; } while(!(n % i));
    }
  }
  if (n != 1) DEC(res, qtt(res, n));
  return res;
}

template <typename T>
T gcd(T a, T b) {
  T r;
  do {
    r = a % b;
    a = b, b = r;
  } while (r);
  return a;
}

} using namespace NT;

namespace RANDOM {

inline uint rand16() { return ((bool)(rand() & 1) << 15) | rand(); }
inline uint rand32() { return (rand16() << 16) | rand16(); }
inline uint64 rand64() { return ((int64)rand32() << 32) | rand32(); }
inline uint64 random(int64 l, int64 r) { return rand64() % (r - l) + l; }
inline uint dice() { return random(1, 6); }
bool coin() { return rand() & 1; }

} using namespace RANDOM;

namespace CLOCK {

double s0, s1, rd, T;
inline double getTime() {
#ifdef LOCAL
  return 1.0 * clock() / CLOCKS_PER_SEC;
#else
  timeval tv;
  gettimeofday(&tv, 0);
  return tv.tv_sec + tv.tv_usec * 1e-6;
#endif
}
inline double elapsed() { return getTime() - s0; }

} //using namespace CLOCK;

template <typename T>
inline T &RD(T &x) {
  static char c;
  static bool flag;
  flag = false;
  for (c = getchar(); !isdigit(c) && c != '-'; c = getchar()) {}
  if (c == '-') flag = true, c = RC();
  x = c - '0';
  for (c = getchar(); isdigit(c); c = getchar()) x = x * 10 + c - '0';
  if (flag) x = -x;
  return x;
}

template <typename T> inline void OT(const T& x) {
  //printf("%lld\n", x);
  printf("%d\n", x);
  //printf("%.2lf\n", x);
  //std::cout << x << std::endl;
}
/** end of template **/

/* -------------------------------------------------------------------------- */

const int N = 500000 + 10;

int n;
char s[N / 2];

struct node {
  node *next[26], *pre;
  int cnt, len;
} buffer[N], *init = buffer, *top = init;

inline void append(char ch) {
  ch -= 'a';
  static node *last = init;
  node *p = last, *np = ++top;
  np->len = p->len + 1;
  for (; p && !p->next[ch]; p = p->pre) p->next[ch] = np;
  if (!p) {
    np->pre = init;
  } else {
    node *q = p->next[ch];
    if (q->len == p->len + 1) {
      np->pre = q;
    } else {
      node *nq = ++top;
      *nq = *q;
      nq->len = p->len + 1;
      q->pre = np->pre = nq;
      for (; p && p->next[ch] == q; p = p->pre) p->next[ch] = nq;
    }
  }
  last = np;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  RS(s);
  int n = strlen(s);
  REP_S(it, s) append(*it);
  node *p = init;
  REP_S(it, s) (p = p->next[*it - 'a'])->cnt++;
  static int f[N], tmp[N];
  static node *q[N];
  for (node *it = init; it <= top; ++it) tmp[it->len]++;
  REP_1(i, n) tmp[i] += tmp[i - 1];
  for (node *it = init; it <= top; ++it) q[--tmp[it->len]] = it;
  DWN_1(i, top - buffer, 1) {
    cmax(f[q[i]->len], q[i]->cnt);
    q[i]->pre->cnt += q[i]->cnt;
  }
  DWN(i, n, 1) cmax(f[i], f[i + 1]);
  REP_1(i, n) OT(f[i]);
  return 0;
}
