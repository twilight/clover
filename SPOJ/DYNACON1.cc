#include <cstdio>
#include <algorithm>

const int N = 100000 + 10;

int n;

struct node {
  node *p, *ch[2];
  bool rev;
  node() {
    p = ch[0] = ch[1] = NULL;
    rev = false;
  }
  inline bool d() { return this == p->ch[1]; }
  inline bool isroot() { return !p || (this != p->ch[0] && this != p->ch[1]); }
  inline void relax();
};

node pool[N];

inline void node::relax() {
  if (rev) {
    if (ch[0]) ch[0]->rev ^= 1;
    if (ch[1]) ch[1]->rev ^= 1;
    std::swap(ch[0], ch[1]);
    rev = 0;
  }
}

inline void rotate(node *u) {
  node *v = u->p;
  v->relax(), u->relax();
  bool d = u->d();
  if (!v->isroot()) v->p->ch[v->d()] = u;
  u->p = v->p;
  if (u->ch[!d]) u->ch[!d]->p = v;
  v->ch[d] = u->ch[!d];
  u->ch[!d] = v;
  v->p = u;
}

inline void splay(node *u) {
  u->relax();
  while (!u->isroot()) {
    if (u->p->isroot())
      rotate(u);
    else
      (u->d() == u->p->d()) ? (rotate(u->p), rotate(u)) : (rotate(u), rotate(u));
  }
}

inline node* expose(node *u) {
  node *v;
  for (v = NULL; u; v = u, u = u->p) {
    splay(u);
    u->ch[1] = v;
  }
  return v;
}

inline void evert(node *u) {
  expose(u)->rev ^= 1;
  splay(u);
}

inline void link(node *u, node *v) {
  evert(v);
  v->p = u;
  expose(v);
}

inline void cut(node *u, node *v) {
  evert(u);
  expose(v);
  splay(v);
  u->p = v->ch[0] = NULL;
}

inline bool connected(node *u, node *v) {
  evert(u);
  evert(v);
  return !u->isroot();
}

int main() {
  scanf("%d", &n);
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    char op[10];
    int a, b;
    scanf(" %s%d%d", op, &a, &b);
    switch (op[0]) {
      case 'a':
        link(pool + a, pool + b);
        break;
      case 'r':
        cut(pool + a, pool + b);
        break;
      case 'c':
        puts(connected(pool + a, pool + b) ? "YES" : "NO");
        break;
    }
  }
  return 0;
}
