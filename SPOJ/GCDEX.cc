#include <cstdio>

typedef long long int64;

const int N = 1000000 + 10;

int phi[N];
int64 ans[N];

void preprocessing() {
  static bool flag[N];
  for (int i = 1; i < N; ++i) phi[i] = i;
  for (int i = 2; i < N; ++i) {
    if (flag[i]) continue;
    for (int j = i; j < N; j += i) {
      flag[j] = true;
      phi[j] = phi[j] / i * (i - 1);
    }
  }
  for (int i = 1; i < N; ++i) ans[i] = phi[i];
  for (int i = 2; i * i < N; ++i) {
    ans[i * i] += i * phi[i];
    for (int j = i * i + i; j < N; j += i) {
      ans[j] += i * phi[j / i] + j / i * phi[i];
    }
  }
  for (int i = 1; i < N; ++i) ans[i] += ans[i - 1];
}

int main() {
  preprocessing();
  for (int n; scanf("%d", &n) != EOF && n;) printf("%lld\n", ans[n] - 1);
  return 0;
}
