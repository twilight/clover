#include <cstdio>

typedef long long int64;

const int N = 1000000 + 10;

int phi[N];
int64 ans[N];

void preprocessing() {
  static bool flag[N] = {false};
  for (int i = 1; i < N; ++i) phi[i] = i;
  for (int i = 2; i < N; ++i) {
    if (flag[i]) continue;
    for (int j = i; j < N; j += i) {
      flag[j] = true;
      phi[j] = phi[j] / i * (i - 1);
    }
  }
  ans[1] = 1;
  for (int i = 1; i * i < N; ++i) {
    for (int j = i; j < N; j += i) {
      if (i * i <= j) ans[j] += j * (int64)(j / i) * phi[j / i] / 2;
      if (i * i < j) ans[j] += j * (i == 1 ? i : (int64)i * phi[i] / 2);
    }
  }
}

int main() {
  int tcase;
  preprocessing();
  for (scanf("%d", &tcase); tcase--;) {
    int n;
    scanf("%d", &n);
    printf("%lld\n", ans[n]);
  }
  return 0;
}

