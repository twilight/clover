#include <cstdio>
#include <cstring>
#include <queue>

const int MOD = 10007, SZ = 70;

typedef int matrix[SZ][SZ];

inline void INC(int &a, int b) { a = (a + b) % MOD; }
inline void DEC(int &a, int b) { a = (a - b + MOD) % MOD; }
inline void MUL(int &a, int b) { a = (a * b) % MOD; }
inline int pdt(int a, int b) { return (a * b) % MOD; }

int next[SZ][26], fail[SZ], cnt, root;
bool flag[SZ];

void add(char s[]) {
  int u = root;
  for (char *it = s; *it != '\0'; ++it) {
    char ch = *it - 'A';
    if (!next[u][ch]) next[u][ch] = ++cnt;
    u = next[u][ch];
  }
  flag[u] = true;
}

matrix adj;
int tot;

void build() {
  static std::queue<int> q;
  q.push(root);
  while (!q.empty()) {
    int a = q.front();
    q.pop();
    flag[a] |= flag[fail[a]];
    for (int i = 0; i < 26; ++i) {
      int &b = next[a][i];
      (b ? (q.push(b), fail[b]) : b) = (a == root ? a : next[fail[a]][i]);
    }
  }
  memset(adj, 0, sizeof adj);
  static int id[SZ];
  memset(id, 0, sizeof id);
  tot = 0;
  for (int i = 1; i <= cnt; ++i) if (!flag[i]) id[i] = ++tot;
  for (int i = 1; i <= cnt; ++i)
    for (int j = 0; j < 26; ++j)
      if (id[i] && id[next[i][j]]) adj[id[i]][id[next[i][j]]]++;
}

inline int pow(int base, int exp) {
  int res = 1;
  while (exp) {
    if (exp & 1) MUL(res, base);
    MUL(base, base);
    exp >>= 1;
  }
  return res;
}

void mul(matrix a, const matrix b) {
  static matrix c;
  memset(c, 0, sizeof(matrix));
  for (int i = 1; i <= tot; ++i)
    for (int j = 1; j <= tot; ++j)
      for (int k = 1; k <= tot; ++k)
        INC(c[i][j], pdt(a[i][k], b[k][j]));
  memcpy(a, c, sizeof(matrix));
}

void pow(matrix a, int exp) {
  static matrix res, base;
  memcpy(base, a, sizeof(matrix));
  memset(res, 0, sizeof(matrix));
  for (int i = 1; i <= tot; ++i) res[i][i] = 1;
  while (exp) {
    if (exp & 1) mul(res, base);
    mul(base, base);
    exp >>= 1;
  }
  memcpy(a, res, sizeof(matrix));
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int n, len;
  while (scanf("%d%d", &n, &len) != EOF) {
    root = cnt = 1;
    memset(next, 0, sizeof next);
    memset(fail, 0, sizeof fail);
    memset(flag, false, sizeof flag);
    while (n--) {
      static char s[10];
      scanf(" %s", s);
      add(s);
    }
    build();
    int ans = pow(26, len);
    pow(adj, len);
    for (int i = 1; i <= tot; ++i) DEC(ans, adj[root][i]);
    printf("%d\n", ans);
  }
  return 0;
}
