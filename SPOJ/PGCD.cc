#include <cstdio>
#include <algorithm>

typedef long long int64;

const int N = 10000000 + 10;

int miu[N], sum[N];

void preprocessing() {
  static int prime[N], cnt;
  static bool flag[N];
  miu[1] = 1;
  for (int i = 2; i < N; ++i) {
    if (!flag[i]) {
      prime[++cnt] = i;
      miu[i] = -1;
    }
    for (int j = 1; j <= cnt && i * prime[j] < N; ++j) {
      flag[i * prime[j]] = true;
      if (i % prime[j]) {
        miu[i * prime[j]] = -miu[i];
      } else {
        miu[i * prime[j]] = 0;
        break;
      }
    }
  }
  for (int i = 1; i < N; ++i) {
    if (!miu[i]) continue;
    for (int j = 1; j <= cnt && i * prime[j] < N; ++j)
      sum[i * prime[j]] += miu[i];
  }
  for (int i = 1; i < N; ++i) sum[i] += sum[i - 1];
}

int64 cal(int a, int b) {
  int64 res = 0;
  if (a > b) std::swap(a, b);
  for (int i = 1, last; i <= a; i = last + 1) {
    last = std::min(a / (a / i), b / (b / i));
    res += (int64)(a / i) * (b / i) * (sum[last] - sum[i - 1]);
  }
  return res;
}

int main() {
  int tcase;
  preprocessing();
  for (scanf("%d", &tcase); tcase--;) {
    int a, b;
    scanf("%d%d", &a, &b);
    printf("%lld\n", cal(a, b));
  }
  return 0;
}
