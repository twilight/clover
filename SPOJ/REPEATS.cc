#include <cstdio>
#include <cstring>
#include <algorithm>

const int N = 50000 + 10, LOG = 16;

int n;
char s[N];

class SuffixArray {
 private:
  int sa[N], rank[N];
  int gap, table[LOG][N];

  inline int lg2(int x) { return 31 - __builtin_clz(x); }

  inline bool Compare(int lhs, int rhs) {
    if (rank[lhs] != rank[rhs]) return rank[lhs] < rank[rhs];
    lhs += gap, rhs += gap;
    return lhs <= n && rhs <= n ? rank[lhs] < rank[rhs] : lhs > rhs;
  }

 public:
  void build() {
    gap = 0;
    static int sum[N];
    memset(sum, 0, sizeof sum);
    for (int i = 1; i <= n; ++i) ++sum[rank[i] = s[i]];
    for (int i = 1; i <= 256; ++i) sum[i] += sum[i - 1];
    for (int i = 1; i <= n; ++i) sa[sum[rank[i]]--] = i;
    static int temp[N];
    for (int i = 1; i <= n; ++i) temp[sa[i]] = temp[sa[i - 1]] + Compare(sa[i - 1], sa[i]);
    memcpy(rank, temp, sizeof temp);
    for (gap = 1;; gap *= 2) {
      for (int i = 1; i <= gap; ++i) temp[i] = n - i + 1;
      for (int i = 1, j = gap; i <= n; ++i) if (sa[i] > gap) temp[++j] = sa[i] - gap;
      memset(sum, 0, sizeof sum);
      for (int i = 1; i <= n; ++i) ++sum[rank[i]];
      for (int i = 1; i <= n; ++i) sum[i] += sum[i - 1];
      for (int i = n; i > 0; --i) sa[sum[rank[temp[i]]]--] = temp[i];
      temp[0] = 0;
      for (int i = 1; i <= n; ++i) temp[sa[i]] = temp[sa[i - 1]] + Compare(sa[i - 1], sa[i]);
      memcpy(rank, temp, sizeof temp);
      if (rank[sa[n]] == n) break;
    }
    for (int i = 1, k = 0; i <= n; ++i) {
      if (rank[i] == 1) continue;
      for (int j = sa[rank[i] - 1]; s[i + k] == s[j + k];) ++k;
      table[0][rank[i]] = k;
      if (k) --k;
    }
    for (int i = 1; i < LOG; ++i)
      for (int j = 1; j + (1 << (i - 1)) <= n; ++j)
        table[i][j] = std::min(table[i - 1][j], table[i - 1][j + (1 << (i - 1))]);
  }

  int lcp(int l, int r) {
    l = rank[l], r = rank[r];
    if (l > r) std::swap(l, r);
    ++l;
    int k = lg2(r - l + 1);
    return std::min(table[k][l], table[k][r - (1 << k) + 1]);
  }
} pre, suf;

int Solve(int len) {
  int res = 1;
  for (int i = 1; i + len <= n; i += len) {
    int t = pre.lcp(i, i + len) + suf.lcp(n - i + 1, n - (i + len) + 1) - 1;
    res = std::max(res, t / len + 1);
  }
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d", &n);
    for (int i = 1; i <= n; ++i) scanf(" %c", s + i);
    pre.build();
    std::reverse(s + 1, s + n + 1);
    suf.build();
    int ans = 1;
    for (int len = 1; len <= n; ++len) ans = std::max(ans, Solve(len));
    printf("%d\n", ans);
  }
  return 0;
}
