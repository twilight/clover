#include <cstdio>
#include <set>

typedef std::pair<int, int> Info;
typedef long long i64;

const int N = 50000 + 10, SZ = 200 * N;

int n, a[N];

namespace seg {

int lch[SZ], rch[SZ], tot;
i64 tag[SZ];

void add(int &id, int l, int r, int p, int q, int det) {
  if (!id) id = ++tot;
  if (p <= l && r <= q) {
    tag[id] += det;
    return;
  }
  int mid = (l + r) / 2;
  if (p <= mid) add(lch[id], l, mid, p, q, det);
  if (q > mid) add(rch[id], mid + 1, r, p, q, det);
}

i64 query(int id, int l, int r, int p) {
  if (!id) return 0;
  i64 res = tag[id];
  if (l == r) return res;
  int mid = (l + r) / 2;
  if (p <= mid) res += query(lch[id], l, mid, p); else res += query(rch[id], mid + 1, r, p);
  return res;
}

}

int root[N];

inline void add(int p, int l, int r, int d) {
  for (; p; p ^= p & -p) seg::add(root[p], 1, n, l, r, d);
}

inline i64 query(int p, int q) {
  i64 res = 0;
  for (; p <= n; p += p & -p) res += seg::query(root[p], 1, n, q);
  return res;
}

std::set<Info> pool;

void touch(int p, int op) {
  std::set<Info>::iterator it = pool.find(Info(a[p], p)), succ = it, pred = it;
  int x = (pred != pool.begin() && (--pred)->first == a[p] ? pred->second : 0);
  int y = (++succ != pool.end() && succ->first == a[p] ? succ->second : (n + 1));
  add(p, p, y - 1, op * a[p]);
  add(x, p, y - 1, -op * a[p]);
}

int main() {
  scanf("%d", &n);
  for (int i = 1; i <= n; ++i) {
    scanf("%d", a + i);
    pool.insert(Info(a[i], i));
    touch(i, 1);
  }
  int q;
  for (scanf("%d", &q); q--;) {
    char op;
    int x, y;
    scanf(" %c%d%d", &op, &x, &y);
    switch (op) {
      case 'U':
        touch(x, -1);
        pool.erase(Info(a[x], x));
        pool.insert(Info(a[x] = y, x));
        touch(x, 1);
        break;
      case 'Q':
        printf("%lld\n", query(x, y));
        break;
    }
  }
  return 0;
}
