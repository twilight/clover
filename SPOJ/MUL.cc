#include <bits/stdc++.h>

const int N = 1 << 15;
const double eps = 1e-5;

const std::complex<double> I(0.0, 1.0);

#define lg2(x) (31 - __builtin_clz(x))

void DFT(std::complex<double> *a, int n, int sw) {
  std::complex<double> tmp[N];
  for (int i = 0, cnt = lg2(n); i < n; ++i) {
    int p = 0;
    for (int tp = i, c = 0; c < cnt; tp >>= 1, ++c) (p <<= 1) |= (tp & 1);
    tmp[p] = a[i];
  }
  for (int m = 2; m <= n; m *= 2) {
    int gap = m / 2;
    for (int i = 0; i < gap; ++i) {
      std::complex<double> w(std::exp(I * (sw * 2 * M_PI * i / m)));
      for (int j = i; j < n; j += m) {
        std::complex<double> u = tmp[j], v = tmp[j + gap];
        tmp[j] = u + w * v;
        tmp[j + gap] = u - w * v;
      }
    }
  }
  for (int i = 0; i < n; ++i) {
    a[i] = tmp[i];
    if (sw == -1) a[i] /= n;
  }
}

int main() {
  int kCase;
  for (scanf("%d", &kCase); kCase--;) {
    static char s[N];
    static std::complex<double> a[N], b[N];
    memset(a, 0, sizeof a);
    memset(b, 0, sizeof b);
    scanf(" %s", s);
    int t0 = strlen(s);
    std::reverse(s, s + t0);
    for (int i = 0; i < t0; ++i) a[i] = s[i] - '0';
    scanf(" %s", s);
    int t1 = strlen(s);
    std::reverse(s, s + t1);
    for (int i = 0; i < t1; ++i) b[i] = s[i] - '0';
    int len = 1;
    while (len < std::max(t0, t1)) len *= 2;
    len *= 2;
    DFT(a, len, 1);
    DFT(b, len, 1);
    for (int i = 0; i < len; ++i) a[i] *= b[i];
    DFT(a, len, -1);
    static int ans[N];
    std::fill(ans, ans + len + 1, 0);
    for (int i = 0; i < len; ++i) {
      ans[i] += (int)(a[i].real() + eps);
      ans[i + 1] += ans[i] / 10;
      ans[i] %= 10;
    }
    while (len > 0 && ans[len] == 0) --len;
    for (int i = len; i >= 0; --i) putchar(ans[i] + '0');
    putchar('\n');
  }
  return 0;
}
