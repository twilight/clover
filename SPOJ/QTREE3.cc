#include <bits/stdc++.h>

const int N = 100000 + 10, E = N * 2;

inline int low_bit(int x) { return x & -x; }

int n;

int adj[N];
int to[E], next[E];

inline void link(int a, int b) {
  static int cnt = 0;
  to[++cnt] = b, next[cnt] = adj[a], adj[a] = cnt;
}

int fa[N], dep[N], size[N], son[N], top[N], dfn[N], map[N];

int arc[N], stack[N];

void dfs(int s) {
  int *top = stack;
  memcpy(arc, adj, sizeof adj);
  for (*++top = s; top > stack;) {
    int a = *top;
    if (!size[a]) size[a] = 1;
    bool flag = false;
    for (int &it = arc[a]; it; it = next[it]) {
      int b = to[it];
      if (fa[b] == a) {
        size[a] += size[b];
        if (size[son[a]] < size[b]) son[a] = b;
      } else if (b != fa[a]) {
        *++top = b;
        fa[b] = a;
        dep[b] = dep[a] + 1;
        flag = true;
        break;
      }
    }
    if (!flag) --top;
  }
}

void split(int s) {
  int *top = stack, tot = 0;
  memcpy(arc, adj, sizeof adj);
  for (::top[*++top = s] = s; top > stack;) {
    int a = *top;
    if (!map[a]) dfn[map[a] = ++tot] = a;
    if (son[a] && !::top[son[a]]) {
      ::top[*++top = son[a]] = ::top[a];
      continue;
    }
    for (int &it = arc[a]; it; it = next[it]) {
      int b = to[it];
      if (!::top[b]) {
        ::top[*++top = b] = b;
        break;
      }
    }
    if (!arc[a]) --top;
  }
}

int col[N], tree[N];

inline void update(int pos, int det) {
  while (pos <= n) {
    tree[pos] += det;
    pos += low_bit(pos);
  }
}

inline int query(int pos) {
  int res = 0;
  for (; pos; pos -= low_bit(pos)) res += tree[pos];
  return res;
}

inline void toggle(int u) {
  update(map[u], col[u] ? -1 : 1);
  col[u] ^= 1;
}

inline int binary_search(int l, int r) {
  int det = query(l - 1);
  while (l < r) {
    int mid = (l + r) / 2;
    if (query(mid) > det) r = mid; else l = mid + 1;
  }
  return (query(l) > det) ? dfn[l] : 0;
}

inline int solve(int u) {
  int res = -1;
  for (int t = top[u]; t; u = fa[t], t = top[u]) {
    int tmp = binary_search(map[t], map[u]);
    if (tmp) res = tmp;
  }
  return res;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  int tcase;
  scanf("%d%d", &n, &tcase);
  for (int i = 1; i < n; ++i) {
    int u, v;
    scanf("%d%d", &u, &v);
    link(u, v), link(v, u);
  }
  dfs(1);
  split(1);
  while (tcase--) {
    int op, u;
    scanf("%d%d", &op, &u);
    switch (op) {
      case 0: toggle(u); break;
      case 1: printf("%d\n", solve(u)); break;
    }
  }
  return 0;
}
