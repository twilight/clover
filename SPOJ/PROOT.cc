#include <cstdio>

typedef long long int64;

const int N = 50, lim = 100000;

inline int64 sqr(int64 x) { return x * x; }

int64 n, phi;
int64 factor[N], cnt;

int64 prime[lim], tot;

void init() {
  static bool flag[lim];
  for (int64 i = 2; i < lim; ++i) {
    if (!flag[i]) prime[++tot] = i;
    for (int64 j = 1; j <= tot && i * prime[j] < lim; ++j) {
      flag[i * prime[j]] = true;
      if (i % prime[j] == 0) break;
    }
  }
}

void preprocessing(int64 n) {
  phi = n;
  for (int64 i = 1; i <= tot && sqr(prime[i]) <= n; ++i) {
    if (n % prime[i] == 0) {
      phi = phi / prime[i] * prime[i - 1];
      while (n % i == 0) n /= i;
    }
  }
  if (n > 1) phi = phi / n * (n - 1);
  int64 tmp = phi;
  cnt = 0;
  for (int64 i = 1; i <= tot && sqr(prime[i]) <= tmp; ++i) {
    if (tmp % prime[i] == 0) {
      factor[++cnt] = prime[i];
      while (tmp % prime[i] == 0) tmp /= prime[i];
    }
  }
  if (tmp > 1) factor[++cnt] = tmp;
}

int64 pow(int64 base, int64 exp, int64 MOD) {
  int64 res = 1;
  while (exp) {
    if (exp & 1) res = (int64)res * base % MOD;
    base = (int64)base * base % MOD;
    exp >>= 1;
  }
  return res;
}

bool isproot(int64 m) {
  for (int64 i = 1; i <= cnt; ++i)
    if (pow(m, phi / factor[i], n) == 1)
      return false;
  return true;
}

int main() {
  int64 tcase;
  init();
  while (scanf("%lld%lld", &n, &tcase), n | tcase) {
    preprocessing(n);
    while (tcase--) {
      int64 m;
      scanf("%lld", &m);
      puts(isproot(m) ? "YES" :"NO");
    }
  }
  return 0;
}
