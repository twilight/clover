#define LOCAL

/** template.h ver 0.5.2 */

#include <cassert>
#include <cctype>
#include <climits>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <deque>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <vector>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <bitset>
#include <complex>
#include <functional>
#include <limits>
#include <numeric>
#include <string>
#include <typeinfo>
#include <utility>
#include <valarray>

#define REP(i, n) for (int i = 0; i < int(n); ++i)
#define FOR(i, a, b) for (int i = int(a); i < int(b); ++i)
#define DWN(i, b, a) for (int i = int(b - 1); i >= int(a); --i)
#define REP_1(i, n) for (int i = 1; i <= int(n); ++i)
#define FOR_1(i, a, b) for (int i = int(a); i <= int(b); ++i)
#define DWN_1(i, b, a) for (int i = int(b); i >= int(a); --i)
#define REP_G(it, u) for (int it = adj[u]; it != -1; it = next[it])
#define DO(n) for (int ____n##__line__ = n; ____n##__line__--;)
#define Rush int T____; RD(T____); DO(T____)
#pragma comment(linker, "/STACK:16777216")

typedef long long int64;

template <typename T> inline T& RD(T &);
template <typename T> inline void OT(const T &);
inline int64 RD() { int64 x; return RD(x); }
inline char& RC(char &c) { scanf(" %c", &c); return c; }
inline char RC() { char c; return RC(c); }
//inline char& RC(char &c) { c = getchar(); return c; }
//inline char RC() { return getchar(); }
inline double& RF(double &x) { scanf("%lf", &x); return x; }
inline double RF() { double x; return RF(x); }
inline char* RS(char *s) { scanf("%s", s); return s; }

template <typename T0, typename T1> inline T0& RD(T0 &x0, T1 &x1) { RD(x0), RD(x1); return x0; }
template <typename T0, typename T1, typename T2> inline T0& RD(T0 &x0, T1 &x1, T2 &x2) { RD(x0), RD(x1), RD(x2); return x0; }
template <typename T0, typename T1, typename T2, typename T3> inline T0& RD(T0 &x0, T1 &x1, T2 &x2, T3 &x3) { RD(x0), RD(x1), RD(x2), RD(x3); return x0; }
template <typename T0, typename T1, typename T2, typename T3, typename T4> inline T0& RD(T0 &x0, T1 &x1, T2 &x2, T3 &x3, T4 &x4) { RD(x0), RD(x1), RD(x2), RD(x3), RD(x4); return x0; }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5> inline T0& RD(T0 &x0, T1 &x1, T2 &x2, T3 &x3, T4 &x4, T5 &x5) { RD(x0), RD(x1), RD(x2), RD(x3), RD(x4), RD(x5); return x0; }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6> inline T0& RD(T0 &x0, T1 &x1, T2 &x2, T3 &x3, T4 &x4, T5 &x5, T6 &x6) { RD(x0), RD(x1), RD(x2), RD(x3), RD(x4), RD(x5), RD(x6); return x0; }
template <typename T0, typename T1> inline void OT(const T0 &x0, const T1 &x1) { OT(x0), OT(x1); }
template <typename T0, typename T1, typename T2> inline void OT(const T0 &x0, const T1 &x1, const T2 &x2) { OT(x0), OT(x1), OT(x2); }
template <typename T0, typename T1, typename T2, typename T3> inline void OT(const T0 &x0, const T1 &x1, const T2 &x2, const T3 &x3) { OT(x0), OT(x1), OT(x2), OT(x3); }
template <typename T0, typename T1, typename T2, typename T3, typename T4> inline void OT(const T0 &x0, const T1 &x1, const T2 &x2, const T3 &x3, const T4 &x4) { OT(x0), OT(x1), OT(x2), OT(x3), OT(x4); }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5> inline void OT(const T0 &x0, const T1 &x1, const T2 &x2, const T3 &x3, const T4 &x4, const T5 &x5) { OT(x0), OT(x1), OT(x2), OT(x3), OT(x4), OT(x5); }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6> inline void OT(const T0 &x0, const T1 &x1, const T2 &x2, const T3 &x3, const T4 &x4, const T5 &x5, const T6 &x6) { OT(x0), OT(x1), OT(x2), OT(x3), OT(x4), OT(x5), OT(x6); }
inline char& RC(char &a, char &b) { RC(a), RC(b); return a; }
inline char& RC(char &a, char &b, char &c) { RC(a), RC(b), RC(c); return a; }
inline char& RC(char &a, char &b, char &c, char &d) { RC(a), RC(b), RC(c), RC(d); return a; }
inline char& RC(char &a, char &b, char &c, char &d, char &e) { RC(a), RC(b), RC(c), RC(d), RC(e); return a; }
inline char& RC(char &a, char &b, char &c, char &d, char &e, char &f) { RC(a), RC(b), RC(c), RC(d), RC(e), RC(f); return a; }
inline char& RC(char &a, char &b, char &c, char &d, char &e, char &f, char &g) { RC(a), RC(b), RC(c), RC(d), RC(e), RC(f), RC(g); return a; }
inline double& RF(double &a, double &b) { RF(a), RF(b); return a; }
inline double& RF(double &a, double &b, double &c) { RF(a), RF(b), RF(c); return a; }
inline double& RF(double &a, double &b, double &c, double &d) { RF(a), RF(b), RF(c), RF(d); return a; }
inline double& RF(double &a, double &b, double &c, double &d, double &e) { RF(a), RF(b), RF(c), RF(d), RF(e); return a; }
inline double& RF(double &a, double &b, double &c, double &d, double &e, double &f) { RF(a), RF(b), RF(c), RF(d), RF(e), RF(f); return a; }
inline double& RF(double &a, double &b, double &c, double &d, double &e, double &f, double &g) { RF(a), RF(b), RF(c), RF(d), RF(e), RF(f), RF(g); return a; }
inline void RS(char *s1, char *s2) { RS(s1), RS(s2); }
inline void RS(char *s1, char *s2, char *s3) { RS(s1), RS(s2), RS(s3); }

template <typename T> inline void RST(T &A) { memset(A, 0, sizeof(A)); }
template <typename T0, typename T1> inline void RST(T0 &A0, T1 &A1) { RST(A0), RST(A1); }
template <typename T0, typename T1, typename T2> inline void RST(T0 &A0, T1 &A1, T2 &A2) { RST(A0), RST(A1), RST(A2); }
template <typename T0, typename T1, typename T2, typename T3> inline void RST(T0 &A0, T1 &A1, T2 &A2, T3 &A3) { RST(A0), RST(A1), RST(A2), RST(A3); }
template <typename T0, typename T1, typename T2, typename T3, typename T4> inline void RST(T0 &A0, T1 &A1, T2 &A2, T3 &A3, T4 &A4) { RST(A0), RST(A1), RST(A2), RST(A3), RST(A4); }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5> inline void RST(T0 &A0, T1 &A1, T2 &A2, T3 &A3, T4 &A4, T5 &A5) { RST(A0), RST(A1), RST(A2), RST(A3), RST(A4), RST(A5); }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6> inline void RST(T0 &A0, T1 &A1, T2 &A2, T3 &A3, T4 &A4, T5 &A5, T6 &A6) { RST(A0), RST(A1), RST(A2), RST(A3), RST(A4), RST(A5), RST(A6); }
template <typename T> inline void FILL(T &A, int x) { memset(A, x, sizeof(A)); }
template <typename T0, typename T1> inline void FILL(T0 &A0, T1 &A1, int x) { FILL(A0, x), FILL(A1, x); }
template <typename T0, typename T1, typename T2> inline void FILL(T0 &A0, T1 &A1, T2 &A2, int x) { FILL(A0, x), FILL(A1, x), FILL(A2, x); }
template <typename T0, typename T1, typename T2, typename T3> inline void FILL(T0 &A0, T1 &A1, T2 &A2, T3 &A3, int x) { FILL(A0, x), FILL(A1, x), FILL(A2, x), FILL(A3, x); }
template <typename T0, typename T1, typename T2, typename T3, typename T4> inline void FILL(T0 &A0, T1 &A1, T2 &A2, T3 &A3, T4 &A4, int x) { FILL(A0, x), FILL(A1, x), FILL(A2, x), FILL(A3, x), FILL(A4, x); }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5> inline void FILL(T0 &A0, T1 &A1, T2 &A2, T3 &A3, T4 &A4, T5 &A5, int x) { FILL(A0, x), FILL(A1, x), FILL(A2, x), FILL(A3, x), FILL(A4, x), FILL(A5, x); }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6> inline void FILL(T0 &A0, T1 &A1, T2 &A2, T3 &A3, T4 &A4, T5 &A5, T6 &A6, int x) { FILL(A0, x), FILL(A1, x), FILL(A2, x), FILL(A3, x), FILL(A4, x), FILL(A5, x), FILL(A6, x); }
template <typename T> inline void CLR(std::priority_queue <T, std::vector <T> , std::less <T> > &q) { while (!q.empty()) q.pop(); }
template <typename T> inline void CLR(std::priority_queue <T, std::vector <T> , std::greater <T> > &q) { while (!q.empty()) q.pop(); }
template <typename T> inline void CLR(T &A) { A.clear(); }
template <typename T0, typename T1> inline void CLR(T0 &A0, T1 &A1) { CLR(A0), CLR(A1); }
template <typename T0, typename T1, typename T2> inline void CLR(T0 &A0, T1 &A1, T2 &A2) { CLR(A0), CLR(A1), CLR(A2); }
template <typename T0, typename T1, typename T2, typename T3> inline void CLR(T0 &A0, T1 &A1, T2 &A2, T3 &A3) { CLR(A0), CLR(A1), CLR(A2), CLR(A3); }
template <typename T0, typename T1, typename T2, typename T3, typename T4> inline void CLR(T0 &A0, T1 &A1, T2 &A2, T3 &A3, T4 &A4) { CLR(A0), CLR(A1), CLR(A2), CLR(A3), CLR(A4); }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5> inline void CLR(T0 &A0, T1 &A1, T2 &A2, T3 &A3, T4 &A4, T5 &A5) { CLR(A0), CLR(A1), CLR(A2), CLR(A3), CLR(A4), CLR(A5); }
template <typename T0, typename T1, typename T2, typename T3, typename T4, typename T5, typename T6> inline void CLR(T0 &A0, T1 &A1, T2 &A2, T3 &A3, T4 &A4, T5 &A5, T6 &A6) { CLR(A0), CLR(A1), CLR(A2), CLR(A3), CLR(A4), CLR(A5), CLR(A6); }
template <typename T> inline void CLR(T &A, int n) { REP(i, n) CLR(A[i]); }

template <typename T> inline void cmax(T &x, T ano) { if (!(ano < x)) x = ano; }
template <typename T> inline void cmin(T &x, T ano) { if (ano < x) x = ano; }
template <typename T> inline T sqr(T x) { return x * x; }

inline bool _1(int64 c, int k) { return c & (1 << k); }

template <typename T>
inline T &RD(T &x) {
  static char c;
  static bool flag;
  flag = false;
  for (c = getchar(); !isdigit(c) && c != '-'; c = getchar()) {}
  if (c == '-') flag = true, c = RC();
  x = c - '0';
  for (c = getchar(); isdigit(c); c = getchar()) x = x * 10 + c - '0';
  if (flag) x = -x;
  return x;
}

template <typename T> inline void OT(const T& x) {
  //printf("%lld\n", x);
  printf("%d\n", x);
  //printf("%.2lf\n", x);
  //std::cout << x << std::endl;
}
/** end of template **/

/* -------------------------------------------------------------------------- */

const int N = 10000 + 10, E = N * 2, lim = 15;

int n;

int adj[N];
int to[E], next[E], cost[E], etot;

int fa[N][lim + 1];
int dep[N], sum[N];

inline void link(int a, int b, int c) {
  to[etot] = b;
  next[etot] = adj[a];
  cost[etot] = c;
  adj[a] = etot++;
}

inline void bfs(int s) {
  static int q[N];
  int *qf, *qr;
  qf = qr = q;
  *qr++ = s;
  RST(fa[s]);
  dep[s] = sum[s] = 0;
  while (qf < qr) {
    int a = *qf++;
    REP_G(it, a) {
      int b = to[it], c = cost[it];
      if (b != fa[a][0]) {
        RST(fa[b]);
        fa[b][0] = a;
        REP_1(k, lim) fa[b][k] = fa[fa[b][k - 1]][k - 1];
        sum[b] = sum[a] + c;
        dep[b] = dep[a] + 1;
        *qr++ = b;
      }
    }
  }
}

inline int lca(int u, int v) {
  if (dep[u] < dep[v]) std::swap(u, v);
  DWN_1(i, lim, 0) {
    if (dep[fa[u][i]] >= dep[v]) {
      u = fa[u][i];
      if (dep[u] == dep[v]) break;
    }
  }
  if (u == v) return u;
  DWN_1(i, lim, 0) {
    if (fa[u][i] && fa[v][i] && fa[u][i] != fa[v][i]) {
      u = fa[u][i];
      v = fa[v][i];
    }
  }
  return fa[u][0];
}

inline int dist(int u, int v) {
  int p = lca(u, v);
  return (int64)sum[u] + sum[v] - 2 * sum[p];
}

inline int up(int u, int k) {
  REP(i, lim) if (_1(k, i)) u = fa[u][i];
  return u;
}

inline int kth(int u, int v, int k) {
  int p = lca(u, v);
  int dis = dep[u] + dep[v] - 2 * dep[p] + 1;
  int first = dep[u] - dep[p] + 1;
  return (k <= first) ? up(u, k - 1) : up(v, dis - k);
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  int tcase;
  for (RD(tcase); tcase--;) {
    FILL(adj, -1), etot = 0;
    RD(n);
    int a, b, c;
    DO(n - 1) {
      RD(a, b, c);
      link(a, b, c);
      link(b, a, c);
    }
    bfs(1);
    char op[10];
    while (RS(op)[1] != 'O') {
      switch (op[0]) {
        case 'D':
          RD(a, b);
          OT(dist(a, b));
          break;
        case 'K':
          RD(a, b, c);
          OT(kth(a, b, c));
          break;
      }
    }
    if (tcase) putchar('\n');
  }
  return 0;
}
