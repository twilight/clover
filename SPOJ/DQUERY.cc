#include <cstdio>
#include <vector>
#include <algorithm>

#define fst first
#define snd second

const int N = 30000 + 10, M = 200000 + 10;

int n, m, a[N];
std::vector< std::pair<int, int> > query[M];

int tree[N];

inline int low_bit(int x) { return x & -x; }

inline void add(int pos, int val) {
  while (pos <= n) {
    tree[pos] += val;
    pos += low_bit(pos);
  }
}

inline int sum(int pos) {
  int res = 0;
  while (pos) {
    res += tree[pos];
    pos -= low_bit(pos);
  }
  return res;
}

int main() {
  scanf("%d", &n);
  static int sorted[N];
  for (int i = 1; i <= n; ++i) scanf("%d", a + i), sorted[i] = a[i];
  std::sort(sorted + 1, sorted + 1 + n);
  for (int i = 1; i <= n; ++i) a[i] = std::lower_bound(sorted + 1, sorted + 1 + n, a[i]) - sorted;
  scanf("%d", &m);
  for (int i = 1; i <= m; ++i) {
    int l, r;
    scanf("%d%d", &l, &r);
    query[r].push_back(std::pair<int, int>(l, i));
  }
  static int last[N], ans[M];
  for (int i = 1; i <= n; ++i) {
    add(last[a[i]] + 1, 1);
    add((last[a[i]] = i) + 1, -1);
    for (std::vector< std::pair<int, int> >::iterator it = query[i].begin(); it != query[i].end(); ++it)
      ans[it->snd] = sum(it->fst);
  }
  for (int i = 1; i <= m; ++i) printf("%d\n", ans[i]);
  return 0;
}
