#include <bits/stdc++.h>

#define fst first
#define snd second

const int N = 100000 * 2 + 10, E = N * 2, SZ = 30 + 1, INF = 1 << 25;

int n;

int _pos[SZ][N], _dis[SZ][N];

class heap {
  inline void up(int), down(int);
  int *data, tot, *pos, *dis;
 public:
  heap(int size, int dep) {
    static int _pool[SZ * N], *pool = _pool;
    pos = _pos[dep], dis = _dis[dep];
    data = pool;
    pool += size + 10;
    tot = 0;
  }
  inline void pop(int), push(int);
  inline int top();
};

inline void heap::up(int p) {
  for (int fa; (fa = p >> 1) >= 1;) {
    if (dis[data[fa]] < dis[data[p]]) {
      std::swap(data[fa], data[p]);
      std::swap(pos[data[fa]], pos[data[p]]);
    } else {
      break;
    }
    p = fa;
  }
}

inline void heap::down(int p) {
  for (int son; (son = p << 1) <= tot;) {
    if (son + 1 <= tot && dis[data[son + 1]] > dis[data[son]]) ++son;
    if (dis[data[son]] > dis[data[p]]) {
      std::swap(data[son], data[p]);
      std::swap(pos[data[son]], pos[data[p]]);
    } else {
      break;
    }
    p = son;
  }
}

inline void heap::push(int num) { up(pos[data[++tot] = num] = tot); }

inline void heap::pop(int num) {
  data[pos[data[tot]] = pos[num]] = data[tot--];
  up(pos[num]);
  down(pos[num]);
}

inline int heap::top() { return tot ? dis[data[1]] : -INF; }

struct edge {
  int adj, cost, id;
  edge *next;
} pool[E], *e[N];

int u[E], v[E], w[E];

inline int id(edge *cur) { return (cur - pool) / 2 + 1; }

void link(int a, int b, int c) {
  static edge *top = pool;
  u[id(top)] = a, v[id(top)] = b, w[id(top)] = c;
  top->adj = b, top->next = e[a], top->cost = c, e[a] = top++;
  top->adj = a, top->next = e[b], top->cost = c, e[b] = top++;
}

bool col[N], del[E];

struct {
  int l, r, p, w, ans;
  heap *h;
} con[E];

int tot = 0, flag[N], fa[N], size[N];

int bfs(int root, int cur, int dep, int sz) {
  static int q[N], f, r;
  int *dis = _dis[dep];
  for (flag[q[f = r = 1] = root] = cur, fa[root] = dis[root] = 0; f <= r; ++f) {
    int a = q[f];
    for (edge *it = e[a]; it; it = it->next) {
      int b = it->adj, c = it->cost;
      if (flag[b] != cur && !del[id(it)]) {
        flag[q[++r] = b] = cur;
        fa[b] = a;
        dis[b] = dis[a] + c;
      }
    }
  }
  int best = sz, res = 0;
  for (int i = r; i; --i) {
    int a = q[i];
    size[a] = 1;
    if (!col[a]) con[cur].h->push(a);
    for (edge *it = e[a]; it; it = it->next) {
      int b = it->adj;
      if (fa[b] == a && !del[id(it)]) {
        size[a] += size[b];
        int tmp = std::max(size[b], sz - size[b]);
        if (tmp < best) {
          best = tmp;
          res = id(it);
        }
      }
    }
  }
  return res;
}

inline void update(int cur) {
  int &tmp = con[cur].ans;
  tmp = std::max(con[con[cur].l].ans, con[con[cur].r].ans);
  tmp = std::max(tmp, con[con[cur].l].h->top() + con[cur].w + con[con[cur].r].h->top());
}

int divide(int root, int dep, int sz) {
  int cur = ++tot, e;
  con[cur].h = new heap(sz, dep);
  if (e = bfs(root, cur, dep, sz)) {
    con[cur].w = w[e];
    int a = u[e], b = v[e];
    if (fa[a] == b) std::swap(a, b);
    del[e] = true;
    int p = size[root] - size[b], q = size[b];
    con[con[cur].l = divide(a, dep + 1, p)].p = cur;
    con[con[cur].r = divide(b, dep + 1, q)].p = cur;
    update(cur);
  }
  return cur;
}

inline void query() {
  switch (n) {
    case 0: puts("They have disappeared."); break;
    case 1: puts("0"); break;
    default: printf("%d\n", con[1].ans); break;
  }
}

inline void toggle(int pos) {
  n += (col[pos] ^= 1) ? -1 : 1;
  for (int i = flag[pos]; i; i = con[i].p) {
    if (col[pos]) con[i].h->pop(pos); else con[i].h->push(pos);
    update(i);
  }
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  scanf("%d", &n);
  int tot = n;
  static int deg[N], cur[N];
  for (int i = 1; i <= n; ++i) cur[i] = i;
  for (int i = 1; i < n; ++i) {
    int a, b, c;
    scanf("%d%d%d", &a, &b, &c);
    int u = cur[a], v = cur[b];
    if (deg[u] == 2) col[cur[a] = ++tot] = 1, link(u, cur[a], 0), deg[u = cur[a]] = 1;
    if (deg[v] == 2) col[cur[b] = ++tot] = 1, link(v, cur[b], 0), deg[v = cur[b]] = 1;
    deg[u]++, deg[v]++;
    link(u, v, c);
  }
  int tcase;
  divide(1, 0, tot);
  con[0].h = new heap(0, 0);
  for (scanf("%d", &tcase); tcase--;) {
    char op;
    int a;
    scanf(" %c", &op);
    switch (op) {
      case 'A': query(); break;
      case 'C':
        scanf("%d", &a);
        toggle(a);
        break;
    }
  }
  return 0;
}
