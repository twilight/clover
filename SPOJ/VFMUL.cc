#include <bits/stdc++.h>

typedef long long int64;

const int SZ = 21, N = 1 << SZ;
const int64 MOD = 479 * N + 1, g = 3;

int64 w[N];

inline int64 pow(int64 base, int64 exp) {
  int64 res = 1;
  for (; exp; exp >>= 1) {
    if (exp & 1) (res *= base) %= MOD;
    (base *= base) %= MOD;
  }
  return res;
}

#define lg2(x) (31 - __builtin_clz(x))

void DFT(int64 *a, int len, int sw) {
  static int64 t[N];
  for (int i = 0; i < len; ++i) {
    int p = 0;
    for (int cnt = lg2(len), num = i; cnt--; num >>= 1) (p <<= 1) |= num & 1;
    t[i] = a[p];
  }
  for (int m = 2; m <= len; m *= 2) {
    int gap = m / 2, layer = len / m;
    for (int i = 0; i < gap; ++i) {
      int64 w = (sw == 1) ? ::w[i * layer] : ::w[len - i * layer];
      for (int j = i; j < len; j += m) {
        int64 u = t[j], v = t[j + gap];
        t[j] = (u + w * v % MOD) % MOD;
        t[j + gap] = ((u - w * v % MOD) % MOD + MOD) % MOD;
      }
    }
  }
  for (int i = 0; i < len; ++i) a[i] = t[i];
}

inline int RD(int64 *a) {
  static char s[N];
  scanf(" %s", s);
  int len = strlen(s);
  std::reverse(s, s + len);
  for (int i = 0; i < len; ++i) a[i] = s[i] - '0';
  return len;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    static int64 a[N], b[N];
    int la = RD(a), lb = RD(b);
    int len = 1;
    while (len < la || len < lb) len *= 2;
    len *= 2;
    std::fill(a + la, a + len + 1, 0);
    std::fill(b + lb, b + len + 1, 0);
    w[0] = 1, w[1] = pow(g, (MOD - 1) / len);
    for (int i = 2; i <= len; ++i) w[i] = w[i - 1] * w[1] % MOD;
    DFT(a, len, 1);
    DFT(b, len, 1);
    for (int i = 0; i < len; ++i) (a[i] *= b[i]) %= MOD;
    DFT(a, len, -1);
    int64 inv = pow(len, MOD - 2);
    for (int i = 0; i < len; ++i) (a[i] *= inv) %= MOD;
    for (int i = 0; i < len; ++i) {
      a[i + 1] += a[i] / 10;
      a[i] %= 10;
    }
    while (len && !a[len]) --len;
    for (int i = len; i >= 0; --i) putchar(a[i] + '0');
    putchar('\n');
  }
  return 0;
}
