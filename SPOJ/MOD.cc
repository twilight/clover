#include <cstdio>
#include <cmath>
#include <cstring>
#include <algorithm>

typedef long long int64;

const int N = 57601;

inline void exgcd(int a, int b, int &x, int &y) {
  if (!b) {
    x = 1, y = 0;
    return;
  }
  exgcd(b, a % b, x, y);
  int t = x;
  x = y;
  y = t - a / b * y;
}

inline int gcd(int a, int b) { return b ? gcd(b, a % b) : a; }

inline int inv(int x, int MOD) {
  int a, b;
  exgcd(x, MOD, a, b);
  return (a % MOD + MOD) % MOD;
}

int table[N], num[N];

inline int hash(int x) {
  int res = x % N;
  while (res < 0) res += N;
  while (table[res] && table[res] != x) res = (res + 1) % N;
  return res;
}

void solve(int a, int b, int c) {
  for (int i = 0, tmp = 1; i <= 30; ++i) {
    if (tmp == b) {
      printf("%d\n", i);
      return;
    }
    tmp = (int64)tmp * a % c;
  }
  int cnt = 0, t = 1;
  for (int tmp; (tmp = gcd(a, c)) > 1; ++cnt) {
    if (b % tmp) {
      puts("No Solution");
      return;
    }
    b /= tmp;
    c /= tmp;
    t = (int64)t * a / tmp % c;
  }
  int m = ceil(sqrt(c));
  memset(table, 0, sizeof table);
  for (int i = 0, tmp = 1; i <= m; ++i) {
    int pos = hash(tmp);
    if (!table[pos]) {
      table[pos] = tmp;
      num[pos] = i;
    }
    tmp = (int64)tmp * a % c;
  }
  b = (int64)b * inv(t, c) % c;
  int step = 1;
  for (int i = 1; i <= m; ++i) step = (int64)step * a % c;
  step = inv(step, c);
  for (int i = 0; i <= m; ++i) {
    int pos = hash(b);
    if (table[pos]) {
      printf("%d\n", i * m + num[pos] + cnt);
      return;
    }
    b = (int64)b * step % c;
  }
  puts("No Solution");
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
  freopen("out.txt", "w", stdout);
#endif
  int a, b, c;
  while (scanf("%d%d%d", &a, &c, &b), a | b | c) {
    b %= c;
    solve(a, b, c);
  }
  return 0;
}
