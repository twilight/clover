#include <cstdio>

typedef long long int64;

const int N = 1000000 + 10;

inline int64 sqr(int64 a) { return a * a; }
inline int64 cube(int64 a) { return a * a * a; }

int miu[N], sum[N];

void preprocessing() {
  static int prime[N], cnt;
  static bool flag[N];
  miu[1] = 1;
  for (int i = 2; i < N; ++i) {
    if (!flag[i]) {
      prime[++cnt] = i;
      miu[i] = -1;
    }
    for (int j = 1; j <= cnt && i * prime[j] < N; ++j) {
      flag[i * prime[j]] = true;
      if (i % prime[j]) {
        miu[i * prime[j]] = -miu[i];
      } else {
        miu[i * prime[j]] = 0;
        break;
      }
    }
  }
  for (int i = 1; i < N; ++i) sum[i] = sum[i - 1] + miu[i];
}

int64 cal(int n) {
  int64 t1, t2, t3;
  t1 = t2 = t3 = 0;
  for (int l = 1, r; l <= n; l = r + 1) {
    r = n / (n / l);
    t3 += cube(n / l) * (sum[r] - sum[l - 1]);
    t2 += sqr(n / l) * (sum[r] - sum[l - 1]);
    t1 += n / l * (sum[r] - sum[l - 1]);
  }
  return t1 * 3 + t2 * 3 + t3;
}

int main() {
  preprocessing();
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    int n;
    scanf("%d", &n);
    printf("%lld\n", cal(n));
  }
  return 0;
}
