#include <cstdio>
#include <vector>
#include <algorithm>

const int N = 30011 + 10;

inline void cmin(int &a, int b) { if (b < a) a = b; }

int n, m;
std::vector<int> e[N], pre[N], dom[N];
int id[N], dfn[N], fa[N], cnt;
int idom[N], semi[N];

int anc[N], low[N];

inline void preprocessing() {
  cnt = 0;
  for (int i = 1; i <= n; ++i) {
    dfn[i] = fa[i] = 0;
    anc[i] = low[i] = semi[i] = i;
    e[i].clear();
    pre[i].clear();
    dom[i].clear();
  }
}

void dfs(int a) {
  id[dfn[a] = ++cnt] = a;
  for (std::vector<int>::iterator it = e[a].begin(); it != e[a].end(); ++it) {
    int b = *it;
    if (!dfn[b]) {
      dfs(b);
      fa[dfn[b]] = dfn[a];
    }
  }
}

inline int eval(int x) {
  if (x == anc[x]) return x;
  int y = eval(anc[x]);
  if (semi[low[x]] > semi[low[anc[x]]]) low[x] = low[anc[x]];
  return anc[x] = y;
}

void tarjan(int s) {
  for (int i = n; i > 1; --i) {
    for (std::vector<int>::iterator it = pre[id[i]].begin(); it != pre[id[i]].end(); ++it) {
      int j = dfn[*it];
      if (!j) continue;
      eval(j);
      cmin(semi[i], semi[low[j]]);
    }
    dom[semi[i]].push_back(i);
    int p = anc[i] = fa[i];
    for (std::vector<int>::iterator it = dom[p].begin(); it != dom[p].end(); ++it) {
      int j = *it;
      eval(j);
      idom[j] = (semi[low[j]] < p ? low[j] : semi[j]);
    }
    dom[p].clear();
  }
  for (int i = 2; i <= n; ++i) if (idom[i] != semi[i]) idom[i] = idom[idom[i]];
  idom[s] = 0;
}

int main() {
#ifndef ONLINE_JUDGE
  freopen("in.txt", "r", stdin);
#endif
  int tcase;
  for (scanf("%d", &tcase); tcase--;) {
    scanf("%d%d", &n, &m);
    preprocessing();
    for (int tmp = m; tmp--;) {
      int p, q;
      scanf("%d%d", &p, &q);
      e[p].push_back(q);
      pre[q].push_back(p);
    }
    dfs(1);
    tarjan(1);
    int ans = dfn[n];
    while (idom[ans] != dfn[1]) ans = idom[ans];
    printf("%d\n", id[ans]);
  }
  return 0;
}
